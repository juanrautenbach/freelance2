<h1>Create Package</h1>
<form class="" action="/packages" method="post">
  @csrf
  <label for="">Package Name</label><br>
  <input type="text" name="package_name"><br>
  <label for="">Description</label><br>
  <textarea name="package_description" rows="8" cols="80"></textarea><br>
  <label for="">Package Price</label><br>
  <input type="text" name="package_amount"><br>
  <button type="submit" name="button">Create Package</button>
</form>
