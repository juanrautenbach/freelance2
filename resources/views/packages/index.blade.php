<h1>Packages</h1>
@foreach ($packages as $package)
  <table>
    <tr>
      <th>#</th>
      <th>Package Name</th>
      <th>Package Description</th>
      <th>Amount</th>
      <th>Actions</th>
    </tr>
    <tr>
      <td>{{$package->id}}</td>
      <td>{{$package->name}}</td>
      <td>{{$package->description}}</td>
      <td>R {{$package->amount}}</td>
      <td>
        <form class="" action="/packages/{{$package->id}}" method="post">
          @csrf
          {{ method_field('DELETE') }}
          <button type="submit" name="button">X</button>
        </form>

      </td>
    </tr>
  </table>
@endforeach
