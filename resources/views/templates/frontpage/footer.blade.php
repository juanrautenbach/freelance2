<section id="footer" class="footer-main tw-p-4 md:tw-p-12">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-6 col-md-12">
        <img src="/img/FCPT_Logo.png" class="footer-image d-block mx-auto d-lg-inline ml-lg-0" alt="Freelance logo">
      </div>

      <div class="col-lg-3 col-md-12 text-center text-lg-left pt-lg-5 pt-0">
        <div class="row">
          <div class="col">
            <ul class="mb-0 pl-0 tw-list-none tw-font-bold tw-leading-8">
              <li> <a href="/">Home</a> </li>
              <li> <a href="/about">About</a> </li>
              <li> <a href="/freelancers">All Freelancers</a> </li>
              <li> <span id="suggestionBox" class="text-red pointer">Suggestion Box</span> </li>
            </ul>
          </div>

        </div>
      </div>
      <div class="col-lg-3 col-md-12 social-icons-section text-center text-lg-left pt-lg-5 pt-0">
          <div class="row ">
            <div class="col text-center text-lg-left">
              <ul class="mb-0 pl-0 tw-list-none tw-font-bold">
                <li> <a href="/contact">Contact Us</a> </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="d-lg-block d-inline">
                <ul class="text-center text-lg-left mb-0 pl-0 tw-list-none">
                  <li>
                    <span class="mr-3"> 
                      <a href="https://www.facebook.com/freelancecapetown/?ref=br_rs" target="_blank"> 
                        <img src="/img/icons/facebook.png" class="mx-auto" alt="Freelance facebook icon"> 
                      </a>
                    </span>
                    <span class="ml-3">
                      <a href="https://instagram.com/freelancecapetown/" target="_blank"> 
                        <img src="/img/icons/instagram.png" class="mx-auto" alt="Instagram link logo"> 
                      </a>
                    </span> 
                  </li>
                </ul>

              </div>
            </div>
          </div>
      </div>

    </div>
    <div class="row payment-details mt-3">
      <div class="col text-center">
        <img src="/img/paygate_plus.png" class="mr-4 d-inline-block tw-h-4" alt="Paygate Plus Logo"> 
        <img src="/img/verified-by-visa.png" class="mr-4 d-inline-block tw-h-12" alt="Verified by Visa logo"> 
        <img src="/img/Visa.png" class="mr-4 d-inline-block tw-h-6" alt="Visa Logo">
        <img src="/img/mastercard.png" class="mr-4 d-inline-block tw-h-20" alt="Mastercard Logo">
        <img src="/img/securecode.png" class="mr-4 d-inline-block tw-h-10" alt="Secure Code Logo">
      </div>
    </div>
  </div>

</section>
<section id="copyright" class="footer-bottom">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 text-center text-md-left">
        <h4>&copy; Copyright Freelance Culture (Pty) Ltd 2019.</h4>
      </div>
      <div class="col-md-6 md:tw-text-right tw-text-center tw-mt-6 md:tw-mt-0">
        <a href="/terms/payment" target="_blank" class="text-underline text-white md:tw-mr-4 ">View Payment T&C</a>
      </div>
    </div>
  </div>
</section>
<div class="suggestion-box-container" id="suggestion-box-container" style="display:none">
  <div class="suggestion-box col-12 col-sm-6">
    <div class="card tw-p-2 tw-px-4 md:tw-p-4 tw-rounded">
      <h4 class="font-weight-bold tw-mb-2 md:tw-mb-5">Suggestion Form <span class="float-right" id="closeBtn"><i class="fas fa-times"></i></span></h4>
      @if (\Request::is('register')))

        @else
          <form class="" action="/suggestion" method="post">
            @csrf
            <label class="font-weight-bold tw-text-xs">Name:</label>
            <input type="text" name="name" class="tw-block focus:tw-outline-none active:tw-outline-none active:tw-border-0" required>
            <label class="font-weight-bold tw-text-xs">Email:</label>
            <input type="email" name="email" class="tw-block focus:tw-outline-none focus:tw-shadow-none active:tw-shadow-none " required>
            <label class="font-weight-bold tw-block tw-text-xs">How will you rate your experience:</label>
            <span id="star1" class="star-rating"><i class="fas fa-star "></i></span>
            <span id="star2" class="star-rating"><i class="fas fa-star"></i></span>
            <span id="star3" class="star-rating"><i class="fas fa-star"></i></span>
            <span id="star4" class="star-rating"><i class="fas fa-star"></i></span>
            <span id="star5" class="star-rating"><i class="fas fa-star"></i></span>
            <input type="hidden" name="star_rating" id="star_rating">
            <label class="font-weight-bold tw-block tw-text-xs">Suggestion:</label>
            <textarea name="suggestion" class="tw-h-14" required></textarea>
            <button type="submit" class="btn btn-red" name="button">Send</button>
          </form>
      @endif

    </div>

  </div>
</div>
<script src="/js/app.js" charset="utf-8"></script>
<script src="/js/suggestion.js" charset="utf-8"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="/js/slider.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/login.js" charset="utf-8"></script>
<script src="/js/vendor/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.js"></script>
<script type="text/javascript">
$('.flexdatalist').flexdatalist({
 minLength: 1
});
</script>

{{-- @if (Request::path() == "/freelancers") --}}
    <script src="/js/vendor/typeahead.bundle.min.js" charset="utf-8"></script>
{{-- @endif --}}
