<nav class="navbar navbar-expand-xl navbar-light p-0" id="top-navbar">
    <div class="container-fluid">
        <div class="top-logo tw-pl-8">
            <a class="navbar-brand" href="/"> <img class="brand-logo " src="/img/FCPT_Logo.png" alt="Freelance Cape Town Logo" style="height: 80px;"> </a>
        </div>
        <button class="navbar-toggler ml-3" type="button" onclick="openNav()" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        @include('components.overlaymenu')


        <div class="collapse navbar-collapse text-dark" id="navbarText">
          @if(Request::is('register') || Request::is('login'))
            <ul class="navbar-nav ml-auto text-uppercase font-weight-bold">
              <li class="nav-item mr-2 d-none d-xl-block text-muted">
                   <a class="nav-link d-inline-block px-0 mr-4" href="/">Home</a>
              </li>
            @if (Request::is('login'))

                  <li class="nav-item mr-2 d-none d-xl-block text-muted">
                      Don't have an account yet? <a class="nav-link d-inline-block px-0 ml-1" href="/register">Register</a>
                  </li>

            @elseif (Request::is('register'))

                  <li class="nav-item mr-2 d-none d-xl-block text-muted">
                      Already have an account? <a class="nav-link d-inline-block px-0" href="/login">Log in</a>
                  </li>

            @endif
          </ul>
            @else
            <ul class="navbar-nav ml-auto text-uppercase font-weight-bold top-menu-left">
                <li class="nav-item py-0 my-0">
                    <a class="nav-link py-0 my-0" href="/"><i class="fas fa-home fs-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Become a freelancer <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/freelancers">Find freelancers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/posts/create">Post a brief</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="/co-working">Find a coworking space</a>
                </li> --}}
            </ul>
            <ul class="navbar-nav text-uppercase">
                <li class="nav-item mr-2 d-none d-xl-block">
                    <a class="nav-link d-inline-block px-0" href="/login">Login</a>
                </li>
                <span class="mt-2 d-none d-xl-block"> <small class="font-weight-bold border-dark border-left"></small> </span>
                <li class="nav-item ml-2 d-none d-xl-block">
                    <a class="nav-link d-inline-block px-0" href="/register">Join now</a>
                </li>

            </ul>
            @endif
        </div>
    </div>

</nav>
<script src="/js/formPopUp.js" charset="utf-8"></script>
@if (Request::path() == "register")
<script src="/js/register.js" charset="utf-8"></script>
@endif
