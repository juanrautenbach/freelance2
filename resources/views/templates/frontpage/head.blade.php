<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="/js/app.js"></script>
<script src="/js/formPopUp.js" charset="utf-8"></script>
<script src="/js/vendor/typeahead.bundle.min.js" charset="utf-8"></script>
<script src="/js/filter.js" charset="utf-8"></script>

@if (Request::path() == "login")
<link rel="stylesheet" href="/css/login.css">
@endif

<script src="https://kit.fontawesome.com/3b6b6a71d3.js" crossorigin="anonymous"></script>

<title>Freelance Cape Town</title>
