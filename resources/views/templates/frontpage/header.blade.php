<nav class="navbar navbar-expand-xl navbar-light p-0 shadow-sm" id="top-navbar">
    <div class="container-fluid">
        <div class="top-logo tw-pl-4 md:tw-pl-8 tw-pt-1 md:tw-pt-0">
          @if (Request::is('register') || Request::is('email/verify'))
            <a class="navbar-brand tw-ml-22" href="/"> <img class="brand-logo-small " src="/img/FCPT_Logo.png" alt="FCT Logo"> <h3 class="ml-3">Register</h3> </a>
          @else
            <a class="navbar-brand tw-left-10" href="/"> <img class="brand-logo " src="/img/FCPT_Logo.png" alt="FCT Logo"> </a>
          @endif

        </div>
        <button class="navbar-toggler ml-3" type="button" onclick="openNav()" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        @include('components.overlaymenu')


        <div class="collapse navbar-collapse text-dark" id="navbarText">
          {{-- @if(Request::is('register'))
            <ul class="navbar-nav ml-auto text-uppercase font-weight-bold">
                <li class="nav-item mr-2 d-none d-xl-block">
                    Already have an account? <a class="nav-link d-inline-block px-0" href="/login">Log in</a>
                </li>


            </ul>
            @else --}}
            <ul class="navbar-nav ml-auto text-uppercase font-weight-bold top-menu-left">
                @if (Request::path() !== "/")
                <li class="nav-item py-0 my-0">
                    <a class="nav-link py-0 my-0" href="/"><i class="fas fa-home fs-20"></i></a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="/register">Become a freelancer</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/freelancers">Find freelancers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/posts/create">Post a brief</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="/co-working">Find a coworking space</a>
                </li> --}}
            </ul>
            <ul class="navbar-nav text-uppercase">
            @auth
            <li class="nav-item mr-2 d-none d-xl-block">
                <a class="nav-link d-inline-block px-0" href="/dashboard">Dashboard</a>
            </li>
            @else
            <li class="nav-item mr-2 d-none d-xl-block">
                <a class="nav-link d-inline-block px-0" href="/login">Login</a>
            </li>
            <span class="mt-2 d-none d-xl-block"> <small class="font-weight-bold border-dark border-left"></small> </span>
            <li class="nav-item ml-2 d-none d-xl-block">
                <a class="nav-link d-inline-block px-0" href="/register">Join now</a>
            </li>
            @endauth
            </ul>
            {{-- @endif --}}
        </div>
    </div>

</nav>
<script src="/js/formPopUp.js" charset="utf-8"></script>
