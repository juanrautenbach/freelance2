@php
$user = \Auth::user();
@endphp
<nav class="navbar navbar-expand-xl navbar-light p-0 shadow-sm" id="admin-navbar">
    <div class="container-fluid border">
        <div class="admin-logo border">
            <a class="navbar-brand" href="/dashboard"> <img class="brand-logo " src="/img/FCPT_Logo.png" alt="">
            <h4 class="d-inline text-secondary">Dashboard</h4></a>
        </div>

        <div class="collapse navbar-collapse text-dark border" id="navbarText">

            <ul class="navbar-nav ml-auto top-menu-left">
                <li class="nav-item">
                  <a href="/dashboard" class="nav-link text-muted mr-3">Dashboard</a>
                </li>
                <li class="nav-item">
                    <div class="nav-link text-muted">Welcome,
                      @php
                      $name = explode(" ", $user->name);
                        echo $name[0] . ".";
                      @endphp
                    </div>
                </li>
                @if (empty($unread_msg))
                  {{-- <li class="nav-item mx-3">
                      <a class="nav-link text-red font-weight-bold" href="/messages">

                        Messages
                      </a>
                  </li> --}}
                @else
                  <li class="nav-item mx-3">
                      <a class="nav-link" href="/messages"><i class="fas fa-envelope"></i>@if ($unread_msg == 0) @else <small class="msg-badge-custom"><span class="badge badge-danger">{{$unread_msg}}</span></small> @endif </a>
                  </li>
                @endif
                @if ($user->role === "Freelancer")
                  <li>
                      <a class="nav-link mr-3 font-weight-bold" href="/posts">Job posts</a>
                  </li>
                @endif

                @if ($user->role === "Finder")
                  <li>
                      <a class="nav-link mr-3 font-weight-bold" href="/posts">Find Freelancers</a>
                  </li>
                  <li>
                      <a class="nav-link mr-3 font-weight-bold" href="/posts">My posts</a>
                  </li>
                <li>
                    <a class="btn btn-red mr-3" href="/posts/create">Create Post </a>
                </li>
                @endif
            </ul>

            <ul class="navbar-nav text-uppercase">
                      @if (($user->role === "Freelancer") && (\Request::is('dashboard')))
                        @if (($profile == 0) || ($subs_users == 0))
                          @php
                            $count = 0;
                            if ($profile == 0) {
                              $count++;
                            }
                            if ($subs_users == 0) {
                              $count++;
                            }
                          @endphp
                        <li class="nav-item dropdown mr-3">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bell"></i> <small class="alert-badge-custom"><span class="badge badge-danger" id="alert-number">{{$count}}</span></small> </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="alert-menu">
                                @if ($profile == 0)
                                  <span class="dropdown-item text-danger" onclick="showModal()">Incomplete</span>
                                  @else
                                  <span class="dropdown-item text-success">Complete</span>
                                @endif
                                @if ($subs_users == 0)
                                  <span class="dropdown-item text-danger" onclick="showSubs()">Unsubscribed</span>
                                @else
                                  <span class="dropdown-item text-success">Subscribed</span>
                                @endif


                            </div>
                        </li>
                      @else
                        {{-- <span class="dropdown-item text-success">Complete</span> --}}
                      @endif
                    @endif


            </ul>
            <ul class="navbar-nav text-uppercase">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    @if (empty($user->profile->profile_pic) )
                      <span class="badge bg-blue p-3 rounded-circle text-white">
                        @php
                        $name = explode(" ", $user->name);
                        if (count($name) == 1) {
                          echo substr($name[0], 0, 2);
                        }elseif (count($name) == 2)  {
                          echo substr($name[0], 0, 1) . substr($name[1], 0, 1);
                        }elseif (count($name) == 3) {
                          echo substr($name[0], 0, 1) . substr($name[2], 0, 1);
                        }

                        @endphp
                      </span>
                    @else
                      <img src="/storage/{{ $user->profile->profile_pic }}" alt="User Profile Pic" class="rounded-circle" height="35px">
                    @endif

                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      @if ($user->role == "Freelancer")
                          <a class="dropdown-item" href="/users/profiles/">My Profile</a>
                      @elseif ($user->role == "Co-Working")
                          <a class="dropdown-item" href="/co-working/admin/">My Co-Workspaces</a>
                      @endif
                      @if (\Auth::user()->role === 'Admin')
                      {{-- <div class="dropdown-divider"></div> --}}
                      <span class="px-4 mt-3 font-weight-bold d-block">Settings</span>
                      {{-- <div class="dropdown-divider"></div> --}}
                      <a class="dropdown-item" href="/settings">Users</a>
                      <a class="dropdown-item" href="/subscriptionplans">Subscription Plans</a>
                      <a class="dropdown-item" href="/vouchers">Vouchers</a>

                      @endif


                      <div class="dropdown-divider"></div>
                      <form class="logout-form pl-3" action="/logout" method="post">
                          @csrf
                          <button type="submit" class="btn btn-red">Logout</button>
                      </form>
                  </div>
              </li>
            </ul>
        </div>
    </div>
</nav>
<script type="text/javascript">
  // var listItems = $("#alert-menu").children();
  //
  // var count = listItems.length;
  // $('#alert-number').html(count);

</script>
