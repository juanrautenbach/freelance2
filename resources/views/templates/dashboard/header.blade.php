@php
$user = \Auth::user();
@endphp
<nav class="navbar navbar-expand-lg navbar-light bg-light px-md-5 shadow-sm" id="admin-navbar">
  <a class="navbar-brand admin-logo" href="/dashboard">
    <img class="brand-logo" src="/img/FCPT_Logo.png" alt="Freelance Cape Town Logo">
    <h4 class="d-inline text-secondary">Dashboard</h4>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="/dashboard" class="nav-link mr-2"><i class="fas fa-home fs-24"></i></a>
      </li>


      @if (empty($unread_count))
        <li class="nav-item mx-3">
            <a class="nav-link" href="/messages">
              <i class="fas fa-envelope fs-24"></i>
            </a>
        </li>
      @else
        <li class="nav-item mx-3">
          <form action="/messages/read/{{ $user->id }}">
            <button class="btn btn-link nav-link" type="submit"><i class="fas fa-envelope fs-24"></i>
              @if ($unread_count == 0)
              @else
                <small class="msg-badge-custom">
                  <span class="badge badge-danger">{{$unread_count}}</span>
                </small>
              @endif
            </button>
          </form>

        </li>
      @endif

      {{-- @if ($user->role === "Freelancer")
        <li>
            <a class="nav-link mr-3 font-weight-bold" href="/messages"><i class="fas fa-envelope fs-24"></i></a>
        </li>
      @endif --}}

      @if ($user->role === "Finder")
        {{-- <li>
            <a class="nav-link mr-3 font-weight-bold" href="/posts">Find Freelancers</a>
        </li> --}}
        <li>
            <a class="nav-link mr-3 font-weight-bold" href="/posts">My posts</a>
        </li>
      <li>
          <a class="btn btn-red mr-3" href="/posts/create">Create Post </a>
      </li>
      @endif
      <li class="nav-item">
          <div class="nav-link text-muted">Welcome,
            @php
            $name = explode(" ", $user->name);
              echo $name[0] . ".";
            @endphp
          </div>
      </li>

    </ul>
    <ul class="navbar-nav text-uppercase">
              @if (($user->role === "Freelancer") && (\Request::is('dashboard')))
                @if (($profile == 0) || ($subs_users == 0))
                  @php
                    $count = 0;
                    if ($profile == 0) {
                      $count++;
                    }
                    if ($subs_users == 0) {
                      $count++;
                    }
                  @endphp
                <li class="nav-item dropdown mr-3">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bell"></i> <small class="alert-badge-custom"><span class="badge badge-danger" id="alert-number">{{$count}}</span></small> </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="alert-menu">
                        @if ($profile == 0)
                          <span class="dropdown-item text-danger" onclick="showModal()">Incomplete</span>
                          @else
                          <span class="dropdown-item text-success">Complete</span>
                        @endif
                        @if ($subs_users == 0)
                          <span class="dropdown-item text-danger" onclick="showSubs()">Unsubscribed</span>
                        @else
                          <span class="dropdown-item text-success">Subscribed</span>
                        @endif


                    </div>
                </li>
              @else
                {{-- <span class="dropdown-item text-success">Complete</span> --}}
              @endif
            @endif


    </ul>
    <ul class="navbar-nav text-uppercase">
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

            @if (empty($user->profile->profile_pic) )
              <span class="badge bg-blue p-3 rounded-circle text-white">
                @php
                $name = explode(" ", $user->name);
                if (count($name) == 1) {
                  echo substr($name[0], 0, 2);
                }elseif (count($name) == 2)  {
                  echo substr($name[0], 0, 1) . substr($name[1], 0, 1);
                }elseif (count($name) == 3) {
                  echo substr($name[0], 0, 1) . substr($name[2], 0, 1);
                }

                @endphp
              </span>
            @else
              <img src="/storage/{{ $user->profile->profile_pic }}" alt="User Profile Pic" class="rounded-circle" height="35px">
            @endif

          </a>
          
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              @if ($user->role == "Freelancer")
                  <a class="dropdown-item" href="/users/profiles/">My Profile</a>
              @elseif ($user->role == "Co-Working")
                  <a class="dropdown-item" href="/co-working/admin/">My Co-Workspaces</a>
              @endif
              @if (\Auth::user()->role === 'Admin')
              {{-- <div class="dropdown-divider"></div> --}}
              <span class="px-4 mt-3 font-weight-bold d-block">Settings</span>
              {{-- <div class="dropdown-divider"></div> --}}
              <a class="dropdown-item" href="/settings">Users</a>
              <a class="dropdown-item" href="/subscriptionplans">Subscription Plans</a>
              <a class="dropdown-item" href="/vouchers">Vouchers</a>

              @endif

              @if ($user->role != "Finder")
                  <div class="dropdown-divider"></div>
              @endif

              <form class="logout-form pl-3" action="/logout" method="post">
                  @csrf
                  <button type="submit" class="btn btn-red">Logout</button>
              </form>
          </div>
      </li>
    </ul>
  </div>
</nav>
