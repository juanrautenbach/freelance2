
@if (Request::path() == "users/profiles/register/about" || Request::path() == "posts/create" || Request::path() == "users/profiles")
  <script src="https://cdn.jsdelivr.net/npm/places.js@1.18.1"></script>

@endif

@if (Request::path() == "users/profiles/register/about")
<script src="/js/register.js" charset="utf-8"></script>
@endif

@if (Request::path() == "posts/create" || Request::path() == "users/profiles" || Request::path() == "users/profiles/register/about")
  <script src="/js/places.js" charset="utf-8"></script>
@endif

@if (Request::path() == "vouchers")
  <script src="https://cdn.rawgit.com/hashids/hashids.github.io/master/public/js/lib/hashids.min.js" charset="utf-8"></script>
  <script src="/js/vouchers.js" charset="utf-8"></script>
@endif

 {{-- @php
  echo Request::path();
@endphp --}}

<script src="/js/app.js" charset="utf-8"></script>
