<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Freelance Cape Town</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    @include('templates.frontpage.head')
  </head>
  <body style="min-height: 85vh">
    @include('templates.frontpage.header')
    <div id="go-to-top">
        <a onclick="topFunction()"> <i class="fas fa-arrow-up"></i></a>
    </div>
    <section id="main" style="min-height: 50vh">
        @include('includes.messages')
        @yield('content')
    </section>
    @include('components.scrolltop')
    @include('templates.frontpage.footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" charset="utf-8"></script>
    <script>
              new WOW().init();

//               $( document ).ready(function() {       
//     $(window).scroll(function(e){ 
//   var $el = $('#go-to-top'); 
//   var isDisplayBlock = ($el.css('display') == 'block');
//   if ($(this).scrollTop() > 200 && !isDisplayBlock){ 
//     $el.css({'display': 'block'}); 
//     alert('Top');
//   }
//   if ($(this).scrollTop() < 200 && isDisplayBlock){
//     $el.css({'display': 'none'}); 
//   } 
// });
//               });

$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 1200) {
    $('#go-to-top').fadeIn();
  } else {
    $('#go-to-top').fadeOut();
  }
});
              </script>
  </body>

</html>
