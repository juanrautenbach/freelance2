@extends('front')
@section('content')
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet">
<style media="screen">
  .navbar{
    -webkit-box-shadow: none !important;
  }
</style>

<section class="make-user">
  @include('components.registerbreadcrumbs')
  <div class="container">
    <div class="row">
      <div class="col make-form-top text-center py-3 mt-3">
        <h1 class="text-uppercase font-weight-bold">Please select your prefered subscription.</h1>
        <p>Please choose the correct subscription.. <small></small> </p>
      </div>
    </div>
  </div>

</section>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/js/mdb.min.js"></script>

@endsection
