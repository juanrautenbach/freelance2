@extends('front')
@section('content')
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Material Design Bootstrap -->
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet"> --}}
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
<style media="screen">
  .navbar{
    -webkit-box-shadow: none !important;
  }
  .box__dragndrop,
  .box__uploading,
  .box__success,
  .box__error {
  display: none;
  }
</style>

<section class="make-user">
  @include('components.registerbreadcrumbs')
  <div class="container">
    <div class="row">
      <div class="col make-form-top text-center py-3 mt-3">
        <h1 class="text-uppercase font-weight-bold">Just confirming your account type.</h1>
        <p>Please choose the correct role.. <small></small> </p>
      </div>
    </div>
    <div class="row mb-3 mt-2">
      <div class="card w-25 ml-auto mr-3">
        <div class="card-body">
          <div class="row">
            <div class="col">
                <h4 class="card-title pt-2 font-weight-bold">Freelancer</h4>
            </div>
            <div class="col float-right text-right">
              <input type="checkbox" id="freelancerCheckbox" name="" class="float-right" data-toggle="toggle" data-on="<i class='fas fa-check'></i>" data-off="<i class='fas fa-times'></i>" data-onstyle="success" data-offstyle="secondary" onchange="showFreelancer()">
            </div>
          </div>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <!-- Button -->

          {{-- <a  class="btn btn-primary text-white" href="#freelancerForm">Confirm</a> --}}
        </div>

      </div>
      <div class="card w-25 mr-auto">
        <div class="card-body">
          <div class="row">
            <div class="col">
                <h4 class="card-title pt-2 font-weight-bold">Finder</h4>
            </div>
            <div class="col float-right text-right">
              <input type="checkbox" id="finderCheckbox" name="finder" class="float-right" data-toggle="toggle" data-on="<i class='fas fa-check'></i>" data-off="<i class='fas fa-times'></i>" data-onstyle="success" data-offstyle="secondary" onchange="showFinder()">
            </div>
          </div>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <!-- Button -->

          {{-- <a  class="btn btn-primary text-white">Confirm</a> --}}
        </div>

      </div>
    </div>



    <div class="container" id="freelancerForm" style="display: none">
    <div class="row text-center mt-5">
      <div class="col">
        <h1 class="text-uppercase font-weight-bold">Complete your profile:</h1>
        <p>Please fill in the following fields so we can populate your FCT profile...</p>
      </div>
    </div>
      <div class="row">
      @include('components.dragdrop')
      </div>
      <div class="row my-3">
        <div class="col-sm-6 offset-sm-3">
          <div class="form-group">
            <label for="exampleFormControlTextarea1" class="text-uppercase font-weight-bold">Here's where you sell yourself, make it good! (It's your bio) </label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label for="exampleFormControlTextarea1" class="text-uppercase font-weight-bold">Quote (a tagline that appears on your profile page)</label>
            <input class="form-control" type="text">
          </div>
          <div class="form-group">
            <label for="exampleFormControlTextarea1" class="text-uppercase font-weight-bold">Provide a list of your skills, seperate them with a comma**</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="6"></textarea>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group">
                <label for="exampleFormControlTextarea1" class="text-uppercase font-weight-bold">Mobile number</label>
                <input class="form-control" type="text">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label for="exampleFormControlTextarea1" class="text-uppercase font-weight-bold">Telephone number</label>
                <input class="form-control" type="text">
              </div>
            </div>
          </div>
          <div class="row text-center">
            <div class="col">
              <div class="form-check md-outline mb-2">
                <input class="form-check-input terms-checkbox " type="checkbox" id="inlineFormCheckMD">
                <label class="form-check-label terms-check ml-2 text-left" for="inlineFormCheckMD">
                  I have read and agree to the <a href="#">terms of service</a> , which can be viewed <a href="#">here.</a>
                </label>
              </div>
              <div class="row">
                <div class="col-md-6 offset-md-3 text-left">
                  <a class="btn custom-btn-red py-3 px-4 mt-4" href="/payment">Next..<small>(testing)</small> </a>
                    {{-- <button type="submit" class="btn custom-btn-red py-3 px-4 mt-4">Register</button> --}}
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="container my-3" id="finderForm" style="display: none">
      <div class="row">
        <div class="col-sm-6 offset-sm-3">
          <h1>Some awesome instructions </h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-sm-6 offset-sm-3">
          <div class="form-check md-outline mb-2">
            <input class="form-check-input terms-checkbox " type="checkbox" id="inlineFormCheckMD">
            <label class="form-check-label terms-check ml-2 text-left" for="inlineFormCheckMD">
              I have read and agree to the <a href="#">terms of service</a> , which can be viewed <a href="#">here.</a>
            </label>
          </div>
          <div class="row">
            <div class="col-md-6 offset-md-3 text-left">
              <a class="btn custom-btn-red py-3 px-4 mt-4" onclick="testGoDash()">Login<small>(testing)</small> </a>
                {{-- <button type="submit" class="btn custom-btn-red py-3 px-4 mt-4">Register</button> --}}
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
    <script type="text/javascript">
      Swal.fire({
          title: 'Verification Email Success!',
          text: 'Next step..',
          type: 'success',
          confirmButtonText: 'Cool'
        });

    function showFinder() {
      if ($('#freelancerCheckbox').prop('checked') === true) {
        $('#finderForm').show();
        $('#freelancerForm').hide();
        $('#freelancerCheckbox').prop('checked', false).change();
      }else {
        $('#finderForm').toggle();
      }
      // $('#finderForm').toggle();
      // $('#freelancerCheckbox').prop('checked', false).change()
    }

    function showFreelancer(){
      if ($('#finderCheckbox').prop('checked') === true) {
        $('#freelancerForm').show();
        $('#finderForm').hide();
        $('#finderCheckbox').prop('checked', false).change();
      }else {
        $('#freelancerForm').toggle();
      }
    }

    function finderChange() {
      // $('#finderCheckbox').bootstrapToggle('toggle');

    }

    function testGoDash() {
      Swal.fire({
          title: 'Testing done for now!',
          text: 'Let me know what you think sofar',
          type: 'success',
          confirmButtonText: 'This is so cool!'
        });
    }
    </script>
  </div>

</section>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/js/mdb.min.js"></script>
@endsection
