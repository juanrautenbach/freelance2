@extends('front')
@section('content')
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet">
<style media="screen">
  .navbar{
    -webkit-box-shadow: none !important;
  }
</style>

<section class="make-user bg-white pb-5">
  @include('components.registerbreadcrumbs')
  <div class="container">
    <div class="row">
      <div class="col make-form-top text-center py-3 mt-3">
        <h1 class="text-uppercase font-weight-bold">REGISTER AT FREELANCE CAPE TOWN</h1>
        <p>You're minutes away from joining one of the most prolific platforms in Cape town</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <form class="" action="/make" method="post">
          @csrf
          <!-- Material input -->
          <div class="md-form md-outline form-lg freelance-forms">
            <input type="text" id="user_name" name="user_name" class="form-control form-control-lg">
            <label for="user_name" class="text-capitalize">First Name</label>
          </div>
          <div class="md-form md-outline form-lg freelance-forms">
            <input type="text" id="user_lastname" name="user_lastname" class="form-control form-control-lg">
            <label for="user_lastname" class="text-capitalize">Last Name</label>
          </div>
          <div class="md-form md-outline form-lg freelance-forms">
            <input type="text" id="user_email" name="user_email" class="form-control form-control-lg">
            <label for="user_email" class="text-capitalize">Email Address (act as username)</label>
          </div>
          <div class="md-form md-outline form-lg freelance-forms">
            <input type="password" id="user_password" name="user_password" class="form-control form-control-lg">
            <label for="user_password" class="text-capitalize">Password</label>
          </div>
          <div class="md-form md-outline form-lg freelance-forms">
            <input type="password" id="user_password_confirm" name="user_password_confirm" class="form-control form-control-lg">
            <label for="user_password_confirm" class="text-capitalize">Confirm Password</label>
          </div>
          @if ($user_type == 'freelancer')
            <div class="form-group">
              <label for="exampleFormControlSelect1">Select Account Type</label>
              <select class="form-control form-control-lg">
                <option value="Freelancer">Freelancer</option>
                <option value="Finder">Finder</option>
              </select>
            </div>
          @else
            <div class="form-group">
              <label for="exampleFormControlSelect">Select Account Type</label>
              <select class="form-control form-control-lg">
                <option value="Finder">Finder</option>
                <option value="Freelancer">Freelancer</option>
              </select>
            </div>
          @endif

      </div>

    </div>
    <div class="row text-center">
      <div class="col">
        <div class="form-check md-outline mb-2 ml-5">
          <input class="form-check-input terms-checkbox " type="checkbox" id="inlineFormCheckMD">
          <label class="form-check-label terms-check ml-2" for="inlineFormCheckMD">
            I have read and agree to the <a href="#">terms of service</a> , which can be viewed <a href="#">here.</a>
          </label>
        </div>
        <div class="row">
          <div class="col-md-6 offset-md-3 text-left">
            <a class="btn custom-btn-red py-3 px-4 mt-4" onclick="msgEmail()">Register <br> <small>(testing)</small> </a>
              {{-- <button type="submit" class="btn custom-btn-red py-3 px-4 mt-4">Register</button> --}}
          </div>
        </div>

      </form>
      </div>
    </div>
  </div>
<script type="text/javascript">
  // function msgEmail(){
  //   Swal.fire({
  //       title: 'Email sent success!',
  //       text: 'Please check your inbox and click on the verify button',
  //       type: 'success',
  //       confirmButtonText: 'Cool',
  //       onAfterClose: 'redirectVerify'
  //     });
  //   }
  // function redirectVerify(){
  //   window.location.href = "/verify/1234";
  // }

  function msgEmail() {
    Swal.fire({
    title: 'Email sent success!',
    text: "Please check your inbox and click on the verify button...",
    type: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    confirmButtonText: "Cool let's go"
  }).then((result) => {
    if (result.value) {
      window.location.href = "/verify/1234";
    }
  })
  }


</script>
</section>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/js/mdb.min.js"></script>

@endsection
