@extends('backend')
@section('content')
{{-- <link href="https://unpkg.com/dropzone/dist/dropzone.css" rel="stylesheet" />
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" /> --}}
<link rel="stylesheet" href="/css/slim.min.css">
<style media="screen">
    /* .dz-message {
        margin-top: 50% !important;
        transform: translateY(-50%);
        color: #fff !important;
    }

    .dropzone {
        width: 291px;
        height: 291px;
        border: 8px solid #fff;
        background-color: #888;
        font-size: 20px;
    }
*/
    #profileSetup {
        width: 90% !important;
    }

    body {
        overflow: initial;
    }
</style>
@php
$user = \Auth::user();
@endphp
@if (session()->has('payfast'))
{{ session()->get('payfast') }}

{{-- </script> --}}

@endif
<section class="profile-dashboard">
        @include('freelancers.profile.header')
        
        @include('freelancers.profile.contact_info')

        @include('freelancers.profile.about')

        @include('freelancers.profile.portfolio')

</section>



<script src="/js/dynamicdropdown.js" charset="utf-8"></script>

<script src="/js/tags.js" charset="utf-8"></script>

<script src="/js/slim.kickstart.min.js"></script>


@endsection