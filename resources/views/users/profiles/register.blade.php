@extends('backend_minimal')
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 d-none d-md-block freelancer-register-left">


      </div>
      <div class="col-md-6 freelancer-register-form-container">
        <div class="freelancer-register-form">
          <h1 class="font-weight-bold mb-2">Let's set up your profile.</h1>
          <p>This will only take a minute.</p>

          <a href="/users/profiles/register/industries" class="col-md-6 btn btn-red btn-lg d-inline px-4 py-1">Let's go</a>
        </div>
      </div>
    </div>
  </div>

@endsection
