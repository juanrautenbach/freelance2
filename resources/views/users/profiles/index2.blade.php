@extends('dashboard')
@section('content')
@php
$user = \Auth::user();
@endphp
<section class="profile-dashboard">
    <div class="container-fluid" style="width: 1440px">
        <div class="row profile_cover" id="profile_cover">
            <div class="col p-0">
                @if (empty($user->profile->profile_cover_pic))
                <img src="http://via.placeholder.com/1440x360" alt="">
                @else
                <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image" alt="">
                @endif
            </div>
        </div>
        <div class="row profile_details">
            <div class="profile_pic shadow" style="height: 291px !important; width: 291px !important">
                @if (empty($user->profile->profile_pic))
                <img src="http://via.placeholder.com/275" alt="">
                @else
                <img src="/storage/{{$user->profile->profile_pic}}" alt="" class="profile_photo_image">
                @endif
            </div>
            <div class="user_details">
                <div class="name d-block">
                    <h3>{{$user->name}}</h3>
                </div>

                <h5 class="d-inline mr-1"></h5>
                @foreach ($subindustries as $value)
                <h5 class="font-weight-normal d-inline mr-1">{{$value}}</h5>
                @endforeach
                <div class="row">
                    <div class="col mt-3">
                        @if (!empty($tags) > 0)
                        @foreach ($tags as $tag)
                        <span class="badge badge-light shadow px-2 py-1 text-uppercase">{{$tag->name}}</span>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col contact_user text-center d-flex align-items-center justify-content-center">

                @if ($user->role !== "Freelancer")
                <div class="contact-button">
                    <a href="#" class="btn btn-red btn-lg px-5">Contact me</a>
                </div>
                @else
                <div class="contact-button">
                    <a href="#" class="btn btn-blue btn-lg px-5">Edit Profile</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row profile_contact_info">
            <div class="col p-0">
                <h4>My contact info</h4>
                <table class="table">
                    <tr>
                        <td><span class="font-weight-bold mr-5">Company:</span></td>
                        <td>{{$user->profile->company_name ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">City:</span></td>
                        <td>{{$user->profile->city ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">Email:</span></td>
                        <td>{{$user->email ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">Mobile:</span></td>
                        <td>{{$user->profile->mobile ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">Tel:</span></td>
                        <td>{{$user->profile->tel ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    @if (!empty($user->profile->social_media) > 0)
                    @php
                    $social = $user->profile->social_media;
                    $social_links = json_decode($social);
                    @endphp
                    @foreach ($social_links as $key => $value)
                    @php
                    $key = ucfirst($key);
                    @endphp
                    @if (!empty($value))
                    <tr>
                        <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                        <td>{{$value}}</td>
                        <td></td>
                    </tr>
                    @else
                    <tr>
                        <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                        <td>Not set</td>
                        <td></td>
                    </tr>
                    @endif
                    @endforeach
                    @else
                    <tr>
                        <td>Social media</td>
                        <td>{{ "Not set" }}</td>
                        <td></td>
                    </tr>

                    @endif
                </table>
            </div>
        </div>
        <div class="row profile_about">
            <div class="col p-0">
                <h4>About me</h4>
                <p>{{$user->profile->about ?? "Not set"}}</p>
            </div>
        </div>
    </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" charset="utf-8"></script>

@endsection
