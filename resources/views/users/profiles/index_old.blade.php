@extends('backend')
@section('content')
@php
  $user = \Auth::user();
@endphp
@if ((empty($user->profile)) && ($user->login_count < 50))
  @include('components.profilesetup')
  <script type="text/javascript">

      setTimeout(() => {
          window.location.replace("/users/profiles/register");
      }, 1000)

      // function showModal(){
      //   $('#profileSetup').modal('show');
      //   }
  </script>

@endif

<link rel="stylesheet" href="{{ asset('css/imgareaselect.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/darkroomjs/2.0.1/darkroom.css">
<section class="profile-dashboard">
    <div class="container-fluid">
      <div class="row profile_cover">
          <div class="col p-0">
              @if (empty($user->profile->profile_cover_pic))
              <div class="d-block" style="height: 120px; width:100vw; background: #D8D8D8">
              </div>
              @else
              <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image" alt="">
              @endif
          </div>
      </div>
        <div class="row profile_details">
            <div class="profile_pic shadow" id="profile_photo">
                @if (empty($user->profile->profile_pic))
                <img src="http://via.placeholder.com/275" alt="">
                @else
                <img src="/storage/{{$user->profile->profile_pic}}" alt="" class="profile_photo_image">
                @endif

                <div class="profile_photo_hover" id="profile_photo_hover" style="display:none">
                    <div class="photo_update">
                        <i class="fas fa-camera"></i>
                    </div>
                </div>
            </div>
            <div class="user_details">
              <div class="name d-block">
                <h3>{{$user->name}}
                  @if ($user->role == "Freelancer")
                    <button type="button" name="button" class="btn btn-link float-right mr-5" id="editskills">
                      <i class="fas fa-pencil-alt btn-icon blue-hover"></i>
                    </button>
                  @endif

                </h3>
              </div>

                <h5 class="d-inline mr-1"></h5>
                @foreach ($subindustries as $value)
                  <h5 class="font-weight-normal d-inline mr-1">{{$value}}</h5>
                @endforeach
                {{-- <h5 class="font-weight-normal">Graphic Design</h5> --}}
                <div class="user_ratings mt-2">
                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="30px">
                </div>
                <div class="row">
                    <div class="col mt-3">
                      @if (!empty($tags) > 0)
                        @foreach ($tags as $tag)
                          <span class="skill-tags badge badge-light px-3 py-2 text-uppercase">{{$tag->name}}</span>
                        @endforeach
                      @endif

                    </div>
                </div>
            </div>
            <div class="col mt-4 contact_user text-center text-right">

              @if ($user->role !== "Freelancer")
                <div class="contact-button">
                    <a href="#" class="btn btn-red btn-lg px-5">Contact me</a>
                </div>
              @else

              @endif


            </div>

        </div>
      </div>
        <div class="container-fluid">
            <div class="row profile_contact_info">
                <div class="card shadow-sm col py-3 px-4 border-0">
                    <h4>Contact information <span class="ml-5"> <button type="button" name="button" class="btn btn-link btn-icon blue-hover" id="editContact"><i class="fas fa-pencil-alt"></i></button> </span> </h4>

                    <table class="table table-borderless table-responsive">
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Company:</span></td>
                            <td>{{$user->profile->company_name ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">City:</span></td>
                            <td>{{$user->profile->city ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Email:</span></td>
                            <td>{{$user->email ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Mobile:</span></td>
                            <td>{{$user->profile->mobile ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Tel:</span></td>
                            <td>{{$user->profile->tel ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        @if (!empty($user->profile->social_media) > 0)
                        @php
                        $social = $user->profile->social_media;
                        $social_links = json_decode($social);
                        @endphp
                        @foreach ($social_links as $key => $value)
                        @php
                        $key = ucfirst($key);
                        @endphp
                        @if (!empty($value))
                          <tr>
                              <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                              <td>{{$value}}</td>
                              <td></td>
                          </tr>
                        @else
                          <tr>
                              <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                              <td>Not set</td>
                              <td></td>
                          </tr>
                        @endif

                        @endforeach
                        @else
                        <tr>
                            <td>Social media</td>
                            <td>{{ "Not set" }}</td>
                            <td></td>
                        </tr>

                        @endif
                    </table>


                </div>
            </div>
            <div class="row profile_about">
                <div class="card shadow-sm col py-3 px-4 border-0">
                    <h4>About me<span class="ml-5"> <button type="button" name="button" class="btn btn-link btn-icon blue-hover" id="aboutEdit"><i class="fas fa-pencil-alt"></i></button> </span></h4>
                    <p>{!! nl2br(e($user->profile->about)) ?? "Not set"!!}</p>
                </div>
            </div>
        </div>
        <div class="container-fluid" >
            <div class="row profile_portfolio">
              <div class="col p-0">
                <span><h4 class="d-inline">Portfolio</h4></span>
              </div>
            </div>

        </div>
        @php
          $profile_count = count($user->showcase);

          //echo $profile_count;
        @endphp

        <div class="container-fluid mb-5">
            <div class="row profile_showcase">
              @if ($profile_count == 0)
                <div class="col mb-3 mb-sm-1 profile_showcase_get_started">
                  <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                </div>
              @elseif ($profile_count == 1)
                <div class="col mb-3 mb-sm-1 card border-0 shadow-sm p-0">
                  <div class="row">
                    <div class="col">
                      <img src="https://via.placeholder.com/350x200" alt="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col py-3 px-4">
                      <p>Project name: <span class="font-weight-bold">{{ $user->showcase[0]->title }}</span> </p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col px-4">
                      <p>Description:</p>
                      <p>{{ $user->showcase[0]->description }}</p>
                    </div>
                  </div>

                </div>
              @endif

              @if ($profile_count == 1)
                <div class="col mb-3 mb-sm-1 profile_showcase_get_started">
                  <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                </div>
              @elseif ($profile_count == 2)
                <div class="col mb-3 mb-sm-1 card border-0 shadow-sm p-0">
                  <div class="row">
                    <div class="col">
                      <img src="https://via.placeholder.com/350x200" alt="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col py-3 px-4">
                      <p>Project name: <span class="font-weight-bold">{{ $user->showcase[1]->title }}</span> </p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col px-4">
                      <p>Description:</p>
                      <p>{{ $user->showcase[1]->description }}</p>
                    </div>
                  </div>

                </div>
              @endif

              @if ($profile_count > 3)
                <div class="col mb-3 mb-sm-1 profile_showcase_get_started">
                  <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                </div>
              @elseif ($profile_count == 3)
                <div class="col mb-3 mb-sm-1 card border-0 shadow-sm p-0">
                  <div class="row">
                    <div class="col">
                      <img src="https://via.placeholder.com/350x200" alt="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col py-3 px-4">
                      <p>Project name: <span class="font-weight-bold">{{ $user->showcase[2]->title }}</span> </p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col px-4">
                      <p>Description:</p>
                      <p>{{ $user->showcase[2]->description }}</p>
                    </div>
                  </div>

                </div>
              @endif


            </div>
            <div class="row profile_showcase">
                <div class="col mb-3 mb-sm-1 profile_showcase_get_started">
                  <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                </div>
                <div class="col mb-3 mb-sm-1 profile_showcase_get_started">
                  <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                </div>
                <div class="col mb-3 mb-sm-1 profile_showcase_get_started">
                  <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                </div>
            </div>
        </div>

        <div class="add_showcase" id="add_showcase" style="display: none">
            <div class="card col-12 col-sm-6 mx-auto">
                <div class="card-body pt-0 pb-4 px-3">
                  <span name="button" class="btn btn-link btn-lg text-lightgrey float-right pr-0 pt-3 blue-hover" id="cancelshowcase"><i class="fas fa-times font-20"></i></span>
                    <h3 class="card-title md-3 mb-sm-5 mt-sm-4">Add showcase</h3>
                    <form class="" action="/user/showcase/add" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="showcase_title" class="font-weight-bold">Add a Showcase image or video</label>
                            <div class="row">
                              <div class="col-6 col-sm-3">
                                <span id="up_photo_empty"><i class="far fa-circle text-lightgrey"></i></span>
                               <span style="display: none" id="up_photo_selected"><i class="fas fa-dot-circle text-blue"></i></span>
                               <span class="ml-2">Upload a photo</span>
                              </div>
                              <div class="col-6 col-sm-3">
                                <span id="up_video_empty"><i class="far fa-circle text-lightgrey"></i></span>
                                <span style="display: none" id="up_video_selected"><i class="fas fa-dot-circle text-blue"></i></span>
                                <span class="ml-2">Add a video</span>
                              </div>
                              <div class="col-12 col-sm-6 mt-3 mt-sm-1">
                                <input type="text" name="video-url" class="w-100" style="display: none" id="video_url" placeholder="Enter video URL">
                              </div>
                            </div>
                        </div>
                        <div class="drop-photo mb-3">
                          <div class="drop-photo-container">
                            <div class="row">
                              <div class="col">
                                <i class="fas fa-cloud-upload-alt text-lightgrey font-48 mb-3"></i>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <h5 class="text-lightgrey font-weight-bold">Upload an Image</h5>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group d-none">
                            <label for="showcase_photo" class="font-weight-bold">Photo:</label>
                            <input type="file" name="showcase_photo">
                        </div>
                        <div class="form-group mt-4">
                            <label for="showcase_title" class="font-weight-bold p-0 mb-0">Title of project</label>
                            <input type="text" class="w-100" name="showcase_title" required placeholder="Enter a project title">
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                        </div>
                        <div class="form-group mt-4">
                            <label for="showcase_desc" class="font-weight-bold p-0 mb-0">Project description</label>
                            <textarea name="showcase_desc"  placeholder="Enter a description"></textarea>
                            {{-- <input type="text" class="w-100" name="showcase_desc" placeholder="Enter a description"> --}}
                        </div>
                        <div class="text-center">
                          <button type="submit" name="button" class="btn btn-blue btn-lg px-md-5 py-md-3 ls-25 w-md-50 mt-4">DONE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="edit_about" id="edit_about" style="display:none">
            <div class="card col-12 col-sm-6 mx-auto">
                <div class="card-body pt-0 pb-4 px-3">
                  <span name="button" class="btn btn-link btn-lg text-lightgrey blue-hover float-right pr-0 pt-3" id="cancel_about"><i class="fas fa-times font-20"></i></span>
                    <h3 class="card-title mb-5 mt-4">About me</h3>
                    <form class="" action="/user/profile/about/edit/{{$user->id}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="form-group">
                            <label for="showcase_title" class="font-weight-bold d-block">Write something good!</label>
                            <textarea name="about" class="w-100">{{$user->profile->about ?? "Not set"}}</textarea>

                        </div>
                        <div class="text-center">
                          <button type="submit" name="button" class="btn btn-blue btn-lg  ls-25  mt-4">SAVE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="edit_contact" id="edit_contact" style="display: none">
            <div class="card col-12 col-sm-6 mx-auto">
                <div class="card-body pt-0 pb-4 px-3">
                  <span name="button" class="btn btn-link btn-lg text-lightgrey blue-hover float-right pr-0 pt-3" id="cancel_contact"><i class="fas fa-times font-20"></i></span>
                    <h3 class="card-title mb-5 mt-4">Contact information</h3>
                    <form class="" action="/user/profile/contact/edit/{{$user->id}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <table class="table table-borderless">
                            <tr>
                                <td><span class="font-weight-bold mr-5">Company:</span></td>
                                <td>
                                  <input type="text" name="company_name" value="{{$user->profile->company_name}}">
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="font-weight-bold mr-5">City:</span></td>
                                <td>
                                  <input type="text" id="user_city" name="city" value="{{$user->profile->city}}">
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="font-weight-bold mr-5">Mobile:</span></td>
                                <td>
                                  <input type="text" name="mobile" value="{{$user->profile->mobile}}">
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="font-weight-bold mr-5">Tel:</span></td>
                                <td>
                                  <input type="text" name="tel" value="{{$user->profile->tel}}">
                                </td>
                                <td></td>
                            </tr>

                            @if (!empty($user->profile->social_media) > 0)
                            @php
                            $social = $user->profile->social_media;
                            $social_links = json_decode($social);
                            @endphp
                            @foreach ($social_links as $key => $value)
                            @php
                            $key = ucfirst($key);
                            @endphp
                              @if ($key === "Whatsapp")
                                <tr>
                                    <td><span class="font-weight-bold mr-5">Whatsapp:</span></td>
                                    <td>
                                      <input type="text" name="whatsapp" value="{{$value}}">
                                    </td>
                                    <td></td>
                                </tr>
                              @endif

                              @if ($key === "Facebook")
                                <tr>
                                    <td><span class="font-weight-bold mr-5">Facebook:</span></td>
                                    <td>
                                      <input type="text" name="facebook" value="{{$value}}">
                                    </td>
                                    <td></td>
                                </tr>
                              @endif

                              @if ($key === "Linkedin")
                                <tr>
                                    <td><span class="font-weight-bold mr-5">Linkedin:</span></td>
                                    <td>
                                      <input type="text" name="linkedin" value="{{$value}}">
                                    </td>
                                    <td></td>
                                </tr>
                              @endif
                              @if ($key === "Instagram")
                                <tr>
                                    <td><span class="font-weight-bold mr-5">Instagram:</span></td>
                                    <td>
                                      <input type="text" name="instagram" value="{{$value}}">
                                    </td>
                                    <td></td>
                                </tr>
                              @endif

                            @endforeach

                            @endif
                        </table>
                        <div class="text-center">
                          <button type="submit" name="button" class="btn btn-blue btn-lg  ls-25  mt-4">SAVE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="edit_skills" id="edit_skills" style="display:none">
            <div class="card col-md-6 col-12 mx-auto">
                <div class="card-body pt-0 pb-4 px-3">
                  <span name="button" class="btn btn-link btn-lg text-lightgrey blue-hover float-right pr-0 pt-3" id="cancel_skills"><i class="fas fa-times font-20"></i></span>
                    <h3 class="card-title mb-5 mt-4">Skills</h3>
                    <form class="" action="/user/profile/skills/edit/{{$user->id}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                                <label for="" class="font-weight-bold">What are your skills?</label><br>
                                <label for="">Enter your skills divided by a comma.</label>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                          <div class="tags-container" data-name="tags_container">
                                            <input type="hidden" name="user_tags" id="hiddenInput" value="">
                                          </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col px-0">
                              <div class="row mb-3">
                                  <div class="col">
                                      <label for="industry" class="font-weight-bold">Your industry</label>
                                      <select class="mb-5" id="industry_id">
                                          <option value="">Select industry</option>
                                      </select>

                                      <label for="subindustries" class="font-weight-bold">Your sub-industries</label>
                                      <select class="" id="subcategory_id_input">
                                        <option value="">Select your sub-industries</option>
                                      </select>
                                      <div class="" id="subcategory_id">

                                      </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="text-center">
                          <button type="submit" name="button" class="btn btn-blue btn-lg  ls-25  mt-4">SAVE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <script type="text/javascript">
        $("#aboutEdit").click(function(){
          $("#edit_about").show();
        })
        $("#cancel_about").click(function() {
            $("#edit_about").hide();
        })
        $(".showcaseAdd").click(function() {
            $("#add_showcase").show();
        })

        $("#cancelshowcase").click(function() {
            $("#add_showcase").hide();
        })

        $("#editContact").click(function(){
          $("#edit_contact").show();
        })
        $("#cancel_contact").click(function(){
          $("#edit_contact").hide();
        })

        $("#editskills").click(function(){
          $("#edit_skills").show();
        })
        $("#cancel_skills").click(function(){
          $("#edit_skills").hide();
        })

        $("#up_photo_empty").click(function(){
          $("#up_photo_empty").hide();
          $("#up_photo_selected").show();
          $("#up_video_selected").hide();
          $("#up_video_empty").show();
          $("#video_url").hide();
        })
        $("#up_photo_selected").click(function(){
          $("#up_photo_empty").show();
          $("#up_video_empty").show();
          $("#up_video_selected").hide();
          $("#up_photo_selected").hide();
          $("#video_url").hide();
        })

        $("#up_video_empty").click(function(){
          $("#up_video_empty").hide();
          $("#up_photo_empty").show();
          $("#up_photo_selected").hide();
          $("#up_video_selected").show();
          $("#video_url").show();
        })
        $("#up_video_selected").click(function(){
          $("#up_video_empty").show();
          $("#up_video_selected").hide();
          $("#video_url").hide();
        })

    </script>
</section>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" charset="utf-8"></script>
<script src="/js/dynamicdropdown.js" charset="utf-8"></script>

<script src="/js/tags.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/darkroomjs/2.0.1/darkroom.js" charset="utf-8"></script>
@endsection
