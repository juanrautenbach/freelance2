@extends('backend_minimal')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 subscription-form-container">
            <div class="subscription-form" id="subscription-select-plan-form">
                <form class="" action="/confirmpayment" method="post">
                    @csrf
                    <div class="mx-md-5 mx-3">
                        <input type="hidden" name="return_url" value="{{ url('/return') }}">
                        <input type="hidden" name="cancel_url" value="{{ url('/cancel') }}">
                        <input type="hidden" name="notify_url" value="{{ url('/notify') }}">

                        <h2 class="font-weight-bold mb-md-2">Let's select a subscription plan. </h2>
                        <p class="mb-0 mb-md-0">This will only take a minute.</p>
                        <p class="mb-1 mb-md-3"><small class="font-weight-bold text-underline"> <a href="/terms/payment" target="_blank" class="text-underline text-dark mr-4">View Payment T&C</a></small></p>
                        
                        <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
                        <input type="hidden" name="subsplan_id">
                        <div id="questionContainer">
                            <label for="" class="font-weight-bold d-block mb-1 text-dark">Do you have a voucher?</label>
                            <div id="voucherQuestions">
                                <button class="btn btn-red btn-sm" onclick="questionBtn('Yes')" type="button">Yes</button>
                                <button class="btn btn-red btn-sm" onclick="questionBtn('No')" type="button">No</button>
                            </div>
                        </div>
                        

                        <div id="voucherDetails" class="d-none mt-2">
                            <input class="d-inline-block" type="text" name="voucher" placeholder="Enter voucher code" id="voucher_code">
                        <span class="btn btn-red" id="validate" style="display:none">Go</span>
                        <div class="row">
                            <div class="col">
                                <p class="text-danger mb-1 font-italic" id="message"></p>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <div class="mb-3 mt-2 mx-2 mx-md-3 d-none" id="subscriptionDetails">
                        @foreach ($subsplans as $subsplan)
                        <div class="card shadow p-1 p-md-2 d-inline-block text-center mx-1 price-plan-card mb-1" data-price="{{$subsplan->price}}" data-item="{{$subsplan->name}}" data-description="{{$subsplan->description}}" id="{{ "plancard". $subsplan->id  }}" data-subsid="{{$subsplan->id}}" onclick="plancard('{{ "plancard". $subsplan->id  }}')">
                            <p class="mb-1">{{$subsplan->name}}</p>
                            <h2 class="text-blue font-weight-normal mb-0 sub-price">R{{$subsplan->price}}</h2>
                            <p class="text-muted mb-1 sub-text">{{strtolower($subsplan->type)}}</p>
                            <h2 class="text-red font-weight-normal mb-0 discount"></h2><span class="text-muted discount-text"></span>
                            <div class="plan-check d-none">
                                <span class="plan-check-circle rounded-circle"><i class="fas fa-check text-white"></i></span>
                            </div>
                        </div>
                        {{-- <span class="btn btn-red px-3" data-price="{{$subsplan->price}}" data-item="{{$subsplan->name}}" data-description="{{$subsplan->description}}">{{$subsplan->name}}<br><br> <strong>R {{$subsplan->price}}</strong>
                        <i><small>{{$subsplan->type}}</small> </i> </span> --}}
                        @endforeach
                    </div>
                    <div class="mx-3 d-none mb-4" id="submitButton">
                        <input type="hidden" id="price" name="subs_price">
                        <h2 class="text-red font-weight-bold mb-3 d-none" id="showTotal"><span class="text-dark">Total:  </span> <span id="priceToPay">R 799</span></h2>
                        <button type="submit" class="btn btn-red btn-lg px-5">Continue</button>
                    </div>
                    

            </div>
            @php
            $user = \Auth::user();
            @endphp
            <div class="" id="subscription-confirm-form" style="display:none">
                {{-- <h2 class="mb-3">Please confirm..</h2>
                <p class="mb-5">Just make sure before we proceed.</p> --}}
                <div class="row mb-3">
                    <div class="col">
                        <input type="text" name="cart_total" value="" id="cart_total">
                        <input type="text" name="user_id" value="FCT{{$user->id}}">
                        @php
                        $name = explode(" ", $user->name);
                        $firstname = $name[0];
                        if (count($name) == 1) {
                        $lastname = '';
                        }elseif (count($name) == 2) {
                        $lastname = $name[1];
                        }elseif (count($name) == 3) {
                        $lastname = $name[2];
                        }
                        @endphp
                        <input type="text" name="user_firstname" value="{{$firstname}}"> <br>
                        <input type="text" name="user_lastname" value="{{$lastname}}"><br>
                        <input type="text" name="user_email" value="{{$user->email}}">
                        <input type="text" name="item_title" value="" id="item_title">
                        <input type="text" name="item_description" value="" id="item_description">
                        <h4> <span class="font-weight-bold mb-4">Total:</span><span id="confirm_total" class="ml-5"></span> </h4>
                        <button type="submit" class="btn btn-red btn-lg px-5 mt-5">Pay now!</button>
                    </div>

                </div>

            </div>
            
        </div>
        <div class="col-md-6 subscription-form-right d-none d-md-block">

        </div>
        </form>
    </div>
    <div class="row payment-details mt-3">
        <div class="col pl-4">
          <img src="/img/paygate_plus.png" class="mr-4 d-inline-block " alt="" height="15px"> 
          <img src="/img/verified-by-visa.png" class="mr-4 d-inline-block " alt="" height="40px"> 
          <img src="/img/visa.png" class="mr-4 d-inline-block " alt="" height="25px">
          <img src="/img/mastercard.png" class="mr-4 d-inline-block " alt="" height="50px">
          <img src="/img/securecode.png" class="mr-4 d-inline-block " alt="" height="30px">
        </div>
        
      </div>
</div>
<script type="text/javascript">
    $("#voucher_code").click(function(){
      // alert("Clicked");
      $("#validate").fadeIn(1000);
    });

    function questionBtn(answer){
        if (answer == "Yes") {
            // $("#voucherQuestions").addClass('d-none');
            $("#voucherDetails").fadeIn(1000).removeClass('d-none');
        } else if (answer == "No") {
            $("#voucherDetails").addClass('d-none').fadeIn(1000);
            $("#subscriptionDetails").removeClass('d-none');
            $("#submitButton").removeClass('d-none');
            
        }
    }

    $('#validate').click(function() {

        val = $("#voucher_code").val();
        let url = `/api/vouchers/` + val;
        $("#priceToPay").text("");
        $("#showTotal").addClass('d-none');
        $(".price-plan-card").removeClass('selected-plan');
        $(".price-plan-card").find(".plan-check").addClass('d-none');
        // console.log(url);
        $("#subscriptionDetails").removeClass('d-none');
            $("#submitButton").removeClass('d-none');

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText) {
                    // console.log(this.responseText);
                    voucher = JSON.parse(this.responseText);
                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0');
                    var yyyy = today.getFullYear();

                    today = yyyy + '-' + mm + '-' + dd;
                    const today_date = new Date(today);
                    const voucher_date = new Date(voucher.valid_until);
                    if (today_date <= voucher_date) {
                      var amount = voucher.amount;
                      var res = amount.charAt(0);

                      if (res == "R") {
                          var voucher_amount = amount.slice(2, 10);
                          var discount = "R " + voucher_amount;
                        //   console.log(discount);
                          $(".discount").text(discount);
                          $(".discount-text").text("discount");
                      } else if ((amount.slice(-1)) == "%") {
                          // alert(amount.slice(-1));
                          var discount = voucher.amount;
                          $(".discount").text(discount);
                          $(".discount-text").text("discount");
                      } else {
                        $("#message").text("Voucher is not valid.");
                      }
                    } else {
                      $("#message").text("Voucher has expired.");
                    }

                    // $('#jobtitle').val(jobspec.title);

                } else {
                    $("#message").text("Voucher is not valid.");
                }

            }
        };

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    });

    function plancard(id){
        var planCards = $(".price-plan-card");
        // console.log(planCards);
        
        for (let index = 0; index < planCards.length; index++) {
          var card = $(planCards[index]).attr("id");
          if (card == id) {
            $(planCards[index]).addClass('selected-plan');
            $(planCards[index]).find(".plan-check").removeClass('d-none');

            var discount_amount = $(planCards[index]).find(".discount").text();
            var price = $(planCards[index]).attr("data-price");

            if ((discount_amount.charAt(0)) === "R") {
       
             var voucher_discount = discount_amount.slice(2, 10);
             var total_price = price - voucher_discount;

            console.log(total_price);
            if (total_price < 1) {
                total_price = 0;
            } 
            //  $(planCards[index]).find(".sub-price").text("R " + total_price);
            //  $(planCards[index]).find(".sub-text").text("total");
            //  $(".discount").text("");
            //  $(".discount-text").text("");
           }else if ((discount_amount.slice(-1)) == "%") {
             
             var voucher_discount = discount_amount.slice(0,-1);
             var total_price = price - (price * voucher_discount / 100);
            //  $(planCards[index]).find(".sub-price").text("R " + total_price);
            //  $(planCards[index]).find(".sub-text").text("total");
            //  $(".discount").text("");
            //  $(".discount-text").text("");
           }else {
             var total_price = $(planCards[index]).attr("data-price");
           }
   
           var price = total_price;
           var item = $(planCards[index]).attr("data-item");
           var desc = $(planCards[index]).attr("data-description");
           var subs_id = $(planCards[index]).attr("data-subsid");
           
           $("#priceToPay").text("R " + price);
           $("#showTotal").removeClass('d-none');
           $("input[name='subsplan_id']").val(subs_id);

           $("#price").val(price);
           $("#item_title").val(item);
           $("#item_description").val(desc);
           $("#cart_total").val(price);
           $("#confirm_total").text("R" + price);
          } else {
            $(planCards[index]).removeClass('selected-plan');
            $(planCards[index]).find(".plan-check").addClass('d-none');
          }
            
        }
    }


    // $('.price-plan-card').click(function() {
    //     $(this).toggleClass('selected-plan');
    
    //     $(this).find(".plan-check").toggle();

    //     var discount_amount = $(this).find(".discount").text();

    //     var price = $(this).attr("data-price");

    //     if ((discount_amount.charAt(0)) === "R") {
       
    //       var voucher_discount = discount_amount.slice(2, 10);
    //       var total_price = price - voucher_discount;
    //       $(this).find(".sub-price").text("R " + total_price);
    //       $(this).find(".sub-text").text("total");
    //       $(".discount").text("");
    //       $(".discount-text").text("");
    //     }else if ((discount_amount.slice(-1)) == "%") {
          
    //       var voucher_discount = discount_amount.slice(0,-1);
    //       var total_price = price - (price * voucher_discount / 100);
    //       $(this).find(".sub-price").text("R " + total_price);
    //       $(this).find(".sub-text").text("total");
    //       $(".discount").text("");
    //       $(".discount-text").text("");
    //     }else {
    //       var total_price = $(this).attr("data-price");
    //     }

    //     var price = total_price;
    //     var item = $(this).attr("data-item");
    //     var desc = $(this).attr("data-description");
    //     console.log(price);
    //     $("#price").val(price);
    //     $("#item_title").val(item);
    //     $("#item_description").val(desc);
    //     $("#cart_total").val(price);
    //     $("#confirm_total").text("R" + price);



    // })
</script>
@endsection
