@extends('backend_minimal')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col freelancer-basics-form-container">
            <div class="freelancer-basics-form">
                <form class="col" action="/users/profiles/register/about" method="post">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
                    <h2 class="font-weight-bold mb-2 mt-5">Now back to some basics.</h2>
                    <p>This will only take a minute.</p>
                    <div class="form-group">
                        <label for="" class="font-weight-bold mx-0">Tell us about yourself</label><br>
                        <textarea name="about" class="w-100 mb-3" rows="8" cols="100" placeholder="Tell us about yourself" required></textarea>
                    </div>
                    <div class="form-group mr-n5 pr-n5">
                        <label for="" class="font-weight-bold">How do we call you?</label>
                        <div class="">
                            <label class="mb-2">Mobile</label>
                            <i class="far fa-square text-muted mr-2" id="mobile"></i>
                            <i class="far fa-check-square mr-2 text-blue" style="display:none" id="mobile_clicked"></i>
                            <input type="text" class="d-none" name="mobile" value="" id="mobile-input" placeholder="0821234567" maxlength="10">
                            <label class="mb-2">Office</label>
                            <i class="far fa-square text-muted mr-2" id="office"></i>
                            <i class="far fa-check-square mr-2 text-blue" style="display:none" id="office_clicked"></i>
                            <input type="text" name="office" class="d-none" id="office-input" placeholder="0112345678" maxlength="10">
                        </div>


                    </div>
                    <div class="form-group w-75">
                          <label class="font-weight-bold mb-1" for="cities">Where are you based:</label><br>
                        <input type="text" name="city" class="" id="user_city" required>
                    </div>
                    <button type="submit" class="btn btn-red btn-lg px-5 py-1 mt-2 mb-5">Next step..</button>
                </form>
            </div>
        </div>
        <div class="col d-none d-md-block freelancer-basics-right">


        </div>

    </div>
</div>

@endsection
