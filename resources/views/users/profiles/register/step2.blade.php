@extends('dashboard')
@section('content')

  <div class="container-fluid">
      <div class="row">
        <div class="col-6 freelancer-step2-right">


        </div>
          <div class="col freelancer-step2-form-container">
              <div class="freelancer-step2-form">
                  <form class="" action="/users/profiles/register/step2" method="post">
                      @csrf
                      {{-- <h2 class="font-weight-bold mb-2">Next up.</h2>
                      <p class="mb-3">This will only take a minute.</p> --}}
                      <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                              <label for="" class="font-weight-bold">Do you have a company?</label>
                              <div class="">
                                {{-- <input type="radio" name="company_question" value="yes" id="company_yes"> --}}
                                <i class="far fa-circle text-muted" id="company_yes"></i>
                                <i class="fas fa-dot-circle text-blue" id="company_yes_clicked" style="display:none"></i>
                                <label for="" class="mr-5">Yes</label>
                                {{-- <input type="radio" name="company_question" value="no" id="company_no"> --}}
                                <i class="far fa-circle text-muted" id="company_no"></i>
                                <i class="fas fa-dot-circle text-blue" id="company_no_clicked" style="display:none"></i>
                                <label for="">No</label>
                              </div>
                              <div class="form-group" id="company-input" style="display:none">
                                {{-- <label for="" class="font-weight-bold">Company name</label> --}}
                                <input type="text" name="company_name" class="w-100" placeholder="Company name">
                              </div>
                          </div>
                          <div class="col px-0">
                            <label for="" class="font-weight-bold mb-0">Connect your social media.</label><br>
                            <label for="" class="text-muted mb-4">(Not a must, but we would like to connect with you via these channels)</label>
                            <div class="row">
                              <div class="col">
                                <div class="row">
                                  <div class="col-4">
                                    <i class="far fa-square text-muted mr-2" id="whatsapp"></i>
                                    <i class="far fa-check-square mr-2 text-blue" style="display:none" id="whatsapp_clicked"></i>
                                    <label class="mb-4">Whatsapp</label>
                                  </div>
                                  <div class="col">
                                    <input type="text" class="" name="whatsapp" value="{{$mobile}}" id="whatsapp-input" style="display:none">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-4">
                                    <i class="far fa-square text-muted mr-2" id="facebook"></i>
                                    <i class="far fa-check-square mr-2 text-blue" style="display:none" id="facebook_clicked"></i>
                                    <label class="mb-4">Facebook</label>
                                  </div>
                                  <div class="col">
                                    <input type="text" class="" name="facebook" id="facebook-input" placeholder="Your Facebook URL" style="display:none">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-4">
                                    <i class="far fa-square text-muted mr-2" id="instagram"></i>
                                    <i class="far fa-check-square mr-2 text-blue" style="display:none" id="instagram_clicked"></i>
                                    <label class="mb-4">Instagram</label>
                                  </div>
                                  <div class="col">
                                    <input type="text" class="" name="instagram" id="instagram-input" placeholder="Your Instagram handle" style="display:none">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-4">
                                    <i class="far fa-square text-muted mr-2" id="linkedin"></i>
                                    <i class="far fa-check-square mr-2 text-blue" style="display:none" id="linkedin_clicked"></i>
                                    <label class="mb-4">LinkedIn</label>
                                  </div>
                                  <div class="col">
                                    <input type="text" class="" name="linkedin" id="linkedin-input" placeholder="Your LinkedIn URL" style="display:none">
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>


                      {{-- <div class="row">
                          <div class="col">
                              <div class="form-group">
                                <label for="skills" class="font-weight-bold">Skills: <small>(Use the , <i>"comma"</i> to add more than 1)</small> </label>

                                <div class="tags-container" data-name="tags_container">
                                  <input type="hidden" name="user_tags" id="hiddenInput" value="">
                                </div>


                              </div>
                          </div>
                      </div> --}}


                      {{-- <div class="row mb-3">
                          <div class="col">
                              <label for="industry">Please select your industry</label>
                              <select class="form-control" name="industry" id="industry_id">
                                  <option value="">-- Select --</option>
                              </select>
                          </div>


                          <div class="col">
                              <label for="subindustries">Sub Industries</label><small>(Use CTRL to select many options)</small>
                              <select class="form-control" multiple="multiple" name="subindustries[]" id="subcategory_id" style="height:250px;">
                                <option value="">-- Select --</option>
                              </select>
                          </div>
                      </div> --}}
                      <button type="submit" class="btn btn-red btn-lg px-5">Next</button>
                  </form>
              </div>
          </div>



      </div>
  </div>
  <script type="text/javascript">
  $('#company_yes').click(function() {

        $('#company-input').show();
        $('#company_yes_clicked').show();
        $('#company_yes').hide();
        $('#company_no').show();
        $('#company_no_clicked').hide();
  });
  $('#company_no').click(function() {
    $('#company-input').hide();
    $('#company_no_clicked').show();
    $('#company_no').hide();
    $('#company_yes').show();
    $('#company_yes_clicked').hide();
  });

  $('#whatsapp').click(function() {
      $('#whatsapp-input').show();
      $('#whatsapp').hide();
      $('#whatsapp_clicked').show();
  });
  $('#whatsapp_clicked').click(function() {
      $('#whatsapp-input').hide();
      $('#whatsapp').show();
      $('#whatsapp_clicked').hide();
  });
  $('#facebook').click(function() {
      $('#facebook-input').show();
      $('#facebook').hide();
      $('#facebook_clicked').show();
  });
  $('#facebook_clicked').click(function() {
      $('#facebook-input').hide();
      $('#facebook').show();
      $('#facebook_clicked').hide();
  });
  $('#linkedin').click(function() {
      $('#linkedin-input').show();
      $('#linkedin').hide();
      $('#linkedin_clicked').show();
  });
  $('#linkedin_clicked').click(function() {
      $('#linkedin-input').hide();
      $('#linkedin').show();
      $('#linkedin_clicked').hide();
  });
  $('#instagram').click(function() {
      $('#instagram-input').show();
      $('#instagram').hide();
      $('#instagram_clicked').show();
  });
  $('#instagram_clicked').click(function() {
      $('#instagram-input').hide();
      $('#instagram').show();
      $('#instagram_clicked').hide();
  });
  // $('#facebook').click(function() {
  //     var n = $("#facebook:checked").length;
  //     if (n === 1) {
  //       $('#facebook-input').show();
  //     }else {
  //       $('#facebook-input').hide();
  //     }
  // });
  // $('#linkedin').click(function() {
  //     var n = $("#linkedin:checked").length;
  //     if (n === 1) {
  //       $('#linkedin-input').show();
  //     }
  //     else {
  //       $('#linkedin-input').hide();
  //     }
  // });
  // $('#twitter').click(function() {
  //     var n = $("#twitter:checked").length;
  //     if (n === 1) {
  //       $('#twitter-input').show();
  //     }else {
  //       $('#twitter-input').hide();
  //     }
  // });

  </script>
  <script src="/js/dynamicdropdown.js" charset="utf-8"></script>
  <script src="/js/jquery.multi-select.js" charset="utf-8"></script>
  <script src="/js/tags.js" charset="utf-8"></script>
@endsection
