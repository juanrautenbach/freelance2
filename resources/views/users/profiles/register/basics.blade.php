@extends('backend')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col freelancer-basics-form-container">
            <div class="freelancer-basics-form">
                <form class="" action="/users/profiles/register/basics" method="post">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
                    <h2 class="font-weight-bold mb-2">Let's start with some basics.</h2>
                    <p>This will only take a minute.!!</p>
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Tell us about yourself</label>
                        <textarea name="about" rows="8" cols="80" placeholder="Tell us about yourself"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-weight-bold">How do we call you?</label>
                        <div class="">
                            <label class="mb-4">Mobile</label>
                            <i class="far fa-square text-muted mr-2" id="mobile"></i>
                            <i class="far fa-check-square mr-2 text-blue" style="display:none" id="mobile_clicked"></i>
                            <input type="text" class="" name="mobile" value="" id="mobile-input" style="display:none">
                            <label class="mb-4">Office</label>
                            <i class="far fa-square text-muted mr-2" id="office"></i>
                            <i class="far fa-check-square mr-2 text-blue" style="display:none" id="office_clicked"></i>
                            <input type="text" class="" name="office" value="" id="office-input" style="display:none">
                        </div>


                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold mb-3" for="cities">Where are you based:</label><br>
                        <input type="text" name="city" class="w-75 mb-3" id="user_city">
                    </div>
                    <button type="submit" class="btn btn-red btn-lg px-5">Next step..</button>
                </form>
            </div>
        </div>
        <div class="col freelancer-basics-right">


        </div>

    </div>
</div>
<script type="text/javascript">
  function activatePlacesSearch(){
    let input = document.getElementById('user_city');
    let autocomplete = new google.maps.places.Autocomplete(input);
  }

  $('#mobile').click(function() {
      $('#mobile-input').show();
      $('#mobile').hide();
      $('#mobile_clicked').show();
  });
  $('#mobile_clicked').click(function() {
      $('#mobile-input').hide();
      $('#mobile').show();
      $('#mobile_clicked').hide();
  });
  $('#office').click(function() {
      $('#office-input').show();
      $('#office').hide();
      $('#office_clicked').show();
  });
  $('#office_clicked').click(function() {
      $('#office-input').hide();
      $('#office').show();
      $('#office_clicked').hide();
  });
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3vqC_XDkDB4N_Nit2boqMJpB9gHF8TQM&libraries=places&callback=activatePlacesSearch"></script>

<script type="text/javascript">
    $('#mobile-selected').click(function() {
        var n = $("#mobile-selected:checked").length;
        if (n === 1) {
          $('#mobile-input').show();
        } else {
          $('#mobile-input').hide();
        }
    });
    $('#office-selected').click(function() {
        var n = $("#office-selected:checked").length;
        if (n === 1) {
          $('#office-input').show();
        } else {
          $('#office-input').hide();
        }
    });

</script>
@endsection
