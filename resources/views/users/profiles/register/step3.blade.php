@extends('dashboard')
@section('content')
@php
$user = \Auth::user();
@endphp
@if ((empty($user->profile)) && ($user->login_count
< 50)) @include('components.profilesetup')
<script type="text/javascript">
    setTimeout(() => {
        $('#profileSetup').modal('show');
    }, 3000)

    function showModal() {
        $('#profileSetup').modal('show');
    }
</script>

@endif

<script src="/js/croppie.js"></script>
<link rel="stylesheet" type="text/css" href="/css/croppie.css">
<section class="profile-dashboard">
    <div class="container-fluid p-0" style="width: 1440px">
        <div class="row profile_cover" id="profile_cover">
            <div class="col p-0">
                @if (empty($user->profile->profile_cover_pic))
                <img src="http://via.placeholder.com/1440x360" alt="" id="unset_cover_pic">
                @else
                <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image" alt="" id="cover_pic">
                @endif
                <div class="" style="display: none">
                  <input type="file" name="cover_pic" value="" id="cover_croppie_file">
                </div>

                <img src="" alt="" id="cover_croppie" style="display:none">
                <div class="profile_cover_hover" id="profile_cover_hover" style="">
                    <div class="cover_photo_update">
                        <i class="fas fa-camera" id="cover_pic_btn"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row profile_details">
            <div class="profile_pic shadow" id="profile_photo">
                @if (empty($user->profile->profile_pic))
                <img src="http://via.placeholder.com/275" alt="">
                @else
                <img src="/storage/{{$user->profile->profile_pic}}" alt="" class="profile_photo_image">
                @endif

                <div class="profile_photo_hover" id="profile_photo_hover" style="">
                    <div class="profile_photo_update">
                        <i class="fas fa-camera"></i>
                    </div>
                </div>
            </div>
            <div class="row w-100">
                <div class="col">
                    <div class="user_details px-3">
                        <div class="name w-100">
                            <h3 class="d-inline">{{$user->name}}</h3>
                            <div class="float-right">
                                @if ($user->role !== "Freelancer")
                                <div class="contact-button">
                                    <a href="#" class="btn btn-red btn-lg px-5">Contact me</a>
                                </div>
                                @else
                                <div class="contact-button">
                                    <a href="#" class="btn btn-link btn-lg px-5"><i class="fas fa-pencil-alt text-muted"></i></a>
                                </div>
                                @endif
                            </div>
                        </div>

                        <h5 class="d-inline mr-1">{{$industry_name}} -</h5>
                        @foreach ($subindustries as $value)
                        <h5 class="font-weight-normal d-inline mr-1">{{$value}}</h5>
                        @endforeach
                        {{-- <h5 class="font-weight-normal">Graphic Design</h5> --}}
                        <div class="user_ratings mt-2">
                            <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                            <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                            <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                            <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="30px">
                            <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="30px">
                        </div>
                        <div class="row">
                            <div class="col mt-3">
                                @if (!empty($tags) > 0)
                                @foreach ($tags as $tag)
                                <span class="badge badge-light shadow-sm px-3 py-2 text-uppercase tag-items mb-2">{{$tag->name}}</span>
                                @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <div class="container-fluid p-0 mb-3 mt-3" style="width: 1440px">
        <div class="row">
            <div class="card shadow-sm col p-3 border-0 rounded-0">
                <div class="row">
                    <div class="col">
                        <h4 class="d-inline ml-2">Contact information</h4>
                        <h4 class="p-0 ml-3 d-inline"><i class="fas fa-pencil-alt text-muted"></i></h4>
                    </div>
                </div>
                <table class="table table-borderless">
                    <tr>
                        <td><span class="font-weight-bold mr-5">Company</span></td>
                        <td>{{$user->profile->company_name ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">City</span></td>
                        <td>{{$user->profile->city ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">Email</span></td>
                        <td>{{$user->email ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">Mobile</span></td>
                        <td>{{$user->profile->mobile ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><span class="font-weight-bold mr-5">Tel</span></td>
                        <td>{{$user->profile->tel ?? "Not set"}}</td>
                        <td></td>
                    </tr>
                    @if (!empty($user->profile->social_media) > 0)
                    @php
                    $social = $user->profile->social_media;
                    $social_links = json_decode($social);
                    @endphp
                    @foreach ($social_links as $key => $value)
                    @php
                    $key = ucfirst($key);
                    @endphp
                    @if (!empty($value))
                    <tr>
                        <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                        <td>{{$value}}</td>
                        <td></td>
                    </tr>
                    @else
                    <tr>
                        <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                        <td>Not set</td>
                        <td></td>
                    </tr>
                    @endif
                    @endforeach
                    @else
                    <tr>
                        <td>Social media</td>
                        <td>{{ "Not set" }}</td>
                        <td></td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>

    <div class="container-fluid p-0 mb-3" style="width: 1440px">
        <div class="row">
            <div class="card shadow-sm col p-3 border-0 rounded-0 mb-3">
                <div class="row">
                    <div class="col mb-3">
                        <h4 class="d-inline ml-2">About me</h4>
                        {{-- <div class=""> --}}
                        <h4 class="p-0 ml-3 d-inline"><i class="fas fa-pencil-alt text-muted"></i></h4>
                        {{-- </div> --}}
                    </div>

                </div>
                <p class="mx-2">{{$user->profile->about ?? "Not set"}}</p>
            </div>
        </div>
    </div>

    <div class="container-fluid p-0 mb-3" style="width: 1440px">
        <div class="row">
            <div class="col p-3 border-0 rounded-0 mb-3">
                <h4 class="d-inline">Portfolio</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0 mb-3" style="width: 1440px">
        <div class="row mb-4">
            <div class="col">
                <div class="show_case_block mx-auto">
                    <div class="show_case_add_btn">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="show_case_block mx-auto">
                    <div class="show_case_add_btn">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="show_case_block mx-auto">
                    <div class="show_case_add_btn">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col">
                <div class="show_case_block mx-auto">
                    <div class="show_case_add_btn">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="show_case_block mx-auto">
                    <div class="show_case_add_btn">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="show_case_block mx-auto">
                    <div class="show_case_add_btn">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col p-0">
                      @if ($showcase_count == 0)
                        <div class="profile_showcase_get_started">
                            <button type="button" class="btn btn-red btn-lg showcaseAdd" name="button"><i class="fas fa-plus"></i></button>
                        </div>
                      @else
                        @foreach ($user_showcases->chunk(3) as $items)
                        <div class="">
                            <div class="row mb-3">
                                @foreach($items as $item)
                                  <div class="col-md-4">
                                    <div class="card p-2" style="width: 20rem; max-height: 380px; min-height:380px;">
                                        <a href="#">
                                            <img class="mx-auto d-block mb-3" src="{{ '/storage/'.$item->showcase_photo }}" alt="">
        </a>
        <div class="px-2">
            <h5>{{$item->title}}</h5>
            <p>{{ str_limit($item->description, 100) }}</p>
        </div>

    </div>
    </div>

    @endforeach
    </div>

    </div>
    @endforeach
    @endif



    </div> --}}
    {{-- </div> --}}
    </div>


    <div class="row profile_comment">

    </div>

    <div class="add_showcase" id="add_showcase" style="display:none">
        <div class="card col-6 mx-auto">

            <div class="card-body">
                <h5 class="card-title ">Add showcase</h5>
                <form class="" action="/usershowcase" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="showcase_title" class="font-weight-bold">Title:</label>
                        <input type="text" class="form-control" name="showcase_title" required>
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                    </div>
                    <div class="form-group">
                        <label for="showcase_desc" class="font-weight-bold">Description:</label>
                        <input type="text" class="form-control" name="showcase_desc">
                    </div>
                    <div class="form-group">
                        <label for="showcase_photo" class="font-weight-bold">Photo:</label>
                        <input type="file" name="showcase_photo" required>
                    </div>
                    <button type="submit" name="button" class="btn btn-blue btn-lg">Save</button>
                    <span name="button" class="btn btn-link btn-lg text-muted" id="cancelshowcase">Cancel</span>
                </form>


            </div>
        </div>
    </div>
    {{-- <div class="add_profile_photo" id="add_profile_photo" style="display:none">
              <div class="card col-6 mx-auto">

                  <div class="card-body">
                      <form class="" action="/usershowcase/profile_photo" method="post" enctype="multipart/form-data">
                          @csrf
                          <div class="form-group">
                              <label for="profile_photo">Photo:</label>
                              <input type="file" name="profile_photo" id="profile_photo" class="image" required>
                              <input type="hidden" name="x1" value="" />
                              <input type="hidden" name="y1" value="" />
                              <input type="hidden" name="w" value="" />
                              <input type="hidden" name="h" value="" />
                          </div>
                          <div class="form-group">

                              <input type="hidden" name="user_id" value="{{$user->id}}">
    </div>
    <div class="row mt-5" style="width: 100vw;">
        <p><img id="previewimage" style="display:none;" /></p>
        @if(session('path'))
        <img src="{{ session('path') }}" />
        @endif
    </div>
    <button type="submit" name="button" class="btn btn-blue btn-lg">Save</button>
    <span name="button" class="btn btn-link btn-lg text-muted" id="cancel_profile_photo">Cancel</span>
    </form>


    </div>
    </div>
    </div> --}}
    {{-- <div class="add_profile_cover" id="add_profile_cover" style="display:none">
              <div class="card mx-auto">
                  <div class="card-body">
                      <div class="container">
                          <form class="" action="/usershowcase/profile_cover" method="post" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                                  <label for="exampleInputImage">Photo:</label>
                                  <input type="file" name="cover_photo" id="exampleInputImage" class="image" required>
                                  <input type="hidden" name="x1" value="" />
                                  <input type="hidden" name="y1" value="" />
                                  <input type="hidden" name="w" value="" />
                                  <input type="hidden" name="h" value="" />
                              </div>
                              <div class="form-group">
                                  <input type="hidden" name="user_id" value="{{$user->id}}">
    </div>
    <button type="submit" name="button" class="btn btn-blue btn-lg">Save</button>
    <span name="button" class="btn btn-link btn-lg text-muted" id="cancel_profile_cover">Cancel</span>
    </form>
    <div class="row mt-5" style="width: 100vw;">
        <p><img id="previewimage" style="display:none;" /></p>
        @if(session('path'))
        <img src="{{ session('path') }}" />
        @endif
    </div>
    </div>
    </div>
    </div>
    </div> --}}

    {{-- <div class="container">
    <tr>
          <td colspan="2"> <strong>Social media</strong><br>



  </div> --}}
    <script type="text/javascript">
        $(".showcaseAdd").click(function() {
            $("#add_showcase").show();
        })

        $("#cancelshowcase").click(function() {
            $("#add_showcase").hide();
        })

        $("#profile_photo").click(function() {
            $("#add_profile_photo").show();
        })
        $("#cancel_profile_photo").click(function() {
            $("#add_profile_photo").hide();
            $("#previewimage1").hide();
        })

        $("#profile_cover").click(function() {
            $("#add_profile_cover").show();
        })
        $("#cancel_profile_cover").click(function() {
            $("#add_profile_cover").hide();
            $("#previewimage").hide();
        })

        function displayGeneral() {
            $("#general").show();
            $("#showcase").hide();
            $("#gallery").hide();
            $("#subscriptions").hide();
        }

        function displayShowcase() {
            $("#general").hide();
            $("#showcase").show();
            $("#gallery").hide();
            $("#subscriptions").hide();
        }

        function displayGallery() {
            $("#general").hide();
            $("#showcase").hide();
            $("#gallery").show();
            $("#subscriptions").hide();
        }

        function displaySubscriptions() {
            $("#general").hide();
            $("#showcase").hide();
            $("#gallery").hide();
            $("#subscriptions").show();
        }
    </script>
</section>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
{{-- <script src="{{ asset('js/jquery.imgareaselect.min.js') }}"></script> --}}


<script>
    $('#cover_pic_btn').click(function() {
        $('#unset_cover_pic').hide();
        $('#cover_pic').hide();
        // $('#cover_croppie').show();

        $('#cover_croppie_file').trigger('click');
        $('#cover_croppie_file').change(function() {
          console.log($(this).val());
            $('#cover_croppie').attr("src", $(this).val());
        });



    })
</script>
@endsection
