@extends('dashboard')
@section('content')

  <div class="container-fluid">
      <div class="row">
        <div class="col-6 freelancer-step2-right">
        </div>
          <div class="col freelancer-step2-form-container">
              <div class="freelancer-step2-form">
                  <form class="" action="/users/profiles/register/step2b" method="post">
                      @csrf
                      <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                              <label for="" class="font-weight-bold">What are your skills?</label><br>
                              <label for="">Enter your skills divided by a comma.</label>
                              <div class="row">
                                  <div class="col">
                                      <div class="form-group">
                                        <div class="tags-container" data-name="tags_container">
                                          <input type="hidden" name="user_tags" id="hiddenInput" value="">
                                        </div>
                                      </div>
                                  </div>
                              </div>

                          </div>
                          <div class="col px-0">
                            <div class="row mb-3">
                                <div class="col">
                                    <label for="industry" class="font-weight-bold">Your industry</label>
                                    <select class="mb-5" name="industry" id="industry_id">
                                        <option value="">Select industry</option>
                                    </select>

                                    <label for="subindustries" class="font-weight-bold">Your sub-industries</label>
                                    {{-- <select class=""  name="subindustries[]" id="subcategory_id">
                                      <option value="">Select your sub-industries</option>
                                    </select> --}}
                                    <div class="" id="subcategory_id">

                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-red btn-lg px-5">Next</button>
                  </form>
              </div>
          </div>



      </div>
  </div>
  <script type="text/javascript" id="scripts">
  $('#company_yes').click(function() {

        $('#company-input').show();
        $('#company_yes_clicked').show();
        $('#company_yes').hide();
        $('#company_no').show();
        $('#company_no_clicked').hide();
  });
  $('#company_no').click(function() {
    $('#company-input').hide();
    $('#company_no_clicked').show();
    $('#company_no').hide();
    $('#company_yes').show();
    $('#company_yes_clicked').hide();
  });

  $('#whatsapp').click(function() {
      $('#whatsapp-input').show();
      $('#whatsapp').hide();
      $('#whatsapp_clicked').show();
  });
  $('#whatsapp_clicked').click(function() {
      $('#whatsapp-input').hide();
      $('#whatsapp').show();
      $('#whatsapp_clicked').hide();
  });
  $('#facebook').click(function() {
      $('#facebook-input').show();
      $('#facebook').hide();
      $('#facebook_clicked').show();
  });
  $('#facebook_clicked').click(function() {
      $('#facebook-input').hide();
      $('#facebook').show();
      $('#facebook_clicked').hide();
  });
  $('#linkedin').click(function() {
      $('#linkedin-input').show();
      $('#linkedin').hide();
      $('#linkedin_clicked').show();
  });
  $('#linkedin_clicked').click(function() {
      $('#linkedin-input').hide();
      $('#linkedin').show();
      $('#linkedin_clicked').hide();
  });
  $('#instagram').click(function() {
      $('#instagram-input').show();
      $('#instagram').hide();
      $('#instagram_clicked').show();
  });
  $('#instagram_clicked').click(function() {
      $('#instagram-input').hide();
      $('#instagram').show();
      $('#instagram_clicked').hide();
  });
  // $('#facebook').click(function() {
  //     var n = $("#facebook:checked").length;
  //     if (n === 1) {
  //       $('#facebook-input').show();
  //     }else {
  //       $('#facebook-input').hide();
  //     }
  // });
  // $('#linkedin').click(function() {
  //     var n = $("#linkedin:checked").length;
  //     if (n === 1) {
  //       $('#linkedin-input').show();
  //     }
  //     else {
  //       $('#linkedin-input').hide();
  //     }
  // });
  // $('#twitter').click(function() {
  //     var n = $("#twitter:checked").length;
  //     if (n === 1) {
  //       $('#twitter-input').show();
  //     }else {
  //       $('#twitter-input').hide();
  //     }
  // });

  </script>
  <script src="/js/dynamicdropdown.js" charset="utf-8"></script>
  <script src="/js/jquery.multi-select.js" charset="utf-8"></script>
  <script src="/js/tags.js" charset="utf-8"></script>
@endsection
