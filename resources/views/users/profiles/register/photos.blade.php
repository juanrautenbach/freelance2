@extends('backend_minimal')
@section('content')
{{-- <link href="https://unpkg.com/dropzone/dist/dropzone.css" rel="stylesheet" />
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" /> --}}
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<link rel="stylesheet" href="/css/slim.min.css">
<style media="screen">
    /* .dz-message {
        margin-top: 50% !important;
        transform: translateY(-50%);
        color: #fff !important;
    }

    .dropzone {
        width: 291px;
        height: 291px;
        border: 8px solid #fff;
        background-color: #888;
        font-size: 20px;
    }
*/
    #profileSetup {
        width: 90% !important;
    }

    body {
        overflow: initial;
    }
    .scroller{
      position: fixed;
      top: 50%;
      right: 30px;
      transform: translateY(-50%);
    }
      .chevron {
        position: absolute;
        width: 28px;
        height: 8px;
        opacity: 0;
        transform: scale3d(0.5, 0.5, 0.5);
        animation: move 3s ease-out infinite;
        }
        
        .chevron:first-child {
        animation: move 3s ease-out 1s infinite;
        }
        
        .chevron:nth-child(2) {
        animation: move 3s ease-out 2s infinite;
        }
        
        .chevron:before,
        .chevron:after {
        content: ' ';
        position: absolute;
        top: 0;
        height: 100%;
        width: 51%;
        background: #f44336;
        }
        
        .chevron:before {
        left: 0;
        transform: skew(0deg, 30deg);
        }
        
        .chevron:after {
        right: 0;
        width: 50%;
        transform: skew(0deg, -30deg);
        }
        
        @keyframes move {
        25% {
        opacity: 1;
        
        }
        33% {
        opacity: 1;
        transform: translateY(30px);
        }
        67% {
        opacity: 1;
        transform: translateY(40px);
        }
        100% {
        opacity: 0;
        transform: translateY(55px) scale3d(0.5, 0.5, 0.5);
        }
        }
        .text {
        display: block;
        margin-top: 65px;
        margin-left: -30px;
        font-family: "Helvetica Neue", "Helvetica", Arial, sans-serif;
        font-size: 12px;
        color: #f44336;
        text-transform: uppercase;
        white-space: nowrap;
        opacity: .25;
        animation: pulse 2s linear alternate infinite;
        }
        
        @keyframes pulse {
        to {
        opacity: 1;
        }
        }
    
    </style>
@php
$user = \Auth::user();
@endphp
<section class="profile-dashboard">
        @include('freelancers.profile.header')

        @include('freelancers.profile.contact_info')

        @include('freelancers.profile.about')

        @include('freelancers.profile.portfolio')

</section>



<script src="/js/dynamicdropdown.js" charset="utf-8"></script>

<script src="/js/tags.js" charset="utf-8"></script>

<script src="/js/slim.kickstart.min.js"></script>

<script>
    $("#submit_1").click(function () {
        $("#form1").submit();
    })
</script>


@endsection
