@extends('dashboard')
@section('content')
<link href="https://unpkg.com/dropzone/dist/dropzone.css" rel="stylesheet" />
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" />
{{-- \<script type="text/javascript">

      setTimeout(() => {
          $('#profileSetup').modal('show');
      }, 1000)
  </script> --}}
<style media="screen">
    .dz-message {
        margin-top: 172px !important;
        /* transform: translateY(-50%); */
        color: #fff !important;
    }
    .dz-image-preview{
      text-align: center;
    }
    .dropzone {
        width: 1456px;
        height: 371px;
        border: 8px solid #fff;
        background-color: #888;
    }
</style>
@php
$user = \Auth::user();
@endphp
{{-- <div id="profileSetup" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <img src="/img/profilepic1.gif" alt="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal">Got it!</button>

        </div>
      </div>
    </div>
  </div> --}}
<section class="profile-dashboard">
    <div class="container-fluid" style="width: 1440px">
      <form class="" action="/users/profiles/register/photos" method="post" enctype="multipart/form-data">
        <div class="row profile_cover">
            <div class="col p-0">
                @if (empty($user->profile->profile_cover_pic))

                    @csrf
                    <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
                    <div class="form-group m-0">
                        <div class="dropzone mx-auto" id="myDropzone" style="width: 1456px; height: 376px"></div>
                        <div class="dropzone-previews mx-auto" id="photo-preview" style="display: none; width: 1456px; height: 376px; border: 3px dashed #000; ">
                        </div>
                    </div>
                    @else
                    <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image" alt="">
                    @endif
                    <input type="hidden" name="profile_photo" id="pphoto" value="">

            </div>
        </div>
        <div class="row profile_details">
            <div class="profile_pic shadow" id="profile_photo">
                <img src="/storage/{{$user->profile->profile_pic}}" alt="" class="profile_photo_image">
            </div>
        </div>
        <button type="submit" class="btn btn-red btn-lg px-5 mt-5 float-right">Next step..</button>
    </form>
    </div>

</section>

<script src="https://unpkg.com/dropzone"></script>
<script src="https://unpkg.com/cropperjs"></script>
<script>
    var x = 1456;
    var y = 376;
    var dzMessage = "Drop your photo here or click to browse";
    var path_url = '/users/profiles/register/photos';
    var popupContainer = 'width: 1440px; height: 360px; top:50%; left:50%; transform: translate(-50%, -50%);';
    var pic_ratio = 1440 / 360;
</script>
<script src="/js/dz_cropper.js" charset="utf-8"></script>
@endsection
