@extends('dashboard')
@section('content')
<link rel="stylesheet" href="{{ asset('css/imgareaselect.css') }}">
<section class="profile-dashboard">
    <div class="container-fluid" style="width: 1200px">
        <div class="row profile_cover" id="profile_cover">
            <div class="col p-0">
                @if (empty($user->profile->profile_cover_pic))
                <img src="http://via.placeholder.com/1200x360" alt="">
                @else
                <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image" alt="">
                @endif
                <div class="profile_cover_hover" id="profile_cover_hover" style="display:none">
                    <div class="photo_update">
                        <i class="fas fa-camera"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row profile_details">
            <div class="profile_pic shadow" id="profile_photo">
                @if (empty($user->profile->profile_pic))
                <img src="http://via.placeholder.com/275" alt="">
                @else
                <img src="/storage/{{$user->profile->profile_pic}}" alt="" class="profile_photo_image">
                @endif
                <div class="profile_photo_hover" id="profile_photo_hover" style="display:none">
                    <div class="photo_update">
                        <i class="fas fa-camera"></i>
                    </div>
                </div>
            </div>
            <div class="user_details">
                <h3>{{$user->name}}</h3>
                @if (empty($industry_name))
                <button type="button" name="button" class="btn btn-red" onclick="showIndustry()"><i class="fas fa-plus"></i></button>
                <div style="display:none" id="profileIndustry">
                    <div class="modal-body col-6 mx-auto bg-white p-4">
                        <div class="row">
                            <div class="col">
                              <h4>Select your industries</h4>
                                <label for="industry">Please select your industry</label>
                                <select class="form-control" name="industry" id="industry_id">
                                    <option value="">-- Select --</option>

                                </select>
                            </div>
                        </div>
                        <div class="row" class="mb-5" style="height: 20rem">
                            <div class="col">
                                <label for="subindustries">Sub Industries</label><small>(Use CTRL to select many options)</small>
                                <select class="form-control" multiple="multiple" name="subindustries[]" id="subcategory_id" style="height:300px;">
                                    <option value="">-- Select --</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="col">
                            <button type="submit" name="button" class="btn btn-red">Save</button>
                            <button type="button" name="button" class="btn btn-link text-muted" onclick="cancelIndustry()">Cancel</button>
                          </div>
                        </div>
                    </div>
                </div>
                @else
                <h5 class="d-inline mr-1">{{$industry_name ?? 'Not set'}} -</h5>
                @foreach ($subindustries as $value)
                <h5 class="font-weight-normal d-inline mr-1">{{$value}}</h5>
                @endforeach
                @endif
                {{-- @foreach ($subindustries as $value)
                <h5 class="font-weight-normal d-inline mr-1">{{$value}}</h5>
                @endforeach --}}
                {{-- <h5 class="font-weight-normal">Graphic Design</h5> --}}
                <div class="user_ratings">
                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="30px">
                    <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="30px">
                </div>
            </div>
            <div class="col contact_user text-center d-flex align-items-center justify-content-center">
                <div class="contact-button">
                    <a href="#" class="btn btn-red btn-lg px-5">Contact me</a>
                </div>
            </div>
            <div class="row">
                <div class="col user_skills">
                    <span class="badge badge-light shadow px-2 py-1">HTML</span>
                    <span class="badge badge-light shadow px-2 py-1">PHP</span>
                    <span class="badge badge-light shadow px-2 py-1">CSS</span>
                    <span class="badge badge-light shadow px-2 py-1">JavaScript</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row profile_contact_info">
            <div class="col p-0">
                <div class="row">
                    <div class="col mb-3">
                        <form class="" action="/profile/update" method="post">
                            @csrf
                            <h4 class="d-inline">My contact info</h4><span class="float-right">
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button id="editBtn" type="button" name="button" class="btn btn-red" onclick="showEdit"><i class="fas fa-edit"></i></button>
                                <button id="saveBtn" style="display:none" type="submit" class="btn btn-red" name="button">Save</button>
                                <button id="cancelBtn" style="display:none" type="button" class="btn btn-blue" name="button">Cancel</button>
                            </span>
                    </div>
                </div>
                {{-- <form id="form1" runat="server" action="/upload" method="post" enctype="multipart/form-data">
                  @csrf
                  <input type='file' id="imgInp" name="upload_file" />
                  <img id="my-image" src="#" />
                  <button id="use">Upload</button>
                  <img id="result" src="">
                  <button type="submit" class="btn btn-primary" name="button">Save</button>
                </form> --}}
                <table class="table">


                    <tr>


                        <td style="display:none" class="editInputs"><span class="font-weight-bold mr-5">Company:</span></td>
                        <td style="display:none" class="editInputs">
                            <input type="text" name="company" class="form-control" value="{{$user->profile->company_name ?? ""}}">
                        </td>
                        <td style="display:none" class="editInputs"></td>


                        <td class="orgInputs"><span class="font-weight-bold mr-5">Company:</span></td>
                        <td class="orgInputs">{{$user->profile->company_name ?? ""}}</td>
                        <td class="orgInputs"></td>

                    </tr>
                    <tr>
                        <td class="orgInputs"><span class="font-weight-bold mr-5">City: <small>(Automatic)</small> </span></td>
                        <td class="orgInputs">{{$user->profile->city ?? ""}}</td>
                        <td class="orgInputs"></td>
                        <td style="display:none" class="editInputs"><span class="font-weight-bold mr-5">City: <small>(Automatic)</small> </span></td>
                        <td style="display:none" class="editInputs"><input type="text" name="city" class="form-control" readonly id="cityInput"></td>
                        <td style="display:none" class="editInputs"></td>
                    </tr>
                    <tr>
                        <td class="orgInputs"><span class="font-weight-bold mr-5">Email:</span></td>
                        <td class="orgInputs">{{$user->email}}</td>
                        <td class="orgInputs"></td>
                    </tr>
                    <tr>


                        <td style="display:none" class="editInputs"><span class="font-weight-bold mr-5">Mobile:</span></td>
                        <td style="display:none" class="editInputs">
                            <input type="text" name="mobile" class="form-control" value="{{$user->profile->mobile ?? ""}}">

                        </td>
                        <td style="display:none" class="editInputs"> </td>

                        <td class="orgInputs"><span class="font-weight-bold mr-5">Mobile:</span></td>
                        <td class="orgInputs">{{$user->profile->mobile ?? "Not set"}}</td>
                        <td class="orgInputs"></td>

                    </tr>
                    <tr>

                        <td style="display:none" class="editInputs"><span class="font-weight-bold mr-5">Telephone:</span></td>
                        <td style="display:none" class="editInputs">
                            <input type="text" name="telephone" class="form-control" value="{{$user->profile->tel ?? ""}}">

                        </td>
                        <td style="display:none" class="editInputs"> </td>

                        <td class="orgInputs"><span class="font-weight-bold mr-5">Telephone:</span></td>
                        <td class="orgInputs">{{$user->profile->tel ?? "Not set"}}</td>
                        <td class="orgInputs"> </td>

                    </tr>
                    </form>
                    @if (!empty($user->profile->social_media) > 0)
                    @php
                    $social = $user->profile->social_media;
                    $social_links = json_decode($social);
                    @endphp
                    @foreach ($social_links as $key => $value)
                    @php
                    $key = ucfirst($key);
                    @endphp
                    <tr>
                        <td><span class="font-weight-bold mr-5">{{$key}}</span></td>
                        <td>{{$value}}</td>
                        <td></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>

                        <td><span class="font-weight-bold mr-5">Social media</span></td>
                        @if (empty($user->profile->social_media))
                        <td> <span class="btn btn-red" onclick="showSocial()"><i class="fas fa-plus"></i></span> </td>
                        @else
                        <td>{{$user->profile->social_media}}</td>
                        @endif
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <form class="" action="/profile/socialmedia" method="post">
                          @csrf

                        <td> <input type="text" id="social_media" name="social_media" value="">
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                        </td>
                        <td>
                            <div class="d-none">
                                <button type="submit">Save</button>
                            </div>
                        </td>
                        </form>
                    </tr>
                    @endif
                    {{-- <tr>
                      <td colspan="3" class="text-center"> <button type="submit" name="button" class="btn btn-red">Save</button> </td>
                    </tr> --}}
                    {{-- </form> --}}
                </table>
            </div>
        </div>
        <div class="row profile_about">
            <div class="col p-0">
                <h4>About me</h4>
                @if (empty($user->profile->about))
                <form class="" action="/profile/about" method="post">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <textarea name="about_me" rows="8" cols="80" class="form-control"></textarea>
                    <button type="submit" name="button" class="btn btn-red mt-2">Save</button>
                </form>
                @else
                <p>{{$user->profile->about}}</p>
                @endif
            </div>
        </div>
        {{-- <div class="contact_basics_form">
        </div> --}}
    </div>
    <script src="/js/dynamicdropdown.js" charset="utf-8"></script>
    <script type="text/javascript">
        function showIndustry() {
            $('#profileIndustry').show();
        }
        function cancelIndustry() {
          $('#profileIndustry').hide();
        }
        function showSocial() {
            Swal.mixin({
                input: 'text',
                confirmButtonText: 'Next',
                showCancelButton: false,
                allowOutsideClick: true,
                progressSteps: ['1', '2', '3']
            }).queue([{
                    title: 'Facebook',
                    text: 'Set the link to your Facebook here.'
                },
                {
                    title: 'Twitter',
                    text: 'Twitter info here. :-)'
                },
                {
                    title: 'Linkedin',
                    text: 'Linkedin info here. :-)'
                }
            ]).then((result) => {
                $('#social_media').val(result.value);
                $('#socialForm').submit();
                let timerInterval
                Swal.fire({
                    timer: 1500,
                    type: 'success',
                    text: 'Social media links saved!',
                    showConfirmButton: false
                })
                $('#socialForm').submit();
            })
        }

        $('#editBtn').on('click', function() {
            $('#saveBtn').show();
            $('#cancelBtn').show();
            $('#editBtn').hide();
            $('.editInputs').show();
            $('.orgInputs').hide();

            $.ajax({
                url: "https://geoip-db.com/jsonp",
                jsonpCallback: "callback",
                dataType: "jsonp",
                success: function(location) {
                    $('#cityInput').val(location.city);
                }
            });
        })

        $('#cancelBtn').on('click', function() {
            $('#saveBtn').hide();
            $('#cancelBtn').hide();
            $('#editBtn').show();
            $('.editInputs').hide();
            $('.orgInputs').show();
        })
        $("#profile_cover").hover(function() {
                $("#profile_cover_hover").show();
            },
            function() {
                $("#profile_cover_hover").hide();
            });
        $("#profile_photo").hover(function() {
                $("#profile_photo_hover").show();
            },
            function() {
                $("#profile_photo_hover").hide();
            });
        $("#showcaseAdd").click(function() {
            $("#add_showcase").show();
        })

        $("#cancelshowcase").click(function() {
            $("#add_showcase").hide();
        })

        $("#profile_photo").click(function() {
            $("#add_profile_photo").show();
        })
        $("#cancel_profile_photo").click(function() {
            $("#add_profile_photo").hide();
            $("#previewimage1").hide();
        })

        $("#profile_cover").click(function() {
            $("#add_profile_cover").show();
        })
        $("#cancel_profile_cover").click(function() {
            $("#add_profile_cover").hide();
            $("#previewimage").hide();
        })
    </script>
</section>
@endsection
