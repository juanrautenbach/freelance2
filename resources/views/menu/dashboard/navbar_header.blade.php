<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item"><a class="navbar-brand" href="/dashboard"><img class="brand-logo" alt="modern admin logo" src="/img/FCPT_Logo.png">
            <h3 class="brand-text d-none d-md-block">Freelance Cape Town</h3>
            <h3 class="brand-text d-md-none">FCT</h3>
        </a></li>
        <li class="nav-item d-md-none"><a class="nav-link open-navbar-container tw-pt-3" data-toggle="collapse" data-target="#navbar-mobile" aria-expanded="true"><i class="la la-ellipsis-v"></i></a></li>
    </ul>
</div>