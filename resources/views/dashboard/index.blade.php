@extends('backend')
@section('content')
<style media="screen">
    .img-modal {
        width: 100%;
        height: 100%;
    }
</style>
@if (session()->has('message'))
<script type="text/javascript">
    Swal.fire({
        type: '{{ session()->get('message_type') }}',
        text: '{{ session()->get('message') }}',
        showConfirmButton: false,
        timer: 2000
    })
</script>

@endif
@php
  $user = \Auth::user();
@endphp

<div class="py-3">
{{-- {{$login_count}} --}}
@if (($user->role !== "Freelancer") )
@elseif (($profile == 0) && ($login_count < 50))
  @include('components.profilemodal')
  <script type="text/javascript">
  setTimeout(() => {
     window.location.replace("/users/profiles/register");
 }, 1000)

  </script>
@else
  {{-- @include('components.job_posts') --}}
  @include('components.freelancer_dashboard')
@endif
@if ($user->role === "Freelancer")
  @include('components.subs')
  {{-- @include('components.job_posts') --}}
  <script type="text/javascript">
  function showSubs(){
    $('#subsModal').modal('show');
  }
  </script>
@endif




{{-- @else --}}

{{-- @endif --}}
@if ($user->role === "Admin")
  @include('components.adminscreens')
@endif

@if ($user->role === "Finder")
  @include('components.finderscreens')
@endif

@if ($user->role === "Co-Working")
  @include('components.coworkingscreens')
@endif

</div>
@endsection