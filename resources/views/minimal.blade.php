<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Freelance Cape Town</title>
    @include('templates.frontpage.head')
  </head>
  <body>

    @include('templates.frontpage.minimal')
    {{-- <div id="go-to-top">
        <a onclick="topFunction()"> <i class="fas fa-arrow-up"></i></a>
    </div> --}}
    <section id="main">
        @include('includes.messages')
        @yield('content')
    </section>
    @if (Request::is('freelancers'))
    {{-- @include('components.scrolltop') --}}
    @include('templates.frontpage.footer')
    @endif
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  </body>

</html>
