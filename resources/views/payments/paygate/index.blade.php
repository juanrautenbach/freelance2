@php

include_once '../resources/views/includes/global.inc.php';
// include_once('/includes/global.inc.php');
require_once '../resources/views/includes/paygate.payweb3.php';
  $location = session('location');
  $transaction = session('transaction');
  $data = session('data');

  // dd($data);
  $encryption_key = session('key');

  $PayWeb3 = new PayGate_PayWeb3();
  	/*
  	 * if debug is set to true, the curl request and result as well as the calculated checksum source will be logged to the php error log
  	 */
  	//$PayWeb3->setDebug(true);
  	/*
  	 * Set the encryption key of your PayGate PayWeb3 configuration
  	 */
  	$PayWeb3->setEncryptionKey($encryption_key);
  	/*
  	 * Set the array of fields to be posted to PayGate
  	 */
  	$PayWeb3->setInitiateRequest($data);

  	/*
  	 * Do the curl post to PayGate
  	 */
  	$returnData = $PayWeb3->doInitiate();

@endphp
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/css/app.css">
  <script src="/js/app.js" charset="utf-8"></script>
  <title>Payment Gateway</title>

    <style>
        label{
          display: none;
        }
        p{
          display: none;
        }
        textarea{
          display: none !important;
        }
    </style>

</head>
<body>
		<div class="container-fluid" style="min-width: 320px;">
			<div class="container">
				<form role="form" class="form-horizontal text-left" action="<?php echo $PayWeb3::$process_url ?>" method="post" name="paygate_process_form">
					<div class="form-group">
						<label for="PAYGATE_ID" class="col-sm-3 control-label">PayGate ID</label>
						<p id="PAYGATE_ID" class="form-control-static"><?php echo $data['PAYGATE_ID']; ?></p>
					</div>
					<div class="form-group">
						<label for="REFERENCE" class="col-sm-3 control-label">Reference</label>
						<p id="REFERENCE" class="form-control-static"><?php echo $data['REFERENCE']; ?></p>
					</div>
					<div class="form-group">
						<label for="AMOUNT" class="col-sm-3 control-label">Amount</label>
						<p id="AMOUNT" class="form-control-static"><?php echo $data['AMOUNT']; ?></p>
					</div>
					<div class="form-group">
						<label for="CURRENCY" class="col-sm-3 control-label">Currency</label>
						<p id="CURRENCY" class="form-control-static"><?php echo $data['CURRENCY']; ?></p>
					</div>
					<div class="form-group">
						<label for="RETURN_URL" class="col-sm-3 control-label">Return URL</label>
						<p id="RETURN_URL" class="form-control-static"><?php echo $data['RETURN_URL']; ?></p>
					</div>
					<div class="form-group">
						<label for="LOCALE" class="col-sm-3 control-label">Locale</label>
						<p id="LOCALE" class="form-control-static"><?php echo $data['LOCALE']; ?></p>
					</div>
					<div class="form-group">
						<label for="COUNTRY" class="col-sm-3 control-label">Country</label>
						<p id="COUNTRY" class="form-control-static"><?php echo $data['COUNTRY']; ?></p>
					</div>
					<div class="form-group">
						<label for="TRANSACTION_DATE" class="col-sm-3 control-label">Transaction Date</label>
						<p id="TRANSACTION_DATE" class="form-control-static"><?php echo $data['TRANSACTION_DATE']; ?></p>
					</div>
					<div class="form-group">
						<label for="EMAIL" class="col-sm-3 control-label">Customer Email</label>
						<p id="EMAIL" class="form-control-static"><?php echo $data['EMAIL']; ?></p>
					</div>
						<?php
							$displayOptionalFields = false;

							// foreach(array_keys($optionalFields) as $key => $value){
							// 	if($data[$value] != ''){
							// 		$displayOptionalFields = true;
							// 	}
							// }

							if($displayOptionalFields){
								echo <<<HTML
					<div class="well">
HTML;


								if($data['PAY_METHOD'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="PAY_METHOD" class="col-sm-3 control-label">Pay Method</label>
						<p id="PAY_METHOD" class="form-control-static">{$data['PAY_METHOD']}</p>
					</div>
HTML;
								}

								if($data['PAY_METHOD_DETAIL'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="PAY_METHOD_DETAIL" class="col-sm-3 control-label">Pay Method Detail</label>
						<p id="PAY_METHOD_DETAIL" class="form-control-static">{$data['PAY_METHOD_DETAIL']}</p>
					</div>
HTML;
								}

								if($data['NOTIFY_URL'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="NOTIFY_URL" class="col-sm-3 control-label">Notify Url</label>
						<p id="NOTIFY_URL" class="form-control-static">{$data['NOTIFY_URL']}</p>
					</div>
HTML;
								}

								if($data['USER1'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="USER1" class="col-sm-3 control-label">User Field 1</label>
						<p id="USER1" class="form-control-static">{$data['USER1']}</p>
					</div>
HTML;
								}

								if($data['USER2'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="USER2" class="col-sm-3 control-label">User Field 2</label>
						<p id="USER2" class="form-control-static">{$data['USER2']}</p>
					</div>
HTML;
								}

								if($data['USER3'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="USER3" class="col-sm-3 control-label">User Field 3</label>
						<p id="USER3" class="form-control-static">{$data['USER3']}</p>
					</div>
HTML;
								}

								if($data['VAULT'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="VAULT" class="col-sm-3 control-label">Vault</label>
						<p id="VAULT" class="form-control-static">{$data['VAULT']}</p>
					</div>
HTML;
								}

								if($data['VAULT_ID'] != ''){
									echo <<<HTML
					<div class="form-group">
						<label for="VAULT_ID" class="col-sm-3 control-label">Vault ID</label>
						<p id="VAULT_ID" class="form-control-static">{$data['VAULT_ID']}</p>
					</div>
HTML;
								}

								echo <<<HTML
					</div>
HTML;
							} ?>
					<div class="form-group">
						<label for="encryption_key" class="col-sm-3 control-label">Encryption Key</label>
						<p id="encryption_key" class="form-control-static"><?php echo $encryption_key; ?></p>
					</div>
					<?php if(isset($PayWeb3->processRequest) || isset($PayWeb3->lastError)){
						/*
						 * We have received a response from PayWeb3
						 */

						/*
						 * TextArea for display example purposes only.
						 */
						?>
					<div class="form-group">
						<label for="request">Request Result</label><br>
						<textarea class="form-control" rows="3" cols="50" id="request"><?php
							if (!isset($PayWeb3->lastError)) {
								foreach($PayWeb3->processRequest as $key => $value){
									echo <<<HTML
{$key} = {$value}

HTML;
								}
							} else {
								/*
								 * handle the error response
								 */
								echo $PayWeb3->lastError;
							} ?>
						</textarea>
					</div>
					<?php
						if (!isset($PayWeb3->lastError)) {
							/*
							 * It is not an error, so continue
							 */

							/*
							 * Check that the checksum returned matches the checksum we generate
							 */
							$isValid = $PayWeb3->validateChecksum($PayWeb3->initiateResponse);

							if($isValid){
								/*
								 * If the checksums match loop through the returned fields and create the redirect from
								 */
								foreach($PayWeb3->processRequest as $key => $value){
									echo <<<HTML
					<input type="hidden" name="{$key}" value="{$value}" />
HTML;
								}
							} else {
								echo 'Checksums do not match';
							}
						}
						/*
						 * Submit form as/when needed
						 */
						?>
					<br>
					<div class="form-group">
						<div class=" col-sm-offset-4 col-sm-4">
							<input class="btn btn-success btn-block d-none" type="submit" name="btnSubmit" value="Redirect" />
						</div>
					</div>
					<?php } ?>
					<br>
				</form>
			</div>
		</div>

    <script type="text/javascript">
    $(function(){

        $('form').submit();
      });
    </script>
	</body>
</html>
