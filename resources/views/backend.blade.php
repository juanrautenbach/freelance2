<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

@php
  $user = \Auth::user();

  // echo $user->role;
@endphp
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Freelance Cape Town">
    <meta name="keywords" content="Freelance, Cape Town">
    <meta name="author" content="Julura Technologies">
    <title>Freelance Cape Town - Dashboard</title>
    {{--    FAVICON--}}
    {{--    <link rel="apple-touch-icon" href="/images/dashboard/ico/apple-icon-120.png">--}}
    {{--    <link rel="shortcut icon" type="image/x-icon" href="/images/dashboard/ico/favicon.ico">--}}

    @include('layouts.dashboard.head')

    
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
@if (Request::path() !== "messages")
<body class="horizontal-layout horizontal-menu material-horizontal-layout material-horizontal-nav material-layout 2-columns " data-open="hover" data-menu="horizontal-menu" data-col="2-columns" >
@endif

  @if (Request::path() == "messages" )
    <body class="horizontal-layout horizontal-menu material-horizontal-layout material-horizontal-nav material-layout content-left-sidebar chat-application " data-open="hover" data-menu="horizontal-menu" data-col="content-left-sidebar">
  @endif

  @if (Request::path() !== "users/profiles/register")
    @include('layouts.dashboard.header')
  @elseif (Request::path() !== "users/profiles/register/industries")
    @include('layouts.dashboard.header')
  @else

  @endif

  {{-- @if (Request::path() !== "users/profiles/register/industries")

  @endif --}}


<div class="app-content content" id="app" style="">

  @yield('content')
</div>


<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

@include('layouts.dashboard.footer')

</body>
{{-- @php
 echo Request::path();
@endphp --}}


</html>
