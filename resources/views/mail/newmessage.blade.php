<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
      <title>New Message on Freelance Cape Town</title>
      <style media="screen">
        .btn{
          padding: 10px 30px;
          background-color: royalblue;
          color: white;
          text-decoration: none;
        }
        a{
          color: white !important;
        }
        h5{
          color: #131313;
          font-weight: bold;
          font-family: sans-serif;
        }
        p{
          font-size: 16px;
          color: #131313;
          font-family: sans-serif;
        }
      </style>
  </head>
  <body>
    <h2>Hi {{$to}},</h2>

    <p>You have a new message from {{$from}}. Click the button below to view your message.</p>

    <a href="http://dev.julura.co.za:81/messages" class="btn btn-primary" >View Message</a>
    <br>
    <br>
  </body>
</html>
