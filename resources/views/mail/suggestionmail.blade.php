<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>Suggestion mail</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style media="screen">
        h4,
        p {
            font-family: sans-serif;
        }
    </style>
</head>

<body
  style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; background-color: #f8fafc; color: #74787e; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
    <style>
        @mediaonlyscreenand(max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @mediaonlyscreenand(max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>

    <tr>
        <td class="body" width="100%" cellpadding="0" cellspacing="0"
          style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; background-color: #ffffff; border-bottom: 1px solid #edeff2; border-top: 1px solid #edeff2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation"
              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; background-color: #ffffff; margin: 0 auto; padding: 0; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                <!-- Body content -->
                <tr>
                    <td class="content-cell" style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; padding: 35px;">
                        <h1
                          style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">
                            Hi Freelance Cape Town,</h4>

                            <p
                              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                This is an email from: {{ $email->name}} - ({{ $email->email}}),</p>

                            <p
                              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                My rating of the website is:</p>

                                @php
                                  if ($email->star_rating == 1) {
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  } elseif ($email->star_rating == 2) {
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  } elseif ($email->star_rating == 3) {
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  } elseif ($email->star_rating == 4) {
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  } elseif ($email->star_rating == 5) {
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt="" style="height:25px">';
                                  } elseif ($email->star_rating == 0) {
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  echo '<img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="" style="height:25px">';
                                  }
                                @endphp

<br>

                            <p
                              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                This is my message:</p>
                            <p
                              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                {{ $email->suggestion }}</p>

                            <p
                              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                Regards,</p>

                            <h4
                              style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                {{ $email->name}}</h4>

                                {{-- <img src="http://dev.julura.co.za:81/img/ratings/empty_star.png" alt="">
                                <img src="http://dev.julura.co.za:81/img/ratings/filled_star.png" alt=""> --}}
                    </td>
                </tr>
        </td>
    </tr>
</body>

</html>
