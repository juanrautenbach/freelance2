@extends('backend')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 tw-mx-3 tw-my-6 tw-p-5 bg-white card">
            <h4 class="mb-2 font-weight-bold">Create a subscription plan</h4>
            <form class="" action="/subscriptionplans" method="post">
                @csrf
                <div class="form-group">
                    <label for="subs_name">Name</label>
                    <input type="text" class="form-control" name="subs_name" value="" required>
                </div>
                <div class="form-group">
                    <label for="subs_desc">Description</label>
                    <textarea class="form-control" name="subs_desc" rows="6" value="" required></textarea>
                </div>
                <div class="form-group">
                    <label for="subs_duration">Duration</label>
                    <div class="input-group mb-3 w-md-50">
                        <input type="number" class="form-control" name="subs_duration" placeholder="Duration" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text border-0 font-weight-bold bg-white py-0 my-0" id="basic-addon2">months</span>
                        </div>
                    </div>
                    <!-- <select class="form-control" name="subs_duration">
                        <option value="">--Select--</option>
                        <option value="6 months">6 months</option>
                        <option value="12 months">1 year</option>
                        <option value="24 months">2 year</option>
                    </select> -->
                </div>
                <div class="form-group">
                    <label for="subs_type">Type</label>
                    <select class="form-control" name="subs_type">
                        <option value="">--Select--</option>
                        <option value="Once off">Once off</option>
                        <!-- <option value="monthly">Monthly</option> -->
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="subs_price">Price</label>
                    <input type="text" class="form-control" name="subs_price" value="" required>
                </div>
                <button type="submit" name="button" class="btn btn-red px-4">Save</button>
                <a href="/subscriptionplans" class="btn btn-link px-4">Cancel</a>
            </form>
        </div>

    </div>
</div>

@endsection