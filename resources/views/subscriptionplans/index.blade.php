@extends('backend')
@section('content')
<div class="container-fluid pt-3">
    <div class="row mb-2">
        <div class="col-12 my-2">
            <div class="card tw-p-3">

                <div class="row tw-my-2">
                    <div class="col-8 tw-px-8">
                        <h2>Subscription Plans</h2>
                    </div>
                    <div class="col text-right">
                        <a href="/subscriptionplans/create" class="tw-px-3 tw-py-2 tw-bg-red-910 tw-text-base tw-text-gray-100 tw-rounded hover:tw-bg-red-800 tw-outline-none tw-border-0" data-toggle="tooltip" data-placement="top" title="Create new subscription plan">New</a>
                    </div>
                </div>


                <div class="row">
                    <div class="col">
                        @if ($subsplans->count() > 0)
                        <table class="table table-responsive">
                            <tr>
                                <th class="tw-font-bold tw-text-sm">Plan Name</th>
                                <th class="tw-font-bold tw-text-sm">Description</th>
                                <th class="tw-font-bold tw-text-sm">Duration</th>
                                <th class="tw-font-bold tw-text-sm">Type</th>
                                <th class="tw-font-bold tw-text-sm">Price</th>
                                <th class="tw-font-bold tw-text-sm">Status</th>
                                <th class="tw-font-bold tw-text-sm">Created</th>
                                <th class="tw-font-bold tw-text-sm">Actions</th>
                            </tr>
                            @foreach ($subsplans as $subsplan)
                            <tr>
                                <td class="nowrap">{{$subsplan->name}}</td>
                                <td class="nowrap">{{$subsplan->description}}</td>
                                <td class="nowrap">{{$subsplan->duration}}</td>
                                <td class="nowrap">{{$subsplan->type}}</td>
                                <td class="nowrap">R{{$subsplan->price}}</td>
                                <td class="nowrap">{{$subsplan->status}}</td>
                                <td class="nowrap">{{ \Carbon\Carbon::parse($subsplan->created_at)->diffForHumans()}}</td>
                                <td class="nowrap"> <a href="/edit/subscriptionplans/{{$subsplan->id}}" class="tw-px-2 tw-py-2 tw-bg-red-910 tw-text-sm tw-text-gray-100 tw-rounded hover:tw-bg-red-800 tw-outline-none tw-border-0" title="Edit" data-toggle="tooltip" data-placement="top"><i class="material-icons">edit</i></a>
                                    <form action="/subscriptionplans/{{ $subsplan->id}}" method="post" class="d-inline-block">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="tw-px-2 tw-py-2 tw-bg-red-910 tw-text-sm tw-text-gray-100 tw-rounded hover:tw-bg-red-800 tw-outline-none tw-border-0" data-toggle="tooltip" data-placement="top" title="Delete"><span class="material-icons">
                                            delete_forever
                                            </span></button>
                                    </form>
                                    <form action="/subscriptionplans/activate/{{ $subsplan->id}}" method="post" class="d-inline-block">
                                        @csrf
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="tw-px-2 tw-py-2 tw-bg-red-910 tw-text-sm tw-text-gray-100 tw-rounded hover:tw-bg-red-800 tw-outline-none tw-border-0" data-toggle="tooltip" data-placement="top" title="Activate"><span class="material-icons">
                                            check_circle
                                            </span></button>
                                    </form>
                                    <form action="/subscriptionplans/deactivate/{{ $subsplan->id}}" method="post" class="d-inline-block">
                                        @csrf
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="tw-px-2 tw-py-2 tw-bg-red-910 tw-text-sm tw-text-gray-100 tw-rounded hover:tw-bg-red-800 tw-outline-none tw-border-0" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="material-icons">
                                                clear
                                            </span></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @else
                        <h3>No info</h3>
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection