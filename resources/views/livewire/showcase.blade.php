<div>
    <div class="row mt-4">
        <div class="col d-flex flex-wrap-reverse">
            @if (\Auth::user()->showcase->count() != 0 )

            @if (\Auth::user()->showcase->count() == 1 )
            @foreach (\Auth::user()->showcase as $showcase)
            <div class="p-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="{{$showcase->photo}}" onclick="editPhoto({{ $showcase->id }})" class="img-fluid hover" alt="" style="">
                    <small class="text-dark font-weight-bold mt-2">Description:</small>
                    <p class="font-medium-3 text-bold-500 text-dark">{{ $showcase->description }}</p>
                </div>
            </div>
            @endforeach
            <div class="p-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="/img/blank.png" onclick="addPhoto()" class="w-100 hover" alt="">
                    <p class=""></p>
                </div>
            </div>
            <div class="p-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="/img/blank.png" onclick="addPhoto()" class="w-100 hover" alt="">
                    <p class=""></p>
                </div>
            </div>
            @elseif (\Auth::user()->showcase->count() == 2)
            @foreach (\Auth::user()->showcase as $showcase)
            <div class="p-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="{{$showcase->photo}}" onclick="editPhoto({{ $showcase->id }})" class="img-fluid hover" alt="" style="">
                    <small class="text-dark font-weight-bold mt-2">Description:</small>
                    <p class="font-medium-3 text-bold-500 text-dark">{{ $showcase->description }}</p>
                </div>
            </div>
            @endforeach
            <div class="p-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="/img/blank.png" onclick="addPhoto()" class="w-100 hover" alt="">
                    <p class="p-3"></p>
                </div>
            </div>
            @elseif (\Auth::user()->showcase->count() > 2)
            @foreach (\Auth::user()->showcase as $showcase)
            <div class="p-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="{{$showcase->photo}}" onclick="editPhoto({{ $showcase->id }})" class="img-fluid hover" alt="" style="">
                    <small class="text-dark font-weight-bold mt-2">Description:</small>
                    <p class="font-medium-3 text-bold-500 text-dark">{{ $showcase->description }}</p>
                </div>
            </div>
            @endforeach

            @endif

            @else
            <div class="p-2  mx-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="/img/blank.png" onclick="addPhoto()" class="w-100 hover" alt="">
                    <p class="p-3"></p>
                </div>
            </div>
            <div class="p-2  mx-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="/img/blank.png" onclick="addPhoto()" class="w-100 hover" alt="">
                    <p class="p-3"></p>
                </div>
            </div>
            <div class="p-2  mx-2 col">
                <div class="card border-0 shadow-sm h-100">
                    <img src="/img/blank.png" onclick="addPhoto()" class="w-100 hover" alt="">
                    <p class="p-3"></p>
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="add_showcase" id="add_showcase" style="display: none">
        <div class="card col-12 col-md-8 mx-auto p-1 tw-z-50 tw-mt-20 md:tw-mt-8">
            <div class="card-body pt-0 pb-4 px-2">
                <span name="button" class="btn btn-link btn-lg text-lightgrey float-right pr-0 pt-2 blue-hover" id="cancelshowcase"><i class="fas fa-times font-20"></i></span>
                <h3 class="mb-sm-2 mt-sm-2 font-medium-3 text-bold-700">Add showcase</h3>
                <form class="" action="/user/showcase/add" method="post" enctype="multipart/form-data" id="form2">
                    <input type="hidden" name="showcase_form" value="showcase">
                    @csrf
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="showcase_title" class="mx-0 font-weight-bold">Add a Showcase image <span class="d-none"> or video</span></label>
                            </div>

                            <div class="mb-3">

                                <div class="row">
                                    <div class="drop-photo col-md-8 px-0 ml-1 col-8">
                                        <div class="slim" data-instant-edit="true" data-push="true" data-size="640,640" data-filter-sharpen="15" ata-default-input-name="photo" data-ratio="1:1">
                                            <input type="file" name="slim[]" accept="image/jpeg, image/png" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group d-none">
                                <label for="showcase_photo" class="font-weight-bold">Photo:</label>
                                <input type="file" name="showcase_photo" id="showcasePhoto">
                            </div>
                        </div>
                        <div class="col-md pr-3">
                            <div class="form-group mt-4 d-none">
                                <label for="showcase_title" class="font-weight-bold p-0 mb-0">Title of project</label>
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                            </div>
                            <div class="form-group">
                                <label for="showcase_desc" class="font-weight-bold p-0 mb-0 mx-0">Project description</label>
                                <textarea name="showcase_desc" placeholder="Enter a description" rows="5"></textarea>
                                {{-- <textarea name="showcase_desc" placeholder="Enter a description" rows="14"></textarea> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="text-center">
                                <input type="hidden" name="edit_photo_id">
                                <button type="submit" class="btn btn-red btn-lg px-md-5 py-md-2 ls-25 w-md-50 mt-md-4 py-1" name="form2" id="submit_2">DONE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('components.profile_photo')

    <script>
        const user_id = "{{ $user->id }}";
    </script>
    <script src="/js/freelancer_profile.js"></script>

</div>
