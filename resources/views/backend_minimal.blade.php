<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

@php
  $user = \Auth::user();

  // echo $user->role;
@endphp
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Freelance Cape Town - Dashboard</title>
    {{--    FAVICON--}}
    {{--    <link rel="apple-touch-icon" href="/images/dashboard/ico/apple-icon-120.png">--}}
    {{--    <link rel="shortcut icon" type="image/x-icon" href="/images/dashboard/ico/favicon.ico">--}}

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

@include('layouts.dashboard.head')

    <link rel="stylesheet" type="text/css" href="/fonts/material-icons/material-icons.css">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/components.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-colors.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/menu/material-horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="/fonts/simple-line-icons/style.css">

    <link rel="stylesheet" type="text/css" href="/css/dashboard/pages/app-chat.css">

    {{--    COLOURS--}}
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-palette-gradient.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/style.css">
    <!-- END: Custom CSS-->

    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
      <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.2/cjs/popper.js" integrity="sha256-J5vU4hM8k+L5cqJqTVHOmvmulP8zNaCd+SH1VEyG01M=" crossorigin="anonymous"></script> --}}

      <script src="/js/filter.js" charset="utf-8"></script>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
@if (Request::path() !== "messages")
<body class="horizontal-layout horizontal-menu material-horizontal-layout material-horizontal-nav material-layout 2-columns " data-open="hover" data-menu="horizontal-menu" data-col="2-columns" >
@endif

  @if (Request::path() == "messages" )
    <body class="horizontal-layout horizontal-menu material-horizontal-layout material-horizontal-nav material-layout content-left-sidebar chat-application " data-open="hover" data-menu="horizontal-menu" data-col="content-left-sidebar">
  @endif

@include('layouts.dashboard.header_mini')

<div class="app-content content" style="">

  @yield('content')
</div>


<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

@include('layouts.dashboard.footer')

</body>
{{-- @php
 echo Request::path();
@endphp --}}


</html>
