<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Freelance Cape Town</title>
    @include('templates.dashboard.head')
    @livewireStyles
  </head>
  <body style="">
    @include('templates.dashboard.header')
    <section id="main" style="min-height: 50vh">
        @include('includes.messages')
        @yield('content')
    </section>
    @include('templates.dashboard.footer')
    @livewireScripts
  </body>

</html>
