@extends('minimal')

@section('content')
<div class="container login-screen">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card shadow rounded border-0 tw-mt-4 md:tw-mt-0">
                <div class="card-body">

                    <div class="row">
                        <div class="col">
                          <div class="row">
                            <div class="col mb-0">
                              <div class="float-right">
                                  <a href="/" class="text-muted "><i class="fas fa-times"></i></a>
                              </div>
                              <div class="mx-md-4 my-md-4 m-2">

                                <div class="mb-4">
                                  <h3 class="font-weight-bold text-center text-uppercase mb-2 mt-4">Freelance Cape Town</h3>
                                  <p class="text-muted text-center mb-0">Welcome back, let's get you signed in.</p>
                                </div>

                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <input id="email" type="email" class="mb-3 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Use your email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input id="password" type="password" class="mb-3 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password here!">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="d-block">
                                      <input class="mb-3 mr-1" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                      <label class="form-check-label" for="remember">
                                          {{ __('Remember Me') }}
                                      </label>
                                    </div>

                                    <button type="submit" class="btn btn-red btn-lg my-3 my-sm-1 d-inline-block text-white text-uppercase mr-2">
                                        {{ __('Login') }}
                                    </button>
                                    @if (Route::has('password.request'))
                                    <a class="text-muted d-block d-md-inline-block mb-3 mb-md-1" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                    <span class="d-block text-right">Don't have a FCT account yet?
                                    <a href="/register" class="text-secondary d-block font-weight-bold">Sign up now.</a></span>
                                </form>
                                </div>
                            </div>
                          </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
