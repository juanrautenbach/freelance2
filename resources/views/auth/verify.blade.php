@extends('minimal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 p-0 verify-form-container">
            <div class="verify-form">
                <h2 class="font-weight-bold mb-4">We've sent you an email verification link</h2>
                @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
                @endif

                <div class="row">
                    <ul>
                        <li class="">You should receive this verification mail within a few second.</li>
                        <li class="">Check your inbox or if not there, check your spam and junk folder.</li>
                    </ul>
                </div>

                <p class="mb-2">
                    <small>(If you havent received the verification mail within 60 seconds, press "resend verification email" below.)</small>
                </p>

                <a href="{{ route('verification.resend') }}" class="btn btn-red btn-lg px-5 py-3 mt-4 hover:tw-bg-gray-800">{{ __('Resend verification email') }}</a>
            </div>
        </div>
        <div class="col-md-6 d-none d-md-block p-0 verify-form-right ">

        </div>
    </div>
</div>
@endsection


{{-- <p class="pr-5"> --}}
{{-- {{ __('Before proceeding, please check your email for a verification link.') }} --}}
{{-- Before proceeding, <strong>please check your email for a verification link.</strong><br> --}}
{{-- {{ __('If you did not receive the email within 60 seconds, click below.') }} --}}
{{-- </p> --}}




{{-- <div class="container verify-screen">
    <div class="row justify-content-center text-center">
        <div class="col-lg-8 col-md-10">
            <div class="card">
                <div class="card-body shadow-sm">
                  <h2 class="font-weight-bold text-uppercase text-center">Verify your email address</h2>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
</div>
@endif
<p class="col-sm-10 offset-sm-1">
    {{ __('Before proceeding, please check your email for a verification link.') }}
    {{ __('If you did not receive the email, click below.') }}
</p>
<a href="{{ route('verification.resend') }}" class="btn btn-red btn-lg">{{ __('Resend verification email') }}</a>
</div>
</div>
</div>
</div>
</div> --}}
