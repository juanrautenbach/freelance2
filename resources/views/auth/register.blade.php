@extends('minimal')
@section('content')
<div class="container register-screen" style="margin-top: 50px;" id="register-front-top">
    <div class="row justify-content-center">
        <div class="text-center mb-5">
            <h2 class="font-weight-bold px-3 px-md-0">Register at Freelance Cape Town</h2>
            <p>Already have an account? <a href="/login" class="text-red font-weight-bold">Log In</a> </p>
        </div>
    </div>
</div>
<div class="container-fluid" style="" id="register-front-main">
    <div class="row">
        <div class="col-md-6 p-0">
            <div class="register-finder">

                <div class="finder-hover" onclick="nextRegister('Finder')">
                    <div class="register-text">
                        <p>I want to</p>
                        <div class="d-flex">
                            <h4 class="d-inline-block">Hire for a project </h4><img class="d-inline-block" src="/img/register/arrow.png" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6 p-0">
            <div class="register-freelancer">
                <div class="freelancer-hover" onclick="nextRegister('Freelancer')">
                    <div class="register-text">
                        <p>I want to</p>
                        <div class="d-flex">
                            <h4 class="d-inline-block">Work as a freelancer </h4><img class="d-inline-block" src="/img/register/arrow.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-4 p-0">
            <div class="register-co-working">
                <div class="co-working-hover" onclick="nextRegister('Co-Working')">
                    <div class="register-text">
                        <p>I want to find a </p>
                        <div class="d-flex">
                            <h4 class="d-inline-block">Co-Working Space </h4><img class="d-inline-block" src="/img/register/arrow.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<div class="container-fluid" id="nextRegister" style="display:none;">
  <div class="row">
    <div class="col-md-6 register-form-container p-0">
      <div class="register-form">
        <h2 class="font-weight-bold mb-3">Register at Freelance Cape Town</h2>
        <p>You're minutes away from joining one of the most<br> prolific platforms in Cape Town. </p>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <input id="name" type="text" class=" @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter your name and surname">
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input id="email" type="email" class="mb-1 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter your email">
              <div id="email_error" style="display:none">
                <p class="text-danger ml-4 mb-1">Email already in use.</p>
              </div>
            @error('email')
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>


            {{-- <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span> --}}
            @enderror
            <input id="password" type="password" class=" @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter your password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input id="password-confirm" type="password" class="" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm your password">

            <input type="hidden" name="role" value="{{ old('role') }}">

            <button type="submit" class="btn btn-red btn-lg px-5 mt-3 mb-5">
                {{ __('Register') }}
            </button>
            <p>Already have an account? <a href="/login" class="font-weight-bold text-red">Log in.</a> </p>
      </div>

    </div>
    <div class="col-md-6 p-0 d-none d-md-block">
      <div class="register-form-right">
      </div>

    </div>
  </div>
</div>

{{-- <div class="card" style="display:block; margin-bottom: 50px" id="nextRegister">
    <div class="card-body">
        <h2 class="font-weight-bold text-uppercase text-center">Register at Freelance Cape Town</h2>
        <p class="text-center mb-4"> <small>You're minutes away from joining one of the most prolific platforms in Cape Town</small> </p>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="container px-5">
                <div class="form-group row">
                    <label for="name" class="">{{ __('Name') }}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="email" class="">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="password" class="">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
                <div class="form-group row">

                    <input type="text" name="role" value="">

                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-blue btn-lg" onclick="submitClick()">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div> --}}
@endsection
