<section class="price-table-main py-4 text-center bg-white">
  <div class="row">
    <div class="col">
      <h2 >PRICING AND REGISTRATION</h2>
    </div>
  </div>
  <div class="container">
  <div class="row price-table-sections pt-2 pb-3">
    <div class="col-md price-table-secondary mx-3 mb-3" onclick="showRegister()">
      <div class="row price-table-head py-3">
        <div class="col">
          <h2 class="p-0 m-0">FREELANCER</h2>
        </div>
      </div>
      <div class="row price-table-body py-4">
        <div class="col">
          <h2>6 MONTHS</h2>
          <h2>R299</h2>
          <p>once off</p>
        </div>
      </div>
      <div class="row">
        <div class="col price-table-footer">
          <span class="top-btn px-4 py-3">Register today</span>
        </div>
      </div>
    </div>
    <div class="col-md price-table-primary mx-3 mb-3" onclick="showRegister()">
      <div class="row price-table-head py-3">
        <div class="col">
          <h2 class="p-0 m-0">FREELANCER</h2>
        </div>
      </div>
      <div class="row price-table-body py-4">
        <div class="col">
          <h2>12 MONTHS</h2>
          <h2>R499</h2>
          <p>once off</p>
        </div>
      </div>
      <div class="row">
        <div class="col price-table-footer">
          <span class="top-btn px-4 py-3">Register today</span>
        </div>
      </div>
    </div>
    <div class="col-md price-table-secondary mx-3 mb-3" onclick="showRegister()">
      <div class="row price-table-head py-3">
        <div class="col">
          <h2 class="p-0 m-0">FREELANCER FINDER</h2>
        </div>
      </div>
      <div class="row price-table-body py-4">
        <div class="col">
          <h2>FREE</h2>
          <p>Make contact with Freelancers at no cost</p>
        </div>
      </div>
      <div class="row">
        <div class="col price-table-footer">
          <span class="top-btn px-4 py-3">Register today</span>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<script type="text/javascript">
  function goToRegister($i){
    window.location = "/make/"+$i;
  }
</script>
