<div class="col-12 col-md-4">
  <div class="card tw-mt-8 md:tw-mt-0">
    <div class="card-header">
      <h3 class="d-inline">Job Posts</h3> 
      @if (count($user->jobpost) > 0)
      <small>(Clicks)</small>
      <div class="heading-elements">
          <ul class="list-inline mb-0">
              <li><a href="/posts" class="btn btn-red">Show all</a></li>
          </ul>
      </div>
      @endif
      
  </div>
        @if (count($user->jobpost) == 0)
        <div class="card-content collapse show mt-n4" style="height: 200px">
          <div class="card-body p-0">
              <div class="row" style="height: 200px">
                <div class="col m-auto text-center">
                  <img src="/img/job-posts.svg" alt="" class="my-auto" style="height: 100px">
                </div>
                <div class="col m-auto text-center">
                  <a href="/posts/create" class="btn btn-red waves-effect waves-light">Create post</a>
                </div>
              </div>
          </div>
      </div>
        @else
        
      <div class="card-content collapse show">
          <div class="card-body p-0">
              <div class="table-responsive">
                  <table class="table mb-0">
                      <thead>
                        <tr class="mb-0">
                          <td class="p-0"></td>
                          <td class="p-0 text-center">Clicks</td>
                          <td class="p-0 text-center">Status</td>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($user->jobpost as $post)
                        <tr>
                          <th scope="row" class="border-top-0"><a class="text-dark font-weight-bold" href="/post/{{ $post->id }}"> {{ str_limit($post->title, $limit = 30, $end = '...') }}</a></th>
                          <td class="border-top-0 text-center">{{ $post->post_clicks }}</td>
                          <td class="border-top-0 text-center">{{ $post->status }}</td>
                      </tr>
                        @endforeach
                          
                          
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
        @endif
        
  </div>
</div>