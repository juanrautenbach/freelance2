<div class="add_showcase" id="add_profile" style="display: none" style="height: 300px">
        <div class="card col-12 col-md-4 mx-auto p-1">
            <div class="card-body pt-0 pb-4 px-2">
                <span name="button" class="btn btn-link btn-lg text-lightgrey float-right pr-0 pt-2 blue-hover" id="cancelprofilephoto"><i class="fas fa-times font-20"></i></span>
                <h3 class="mb-sm-2 mt-sm-2 font-medium-3 text-bold-700">Profile photo</h3>
                <form class="" action="/user/profile_photo/add" method="post" enctype="multipart/form-data" id="form2">
                    <input type="hidden" name="showcase_form" value="showcase">
                    @csrf
                    <div class="row">
                        <div class="col">
                           
                            <div class="mb-1">

                                <div class="row">
                                    <div class="drop-photo col-md-8 px-0 ml-1 col-8 mx-auto">
                                        <div class="slim" data-instant-edit="true" data-push="true" data-size="640,640" data-filter-sharpen="15" ata-default-input-name="photo" data-ratio="1:1">
                                            <input type="file" name="slim[]" accept="image/jpeg, image/png" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group d-none">
                                <label for="showcase_photo" class="font-weight-bold">Photo:</label>
                                <input type="file" name="showcase_photo" id="showcasePhoto">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="text-center">
                                <input type="hidden" name="edit_photo_id">
                                <button type="submit" class="btn btn-red btn-lg px-md-5 py-md-2 ls-25 w-md-50 mt-md-2 py-1" name="form2" id="submit_2">DONE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>