{{-- @if (\Request::('make/*'))
  <h1>Make</h1>
{{-- @elseif (\Request::('verify'))
  <h1>Verify</h1>
@elseif (\Request::('payment')) --}}
{{-- @endif  --}}
{{-- {{dd(request()->route())}} --}}

@if (Request::is('make/*'))
  <div class="breadcrumbs-section">
    <div class="h-100">
      <div class="row align-items-center h-100 py-4">
        <div class="col-sm-12 text-center mx-auto">
          <a href="/make/freelancer">
          <div class="bc-button bc-active">Step 1</div>
          </a>
            <i class="fas fa-angle-right bc-divider "></i>
          <div class="bc-button">Step 2</div>
          <i class="fas fa-angle-right bc-divider"></i>
          <div class="bc-button">Step 3</div>
        </div>
      </div>
    </div>
  </div>
@elseif (Request::is('verify/*'))

<div class="breadcrumbs-section">
  <div class="h-100">
    <div class="row align-items-center h-100 py-4">
      <div class="col-sm-12 text-center mx-auto">
        <a href="/make/freelancer">
        <div class="bc-button bc-active">Step 1</div>
        </a>
          <i class="fas fa-angle-right bc-divider bc-div-active"></i>
        <div class="bc-button bc-active">Step 2</div>
        <i class="fas fa-angle-right bc-divider"></i>
        <div class="bc-button">Step 3</div>
      </div>
    </div>
  </div>
</div>

@elseif (Request::is('payment'))
  <div class="breadcrumbs-section">
    <div class="h-100">
      <div class="row align-items-center h-100 py-4">
        <div class="col-sm-12 text-center mx-auto">
          <a href="/make/freelancer">
          <div class="bc-button bc-active">Step 1</div>
          </a>
            <i class="fas fa-angle-right bc-divider bc-div-active"></i>
          <div class="bc-button bc-active">Step 2</div>
          <i class="fas fa-angle-right bc-divider bc-div-active"></i>
          <div class="bc-button bc-active">Step 3</div>
        </div>
      </div>
    </div>
  </div>
@endif
