<section class="quicksearch text-center py-4">
  <div class="row pt-3">
    <div class="col-sm-10 offset-sm-1">
      <h2 class="mb-0"> <strong>FIND TALENT QUICK</strong></h2>
      <p class="mb-1">Choose an industry you're interested in to get started</p>
    </div>
  </div>
  <div class="row pb-3">
    <form class="form-inline mx-auto col-12">
    <div class="row mx-auto">
      <div class="col">
            <select class="custom-select mx-3 w-90" name="">
              <option value="">Please Select an Industry</option>
              <option value="">Accounting</option>
              <option value="">Animation</option>
            </select>
      </div>
      <div class="col">
        <button class="btn btn-quicksearch" type="submit">Search <i class="fas fa-search"></i> </button>
      </div>
    </div>
        </form>
    </div>

</section>
