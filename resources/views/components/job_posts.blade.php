@extends('dashboard')
@section('content')
<style media="screen">
    .filter {
        display: none;
    }
</style>
<link rel="stylesheet" href="/js/vendor/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.css">
<div class="container-fluid my-5">
    <div class="row">

        <div class="col-lg-3 col-md-3">
            <div class="accordion" id="accordionExample">
                <div class="card border-0">
                    <div class="card-header border-0 bg-white text-dark">
                        <h4 class="font-weight-bold d-inline">Filter by</h4>

                        <span class="btn btn-red btn-sm float-right" id="reset-btn">Reset</span>
                    </div>
                </div>
                <div class="card border-0" id="industryCard">
                    <div class="card-header bg-white" id="headingOne">
                        <p class="mb-0">
                            <a class="text-dark font-weight-bold d-block" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Industry
                                <span class="float-right" id="industryMinus"><i class="fas fa-minus"></i></span>
                                <span class="float-right d-none" id="industryPlus"><i class="fas fa-plus"></i></span>
                            </a>
                        </p>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="pointer-cursor" onclick="resetFilter()">
                                <p class="font-weight-bold  d-inline">All Industries</p>
                                {{-- <span class="float-right"><i class="fas fa-check-square text-blue"></i></span> --}}
                            </div>
                            <ul class="mt-1 pl-0">
                                @if (!empty($post_industries))
                                @foreach ($post_industries as $industry)
                                <li class="mb-2" style="list-style-type: none"><a class="pointer-cursor text-dark" onclick="filter('category', 'filter-industry', '{{ $industry }}'); this.onclick=null;">{{ $industry }}</a></li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card border-0" id="subIndustryCard">
                    <div class="card-header bg-white" id="headingTwo">
                        <p class="mb-0">
                            <a class="d-block collapsed text-dark font-weight-bold" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Sub Industry
                                <span class="float-right" id="subIndustryPlus"><i class="fas fa-plus"></i></span>
                                <span class="float-right d-none" id="subIndustryMinus"><i class="fas fa-minus"></i></span>
                            </a>
                        </p>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p class="font-weight-bold pointer-cursor" onclick="resetFilter()">All Sub Industries</p>
                            <div class="mt-3 pl-0">
                                @if (!empty($post_subindustries))
                                @foreach ($post_subindustries as $subindustry)
                                <li class="mb-2" style="list-style-type: none"><a class="pointer-cursor text-dark" onclick="filter('category', 'sub-industry-filter', '{{ $subindustry }}'); this.onclick=null;">{{ $subindustry }}</a></li>
                                @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-0" id="locationCard">
                    <div class="card-header bg-white" id="headingThree">
                        <p class="mb-0">
                            <a class="d-block collapsed text-dark font-weight-bold" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Find by location
                                <span class="float-right" id="locationPlus"><i class="fas fa-plus"></i></span>
                                <span class="float-right d-none" id="locationMinus"><i class="fas fa-minus"></i></span>
                            </a>
                        </p>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            @if (!empty($post_cities))
                            @foreach ($post_cities as $city)
                            <li class="mb-2" style="list-style-type: none"><a class="pointer-cursor text-dark text-capitalize" onclick="filter('category', 'tag-filter', '{{ $city }}')">{{ $city }}</a></li>
                            @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 mx-0 pr-md-5">
            <div class="card border-0 shadow-sm px-md-4 px-2 py-4">
                <div class="row">
                    <div class="col">
                        <h3 class="d-md-inline mb-1 mb-md-3 font-weight-bold">Job Posts</h3>
                        <!-- <ul class="list-inline mt-3">

                        </ul> -->
                        <!-- <span class="float-md-right d-block d-md-inline text-muted mt-2">Total results: <span id="count"></span> </span> -->
                        <div class="row my-1">
                            <div class="col-12 col-md-10 px-0">
                                <input type="text" name="search" class="form-control mt-1 mb-2 mb-md-0 main-panel-search" placeholder="Search for anything, skill, industry, name" onkeyup="filter('freetext','freelancer-cards','main-panel-search')">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @if (!empty($posts))
                                @foreach ($posts as $post)
                            {{-- <form class="" action="/post/message/view" method="post">
                                @csrf
                                <input type="hidden" name="post_id" value="{{ $post->id }}">
                                <button type="submit" class="btn btn-red">View more details</button>
                            </form> --}}
                                <hr class="my-0">
                                <a href="/post/click/{{$post->id}}" class="freelancer-cards text-dark" style="text-decoration: none">
                                    <div class="row freelancer-row py-1">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="row">

                                                        <div class="col">
                                                            <div class="text-capitalize d-inline-block">
                                                                <h5 class="font-weight-bold">{{$post->title}}</h5>
                                                            </div>
                                                            <div class="d-inline-block float-right text-muted">
                                                                <p><span class="font-weight-bold text-underline mb-1">Deadline:</span>
                                                                    {{ $post->deadline }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <p class="mb-2"><span class="font-weight-bold">Location:</span> {{ $post->location }}</p>
                                                            <p class="mb-2"><span class="font-weight-bold">Industry:</span>
                                                                @foreach ($post->industries as $industry)
                                                                {{ $industry->name }}</p>
                                                            @endforeach
                                                            <p class="mb-1"><span class="font-weight-bold">Sub Industry:</span></p>
                                                            @foreach ($post->sub_industries as $sub_industry)
                                                            <span class="mr-2 mb-1 d-inline-block">{{ $sub_industry->name }}</span>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="filter filter-industry">
                                                                @foreach ($post->industries as $industry)
                                                                {{ $industry->name }}</p>
                                                                @endforeach
                                                            </div>
                                                            <div class="filter sub-industry-filter">
                                                                @foreach ($post->sub_industries as $sub_industry)
                                                                {{ $sub_industry->name }}
                                                                @endforeach
                                                            </div>

                                                            <div class="filter tag-filter">
                                                                {{ $post->location }}
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </a>
                                {{-- @php
                                    $count++;
                                    @endphp--}}
                                {{-- @endif --}}
                                @endforeach
                                @else

                                @endif


                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col text-center mx-auto">
                    {{-- <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">1</span>
              <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">2</span>
              <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">3</span>
              <span class="text-white bg-blue rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">4</span> --}}
                    {{-- {{$freelancers->links()}} --}}
                </div>
            </div>
        </div>

    </div>

</div>
<script type="text/javascript">
    // function initialisePage() {
    //     postsCount({{ $posts_count }});
    // }
    // postsCount({{ $posts_count }});
    //     function postsCount(val) {
    //         $("#count").text(val);
    //     };



    $('#industryCard').click(function() {
        $('#collapseOne').addClass('show');
        $('#collapseTwo').removeClass('show');
        $('#collapseThree').removeClass('show');

        $('#industryMinus').removeClass('d-none');
        $('#industryPlus').addClass('d-none');

        $('#subIndustryPlus').removeClass('d-none');
        $('#subIndustryMinus').addClass('d-none');

        $('#locationMinus').addClass('d-none');
        $('#locationPlus').removeClass('d-none');
    });

    $('#subIndustryCard').click(function() {
        $('#collapseOne').removeClass('show');
        $('#collapseTwo').addClass('show');
        $('#collapseThree').removeClass('show');

        $('#industryMinus').addClass('d-none');
        $('#industryPlus').removeClass('d-none');

        $('#subIndustryPlus').addClass('d-none');
        $('#subIndustryMinus').removeClass('d-none');

        $('#locationMinus').addClass('d-none');
        $('#locationPlus').removeClass('d-none');
    });

    $('#locationCard').click(function() {
        $('#collapseOne').removeClass('show');
        $('#collapseTwo').removeClass('show');
        $('#collapseThree').addClass('show');

        $('#industryMinus').addClass('d-none');
        $('#industryPlus').removeClass('d-none');

        $('#subIndustryPlus').removeClass('d-none');
        $('#subIndustryMinus').addClass('d-none');

        $('#locationMinus').removeClass('d-none');
        $('#locationPlus').addClass('d-none');
    });
</script>

@endsection