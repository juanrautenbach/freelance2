<section id="testimonial-slider" class="pb-4 pt-4 text-center">
<div class="container">


  <div class="testimonial-slider mt-3 row">
  <div>

  </div>
  <div >

  </div>
  <div>

  </div>
  <div>

  </div>
  <div>


    {{-- <a href="#">View Profile</a> --}}
  </div>
</div>
</div>
<div class="slider-nav row border">
  <div style="">
    <div class="slider-nav-image">
      <img src="/img/testimonials/deborah.jpg" alt="">
      <p>Deborah Butler (The Startum Project),<br> <span class="font-weight-normal">Web Developer</span> </p>
    </div>
    <div class="slider-nav-image-overlay"></div>
  </div>
  <div style="">
    <div class="slider-nav-image">
      <img src="/img/testimonials/renier.jpg" alt="">
      <p>Renier Lombard (SocialAnimal),<br> <span class="font-weight-normal">Social Media Expert</span> </p>
    </div>
    <div class="slider-nav-image-overlay"></div>
  </div>
  <div style="">
    <div class="slider-nav-image">
      <img src="/img/testimonials/candice.jpg" alt="">
      <p>Candice,<br> <span class="font-weight-normal">Marketing & Media</span> </p>
    </div>
    <div class="slider-nav-image-overlay"></div>
  </div>
  <div style="">
    <div class="slider-nav-image">
      <img src="/img/testimonials/simon.png" alt="">
      <p>Simon Dowdles,<br> <span class="font-weight-normal">Web Developer</span></p>
    </div>
    <div class="slider-nav-image-overlay"></div>
  </div>
  <div style="">
    <div class="slider-nav-image">
      <img src="/img/testimonials/lida.jpg" alt="">
      <p>Lidia van Wyk,<br> <span class="font-weight-normal">Writing</span></p>
    </div>
    <div class="slider-nav-image-overlay"></div>
  </div>
</div>
<div class="slider-arrows">
    <button class="leftArrow"> < </button>
    <button class="rightArrow"> > </button>
</div>
</section>
