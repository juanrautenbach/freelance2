<div class="modal fade bd-example-modal-lg" id="subsModal" tabindex="-1" role="dialog" aria-labelledby="subsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content pb-4" id="subsFirst">
            <div class="modal-header">
                <h5 class="modal-title" id="subsModalLabel">Subscription Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4" id="profilestep1">
                <br>
                <h4 class="mb-3 text-center">Please select a subscription plan from the list below.</h4><br>
                @foreach ($subs_plans as $subs_plan)
                  <div class="container">
                    <div class="subs-plan-btn" onclick="subsPlan('{{$subs_plan->description}}','{{$subs_plan->price}}')">
                      <div class="row">
                        <div class="col-9 ml-3 align-middle">
                          <p>{{ $subs_plan->description}}</p>
                        </div>
                        <div class="col-2 text-right mr-3 align-middle">
                          <strong>R{{ $subs_plan->price}}</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
            </div>
            {{-- <div class="modal-footer d-none" id="profilebuttons">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-red">Save changes</button>
            </div> --}}
            </form>
        </div>

        <div class="modal-content pb-4" id="subsNext" style="display:none">
          <form class="" action="/confirmpayment" method="post">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="subsModalLabel">Confirm Subscription</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4" id="profilestep1">
                <br>
                <h4 class="mb-3 text-center">Please confirm your subscription plan.</h4><br>
                <div class="row mb-3">
                  <div class="col-8 offset-1" >
                    <p id="subsText"></p>
                  </div>
                  <div class="col-2 text-right">
                    <p id="subsPrice" class="font-weight-bold"></p>
                  </div>
                  <input type="hidden" name="cart_total" value="">
                  <input type="hidden" name="user_id" value="FCT{{$user->id}}">
                  @php
                    $name = explode(" ", $user->name);
                    $firstname = $name[0];
                    if (count($name) == 1) {
                      $lastname = '';
                    }elseif (count($name) == 2) {
                      $lastname = $name[1];
                    }elseif (count($name) == 3) {
                      $lastname = $name[2];
                    }
                  @endphp
                  <input type="hidden" name="user_firstname" value="{{$firstname}}"> <br>
                  <input type="hidden" name="user_lastname" value="{{$lastname}}"><br>
                  <input type="hidden" name="user_email" value="{{$user->email}}">
                  <input type="hidden" name="item_title" value="">
                  <input type="hidden" name="item_description" value="">
                </div>
                <div class="mx-auto text-center">
                  <button type="submit" class="btn btn-red mx-auto" name="button">Confirm Order</button>
                </div>

            </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function subsPlan(description, price){

      $('#subsNext').show();
      $('#subsFirst').hide();
      $('#subsText').text(description);
      $('#subsPrice').text('R'+price);
      $("input[name=cart_total]").val(price);
      $("input[name=item_title]").val(description);
      $("input[name=item_description]").val(description);
      // $('#profilebuttons').show();
    }
    $('.close').click(function(){
      $('#subsNext').hide();
      $('#subsFirst').show();
      $('#subsText').text('');
      $('#subsPrice').text('');
    });

</script>
