<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card shadow border-0" style="">
                <div class="card-body">
                    <table class="table table-borderless">
                        <tr>
                            <th colspan="2" class="p-0 m-0">
                                <h3 class="d-inline">Freelancers</h3>
                                <h3 class="d-inline float-right"><i class="fas fa-palette text-muted"></i></h3>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="p-0 pt-2 m-0 text-blue">X <span class="text-uppercase">Logged in</span> </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-4">Registered:</th>
                            <td class="text-center pt-4">{{$total_freelancers}}</td>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-2">Last login more than 2 weeks ago:</th>
                            <td class="text-center pt-2">{{$freelancers_older_than_2_weeks}}</td>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-2">Not verified:</th>
                            <td class="text-center pt-2">{{$unverified_freelancers}}</td>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-2">Not paid:</th>
                            <td class="text-center pt-2">{{$freelancers_unpaid}}</td>
                        </tr>
                    </table>
                    <div class="mt-5">
                        <table class="table table-borderless">
                            <tr>
                                <th colspan="3" class="border-bottom">
                                    <h5 class="font-weight-bold">Average new registrations</h5>
                                </th>
                            </tr>
                            <tr>
                                <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                    <h2 class="text-blue">{{ $freelancers_per_week }}</h2>
                                </td>
                                <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                    <h2 class="text-blue">{{ $freelancers_per_month}}</h2>
                                </td>
                                <td class="m-0 p-0 text-center font-weight-bold"></td>
                            </tr>
                            <tr>
                                <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">This week</td>
                                <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">This month</td>
                                <td class="p-0 m-0 text-center font-weight-bold"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow border-0" style="">
                <div class="card-body">
                    <table class="table table-borderless">
                        <tr>
                            <th colspan="2" class="p-0 m-0">
                                <h3 class="d-inline">Finders</h3>
                                <h3 class="d-inline float-right"><i class="fas fa-user-tie text-muted"></i></h3>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="p-0 pt-2 m-0 text-blue">X <span class="text-uppercase">Logged in now</span> </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-4">Total:</th>
                            <td class="text-center pt-4">{{$total_finders}}</td>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-2">Last login more than 2 weeks ago:</th>
                            <td class="text-center pt-2">{{$finders_older_than_2_weeks}}</td>
                        </tr>
                        <tr class="border-bottom">
                            <th class="p-0 m-0 pt-2">Not verified:</th>
                            <td class="text-center pt-2">{{ $unverified_finders}}</td>
                        </tr>
                    </table>
                    <div class="mt-5">
                        <table class="table table-borderless">
                            <tr>
                                <th colspan="3" class="border-bottom">
                                    <h5 class="font-weight-bold">Average new registrations</h5>
                                </th>
                            </tr>
                            <tr>
                                <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                    <h2 class="text-blue">{{$finders_per_day}}</h2>
                                </td>
                                <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                    <h2 class="text-blue">{{$finders_per_week}}</h2>
                                </td>
                                <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                    <h2 class="text-blue">{{$finders_per_month}}</h2>
                                </td>
                            </tr>
                            <tr>
                                <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">Per day</td>
                                <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">Per week</td>
                                <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">Per month</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row mb-4">
                <div class="col py-0 my-0">
                    <div class="card shadow border-0">
                        <div class="card-body">
                            <table class="table table-borderless">
                                <tr>
                                    <th colspan="2" class="p-0 m-0">
                                        <h3 class="d-inline">Job Posts</h3>
                                        <h3 class="d-inline float-right"><i class="fas fa-briefcase text-muted"></i></h3>
                                    </th>
                                </tr>
                            </table>
                            <div class="row">
                                <div class="col pt-5">
                                    <table class="table table-borderless">
                                        <tr>
                                        </tr>
                                        <tr>
                                            <th class="p-0 pt-2 m-0">Active</th>
                                            <td class="p-0 pt-2 m-0"></td>
                                        </tr>
                                        <tr>
                                            <th class="p-0 pt-2 m-0">Completed</th>
                                            <td class="p-0 pt-2 m-0"></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-borderless">
                                        <tr>
                                            <th colspan="3" class="border-bottom">
                                                <h5 class="font-weight-bold">Average posts</h5>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                                <h2 class="text-blue">0</h2>
                                            </td>
                                            <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                                <h2 class="text-blue">0</h2>
                                            </td>
                                            <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                                <h2 class="text-blue"></h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">Per month</td>
                                            <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">Per day</td>
                                            <td class="p-0 m-0 text-center font-weight-bold"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card shadow border-0">
                        <div class="card-body">
                            <table class="table table-borderless">
                                <tr>
                                    <th colspan="2" class="p-0 m-0">
                                        <h3 class="d-inline">Messages</h3>
                                        <h3 class="d-inline float-right"><i class="fas fa-envelope text-muted"></i></h3>
                                    </th>
                                </tr>
                            </table>
                            <div class="row">
                                <div class="col pt-5">
                                    <table class="table table-borderless">
                                        <tr>
                                        </tr>
                                        <tr>
                                            <th class="p-0 pt-2 m-0">Total</th>
                                            <td class="p-0 pt-2 m-0">{{$total_msg}}</td>
                                        </tr>
                                        <tr>
                                            <th class="p-0 pt-2 m-0">Unread</th>
                                            <td class="p-0 pt-2 m-0">{{$unread_count}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-borderless">
                                        <tr>
                                            <th colspan="3" class="border-bottom">
                                                <h5 class="font-weight-bold">Average messages</h5>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                              @if (!empty($messages_per_day))
                                                <h2 class="text-blue">{{$messages_per_day}}</h2>
                                                @else
                                                <h2 class="text-blue">0</h2>
                                              @endif

                                            </td>
                                            <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                                <h2 class="text-blue">{{$messages_per_week}}</h2>
                                            </td>
                                            <td class="m-0 p-0 pt-2 text-center font-weight-bold">
                                                <h2 class="text-blue">{{$messages_per_month}}</h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">Today</td>
                                            <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">This week</td>
                                            <td class="p-0 m-0 text-center font-weight-bold tw-text-sm">This Month</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
