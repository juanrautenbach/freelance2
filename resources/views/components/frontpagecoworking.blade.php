<section class="co-working pt-5">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <h2 class="text-center font-weight-bold">Co-Working Spaces</h2>
      </div>
    </div>
    <div class="row pt-5 px-4 mb-5">
      <div class="col">
        <div class="card p-2 border-0 shadow-sm">
          <img src="/storage/showcasephoto/SAE1cHiu9ridhOtDAx0Vf0OcAciz64AYTgCBDROr.png" alt="">
          <div class="row pt-3">
            <div class="col-2 px-1 ml-2 text-center">
              <img src="storage/profilepics/8W4W4EIusltDeuR2jyQgxRhN5kvxcqmchbRBI1Hq.jpeg" class="rounded-circle mx-auto text-center" alt="" style="height: 40px">
            </div>
            <div class="col pl-2 pr-3">
              <h5 class="font-weight-bold">Name</h5>
              <p class="mb-0">Location</p>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card p-2 border-0 shadow-sm">
          <img src="/storage/showcasephoto/0NxW04xKhU3NSSE55GEOcJwYiJPNwigjfl7zD3y7.png" alt="">
          <div class="row pt-3">
            <div class="col-2 px-1 ml-2 text-center">
              <img src="storage/profilepics/8W4W4EIusltDeuR2jyQgxRhN5kvxcqmchbRBI1Hq.jpeg" class="rounded-circle mx-auto text-center" alt="" style="height: 40px">
            </div>
            <div class="col pl-2 pr-3">
              <h5 class="font-weight-bold">Name</h5>
              <p class="mb-0">Location</p>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card p-2 border-0 shadow-sm">
          <img src="/storage/showcasephoto/SAE1cHiu9ridhOtDAx0Vf0OcAciz64AYTgCBDROr.png" alt="">
          <div class="row pt-3">
            <div class="col-2 px-1 ml-2 text-center">
              <img src="storage/profilepics/8W4W4EIusltDeuR2jyQgxRhN5kvxcqmchbRBI1Hq.jpeg" class="rounded-circle mx-auto text-center" alt="" style="height: 40px">
            </div>
            <div class="col pl-2 pr-3">
              <h5 class="font-weight-bold">Name</h5>
              <p class="mb-0">Location</p>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card p-2 border-0 shadow-sm">
          <img src="/storage/showcasephoto/0NxW04xKhU3NSSE55GEOcJwYiJPNwigjfl7zD3y7.png" alt="">
          <div class="row pt-3">
            <div class="col-2 px-1 ml-2 text-center">
              <img src="storage/profilepics/8W4W4EIusltDeuR2jyQgxRhN5kvxcqmchbRBI1Hq.jpeg" class="rounded-circle mx-auto text-center" alt="" style="height: 40px">
            </div>
            <div class="col pl-2 pr-3">
              <h5 class="font-weight-bold">Name</h5>
              <p class="mb-0">Location</p>
              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row my-5">
      <div class="col text-center">
        <span class="text-light-front mx-3 f-18"><i class="fas fa-circle"></i></span>
        <span class="text-light-front mx-3 f-18"><i class="fas fa-circle"></i></span>
        <span class="text-light-front mx-3 f-18"><i class="fas fa-circle"></i></span>
        <span class="text-red mx-3 f-18"><i class="fas fa-circle"></i></span>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="px-lg-5 px-0 freelance-bottom">
          <div class="d-md-block d-none">
            <hr class="mb-4 " style="border: 1px solid #979797">
          </div>
          <div class="freelance-bottom-button text-center" style="position: absolute; margin-top: 5px; top: 0; text-align: center; left: 50%; transform: translateX(-50%)">
            <a href="/co-working" class="secondary-button py-2">View all co-working spaces</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
