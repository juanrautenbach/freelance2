<link rel="stylesheet" href="/css/multi-select.dist.css">
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" id="popup-modal">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Profile Setup</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="modal-body p-4 text-center" id="profilesteps1">
                <br>
                <h4 class="mb-3">Lets quickly setup your profile. It will only take a minute.</h4><br>
                <a class="btn btn-red" href="/users/profiles" >Let's go!</a>
                {{-- <span class="btn btn-red" onclick="showStep1()">Let's go!</span> --}}
            </div>
            {{-- <div class="modal-body p-4" style="display:none" id="profilestep2">
                <h4 class="mb-3">Let's get your profile setup.</h4>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="" action="/settings/profile/" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="profilephoto">Upload a profile photo:</label>
                                <input type="file" class="form-control" name="profilephoto">
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="cover_photo">Upload a cover photo:</label>
                                <input type="file" class="form-control" name="cover_photo">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="about">Tell us about yourself:</label>
                        <textarea rows="8" class="form-control" name="about"></textarea>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tel">Telephone number:</label>
                                <input type="text" class="form-control" name="tel">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="mobile">Mobile number:</label>
                                <input type="text" class="form-control" name="mobile">
                            </div>
                        </div>
                    </div>
                    <span class="btn btn-red" onclick="showStep2()">Next step!</span>

            </div> --}}

            {{-- <div class="modal-body p-4" style="display:none" id="profilestep3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="company_name">Company name:</label>
                            <input type="text" class="form-control" name="company_name">
                        </div>
                    </div>
                    <div class="col">
                        <label for="cities">Where are you based:</label>
                        <select class="form-control" name="cities">
                            <option value="">-- Select --</option>
                            @foreach ($cities as $city)
                            <option value="{{ $city->accentCity }}">{{ $city->accentCity }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="social_media">Social Media Links:</label>
                        <table class="table table-borderless">
                            <tr>
                                <td class=""> <strong>Facebook </strong> </td>
                                <td class=""> <input type="text" class="form-control w-60" name="social_media[]"> </td>
                            </tr>
                            <tr>
                                <td class=""> <strong>Twitter </strong> </td>
                                <td class=""> <input type="text" class="form-control w-60" name="social_media[]"> </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="skills">List your skills: <small>Seperate with ; (social media; html; )</small> </label>
                            <textarea rows="4" class="form-control" name="skills"></textarea>
                        </div>
                    </div>
                </div>
                <span class="btn btn-red" onclick="showStep3()">Final step!</span>
            </div> --}}


            {{-- <div class="modal-body p-4" style="display:none" id="profilestep4">
                <div class="row">
                    <div class="col">
                        <label for="industry">Please select your industry</label>
                        <select class="form-control" name="industry" id="industry_id">
                            <option value="">-- Select --</option> --}}
                            {{-- @foreach ($industries as $industry)
                            <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                            @endforeach --}}
                        {{-- </select>
                    </div>
                </div>
                <div class="row" style="height: 20rem">
                    <div class="col">
                        <label for="subindustries">Sub Industries</label><small>(Use CTRL to select many options)</small>
                        <select class="form-control" multiple="multiple" name="subindustries[]" id="subcategory_id" style="height:300px;">
                          <option value="">-- Select --</option>
                        </select>
                    </div>
                </div>

            </div> --}}

            {{-- <div class="modal-footer" id="profilebutton" style="display:none">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-red">Save changes</button>
            </div>
            </form> --}}
        </div>
    </div>
</div>
<script src="/js/dynamicdropdown.js" charset="utf-8"></script>
<script src="/js/jquery.multi-select.js" charset="utf-8"></script>
<script type="text/javascript">
    // $('#subcategory_id').multiSelect();

    function showStep1() {
        $('#profilesteps1').hide();
        $('#profilestep2').show();
        $('#profilestep3').hide();
        $('#profilestep4').hide();
        $('#profilebutton').hide();
    }

    function showStep2() {
        $('#profilesteps1').hide();
        $('#profilestep2').hide();
        $('#profilestep3').show();
        $('#profilestep4').hide();
        $('#profilebutton').hide();
    }

    function showStep3() {
        $('#profilesteps1').hide();
        $('#profilestep2').hide();
        $('#profilestep3').hide();
        $('#profilestep4').show();
        $('#profilebutton').show();
    }
</script>
