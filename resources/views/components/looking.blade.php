<section class="looking-search pb-3 pt-4">
  {{-- <div class="container"> --}}
    <div class="row">
      <div class="col-sm-12 col-lg-6 offset-lg-2 text-lg-right pt-1 looking-text text-sm-center mb-2">
        <h2>Looking for some talent?</h2>
      </div>
      <div class="col-sm-12 col-lg-4 text-lg-left looking-button text-sm-center ">
        <a class="btn-looking inline-btn" href="/freelancers">All Freelancers</a>
      </div>
    </div>

  {{-- </div> --}}

</section>
