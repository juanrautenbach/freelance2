<section class="hero-custom">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-8 text-area d-none d-lg-block">
                <div data-aos="slide-right">
                    <div class="hero-header">
                        <h1 class="">Freelance Cape Town,<br> where creatives connect and <br> showcase their work.</h1>
                    </div>
                    <div class="hero-strapline">
                        <h3>Find talent in an industry you're interested in.</h3>
                    </div>

                    <div class="hero-search">
                        <a href="/freelancers" class="btn btn-grey fs-24 px-4 py-3 text-white"><i class="fas fa-search mr-2"></i>Search</a>
                       
                    </div>
                </div>

            </div>
            <div class="col hero-right px-0 d-none d-lg-block background">

            </div>
            <div class="col-12 hero-top px-0 d-inline-block d-lg-none">

            </div>
        </div>
        <div class="row text-area d-block d-lg-none">
            <div class="hero-header col">
                <h1 class="tw-p-8 tw-mb-0 tw-text-2xl">Freelance Cape Town,<br> where creatives connect and showcase their work.</h1>
            </div>
        </div>
        <div class="row text-area d-block d-lg-none tw-border-0">
            <div class="hero-strapline col tw-border-0">
                <h3 class="pl-lg-0">Find talent in an industry you're interested in.</h3>
            </div>
        </div>
        <div class="row text-area d-flex d-block d-lg-none pb-5 pl-2 pr-4">
            <div class="col text-center">
                <a href="/freelancers" class="btn btn-grey fs-24 px-4 py-3 text-white"><i class="fas fa-search mr-2"></i>Search</a>
            </div>
            {{-- <div class="col-10 col-sm-8 text-right pr-0">
                
                <select class="custom-select p-2" name="">
                    <option value="">Please Select an Industry Small</option>
                    @foreach ($industries as $industry)
                    <option value="{{$industry->name}}">{{$industry->name}}</option>
                    @endforeach
                </select>
            </div> --}}
            {{-- <div class="col-2 text-left pl-0">
                <button type="button" class="custom-sec-btn rounded" name="button"><img src="/img/icons/search-icon.png" alt=""> </button>
            </div> --}}
        </div>
    </div>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init({
            offset: 250, // offset (in px) from the original trigger point
            delay: 0, // values from 0 to 3000, with step 50ms
            duration: 1000 // values from 0 to 3000, with step 50ms
        });
    </script>
</section>