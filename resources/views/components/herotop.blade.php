<section class="hero-top py-4 text-center bg-white">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2>Become part of Cape Town's number 1 freelance platform</h2>
      </div>

    </div>
    <div class="container">
      <div class="row mt-4">
        <div class="col">
          <div class="row">
            <div class="col">
              <img src="/img/icons/computer-icon.png" alt="">
            </div>
          </div>
          <div class="row mt-4">
            <div class="col">
              <h4> <strong>347</strong> </h4>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <p>Visually attractive <br> <strong>Freelance</strong> profiles</p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="row">
            <div class="col">
              <img src="/img/icons/speech-icon.png" alt="">
            </div>
          </div>
          <div class="row mt-4">
            <div class="col">
              <h4> <strong>2948 +</strong> </h4>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <p>Simple communication <br> between clients <br> and freelancers</p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="row">
            <div class="col">
              <img src="/img/icons/briefcase-icon.png" alt="">
            </div>
          </div>
          <div class="row mt-4">
            <div class="col">
              <h4> <strong>897</strong> </h4>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <p>Number of <strong>clients</strong><br>who registered <br>to brief freelancers</p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="row">
            <div class="col">
              <img src="/img/icons/users-icon.png" alt="">
            </div>
          </div>
          <div class="row mt-4">
            <div class="col">
              <p>Join the fastest <br> growing global economy</p>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col">
              <a onclick="showRegister()" class="top-btn py-3 px-2 text-capitalize">Start here!</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>




</section>
