<div class="section tw-my-8 tw-py-8">
					<div class="container clearfix">
            @if (count($freelancers) == 0)
                
            @else
                
            
						<div class="heading-block center">
							<h3>Meet Our Freelancers</h3>
						</div>
            @foreach ($freelancers->chunk(4) as $items)
						<div class="row">
              @foreach($items as $item)
              @if (empty($item->profile->profile_pic))

              @else
							<div class="col-lg-3 col-md-6 bottommargin">
                <div class="tw-shadow-md tw-px-1 tw-rounded-md tw-h-full">
                  <a href="/freelancer/click/{{$item->id}}">
                    <div class="team">
                      <div class="team-image tw-p-1">
                        {{-- <img src="/images/team/3.jpg" alt="John Doe"> --}}
                        @if (!empty($item->profile->profile_pic))
                          <img src="{{$item->profile->profile_pic}}" class="" alt="Card image cap">
                        @else
                          <img src="/img/FCPT_Logo.png" class="" alt="" height="360px">
                        @endif
                      </div>
                      <div class="team-desc team-desc-bg text-center">
                        <div class="team-title text-dark my-2"><h5 class="font-weight-bold">{{ $item->name }}</h5>
                          @foreach ($item->industry as $industry)
                          <span class="mr-2">{{ $industry->industry_name }}</span>
                          @endforeach
                          <div>
                            
                            @foreach ($item->subindustry->take(2) as $subindustry)
                           
                                <span class="mr-2 tw-text-xs">{{ $subindustry->sub_industry_name }}</span>
                              
         
                            @endforeach
                          </div>
                          
                          {{-- <span class="">{{ $item->industry[0]->industry_name }}</span> --}}
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                
              </div>

                @endif
              @endforeach


						</div>
            @endforeach

            @endif
					</div>
				</div>