<div class="container-fluid">
  <div class="row md:tw-px-12">
    <div class="col-md-4 offset-md-2 d-none">
      <div class="your_profile tw-p-1 md:tw-p-2">
        <div class="card tw-shadow-md tw-w-full tw-border-solid tw-border-transparent tw-border hover:tw-border-blue-600 d-none">
          <div class="card-content">
            <div class="card-body">
              <div class="media d-flex">
                <div class="align-self-center">
                  <i class="icon-user tw-text-blue-600 font-large-2 float-left"></i>
                </div>
                <div class="media-body text-right">
                  <a href="/users/profiles" class="btn btn-red tw-mb-3">Your profile</a>
                  <span class="d-block tw-text-gray-800">Account: <span>Active</span> </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- <div class="tw-bg-white tw-shadow-md d-flex justify-content-center align-items-center tw-rounded">
            <a href="/users/profiles" class="btn btn-red">Your profile</a>
        </div> --}}
    </div>
    <div class="your_profile tw-p-2">
        {{-- <div class="tw-bg-white tw-shadow-md d-flex justify-content-center align-items-center tw-rounded">
            <a href="/users/profiles" class="btn btn-red">Your briefs</a>
        </div> --}}
        <div class="card tw-shadow-md tw-w-full tw-border-solid tw-border-transparent tw-border hover:tw-border-blue-600 d-none">
          <div class="card-content">
            <div class="card-body">
              <div class="media d-flex">
                <div class="align-self-center">
                  <i class="icon-envelope tw-text-blue-600 font-large-2 float-left"></i>
                </div>
                <div class="media-body text-right">
                  <a href="/messages" class="btn btn-red">Your briefs</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="your_profile tw-p-2">
        <div class="tw-bg-white tw-shadow-md d-flex justify-content-center align-items-center tw-rounded d-none">
            <a href="/users/profiles" class="btn btn-red">Send brief</a>
        </div>
    </div>
    </div>
    {{-- <div class="col-md-6">

    </div> --}}


    
    <div class="col-md-4 your_profile tw-p-2">
        
        <div class="tw-rounded tw-flex tw-flex-wrap">
            <div class="card tw-shadow-md tw-w-full tw-border-solid tw-border-transparent tw-border hover:tw-border-blue-600 tw-mx-12 md:tw-mx-0 messages-card-link pointer">
                <div class="card-content">
                  <div class="card-body">
                    <div class="media d-flex">
                      <div class="align-self-center">
                        <i class="icon-bubble tw-text-blue-600 font-large-2 float-left"></i>
                      </div>
                      <div class="media-body text-right">
                        <h3 class="tw-text-4xl tw-text-blue-600">{{ count($user->messages) }}</h3>
                        <span class="tw-text-gray-800">Messages</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            <div class="card tw-shadow-md tw-w-full tw-border-solid tw-border-transparent tw-border hover:tw-border-blue-600 tw-mx-12 md:tw-mx-0 profile-card-link pointer">
                <div class="card-content">
                  <div class="card-body">
                    <div class="media d-flex">
                      <div class="align-self-center">
                        <i class="icon-mouse tw-text-blue-600 font-large-2 float-left"></i>
                      </div>
                      <div class="media-body text-right">
                        <h3 class="tw-text-4xl tw-text-blue-600">{{ $user->click_count }}</h3>
                        <span class="tw-text-gray-800">Profile Clicks</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card tw-shadow-md tw-w-full tw-border-solid tw-border-transparent tw-border hover:tw-border-blue-600 tw-mx-12 md:tw-mx-0 subscription-card-link pointer">
                <div class="card-content">
                  <div class="card-body">
                    <div class="media d-flex">
                      <div class="align-self-center">
                        <i class="icon-key tw-text-blue-600 font-large-2 float-left"></i>
                      </div>
                      <div class="media-body text-right">
                        @php
                            $sub_count = count($user->subscription) - 1;
                            $now = \Carbon\Carbon::now();
                        @endphp
                        @if ($sub_count < 0)
                            {{ "No subscription" }}
                        @else
                        @for ($i = $sub_count; $i < count($user->subscription); $i++)
                        @php
                            $expiry_date = \Carbon\Carbon::parse($user->subscription[$sub_count]->expire_date);
                        @endphp
                        <span class="tw-text-gray-800 d-block tw-mb-4">{{ "Expires in " . $now->longAbsoluteDiffForHumans($expiry_date)  }}</span>
                    @endfor
                        @endif
                       <div class="tw-mt-5">
                        <a href="/subscriptions" class="tw-bg-red-910 tw-rounded tw-text-white tw-font-bold tw-py-1 tw-px-2 tw-text-sm"><i class="fas fa-plus tw-mr-2"></i> Subscription</a>
                        <button class="tw-border-0 tw-bg-gray-900 tw-rounded tw-text-white tw-font-bold tw-py-1 tw-px-2 tw-text-sm"><i class="fas fa-times tw-mr-2"></i> Subscription</button> 
                       </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>
</div>

<script>
  $(".subscription-card-link").click(function () {
    window.location.href = "/subscriptions";
  })

  $(".profile-card-link").click(function () {
    window.location.href = "/users/profiles";
  })

  $(".messages-card-link").click(function () {
    window.location.href = "/messages";
  })
</script>

