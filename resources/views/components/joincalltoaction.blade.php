<section class="custom-cta-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-8 col-md-12 ">
        <h2 class="my-auto">Join the fastest growing global economy</h2>
      </div>
      <div class="col-lg-4 col-12 pt-0 text-center text-lg-left button-col">
        {{-- <a href="/register" class="primary-button mt-4 p-3 d-block d-md-inline-block text-center rounded ">Get started</a> --}}
        <button id="join_button" class="tw-w-40 tw-bg-red-911 tw-border-solid tw-border-2 tw-border-red-911 hover:tw-bg-transparent tw-font-bold tw-text-white tw-mt-6 tw-text-lg tw-p-3 md:tw-inline-block tw-text-center tw-rounded ">Get started</button>
      </div>
    </div>

  </div>
</section>