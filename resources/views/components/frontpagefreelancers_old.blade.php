<section class="frontpagefreelancers">
    <div class="container-fluid">
      @if ($freelancer_count < 1)
        <div class="row text-center freelancer-header">
            <div class="col">
                <h2>No Freelancers</h2>
            </div>
        </div>
      @else
        <div class="row text-center freelancer-header">
            <div class="col">
                <h2>Freelancers</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0">
      @foreach ($freelancers->chunk(4) as $items)
          <div class="row cards-row mb-3">
              @foreach($items as $item)
                @if (empty($item->profile->profile_pic))

                @else
                <div class="col-12 col-md-6 col-xl-3">
                  <a href="/freelancer/{{$item->id}}">
                  <div class="card mx-auto shadow-sm mb-3 p-2" style="">
                    {{-- @if (!empty($item->profile->profile_pic)) --}}
                    @if (!empty($item->profile->profile_pic))
                      <img class="card-img-top " src="{{$item->profile->profile_pic}}" alt="Card image cap">
                    @else
                      {{-- <img class="p-2" src="http://via.placeholder.com/360" alt=""> --}}
                      {{-- <img class="p-2" src="https://i.pravatar.cc/360" alt=""> --}}
                      <img class="" src="/img/FCPT_Logo.png" alt="" height="360px">
                    @endif
                      <div class="card-body">
                          <div class="row">
                              {{-- <div class="col-2 col-lg-3 text-center px-0">
                              </div> --}}
                              <div class="col">
                                  <p class="font-weight-bold text-capitalize">{{ $item->name }}</p>
                                  <p class="tags"></p>
                                  <p class="freelancer-profile-body">
                                    {{-- {{$item->subindustry->name}} --}}
                                    {{-- @if (!empty($item->profile->about))
                                      {{$item->profile->about}}
                                    @else

                                    @endif --}}

                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
                  </a>
                </div>
                <script type="text/javascript">
                $.ajax({
                    url: "{{url('')}}/api/user/tags/{{$item->id}}/complete",
                    type: 'GET',
                    dataType: 'json',
                    success: function(tags) {
                        // console.log(res);
                        if (tags.length > 0) {
                          var tags_data = '';
                          $.each(tags, function(index) {

                              tags_data += `<span class="badge badge-pill mr-1 shadow p-2">`+ tags[index].name +`</span>`;
                          })
                          // $(this).find(".tags").html(tags_data);
                          $(".tags").html(tags_data);
                        } else {
                        }
                    }
                });
                </script>

                @endif
              @endforeach
          </div>
      @endforeach
      <div class="row">
        <div class="col">
          <div class="px-lg-5 px-0 freelance-bottom">
            <div class="d-md-block d-none">
              <hr class="mb-4 ">
            </div>
            <div class="freelance-bottom-button text-center">
              <a href="/freelancers" class="secondary-button">View all freelancers</a>
            </div>
          </div>
        </div>
      </div>



    </div>
      @endif

</section>
