<link rel="stylesheet" href="/css/multi-select.dist.css">
<div class="modal fade bd-example-modal-lg" id="profileSetup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" id="popup-modal">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Profile Setup!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="modal-body p-4 text-center" id="profilesteps1">
                <br>
                <h4 class="mb-3">Lets start with the basics.</h4><br>
                {{-- <a class="btn btn-red" href="/settings/profile" >Let's go!</a> --}}
                <span class="btn btn-red" onclick="showStep1()">Let's go!</span>
            </div>
            <div class="modal-body p-4" style="display:none" id="profilestep2">
                <h4 class="mb-3">Let's get your profile setup.</h4>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="" action="/users/profiles/" method="post" enctype="multipart/form-data">
                    @csrf
                    {{-- <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="profilephoto">Upload a profile photo:</label>
                                <input type="file" class="form-control" name="profilephoto">
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="cover_photo">Upload a cover photo:</label>
                                <input type="file" class="form-control" name="cover_photo">
                            </div>
                        </div>
                    </div> --}}

                    <div class="form-group">
                        <label for="about">Tell us about yourself:</label>
                        <textarea rows="8" class="form-control" name="about"></textarea>
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tel">Telephone number:</label>
                                <input type="text" class="form-control" name="tel">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="mobile" class="">Mobile number: <span class="font-weight-bold">(Required)</span> </label>
                                <input type="text" class="form-control" name="mobile" required>
                            </div>
                        </div>
                    </div>
                    <span class="btn btn-red" onclick="showStep2()">Next step!</span>

            </div>

            <div class="modal-body p-4" style="display:none" id="profilestep3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="company_name">Company name:</label>
                            <input type="text" class="form-control" name="company_name">
                        </div>
                    </div>
                    <div class="col">
                        <label for="cities">Where are you based:</label>
                        <input type="text" name="city" class="form-control" id="user_city">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="social_media">Social Media Links:</label>
                        <table class="table table-borderless">
                            <tr>
                                <td class=""> <strong>Facebook </strong> </td>
                                <td class=""> <input type="text" class="form-control w-60" name="social_media[]"> </td>
                            </tr>
                            <tr>
                                <td class=""> <strong>Twitter </strong> </td>
                                <td class=""> <input type="text" class="form-control w-60" name="social_media[]"> </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label for="skills">Skills: <small>(Use the , to add more than 1)</small> </label>

                          <div class="tags-container" data-name="tags_container">
                            <input type="hidden" name="user_tags" id="hiddenInput" value="">
                          </div>


                        </div>
                    </div>
                </div>
                <span class="btn btn-red" onclick="showStep3()">Final step!</span>
            </div>


            <div class="modal-body p-4" style="display:none" id="profilestep4">
                <div class="row">
                    <div class="col">
                        <label for="industry">Please select your industry</label>
                        <select class="form-control" name="industry" id="industry_id">
                            <option value="">-- Select --</option>
                            {{-- @foreach ($industries as $industry)
                            <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="row" style="height: 20rem">
                    <div class="col">
                        <label for="subindustries">Sub Industries</label><small>(Use CTRL to select many options)</small>
                        <select class="form-control" multiple="multiple" name="subindustries[]" id="subcategory_id" style="height:300px;">
                          <option value="">-- Select --</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="modal-footer" id="profilebutton" style="display:none">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-red">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
  function activatePlacesSearch(){
    let input = document.getElementById('user_city');
    let autocomplete = new google.maps.places.Autocomplete(input);
  }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3vqC_XDkDB4N_Nit2boqMJpB9gHF8TQM&libraries=places&callback=activatePlacesSearch"></script>
<script src="/js/dynamicdropdown.js" charset="utf-8"></script>
<script src="/js/jquery.multi-select.js" charset="utf-8"></script>
<script src="/js/tags.js" charset="utf-8"></script>
<script type="text/javascript">
    // $('#subcategory_id').multiSelect();

    function showStep1() {
        $('#profilesteps1').hide();
        $('#profilestep2').show();
        $('#profilestep3').hide();
        $('#profilestep4').hide();
        $('#profilebutton').hide();
    }

    function showStep2() {
        $('#profilesteps1').hide();
        $('#profilestep2').hide();
        $('#profilestep3').show();
        $('#profilestep4').hide();
        $('#profilebutton').hide();
    }

    function showStep3() {
        $('#profilesteps1').hide();
        $('#profilestep2').hide();
        $('#profilestep3').hide();
        $('#profilestep4').show();
        $('#profilebutton').show();
    }
    // $.ajax({
    //     url: "https://geoip-db.com/jsonp",
    //     jsonpCallback: "callback",
    //     dataType: "jsonp",
    //     success: function(location) {
    //         $('#user_city').val(location.city);
    //     }
    // });
</script>
