<section id="testimonial-slider" class="text-center px-1 px-md-5">
    <div class="container-fluid">
        <div id="carouselExampleControls" class="carousel slide carousel-fade" data-interval="8000" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                      <div class="d-block d-md-block slider-quote">
                          <h4>"As a newbie to the Freelance arena, I've found Freelance Cape Town to be great exposure to the industry."</h4>
                  </div>
                      <img class="d-inline" src="/img/testimonials/deborah.jpg" alt="First slide">
                      <p>Deborah Butler (The Startum Project),<br> <span class="font-weight-normal">Web Developer</span> </p>
                </div>
                <div class="carousel-item">
                  <div class="d-block d-md-block slider-quote">
                      <h4>"My experience with Freelance Cape Town has been absolutely phenomenal, Truly a great service to the freelance community."</h4>
                  </div>
                    <img class="d-inline" src="/img/testimonials/renier.jpg" alt="Second slide">
                    <p>Renier Lombard (SocialAnimal),<br> <span class="font-weight-normal">Social Media Expert</span> </p>
                </div>
                <div class="carousel-item">
                    <div class=" d-block d-md-block slider-quote">
                        <h4>"Having just started my own business as a creative entrepreneur, I took the leap and joined Freelance Cape Town in January, with the goal of getting my name out there. Within 4 days, I had gained an enquiry from an overseas company looking to employ my services on a retainer basis for 11 days of the month. This would not have been possible without the exposure of FCT."</h4>
                    </div>
                    <img class="d-inline" src="/img/testimonials/candice.jpg" alt="Third slide">
                    <p>Candice,<br> <span class="font-weight-normal">Marketing & Media</span> </p>
                </div>
                <div class="carousel-item">

                    <div class="d-block d-md-block slider-quote">
                        <h4>"Freelance Cape Town is unique in a sense that it gives local Freelancers the chance to fulfil local job demand. And it does it well!"</h4>
                    </div>
                    <img class="d-inline" src="/img/testimonials/simon.png" alt="Third slide">
                    <p>Simon Dowdles,<br> <span class="font-weight-normal">Web Developer</span></p>
                </div>
                <div class="carousel-item">

                    <div class="d-block d-md-block slider-quote">
                        <h4>"It was great to have the right exposure on a site that has a great user-experience and functionality. Reputable businesses got in touch with me and I can definitely recommend Freelance Cape Town as a go to platform for freelancers. It will open up many doors and opportunities for you. It did for me."</h4>
                    </div>
                    <img class="d-inline" src="/img/testimonials/lida.jpg" alt="Third slide">
                    <p>Lidia van Wyk,<br> <span class="font-weight-normal">Writing</span></p>
                </div>
            </div>
            <a class="carousel-control-prev d-none d-md-flex" href="#carouselExampleControls" role="button" data-slide="prev">
                {{-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> --}}
                <img class="slider-control" src="/img/left-large.png" alt="">
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next d-none d-md-flex" href="#carouselExampleControls" role="button" data-slide="next">
                {{-- <span class="carousel-control-next-icon" aria-hidden="true"></span> --}}
                <img class="slider-control" src="/img/right-large.png" alt="">
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
