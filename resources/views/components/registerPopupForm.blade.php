<form class="" id="msform" action="/register" method="post">
  @csrf
    <!-- progress bar -->
  <!-- fieldsets -->
  <fieldset id="fs-step1">
    <div class="row">
      <div class="col">
        <h2 class="fs-title">Choose a role:</h2>
      </div>
    </div>
    <div class="row">
      <div class="col my-3">
        <h3 class="fs-subtitle font-weight-bold">I want to:</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 text-sm-right">
        <a class="fs-check-button" onclick="nextForm('finder')">Hire for a Project</a>
      </div>
      <div class="col-sm-6 text-sm-left">
        <a class="fs-check-button" onclick="nextForm('freelancer')">Work as a Freelancer</a>
      </div>
    </div>


  </fieldset>

  <fieldset id="fs-step2" class="fs-next">
    <input type="hidden" name="user_role" id="user_role">
    <h2 class="fs-title">Let's get you account set up</h2>
    <label for="user_email">Work Email:</label>
    <input type="text" name="email" placeholder="Email">
    <label for="user_email">Create a Password</label>
    <input type="password" name="password" placeholder="Password">
    <input type="submit" class="fs-button" name="submit" value="Submit to verify"></input>
    <div class="fs-bottom">
      <div class="tos-left">I agree to <a href="#">FCT's Terms of Service</a></div>
      <div class="tos-right f-right"> <a href="/login">Already have an account?</a> </div>
    </div>

  </fieldset>

  </form>
