<section id="inbox" class="card border-0 mx-2 my-4 shadow-sm">
    @if ($inbox_count == 0)
    <h4 class="text-muted text-center py-3">No messages to display</h4>
    @else
    @foreach ($messages as $message)
    @php
    $sender_id = $message->sender_id;
    @endphp
    @foreach ($users as $user)
    @if ($user->id == $sender_id)
    <div class="row px-4 py-2">
        <div class="col pointer" onclick="showMessage({{ $message->id }})">
            <div class="row">
                <div class="col-8">
                    <p class="font-weight-bold m-0">
                        {{$user->name}}
                    </p>
                </div>
                @endif
                @endforeach
                <div class="col text-right">
                    <p class="text-muted float-right m-0"> <small>{{ Carbon\Carbon::parse($message->created_at)->diffForHumans()}}</small> </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="mb-0"> <a class="text-muted">{{$message->subject}}</a> </p>
                </div>
            </div>
        </div>
    </div>
    <hr class="my-1">
    @endforeach
    @endif
</section>
