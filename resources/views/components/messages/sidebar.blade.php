<div class="msg-sidebar-top">
    <div class="row text-center py-3 pt-3">
        {{-- <div class="col-8">
            <h5 class="mt-1">Messages</h5>
        </div> --}}
        <div class="col">
          @php
            $user = \Auth::user();
          @endphp

          @if ($user->role == "Admin")
            {{-- @if () --}}
            <span class="badge badge-pill bg-white shadow-sm py-3 d-block mx-4 mt-2 pointer" onclick="showMsg()">Compose</span>
            {{-- @endif --}}
          @endif

        </div>
    </div>
</div>
<div class="msg-sidebar-body">
    <table class="table text-center text-muted table-borderless">
        <tr>
            <td class="pr-1">
              <span class="w-25 mr-2">
                <i class="fas fa-inbox"></i>
              </span>

            <span class="text-center pl-0 d-none d-lg-inline-block w-50">
              <a onclick="showInbox()">Inbox</a>
            </span>
            <span class="text-right w-25 ml-2">{{$inbox_count}}</span>
            </td>
        </tr>
        <tr>
            <td class="pr-1">
              <span class="w-25 mr-2">
                <i class="fab fa-telegram-plane"></i>
              </span>

            <span class="text-center pl-0 d-none d-lg-inline-block w-50">
              <a onclick="showSent()" disable>Sent</a>
            </span>
            <span class="text-right w-25 ml-2">{{$sent_count}}</span>
          </td>
        </tr>
        <tr>
            <td class="pr-1">
              <span class="w-25 mr-2">
                <i class="fas fa-trash-alt"></i>
              </span>

            <span class="text-center pl-0 d-none d-lg-inline-block w-50">
              <a onclick="showTrash()" disable>Trash</a>
            </span>
            <span class="text-right w-25 ml-2">{{$trash_count}}</span>
            </td>
        </tr>
    </table>
</div>
