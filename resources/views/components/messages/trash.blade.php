<section id="trash" style="display:none" class="card border-0 mx-2 my-4 shadow-sm">
    @if ($trash_count == 0)
    <h4 class="text-muted text-center py-3">No messages to display</h4>
    @else
    @foreach ($trash_messages as $trash_message)
    @php
    $sender_id = $trash_message->sender_id;
    $user_id = $trash_message->user_id;
    $sent_user = \Auth::user();
    @endphp
    @if (($sent_user->id == $sender_id) || ($sent_user->id == $user_id))
    <div class="row px-4 py-2">
        <div class="col pointer" onclick="showMessage({{ $trash_message->id }})">
            <div class="row">
                <div class="col-8">
                    <p class="font-weight-bold m-0">
                      @if ($sent_user->id == $sender_id)
                         {{$trash_message->user->name}}
                        @else
                          @php
                            $sender_id = $trash_message->sender_id;
                          @endphp
                          @foreach ($users->where('id', $sender_id) as $user)
                             {{$user->name}}
                          @endforeach
                      @endif
                    </p>
                </div>
                @endif
                <div class="col">
                    <p class="text-muted float-right mb-0"> <small>{{ Carbon\Carbon::parse($trash_message->created_at)->diffForHumans()}}</small> </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="mb-0"> <a class="text-muted">{{$trash_message->subject}}</a> </p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endif
</section>
