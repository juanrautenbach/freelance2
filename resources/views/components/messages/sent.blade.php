<section id="sent" style="display:none" class="card border-0 mx-2 my-4 shadow-sm">
  @if ($sent_count == 0)
    <h4 class="text-muted text-center py-3">No messages to display</h4>
  @else
    @foreach ($sent_messages as $sent_message)
      @php
        $sender_id = $sent_message->sender_id;
        $sent_user = \Auth::user();
      @endphp
        @if ($sent_user->id == $sender_id)
          <div class="row px-4 py-2">
              <div class="col pointer" onclick="showMessage({{ $sent_message->id }})">
                  <div class="row">
                      <div class="col-8">
                          <p class="font-weight-bold mb-0">
                            {{$sent_message->userid}}
                          </p>
                      </div>
                      @endif
                      <div class="col">
                          <p class="text-muted float-right mb-0"> <small>{{ Carbon\Carbon::parse($sent_message->created_at)->diffForHumans()}}</small> </p>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col">
                          <p class=" mb-0"> <a class="text-muted">{{$sent_message->subject}}</a> </p>
                      </div>
                  </div>
              </div>
          </div>
    @endforeach
  @endif
</section>
