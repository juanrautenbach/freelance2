<div id="messagePanel" class="">
  <div class="card col-6 offset-3 py-3">
    <div class="row">
      <div class="col">
        <h2>Send Message</h2>
      </div>
      <div class="col text-right">
        <span onclick="closeMsg()" class="close-btn pointer"><i class="fas fa-times"></i></span>
      </div>
    </div>
    <div class="card-body ">
      <form class="" action="/messages" method="post">
        @csrf
        @php
          $current_user = \Auth::user();
          $current_user_role = $current_user->role;
          $current_user = $current_user->id;
          // echo "Current user" . $current_user;
        @endphp
      <div class="row">
        <div class="col">
          {{-- <input type="text" name="message_to" value="{{$user->name}} ({{$user->email}}) " class="form-control"> --}}
          @if ($user->role == "Admin")
            <label for="message_to" class="font-weight-bold">To:</label>
          <select class="form-control" id="send_to" name="message_to">
            <option value="">--Select--</option>
              @foreach ($users as $user)
                  <option value="{{$user->id}}">{{$user->name}} <strong>({{$user->email}})</strong> </option>
            @endforeach
            </select>
            <input type="hidden" name="message_to" value="{{$user->id}}" disabled id="hidden_sent_to">
            @else
              <select class="form-control d-none" id="send_to" name="message_to">
                <option value="">--Select--</option>
                  @foreach ($users as $user)
                      <option value="{{$user->id}}">{{$user->name}} <strong>({{$user->email}})</strong> </option>
                @endforeach
                </select>
              <input type="hidden" name="message_to" value="{{$user->id}}"  id="hidden_sent_to">
              <input type="hidden" id="message_post_id" name="post_id">
            @endif
          <br>
        </div>
      </div>

      @if ($current_user_role == "Admin")
        <div class="row">
      @else
        <div class="row d-none">
      @endif

        <div class="col">
          <label for="message_subject" class="font-weight-bold">Subject</label>
          <input name="message_subject" class="form-control" id="subject_to" type="text"><br>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label for="message_text" class="font-weight-bold">Message</label>
          <textarea name="message_text" class="form-control"  rows="8" cols="80"></textarea><br>
          <input type="hidden" name="user_id" value="{{ $current_user}}">
        </div>
      </div>
      <div class="row">
        <div class="col text-right">
          <button type="submit" class="btn btn-red px-4" name="button">Send</button>
          <span onclick="closeMsg()" class="btn btn-link text-dark">Cancel</span>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
