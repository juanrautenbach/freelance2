<section class="carousel-custom">
  <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="12000">
      <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/img/carousel/1.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/2.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/3.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/4.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/5.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/6.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/7.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/8.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/9.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/10.jpg" alt="">
        </div>

        <div class="carousel-item ">
            <img class="d-block w-100" src="/img/carousel/11.jpg" alt="">
        </div>
        <div class="carousel-item ">
            <img class="d-block w-100" src="/img/carousel/12.jpg" alt="">
        </div>
        <div class="carousel-item ">
            <img class="d-block w-100" src="/img/carousel/13.jpg" alt="">
        </div>
        <div class="carousel-item ">
            <img class="d-block w-100" src="/img/carousel/14.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/15.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/16.jpg" alt="">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="/img/carousel/17.jpg" alt="">
        </div>
        <div class="carousel-cover">
          <div class="carousel-caption-custom">
              <h1 class="mb-5">Freelance Cape Town, <br> where the city's creative <br> talent connect and showcase <br> their work.</h1>
              <a onclick="showRegister()" class="top-btn py-3 px-4">Why not join them?</a>
          </div>
        </div>
    </div>
</div>
</section>
