<section class=" wow slideInLeft">


<section class="freelance-platform">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <h3 class="text-center font-weight-bold">Become part of Cape Town's number 1 freelance platform</h3>
      </div>

    </div>
    <div class="row stats-row">
      <div class="col-lg-3 col-md-12">
        <div class="row">
          <div class="col">
            <h2>{{ $freelancer_count }}</h2>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <h4>Visually attractive</h4>
            <h4 class="font-weight-bold">Freelance profiles</h4>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="row">
          <div class="col">
            <h2>{{$message_count."+" ?? "Lots"}}</h2>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <h4 class="font-weight-bold">Simple communication</h4>
            <h4>between clients and freelancers</h4>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-12">
        <div class="row">
          <div class="col">
            <h2>{{ $finder_count }}</h2>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <h4 class="bold">Clients registered</h4>
            <h4>to brief freelancers</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</section>
