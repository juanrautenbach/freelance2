{{-- <div class="container-fluid mb-5 px-5">
    <div class="row">
    <div class="col">

			<input type="file" id="showcase_photo1" name="showcase_photo_1"/>

    </div>
    <div class="col">
      <input type="file" id="showcase_photo2"/>
    </div>
    <div class="col">
      <input type="file" id="showcase_photo3"/>
    </div>
    </div>
    <div class="row mt-4">
      <div class="col">
        <input type="file" id="showcase_photo4"/>
      </div>
      <div class="col">
        <input type="file" id="showcase_photo5"/>
      </div>
      <div class="col">
        <input type="file" id="showcase_photo6"/>
      </div>
    </div>

</div> --}}

<div class="add_showcase" id="add_showcase" style="display: none">
    <div class="card col-12 col-sm-6 mx-auto">
        <div class="card-body pt-0 pb-4 px-3">
          <span name="button" class="btn btn-link btn-lg text-lightgrey float-right pr-0 pt-3 blue-hover" id="cancelshowcase"><i class="fas fa-times font-20"></i></span>
            <h3 class="card-title md-3 mb-sm-5 mt-sm-4">Add showcase</h3>
            <form class="" action="/user/showcase/add" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="showcase_title" class="font-weight-bold">Add a Showcase image or video</label>
                    <div class="row">
                      <div class="col-6">
                        <span id="up_photo_empty"><i class="far fa-circle text-lightgrey"></i></span>
                       <span style="display: none" id="up_photo_selected"><i class="fas fa-dot-circle text-blue"></i></span>
                       <span class="ml-2">Upload a photo</span>
                      </div>
                      <div class="col-6 ">
                        <span id="up_video_empty"><i class="far fa-circle text-lightgrey"></i></span>
                        <span style="display: none" id="up_video_selected"><i class="fas fa-dot-circle text-blue"></i></span>
                        <span class="ml-2">Add a video</span>
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6 offset-sm-6 mt-sm-1">
                          <input type="text" name="video-url" class="w-100 mt-3" style="display: none" id="video_url" placeholder="Enter video URL">
                        </div>
                      </div>


                </div>
                <div class="drop-photo mb-3">
                  <div class="drop-photo-container">
                    <div class="row">
                      <div class="col">
                        <i class="fas fa-cloud-upload-alt text-lightgrey font-48 mb-3"></i>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <h5 class="text-lightgrey font-weight-bold">Upload an Image</h5>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group d-none">
                    <label for="showcase_photo" class="font-weight-bold">Photo:</label>
                    <input type="file" name="showcase_photo">
                </div>
                <div class="form-group mt-4">
                    <label for="showcase_title" class="font-weight-bold p-0 mb-0">Title of project</label>
                    <input type="text" class="w-100" name="showcase_title" required placeholder="Enter a project title">
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                </div>
                <div class="form-group mt-4">
                    <label for="showcase_desc" class="font-weight-bold p-0 mb-0">Project description</label>
                    <textarea name="showcase_desc"  placeholder="Enter a description"></textarea>
                    {{-- <input type="text" class="w-100" name="showcase_desc" placeholder="Enter a description"> --}}
                </div>
                <div class="text-center">
                  <button type="submit" name="button" class="btn btn-blue btn-lg px-md-5 py-md-3 ls-25 w-md-50 mt-4">DONE</button>
                </div>
            </form>
        </div>
    </div>
</div>
