@if (session()->has('message'))
<script type="text/javascript">
    Swal.fire({
        type: 'success',
        text: '{{ session()->get('message') }}',
        showConfirmButton: false,
        timer: 2000
    })
</script>

@endif
