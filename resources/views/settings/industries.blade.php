@extends('backend')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col pt-5 pb-2">
                <div class="tw-w-full d-flex justify-content-end">
                    <button type="button" class="btn btn-red tw-rounded tw-border-0 btn-sm" id="add_industry"><i class="fas fa-plus tw-px-1"></i> Industry</button>
                    <form action="/industry/add" class="d-none tw-border tw-border-gray-400 tw-p-3 tw-rounded tw-bg-gray-100 tw-shadow" id="add_industry_form" method="POST">
                        <button class="btn btn-link float-right tw--mr-4 tw--mt-4" id="industry_close"><i class="fas fa-times"></i></button>
                        @csrf
                        <label for="industry_name" class="tw-text-xs tw-text-gray-910 tw-font-bold">Add new industry</label><br>
                        <input type="text" name="industry_name" class="tw-mr-3" placeholder="Industry name">
                        <button type="submit" class="btn btn-danger"><i class="fas fa-plus tw-px-1"></i></button>
                    </form>
                    
                </div>
            </div>
            <div class="col">

            </div>
        </div>
        <div class="row">
            <div class="col">
                 <div class="accordion" id="industry_accordion">
                    @php
                        $count = 0;
                    @endphp
                     @foreach ($industries as $industry)
                     <div class="card">
                        <div class="card-header tw-bg-gray-910" id="heading{{ $count }}">
                            <div class="row">
                                <div class="col">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left tw-text-gray-100 tw-text-xl {{ $count == 0 ? "" : "collapsed" }}" type="button" data-toggle="collapse" data-target="#collapse{{ $count }}" aria-expanded="true" aria-controls="collapse{{ $count }}">
                                          {{ $industry->name }}
                                        </button>
                                      </h2>
                                </div>
                                <div class="tw-w-64 tw-mr-3 tw-mt-2 tw-text-right">
                                    <div id="add_subindustry_buttons_{{ $industry->id }}">
                                        <button class="btn btn-success btn-sm" onclick="add_subindustry({{ $industry->id }})"><i class="fas fa-plus"></i></button>
                                    <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    </div>
                                    <div class="d-flex d-none" id="add_subindustry_form_{{ $industry->id }}">
                                        <input type="text" class="form-control" name="subindustry_name_{{ $industry->id }}" id="subindustry_name_{{ $industry->id }}">
                                        <button class="btn btn-success tw-px-3 tw-py-1 tw-rounded tw-border-0 btn-sm tw-ml-1" onclick="subindustry_add_btn({{ $industry->id }})"><i class="fas fa-plus"></i></button>
                                        <button class="btn btn-danger tw-px-3 tw-py-1 tw-rounded tw-border-0 btn-sm tw-ml-1" onclick="subindustry_cancel_btn({{ $industry->id }})"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                    
                        <div id="collapse{{ $count }}" class="collapse {{ $count == 0 ? "show" : "" }}" aria-labelledby="headingOne" data-parent="#industry_accordion">
                          <div class="card-body">
                            <ul class="nav flex-column">
                                @foreach ($industry->subindustry as $subindustry)
                                <li class="nav-item ">
                                    <div class="row">
                                        <div class="col ">
                                            <div class="nav-link active" href="#">{{ $subindustry->name }}</div>
                                        </div>
                                        <div class="col-2 ">
                                            <button class="btn btn-danger btn-sm" onclick="delete_subindustry({{ $subindustry->id }})">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>   
                                    </div>
                                </li>
                                @endforeach
                              </ul>
                          </div>
                        </div>
                      </div>
                      @php
                          $count++;
                      @endphp
                     @endforeach
                    
                  </div>
            </div>
            <div class="col">

            </div>
        </div>
    </div>

    <script>
        function delete_subindustry(id) {

            toastr.error("<br /><button type='button' id='confirmationRevertYes' class='btn tw-bg-white clear'>Yes</button><button type='button' id='confirmationRevertNo' class='btn tw-bg-white clear tw-ml-2'>No</button>",'<h4 class="tw-text-gray-100">Delete sub industry?</h4>',
            {
                closeButton: false,
                allowHtml: true,
                positionClass: "toast-top-full-width",
                onShown: function (toast) {
                    $("#confirmationRevertYes").click(function(){
                        axios.post('/api/subindustry/delete', {
                            subindustry_id: id,
                        })
                        .then(function (response) {
                            console.log(response);
                            toastr.options.onHidden = function(){
                            // this will be executed after fadeout, i.e. 2secs after notification has been show
                            window.location.reload();
                            };
                            toastr.success('Sub industry successfully deleted.',
                            {
                                timeOut: 500,
                                fadeOut: 500,
                                positionClass: "toast-top-center",
                            });
                            
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    });
                    $("#confirmationRevertNo").click(function () {
                        toastr.clear();
                    });
                    }
            });
            
        }

        $("#add_industry").click(function () {
            $("#add_industry_form").removeClass('d-none');
            $("#add_industry").addClass('d-none');
            $("input[name='industry_name']").prop('required', true);
        })

        $("#industry_close").click(function (e) {
            e.preventDefault();
            $("#add_industry_form").addClass('d-none');
            $("#add_industry").removeClass('d-none');
            $("input[name='industry_name']").prop('required', false);
            $("input[name='industry_name']").val("");
        })

        function add_subindustry(industry_id) {
            var name = "#subindustry_name_" + industry_id;
            $("#add_subindustry_buttons_" + industry_id).addClass('d-none');
            $("#add_subindustry_form_" + industry_id).removeClass('d-none');

            $(name).focus();
        }

        function subindustry_cancel_btn(industry_id) {
            var name = "#subindustry_name_" + industry_id;

            $(name).val("");
            $("#add_subindustry_buttons_" + industry_id).removeClass('d-none');
            $("#add_subindustry_form_" + industry_id).addClass('d-none');
        }

        function subindustry_add_btn(industry_id) {
            var name = "#subindustry_name_" + industry_id;

            var subindustry = $(name).val();

            axios.post('/api/subindustry/add', {
                subindustry: subindustry,
                industry_di: industry_id,
            })
            .then(function (response) {
                $("#add_subindustry_buttons_" + industry_id).removeClass('d-none');
                $("#add_subindustry_form_" + industry_id).addClass('d-none');
                $(name).val("");
            })
            .catch(function (error) {
                console.log(error);
            });

            
        }
    </script>
@endsection

