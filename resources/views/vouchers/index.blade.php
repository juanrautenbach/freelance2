@extends('backend')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 my-2">
            <div class="card tw-p-3">
                {{-- <div class="card-header">
                    <h4 class="card-title">Users</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div> --}}
                <div class="card-content collapse show">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12">
                             @if ($vouchers->count() > 0)
                             <div class="row">
                                <div class="col">
                                <h4 class="">Vouchers</h4>
                                </div>
                                <div class="col text-right">
                                <button type="button" class="btn btn-red btn-sm" data-toggle="modal" data-target="#defaultSize">Create voucher</button>
                                </div>
                             </div>
                                
                            @endif
                                @php
                                $time = time();
                                @endphp
                            </div>
                            <div class="col text-right">
                                
                                <div class="modal fade text-left" id="defaultSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                      <div class="modal-content px-2">
                                          <div class="modal-header">
                                              <h4 class="modal-title" id="myModalLabel1">Create new voucher</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true"><i class="ft-x text-dark"></i></span>
                                              </button>
                                          </div>
                                          <div class="modal-body">
                                              <form class="vouchers" action="/vouchers" method="post">
                                                  @csrf
                                                  <div class="d-flex justify-content-between">
                                                      <div class="mt-2 d-inline-block bg-dark text-white py-2 px-3">
                                                          <div id="output" class="font-large-2"></div>
                                                          <small>This is random generated</small>
                                                      </div>
                                                      <div class="w-25 mt-3">
                                                          <div class="d-flex align-items-center justify-content-center h-100 text-center">
                                                              <h4>
                                                                  OR
                                                              </h4>

                                                          </div>
                                                      </div>
                                                      <div class="mt-3 d-inline-block py-2 px-3">
                                                          <div>
                                                              <input type="text" name="voucher_manual" maxlength="6" class="font-weight-bold text-monospace display-4 w-100">
                                                          </div>
                                                          <small>Here you can make the voucher MAX 6 Characters</small>
                                                      </div>
                                                  </div>

                                                  <div class="row">
                                                      <div class="col mt-3">
                                                          <div class="row mx-auto">
                                                              <div class="col">
                                                              <div class="row">
                                                                  <div class="col-md-2">
                                                                    <label for="" class=" py-1">Amount</label>
                                                                  </div>
                                                                  <div class="col-md-4">
                                                                    <input type="hidden" name="code" id="voucher_code">
                                                                      <div class="input-group mt-0 ">
                                                                          <div class="input-group-prepend">
                                                                              <span class="input-group-text bg-white border-0 px-0 font-weight-bold" id="basic-addon1">R</span>
                                                                          </div>
                                                                          <input type="number" name="amount" class="form-control" aria-label="Username" aria-describedby="basic-addon1" required>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              </div>
                                                            </div>
                                                              <div class="row mx-auto">
                                                                <div class="col">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                            <label for="" class="py-1">How many?</label>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                            <input type="number" name="number" class="form-control" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    
                                                                </div>
                                                              </div>
                                                              <div class="row mx-auto">
                                                                <div class="col">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                            <label for="" class="py-1">Valid Until</label>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                            <input type="date" class="form-control ml-1" name="valid_until" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                              </div>
                                                              </div>
                                                              
                                                          </div>

                                                      </div>
                                                  {{-- </div> --}}
                                                  <div class="row mx-auto">
                                                      <div class="col">
                                                          <input type="hidden" name="factor" value="fixed">
                                                          <button type="submit" class="btn btn-red my-3 px-3" name="button">Create</button>
                                                          <button type="button" class="btn btn-link text-uppercase px-3" data-dismiss="modal" aria-label="Close">
                                                                Cancel
                                                            </button>
                                                          
                                                      </div>
                                                  </div>


                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @if ($vouchers->count() > 0)
                                <table class="table table-responsive">
                                    <tr>
                                        <th class="tw-font-bold nowrap">Code</th>
                                        <th class="tw-font-bold nowrap">Value</th>
                                        <th class="tw-font-bold nowrap">Valid until</th>
                                        <th class="tw-font-bold nowrap">Created by</th>
                                        <th class="tw-font-bold nowrap">Status</th>
                                        <th class="tw-font-bold nowrap">Created</th>
                                    </tr>
                                    @foreach ($vouchers as $voucher)
                                    <tr>
                                        <td class="font-weight-bold tw-font-mono nowrap">{{$voucher->code}}</td>
                                        <td class="nowrap">{{$voucher->amount}}</td>
                                        <td class="nowrap">{{$voucher->valid_until}}</td>
                                        <td class="nowrap">{{$voucher->created_by}}</td>
                                        <td class="nowrap">{{ $voucher->status }}</td>
                                        <td class="nowrap">{{$voucher->created_at->diffForHumans()}}</td>
                                    </tr>
                                    @endforeach

                                </table>
                                @else
                                <div class="container py-3">
                                <div class="row">
                                    <div class="col pr-5" style="line-height: 28px">
                                        <h2 class="font-weight-bold mb-2">
                                            Create a discount voucher
                                        </h2>
                                        <p>
                                            Let's create some vouchers and spread the love. This will increase the love to our Freelancers and then they will spread the love where they go. It all starts with us.
                                        </p>
                                        <button type="button" class="btn btn-red mt-2" data-toggle="modal" data-target="#defaultSize">Create voucher</button>
                                    </div>
                                    <div class="col text-center">
                                        <img src="/img/SVG/voucher.svg" style="max-width: 300px" alt="">
                                    </div>
                                    
                                </div>
                                </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




{{-- <div class="container pt-3">
    <div class="new-voucher-container" id="new-voucher-container" style="display:none">
        <div class="card new-voucher-form py-2 px-3 shadow-sm">
            <div class="row">
                <div class="col mt-2">
                    <h4 class="d-inline"></h4><span class="d-inline float-right"><i class="fas fa-times pointer-cursor" onclick="closeBtn()"></i></span>
                </div>

            </div>


        </div>

    </div>

</div> --}}

@endsection
