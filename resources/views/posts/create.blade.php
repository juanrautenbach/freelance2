@extends('backend')
@section('content')
@php
$user = \Auth::user();
@endphp

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<div class="container brief_post">
    <form class="" action="/posts" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card my-4  border-0 shadow px-4 py-1">
            <div class="row pt-3">
                <div class="col">
                    <h4 class="mb-1">Create a new job post</h4>
                    <div class="row">
                      <div class="col">
                        <div class="form-group mt-1">
                          <label for="post_title" class="font-weight-bold mb-2 ml-0">Brief title:</label>
                          <input  class="w-100 mb-3 d-inline fs-24" name="post_title" type="text" placeholder="Give us a nice one line so you job post stands out!" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                              <label for="description" class="font-weight-bold ml-0 mb-2">Enter brief description:</label><br>
                                <textarea class="w-100" name="description" rows="10" placeholder="Describe the job you need to get done" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="industry" class="font-weight-bold mb-3 ml-0">Industry</label>
                            <select class="mb-5 no-radius pl-1" name="industry" id="industry_id">
                                <option value="">Select industry</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="subindustries" class="font-weight-bold mb-3 ml-0">Sub-industries</label>
                            <select class="" id="subcategory_id_input">
                                <option value="">Sub-industries</option>
                            </select>
                            <div class="" id="subcategory_id">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mb-1">
                      <div class="col">
                        <div class="form-group mt-0 pt-0">
                          <label for="brief_deadline" class="font-weight-bold ml-0 mt-0 pt-0">Deadline:</label>
                            <input class="w-50" name="brief_deadline" type="text" id="brief_deadline" width="276" placeholder="Deadline" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                            <label for="budget" class="font-weight-bold ml-0 mb-2">Budget:</label><br>
                            <input type="text" id="budget-letter" value="R">
                            <input type="number" name="brief_budget" id="budget" placeholder="Estimated budget" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                            <label for="brief_contact" class="font-weight-bold ml-0 mb-2">How do we contact you?</label><br>
                            <input type="text" class="w-100 mb-3" name="brief_contact" placeholder="Give your name and perhaps a number or email.." required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                            <label for="brief_attachment" class="font-weight-bold ml-0">Any attachments? <small>(.pdf, .doc, .docx, .xls, .xlsx)</small> </label><br>
                            <input type="file" class="filestyle" name="brief_attachment[]" id="attachment" multiple data-placeholder="Attach reference or drop here" accept=".pdf, .doc, .docx, .xls, .xlsx">
                        </div>
                      </div>
                    </div>




                </div>
            </div>

            <div class="row mb-3">
                {{-- <div class="col">
                  <label for="location" class="font-weight-bold">Where are you based?</label><br>
                    <input type="text" name="location" class="w-50" placeholder="Where are you based?">
                </div> --}}
                <div class="form-group col-md-4">
                      <label class="font-weight-bold mb-2" for="cities">Where are you based:</label><br>
                    <input type="text" name="city" class="pb-n3 form-control" id="user_city">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <button type="submit" class="btn btn-red btn-lg text-uppercase px-4 ls-25" name="button">Submit</button>
                
                    <a href="/dashboard" class="btn btn-link text-dark btn-lg px-4 font-weight-bold" name="button">Discard</a>
                </div>
            </div>
        </div>
    </form>
</div>

<script src="/js/dynamicdropdown.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/bootstrap-filestyle.min.js"> </script>
<script type="text/javascript">

  $('#brief_deadline').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd mmmm yyyy',
            showRightIcon: false
        });

  $("#attachment").filestyle({
    dragdrop: true,
    badge: true,
    htmlIcon: '<i class="fas fa-paperclip"></i>',
    text: "<br><br>  Drag files here or click to browse",
    btnClass: "btn-drag-n-drop",
    input: false,
    badgeName: "badge-red"
  });


</script>

@endsection
