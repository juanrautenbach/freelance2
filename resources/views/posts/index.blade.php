@extends('backend')
@section('content')
@php
    $user = Auth::user();
@endphp
<div class="container-fluid">
  <div class="row mx-5 mt-3">
    <div class="col">
      <h2>Job posts</h2>
    </div>
    <div class="col text-right">
      <a href="/posts/create" class="btn btn-red waves-effect waves-light">Create new</a>
    </div>
  </div>
  <div class="row">
    <div class="col mt-3">
      @foreach ($user->jobpost as $post)
      <a href="/post/{{ $post->id }}" class="">
        <div class="card shadow-sm mb-3 mx-5 card-hover-border">
          <div class="row pt-2 px-2">
            <div class="col">
              <h3>{{ $post->title }}</h3>
            </div>
            <div class="col text-right text-dark">
              <small class="">DEADLINE:</small>
              {{ $post->deadline }}
            </div>

          </div>
          <div class="row px-2">
            <div class="col ">
              <p class="text-dark"><span class="font-weight-bold text-dark">Location: </span>{{ $post->location }}  </p>


            </div>
            <div class="col text-right">
              {{-- <a href="/post/{{ $post->id }}" class="btn btn-red">View more details</a> --}}
              {{-- <form class="" action="/post/message/view" method="post">
                @csrf
                <input type="hidden" name="post_id" value="{{ $post->id }}">
                <button type="submit" class="btn btn-red">View more details</button>
              </form> --}}
            </div>
          </div>
          <div class="row px-2 pb-0">
            <div class="col text-right">
              <span class="text-dark"><h3> Post views <span class="font-weight-bold ml-2">{{ $post->post_clicks }}</span></h3> </span>
            </div>
          </div>

        </div>
      </a>
      @endforeach
    </div>

  </div>
</div>

@endsection
