@extends('backend')
@section('content')
@php
    $user = \Auth::user();
@endphp
<div class="container-fluid">
  <div class="row">
    <div class="col m-5">
      <div class="card border-0 shadow-sm">
        <div class="row px-2 pt-2">
          <div class="col">
            <h2 class="mb-1">{{ $post->title }}</h2>
          </div>
          <div class="col text-right">
            {{-- <small class="text-muted">DEADLINE:</small>
            {{ $post->deadline }} <br> --}}
            <small class="text-dark">DEADLINE:</small>
            {{ $post->deadline }}
          </div>
        </div>
        <div class="row px-2">
          <div class="col ">
            <p class="mb-1"><span class="font-weight-bold">Location: </span>{{ $post->location }}  </p>
            <p class="mb-1"><span class="font-weight-bold">Posted by: </span>{{ $finder->name }}  </p>
          </div>
          <div class="col">
            <p class="mb-1"><span class="font-weight-bold">Budget: </span>R {{ $post->budget }}  </p>
          </div>
          <div class="col text-right d-none">
            <button type="button" class="btn btn-red" name="button">Get in touch! </button>

          </div>
        </div>
        <div class="row pl-2">
          <div class="col">
            <p class="font-weight-bold">Description:</p>
            <p>{{ $post->description }}</p>
          </div>
          <div class="col text-right mt-auto">
            @if ($user->role == "Finder")
            <div class="btn-group" role="group">
              <a class="btn btn-info btn-icon" name="button"><i class="fas fa-pencil-alt fs-24 text-white"></i></a>
              <a class="btn btn-danger btn-icon" name="button"><i class="fas fa-trash-alt fs-24 text-white"></i></a>
            </div>
            @else
                
            @endif
            
          </div>

        </div>
      </div>

    </div>

  </div>

</div>

@endsection
