@extends('backend')
@section('content')
@php
    $user = \Auth::user();
@endphp
  <!-- BEGIN: Content-->
  <div class="app-content content">
      <div class="sidebar-left">
          <div class="sidebar">
              <!-- app chat user profile left sidebar start -->
              <div class="chat-user-profile">
                  {{-- <header class="chat-user-profile-header text-center border-bottom">
                      <span class="chat-profile-close">
                          <i class="ft-x"></i>
                      </span>
                      <div class="my-2">

                          <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" class="round mb-1" alt="user_avatar" height="100" width="100">

                          <h5 class="mb-0">John Doe!</h5>
                          <span>Designer</span>
                      </div>
                  </header> --}}
                  {{-- <div class="chat-user-profile-content">
                      <div class="chat-user-profile-scroll">
                          <h6 class="text-uppercase mb-1">ABOUT</h6>
                          <p class="mb-2">It is a long established fact that a reader will be distracted by the readable content .</p>
                          <h6>PERSONAL INFORAMTION</h6>
                          <ul class="list-unstyled mb-2">
                              <li class="mb-25">email@gmail.com</li>
                              <li>+1(789) 950 -7654</li>
                          </ul>
                          <h6 class="text-uppercase mb-1">CHANNELS</h6>
                          <ul class="list-unstyled mb-2">
                              <li><a href="javascript:void(0);"># Devlopers</a></li>
                              <li><a href="javascript:void(0);"># Designers</a></li>
                          </ul>
                          <h6 class="text-uppercase mb-1">SETTINGS</h6>
                          <ul class="list-unstyled">
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-tag mr-50"></i>
                                      Add
                                      Tag</a></li>
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-star mr-50"></i>
                                      Important Contact</a>
                              </li>
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-image mr-50"></i>
                                      Shared
                                      Documents</a></li>
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-trash-2 mr-50"></i>
                                      Deleted
                                      Documents</a></li>
                              <li><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-x-circle mr-50"></i> Blocked
                                      Contact</a></li>
                          </ul>
                      </div>
                  </div> --}}
              </div>
              <!-- app chat user profile left sidebar ends -->
              <!-- app chat sidebar start -->
              <div class="chat-sidebar card">
                  <span class="chat-sidebar-close">
                      <i class="ft-x"></i>
                  </span>
                 
                  <div class="chat-sidebar-list-wrapper pt-2 mt-0">
                      <h6 class="px-2 pt-2 pb-25 mb-0">MESSAGES</h6>
                      <ul class="chat-sidebar-list" id="chat-sidebar-list">

                        {{-- @foreach (\Auth::user()->messages->unique('sender_name') as $message)

                          <li onclick="getMessages({{ $message->sender_id }}, {{ $message->user_id }})">
                              <div class="d-flex align-items-center">
                                  <div class="avatar m-0 mr-50">
                                  <a class="avatar m-0">
                                      <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                  </a>
                                      <span class="avatar-status-busy"></span>
                                  </div>
                                  <div class="chat-sidebar-name">
                                      <h6 class="mb-0">{{ $message->sender_name }}</h6>
                                  </div>
                              </div>
                          </li>
                        @endforeach --}}

                          
                      </ul>
                      @if(\Auth::user()->role == 'Admin')
                          <ul class="chat-sidebar-list">

                        <h6 class="px-2 pt-2 pb-25 mb-0">OTHER USERS</h6>
                            @foreach($users as $contact)
                                <li onclick="getMessages({{ $contact->id }}, {{ $user->id }})">
                              <div class="d-flex align-items-center">
                                  <div class="avatar m-0 mr-50">
                                  <a class="avatar m-0">
                                      <img src="{{ empty($contact->profile) ? "/img/FCPT_Logo.png" : $contact->profile->profile_pic}}" alt="avatar" height="36" width="36" />
                                  </a>
                                      <span class="avatar-status-busy"></span>
                                  </div>
                                  <div class="chat-sidebar-name">
                                      <h6 class="mb-0">{{ $contact->name }}</h6>
                                  </div>
                              </div>
                          </li>
                            @endforeach
                          
                      </ul>
                      @endif
                      
                  </div>
              </div>
              <!-- app chat sidebar ends -->

          </div>
      </div>
      <div class="content-right">
          <div class="content-header row">
          </div>
          <div class="content-overlay"></div>
          <div class="content-wrapper">
              <div class="content-body">
                  <!-- app chat overlay -->
                  <div class="chat-overlay"></div>
                  <!-- app chat window start -->
                  <section class="chat-window-wrapper">
                      <div class="chat-start" id="chat-start">
                          <span class="ft-message-square chat-sidebar-toggle chat-start-icon font-large-3 p-3 mb-1"></span>
                          <h4 class="d-none d-lg-block py-50 text-bold-500">Select a contact to start a chat!</h4>
                          <span class="btn btn-light-primary chat-start-text chat-sidebar-toggle d-block d-lg-none py-50 px-1">Start
                              Conversation!</span>
                      </div>
                      
                      <div class="chat-area d-none" id="chat-area">
                          <div class="chat-header" id="chat-header">
                              <header class="d-flex justify-content-between align-items-center px-1 py-75">
                                  <div class="d-flex align-items-center w-100">
                                      
                                      <div class="avatar chat-profile-toggle m-0 mr-1">
                                          
                                          <a class="avatar m-0">
                                              <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" id="avatar-photo" alt="avatar" height="36" width="36" />
                                          </a>

                                          <span class="avatar-status-busy"></span>
                                      </div>
                                      <div class="col">
                                      <div class="row">
                                          <div class="col">
                                            <h6 class="mb-0" id="message_user">Elizabeth Elliott</h6>
                                          </div>
                                          <div class="col text-right">
                                            <a class="btn btn-red btn-sm" id="button" href="#message_86">To bottom</a>
                                          </div>
                                      </div>
                                      </div>
                                      
                                      
                                  </div>
                                  
                              </header>
                          </div>
                          <!-- chat card start -->
                          <div class="card chat-wrapper shadow-none mb-0" >
                              <div class="card-content">
                                  <div class="card-body chat-container" style="overflow-y: scroll">
                                      <div class="chat-content" id="chat-content">
                                      
                                      </div>
                                  </div>
                              </div>
                              <div class="card-footer chat-footer px-2 py-1 pb-0 d-none" id="chat-footer">
                                  <form class="d-flex align-items-center" action="/messages" method="post">
                                  @csrf
                                      <input type="hidden" name="sender_id" value="">
                                      <input type="hidden" name="user_id" value="">
                                      <input type="hidden" name="sender_name" value="">
                                      <input type="hidden" name="user_name" value="">
                                      <input name="message_subject" type="hidden">
                                      <input type="text" class="form-control chat-message-send mx-1" name="message_text" placeholder="Type your message here...">
                                      <button type="submit" class="btn btn-primary glow send d-lg-flex"><i class="ft-play"></i>
                                          <span class="d-none d-lg-block mx-50">Send</span></button>
                                  </form>
                              </div>
                          </div>
                          <!-- chat card ends -->
                      </div>
                  </section>
                  <!-- app chat window ends -->

              </div>
          </div>
      </div>
  </div>

  <script>
    $(document).ready(function() {
        console.log( "ready!" );
        $('body').css('overflow-y', 'hidden');
        $('.app-content').css('overflow-y', 'hidden');
        $('.chat-sidebar').css('overflow-x', 'hidden');
        $('.chat-sidebar').css('overflow-y', 'scroll')
        messageUsers({{ \Auth::user()->id }});
    });
  </script>
  <!-- END: Content-->
{{-- {{ \Auth::user()->messages }} --}}

@endsection
