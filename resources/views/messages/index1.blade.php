@extends('dashboard')
@section('content')
<div class="row">
    @if (session()->has('message'))
    <script type="text/javascript">
        Swal.fire({
            type: 'success',
            text: '{{ session()->get('
            message ') }}',
            showConfirmButton: false,
            timer: 2000
        })
    </script>
    @endif
    @php
    $sent_users = $users;
    $trash_users = $users;
    @endphp
</div>
<div class="row">
    <div class="msg-sidebar border-right " style="min-height: 85vh;">
        @include('components.messages.sidebar')
    </div>
    <div class="msg-section pt-0 px-0 pb-0" style="min-height: 85vh;">
        @include('components.messages.inbox')
        @include('components.messages.sent')
        <section id="draft" style="display:none">
            Draft
        </section>
        @include('components.messages.trash')
    </div>
    <div class="message-panel mt-4 px-0" style="display:none">
        <div class="card border-0 shadow-sm px-4 py-4 mr-5">
            <h4 id="message-subject"></h4>
            <div class="row mt-4">
                <div class="col-2">
                    <p class="font-weight-bold mb-1">From: </p>
                    <p class="font-weight-bold mb-1">To: </p>
                </div>
                <div class="col">
                    <p class="mb-1" id="message-from"></p>
                    <p class="mb-1" id="message-to"></p>
                </div>
                <div class="col text-right">
                    <p id="message-time"></p>
                </div>
            </div>
            <hr class="px-0 mx-0">
            <div class="row mt-3">
                <div class="col">
                    <p id="message-body"></p>
                </div>
            </div>
            <div class="row mt-3" style="" id="message_bottom">
                <div class="col">
                    <button class="btn btn-link text-dark font-weight-bold pointer-cursor" id="replyBtn"><i class="fas fa-reply mr-2"></i>Reply</button>
                </div>
                <div class="col text-right">
                    <form method="post" class="d-inline-block" id="formTrash">
                        @csrf
                        <button class="btn btn-link text-red font-weight-bold pointer-cursor" type="submit" name="button"><i class="fas fa-trash-alt mr-2"></i>Trash</button>
                    </form>
                </div>
            </div>
            <div class="row mt-3" style="display:none" id="trash_bottom">
                <div class="col">
                </div>
                <div class="col text-right">
                    <form  method="post" class="d-inline-block" id="formRestore">
                        @csrf
                        <button class="btn btn-link text-red font-weight-bold pointer-cursor" type="submit" name="button"><i class="fas fa-undo mr-2"></i>Restore</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('components.messages.popup')
<script type="text/javascript">
    function showMessage(id) {
        $(".message-panel").show();
        $.ajax({
            url: "{{url('')}}/api/message/" + id,
            type: 'GET',
            dataType: 'json',
            success: function(message) {
                $("#message-from").text(message.sender_name);
                $("#message-to").text("Me");
                $("#message-subject").text(message.subject);
                $("#message-time").text(message.created_at);
                $("#message-body").text(message.messages);
                $("#message_post_id").val(message.post_id);
                $('#replyBtn').attr('onClick', 'replyMsg("'+message.sender_id+'", "'+message.subject+'");');
                $('#formRestore').attr('action', '/message/restore/' + message.id);
                $('#formTrash').attr('action', '/message/delete/' + message.id);
            }
        });
    }

    function showInbox() {
        $(".message-panel").hide();
        $("#inbox").show();
        $("#sent").hide();
        $("#draft").hide();
        $("#trash").hide();
        $("#message_bottom").show();
        $("#trash_bottom").hide();
    }

    function showSent() {
        $(".message-panel").hide();
        $("#inbox").hide();
        $("#sent").show();
        $("#draft").hide();
        $("#trash").hide();
        $("#message_bottom").show();
        $("#trash_bottom").hide();
    }

    function showDrafts() {
        $(".message-panel").hide();
        $("#inbox").hide();
        $("#sent").hide();
        $("#draft").show();
        $("#trash").hide();
    }

    function showTrash() {
        $(".message-panel").hide();
        $("#inbox").hide();
        $("#sent").hide();
        $("#draft").hide();
        $("#trash").show();
        $("#message_bottom").hide();
        $("#trash_bottom").show();
    }

    function showMsg() {
        $("#messagePanel").show();
        $("input[name=message_subject]").prop('readonly', false);
        $('#send_to').prop('disabled', false);
        $("#send_to").val('');
        $("input[name=message_subject]").val('');
    }

    function closeMsg() {
        $("#messagePanel").hide();
    }

    function replyMsg($id, $subject) {
        $("#messagePanel").show();
        $("input[name=message_subject]").val('re:' + $subject);
        $('#send_to').attr('disabled', 'disabled');
        $('#hidden_sent_to').prop('disabled', false);
        $("input[name=message_subject]").prop('readonly', true);
        $("#send_to").val($id);
        $('#hidden_sent_to').val($id);
    }
</script>
@endsection
