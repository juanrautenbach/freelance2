@extends('backend')
@section('content')
@php
    $user = \Auth::user();
@endphp
  <!-- BEGIN: Content-->
  <div class="app-content content" style="overflow-y: hidden">
      <div class="sidebar-left">
          <div class="sidebar">
              <!-- app chat user profile left sidebar start -->
              <div class="chat-user-profile d-none">
                  <header class="chat-user-profile-header text-center border-bottom">
                      <span class="chat-profile-close">
                          <i class="ft-x"></i>
                      </span>
                      <div class="my-2">

                          <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" class="round mb-1" alt="user_avatar" height="100" width="100">

                          <h5 class="mb-0">John Doe!</h5>
                          <span>Designer</span>
                      </div>
                  </header>
                  <div class="chat-user-profile-content">
                      <div class="chat-user-profile-scroll">
                          <h6 class="text-uppercase mb-1">ABOUT</h6>
                          <p class="mb-2">It is a long established fact that a reader will be distracted by the readable content .</p>
                          <h6>PERSONAL INFORAMTION</h6>
                          <ul class="list-unstyled mb-2">
                              <li class="mb-25">email@gmail.com</li>
                              <li>+1(789) 950 -7654</li>
                          </ul>
                          <h6 class="text-uppercase mb-1">CHANNELS</h6>
                          <ul class="list-unstyled mb-2">
                              <li><a href="javascript:void(0);"># Devlopers</a></li>
                              <li><a href="javascript:void(0);"># Designers</a></li>
                          </ul>
                          <h6 class="text-uppercase mb-1">SETTINGS</h6>
                          <ul class="list-unstyled">
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-tag mr-50"></i>
                                      Add
                                      Tag</a></li>
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-star mr-50"></i>
                                      Important Contact</a>
                              </li>
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-image mr-50"></i>
                                      Shared
                                      Documents</a></li>
                              <li class="mb-50 "><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-trash-2 mr-50"></i>
                                      Deleted
                                      Documents</a></li>
                              <li><a href="javascript:void(0);" class="d-flex align-items-center"><i class="ft-x-circle mr-50"></i> Blocked
                                      Contact</a></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <!-- app chat user profile left sidebar ends -->
              <!-- app chat sidebar start -->
              <div class="chat-sidebar card">
                  <span class="chat-sidebar-close">
                      <i class="ft-x"></i>
                  </span>
                 
                  <div class="chat-sidebar-list-wrapper pt-2 mt-0">
                      <h6 class="px-2 pt-2 pb-25 mb-0">MESSAGES</h6>
                      <ul class="chat-sidebar-list">

                        @foreach (\Auth::user()->messages->unique('sender_name') as $message)
                          <li onclick="getMessages({{ $message->sender_id }}, {{ $message->user_id }})">
                              <div class="d-flex align-items-center">
                                  <div class="avatar m-0 mr-50">
                                  <a class="avatar m-0">
                                      <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                      {{-- @if (empty($user->profile->profile_pic) )
                                      <span class="avatar avatar-online badge bg-blue rounded-circle text-white">
                                          @php
                                          $name = explode(" ", $user->name);
                                          if (count($name) == 1) {
                                          echo substr($name[0], 0, 2);
                                          }elseif (count($name) == 2) {
                                          echo substr($name[0], 0, 1) . substr($name[1], 0, 1);
                                          }elseif (count($name) == 3) {
                                          echo substr($name[0], 0, 1) . substr($name[2], 0, 1);
                                          }

                                          @endphp
                                      </span>
                                      @else
                                      <img src="{{ $user->profile->profile_pic }}" alt="User Profile Pic" class="rounded-circle" height="35px">
                                      @endif --}}

                                  </a>

                                      <span class="avatar-status-busy"></span>
                                  </div>
                                  <div class="chat-sidebar-name">
                                      <h6 class="mb-0">{{ $message->sender_name }}</h6>
                                  </div>
                              </div>
                          </li>
                        @endforeach

                          
                      </ul>
                      
                  </div>
              </div>
              <!-- app chat sidebar ends -->

          </div>
      </div>
      <div class="content-right h-100">
          <div class="content-header row">
          </div>
          <div class="content-overlay"></div>
          <div class="content-wrapper">
              <div class="content-body">
                  <!-- app chat overlay -->
                  <div class="chat-overlay"></div>
                  <!-- app chat window start -->
                  <section class="chat-window-wrapper">
                      <div class="chat-start d-none">
                          <span class="ft-message-square chat-sidebar-toggle chat-start-icon font-large-3 p-3 mb-1"></span>
                          <h4 class="d-none d-lg-block py-50 text-bold-500">Select a contact to start a chat!</h4>
                          <button class="btn btn-light-primary chat-start-text chat-sidebar-toggle d-block d-lg-none py-50 px-1">Start
                              Conversation!</button>
                      </div>
                      
                      <div class="chat-area h-100 ">
                          <div class="chat-header">
                              <header class="d-flex justify-content-between align-items-center px-1 py-75">
                                  <div class="d-flex align-items-center">
                                      <div class="chat-sidebar-toggle d-block d-lg-none mr-1">
                                        <i class="ft-align-justify font-large-1 cursor-pointer"></i>
                                      </div>
                                      <div class="avatar chat-profile-toggle m-0 mr-1">
                                          {{-- <img src="../../../app-assets/images/portrait/small/avatar-s-26.png" class="cursor-pointer" alt="avatar" height="36" width="36" /> --}}
                                          <a class="avatar m-0">
                                              <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                          </a>

                                          <span class="avatar-status-busy"></span>
                                      </div>
                                      <h6 class="mb-0" id="message_user">Elizabeth Elliott</h6>
                                  </div>
                                  <div class="chat-header-icons d-none">
                                      <span class="chat-icon-favorite">
                                          <i class="ft-star font-medium-5 cursor-pointer"></i>
                                      </span>
                                      <span class="dropdown">
                                          <i class="ft-more-vertical font-medium-4 ml-25 cursor-pointer dropdown-toggle nav-hide-arrow cursor-pointer" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
                                          </i>
                                          <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                              <a class="dropdown-item" href="JavaScript:void(0);"><i class="ft-tag mr-25"></i> Pin to top</a>
                                              <a class="dropdown-item" href="JavaScript:void(0);"><i class="ft-trash-2 mr-25"></i> Delete chat</a>
                                              <a class="dropdown-item" href="JavaScript:void(0);"><i class="ft-x-circle mr-25"></i> Block</a>
                                          </span>
                                      </span>
                                  </div>
                              </header>
                          </div>
                          <!-- chat card start -->
                          <div class="card chat-wrapper shadow-none mb-0 h-100" style="max-height: 75vh; overflow-y: scroll">
                              <div class="card-content">
                                  <div class="card-body chat-container mh-100">
                                      <div class="chat-content">
                                      {{-- @foreach(\Auth::user()->messages->take(20) as $chat)

                                      @if($chat->sender_id == \Auth::user()->id)
                                            <div class="chat chat-left">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      
                                                      @if (empty($user->profile->profile_pic) )
                                                      <span class="avatar avatar-online badge bg-blue rounded-circle text-white">
                                                          @php
                                                          $name = explode(" ", $user->name);
                                                          if (count($name) == 1) {
                                                          echo substr($name[0], 0, 2);
                                                          }elseif (count($name) == 2) {
                                                          echo substr($name[0], 0, 1) . substr($name[1], 0, 1);
                                                          }elseif (count($name) == 3) {
                                                          echo substr($name[0], 0, 1) . substr($name[2], 0, 1);
                                                          }

                                                          @endphp
                                                      </span>
                                                      @else
                                                      <img src="{{ $user->profile->profile_pic }}" alt="User Profile Pic" class="rounded-circle" height="35px">
                                                      @endif

                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message text-left">
                                                      {{ $chat->messages }}
                                                      <span class="chat-time">{{ $chat->created_at->toDateTimeString() }}</span>
                                                  </div>
                                                
                                              </div>
                                          </div>
                                      @else
                                         <div class="chat">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      
                                                      @if (empty($user->profile->profile_pic) )
                                                      <span class="avatar avatar-online badge bg-blue rounded-circle text-white">
                                                          @php
                                                          $name = explode(" ", $user->name);
                                                          if (count($name) == 1) {
                                                          echo substr($name[0], 0, 2);
                                                          }elseif (count($name) == 2) {
                                                          echo substr($name[0], 0, 1) . substr($name[1], 0, 1);
                                                          }elseif (count($name) == 3) {
                                                          echo substr($name[0], 0, 1) . substr($name[2], 0, 1);
                                                          }

                                                          @endphp
                                                      </span>
                                                      @else
                                                      <img src="{{ $user->profile->profile_pic }}" alt="User Profile Pic" class="rounded-circle" height="35px">
                                                      @endif

                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message text-left">
                                                      {{ $chat->messages }}
                                                      <span class="chat-time">{{ $chat->created_at->toDateTimeString() }}</span>
                                                  </div>
                                              </div>
                                          </div>


                                      @endif
                                          
                                      @endforeach --}}
                                          {{-- <div class="chat">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>How can we help? We're here for you! 😄</p>
                                                      <span class="chat-time">7:45 AM</span>
                                                  </div>
                                              </div>
                                          </div> --}}
                                          {{-- <div class="chat chat-left">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-26.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>Hey John, I am looking for the best admin template.</p>
                                                      <p>Could you please help me to find it out? 🤔</p>
                                                      <span class="chat-time">7:50 AM</span>
                                                  </div>
                                                  <div class="chat-message">
                                                      <p>It should be Bootstrap 4 🤩 compatible.</p>
                                                      <span class="chat-time">7:58 AM</span>
                                                  </div>
                                              </div>
                                          </div> --}}
                                          {{-- <div class="badge badge-pill badge-light-secondary my-1">Yesterday</div> --}}
                                          {{-- <div class="chat">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>Absolutely!</p>
                                                      <span class="chat-time">8:00 AM</span>
                                                  </div>
                                                  <div class="chat-message">
                                                      <p>modern admin is the responsive bootstrap 4 admin template.</p>
                                                      <span class="chat-time">8:01 AM</span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="chat chat-left">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-26.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>Looks clean and fresh UI. 😃</p>
                                                      <span class="chat-time">10:12 AM</span>
                                                  </div>
                                                  <div class="chat-message">
                                                      <p>It's perfect for my next project.</p>
                                                      <span class="chat-time">10:15 AM</span>
                                                  </div>
                                                  <div class="chat-message">
                                                      <p>How can I purchase 🤑 it?</p>
                                                      <span class="chat-time">10:18 AM</span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="chat">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>Thanks 🤝 , from ThemeForest.</p>
                                                      <span class="chat-time">10:20 AM</span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="chat chat-left">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-26.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>I will purchase it for sure. 👍</p>
                                                      <span class="chat-time">3:32 PM</span>
                                                  </div>
                                                  <div class="chat-message">
                                                      <p>Thanks.</p>
                                                      <span class="chat-time">3:33 PM</span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="chat">
                                              <div class="chat-avatar">
                                                  <a class="avatar m-0">
                                                      <img src="../../../app-assets/images/portrait/small/avatar-s-11.png" alt="avatar" height="36" width="36" />
                                                  </a>
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-message">
                                                      <p>Great, Feel free to get in touch on</p>
                                                      <span class="chat-time">3:34 AM</span>
                                                  </div>
                                                  <div class="chat-message">
                                                      <p>https://pixinvent.ticksy.com/</p>
                                                      <span class="chat-time">3:35 AM</span>
                                                  </div>
                                              </div>
                                          </div> --}}
                                      </div>
                                  </div>
                              </div>
                              <div class="card-footer chat-footer px-2 py-1 pb-0">
                                  <form class="d-flex align-items-center" action="/messages" method="post">
                                  @csrf
                                  {{-- <form class="d-flex align-items-center" onsubmit="chatMessagesSend();" action="javascript:void(0);"> --}}
                                      {{-- <i class="ft-user cursor-pointer"></i>
                                      <i class="ft-paperclip ml-1 cursor-pointer"></i> --}}
                                      <input name="message_subject" type="hidden">
                                      <input type="text" class="form-control chat-message-send mx-1" name="message_text" placeholder="Type your message here...">
                                      <button type="submit" class="btn btn-primary glow send d-lg-flex"><i class="ft-play"></i>
                                          <span class="d-none d-lg-block mx-50">Send</span></button>
                                  </form>
                              </div>
                          </div>
                          <!-- chat card ends -->
                      </div>
                  </section>
                  <!-- app chat window ends -->
                  <!-- app chat profile right sidebar start -->
                  <section class="chat-profile">
                      <header class="chat-profile-header text-center border-bottom">
                          <span class="chat-profile-close">
                              <i class="ft-x"></i>
                          </span>
                          <div class="my-2">

                              <img src="../../../app-assets/images/portrait/small/avatar-s-26.png" class="round mb-1" alt="chat avatar" height="100" width="100">

                              <h5 class="app-chat-user-name mb-0">Elizabeth Elliott</h5>
                              <span>Devloper</span>
                          </div>
                      </header>
                      <div class="chat-profile-content p-2">
                          <h6 class="mt-1">ABOUT</h6>
                          <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                          <h6 class="mt-2">PERSONAL INFORMATION</h6>
                          <ul class="list-unstyled">
                              <li class="mb-25">email@gmail.com</li>
                              <li>+1(789) 950-7654</li>
                          </ul>
                      </div>
                  </section>
                  <!-- app chat profile right sidebar ends -->

              </div>
          </div>
      </div>
  </div>
  <!-- END: Content-->
{{-- {{ \Auth::user()->messages }} --}}

@endsection
