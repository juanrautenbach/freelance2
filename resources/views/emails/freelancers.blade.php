@extends('backend')
@section('content')
    <div class="container-fluid"></div>
    <div class="row">
        <div class="col m-3">
            <div class="card p-3">
                <div class="row">
                    <div class="col">
                        <h2>Email Freelancers</h2>
                        <form action="/send/email/freelancers" method="post">
                            @csrf
                            <input type="text" class="form-control w-50 mb-3" placeholder="Search...">
                            <div class="border p-1 mb-2" id="freelancer-container">
                               {{-- <span> <input type="checkbox" name="freelancers" class="name mx-2" value="Juan Rautenbach">Juan Rautenbach </span> --}}
                            </div>
                            <button type="submit" class="btn btn-red">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        $( document ).ready(function() {
            axios.get('/api/users/all/freelancers')
                .then(function (response) {
                    // handle success
                    console.log(response.data);
                    var users = response.data;
                    var html = "";

                    for (let index = 0; index < users.length; index++) {
                        var name = users[index].name;
                        // console.log(name);
                        html += `
                        <span> <input type="checkbox" name="freelancers[]" class="name mx-2 mb-1" value="${name}">${name}</span><br>
                        
                        `;
                        
                    }

                    $("#freelancer-container").append(html);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        });
        
    </script>
@endsection
