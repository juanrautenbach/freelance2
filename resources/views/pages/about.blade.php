@extends('front')
@section('content')
  <div class="container my-5 pt-5">
    <div class="row pt-4">
      <div class="col">
        <h1> <strong>ABOUT FREELANCE CAPE TOWN</strong></h1>
        <p>Welcome to Freelance Cape Town; proud to be Cape Town’s #1 Freelance and outsourcing platform.</p>
        <p>We’re a small, dynamic team with 3 major goals:</p>
        <ol class="about-list">
          <li>Increase Cape Town’s Freelance industry</li>
          <li>Create more opportunities for our local creatives and entrepreneurs</li>
          <li>Make our Freelance Finders (clients) happy</li>
        </ol>
        <p>Since September 2014 our platform has become the hotspot where Cape Town freelancers get connected to freelance finders from all over the world.</p>
        <p> <strong>Why did we start FCT?</strong> <br> We realized that there is more to life than a 9-5 job and that you don’t need to sell your soul to a corporate who doesn’t really value you or your future. The idea that corporate provides security is a façade when we see all the retrenchments taking place around us. So we felt it our obligation to create a platform for local creatives and entrepreneurs on which they could showcase their skills and connect with potential clients. This leads to a brilliant group of credible freelancers that can assist clients with whatever business needs they might have.</p>
        <p> <strong>How did we find our gap?</strong><br>During 2014 we knew that the only way to become free from the corporate chains were to outsource and sell our creative skills and earn a living from month to month. The question was: <i> Where can I advertise my skills and not have to compete with millions of other freelancers on a global scale?</i> </p>
        <p>When we launched FCT we knew we had something special. We became a hyper local platform in Cape Town and after 4 years and more than 3 000 "client to freelancer” engagements via our site, we are confident to share that FCT has made a lasting difference in the lives of Cape Town freelancers and those starting their freelance journeys.</p>
        <p>This is purely just the start, as we are now working on more functionality within the site to make this platform even more beneficial for our freelancers and freelance finders.</p>
        <p>FCT has an open door policy, so if you have suggestions/criticism or you’d like more information regarding our platform, give us a shout: info@freelancecapetown.com</p>
        <p>The future of job creators doesn’t lie with corporates; it lies with Startups. We believe that if you manage your freelance business well, you’ll grow and soon you’ll start employing people; becoming a job creator… now we’re talking.</p>
        <p>Let’s stay connected,</p>
        <h4> <strong>Marius, Erin, Dawid and Karel</strong> </h4>
      </div>

    </div>

  </div>

@endsection
