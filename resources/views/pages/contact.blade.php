@extends('front')
@section('content')
  <div class="container my-5 pt-5">
    <div class="card p-5 border-0 shadow bg-pure-white">


    <div class="row pt-4">
      <div class="col">
        <h1><strong>CONTACT DETAILS</strong></h1>
        <p>Please fill in the contact form below and we'll get back to you as soon as possible.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-6">
        <form class="" action="/contact" method="post">
          @csrf
          <div class="form-group col-lg-10">
            <label for="name"> <strong>Name & Surname</strong> </label>
            <input type="text" class="form-control" name="name" placeholder="John Smith">
          </div>
          <div class="form-group col-lg-10">
            <label for="email"> <strong>Email Address</strong> </label>
            <input type="email" class="form-control" name="email" placeholder="example@example.com">
          </div>
          <div class="form-group col-lg-10">
            <label for="message"> <strong>Message</strong> </label>
            <textarea class="form-control" name="message" rows="8" placeholder="I have something to say..."></textarea>
          </div>
          <div class="row">
            <div class="col-7">
              <p class="ml-3 mt-2">8 + 6 = <input type="text" class="p-1 ml-2 text-center" name="answer" placeholder="?" style="width: 50px; font-size: 18px;" id="answer"></p>
            </div>
            <div class="col text-lg-center text-md-right">
              <button type="submit" class="btn btn-freelance" name="button" disabled id="submitBtn">Send</button>
            </div>
          </div>
        </form>
      </div>
      <div class="d-none d-lg-block col-lg-6 contact-sidebar pt-3">
        <h4 class="font-weight-bold">Contact Details</h4>
        <div class="row pt-2 pl-2">
          <div class="col-1 text-center pt-1">
            <i class="far fa-envelope"></i>
          </div>
          <div class="col-11 pl-0">
            <a href="">info@freelancecapetown.co.za</a>
          </div>
        </div>
        <div class="row pl-2">
          <div class="col-1 text-center pt-1">
            <i class="fab fa-facebook-f"></i>
          </div>
          <div class="col-11 pl-0">
            <a href="https://www.facebook.com/freelancecapetown">Facebook</a>
          </div>
        </div>
        <div class="row pl-2">
          <div class="col-1 text-center pt-1">
            <i class="fab fa-twitter"></i>
          </div>
          <div class="col-11 pl-0">
            <a href="https://www.twitter.com/capefreelance">Twitter</a>
          </div>
        </div>
        <div class="row pl-2">
          <div class="col-1 text-center pt-1">
            <i class="fab fa-instagram"></i>
          </div>
          <div class="col-11 pl-0">
            <a href="https://instagram.com/freelancecapetown/">Instagram</a>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $("#answer").keydown(function(){
      var value = $("#answer").val();
      if (value == 14) {
        $("#submitBtn").removeAttr("disabled");
      }else {
        $("#submitBtn").attr("disabled", true);
        // Swal.fire({
        //     type: 'warning',
        //     text: 'Please give the correct answer to proceed.',
        //     showConfirmButton: false,
        //     timer: 1500
        // })
      }

    })
  </script>

@endsection
