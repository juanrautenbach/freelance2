@extends('backend')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 my-2">
            <div class="card">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <h4 class="ml-1">Users</h4>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                  <tr>
                                    <th class="nowrap">Name</th>
                                    <th class="nowrap">Email</th>
                                    <th class="nowrap">Role</th>
                                    <th class="nowrap">Last Login</th>
                                    <th class="nowrap">Login Count</th>
                                    <th class="nowrap">Status</th>
                                    <th class="nowrap">Paid</th>
                                    <th class="nowrap">Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($all_users as $user)
                                    <tr>
                                      <td class="nowrap">{{$user->name}}</td>
                                <td class="nowrap">{{$user->email}}</td>
                                <td class="nowrap">{{$user->role}}</td>
                                <td class="nowrap">{{ Carbon\Carbon::parse($user->last_login)->diffForHumans()}}</td>
                                <td class="nowrap">{{$user->login_count}}</td>
                                <td class="nowrap">{{$user->status}}</td>
                                <td id="" class="nowrap"></td>
                                <td class="tw-w-42 tw-flex nowrap"> 
                                   
                                            <a href="/edit/user/{{$user->id}}" class="">
                                                <span class="btn tw-py-2 tw-px-3 tw-text-gray-100 tw-bg-red-910"><i class="material-icons">edit</i></span>
                                                
                                            </a>
                                       
                                            <form action="/user/action/activate/{{$user->id}}" method="post" class="d-inline-block">
                                                <input name="_method" type="hidden" value="PUT">
                                                @csrf
                                                <button type="submit" class="btn tw-py-2 tw-px-3 tw-text-gray-100 tw-bg-blue-910 tw-ml-1" name="button"><span class="material-icons">
                                                    done
                                                    </span></button>
                                            </form>
                                       
                                            <form action="/user/action/deactivate/{{$user->id}}" method="post" class="d-inline-block">
                                                <input name="_method" type="hidden" value="PUT">
                                                @csrf
                                                <button type="submit" class="btn tw-py-2 tw-px-3 tw-text-gray-100 tw-bg-red-910 tw-ml-1" name="button"><span class="material-icons">
                                                    remove_circle
                                                    </span></button>
                                            </form>

                                   
                                    
                                    
                                    

                                </td>

                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

  
</div>
</div>

@endsection
