<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Payment Gateway - Thank you</title>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="/js/app.js" charset="utf-8"></script>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.open('', '_parent', '');
            window.close();
        }
    </script>
</head>

<body>

  @if (session()->has('message'))
<script type="text/javascript">
    Swal.fire({
        icon: '{{ session()->get('message_status') }}',
        text: '{{ session()->get('message') }}',
        showConfirmButton: true,

    });
</script>

@endif
    {{-- <input type="button" value="Close this window" onclick="windowClose();"> --}}
</body>

</html>
