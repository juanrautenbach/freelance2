@extends('front')
@section('content')
  PRIVACY POLICY

PRIVACY STATEMENT

1. WHAT DO WE DO WITH YOUR INFORMATION?
When you submit your profile on our website, we collect the personal information you give us such as your name, email address and contact number.

When you browse through our website, we also automatically receive your computer’s internet protocol (IP) address in order to provide us with information that helps us learn about your browser and operating system.

Email marketing (if applicable): With your permission, we may send you emails about our website, new products and other updates. We will also run regular competitions where where we would like to gather more information about your likes and dislikes.


2. CONSENT
How do you get my consent?

When you provide us with personal information to complete or update your profile or verify your credit card, we imply that you consent to our collecting it and using it for that specific reason only.

If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.

How do I withdraw my consent?

If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us via email on info@freelancecpt.com


3. DISCLOSURE
We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.


4. PAYU
Our payment gate is managed by PayU, they provide and secure the online e-commerce platform that allows us to sell our services to you.

Your data is stored through PayU’s data storage, databases and the general PayU application. They store your data on a secure server behind a firewall.

For more insight, you may also want to read PayU's Terms of Service or their Privacy Statement here. https://www.payu.co.za/legal/privacy-policy/


5. THIRD-PARTY SERVICES
In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.

However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.

For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.

In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.

Once you leave our website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service.

Links

When you click on links on our site, they may direct you away from our site. We are not responsible for the privacy practices of other sites and encourage you to read their privacy statements.


6. SECURITY
To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.

We do NOT store any part of your credit / cheque card information. Such information is supplied, by you, to our third party payment gateway provider PayU and is encrypted using secure socket layer technology (SSL) and stored using generally accepted industry standard security implementations that follow the PCI-DSS best practice code of conduct. Although no method of transmission over the Internet or electronic storage is 100% secure, we follow all PCI-DSS requirements and implement additional generally accepted industry standards.


7. AGE OF CONSENT
By using this site, you represent that you are at least the age of 18 and you have given us your consent to allow any of your minor dependents to use this site.
@endsection
