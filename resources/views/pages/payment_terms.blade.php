@extends('front')
@section('content')
  <div class="container my-5 pt-5">
    <div class="row pt-4">
      <div class="col">
          <h4 class="mb-4">TERMS AND CONDITIONS</h4>
          <ol>
            <li class="mb-3"><span class="font-weight-bold text-dark">Detailed description of goods and/or services</span><br>
            Freelance Cape Town is a business in the service and recruitment industry that provides organizations and individuals access to a local workforce of freelancers.</li>
            {{-- <li class="mb-3"><span class="font-weight-bold text-dark">Delivery policy</span><br>
                Subject to availability and receipt of payment, requests will be processed within ____ days and delivery confirmed by way ______________________ .
                (for e.g. booking number / booking voucher etc. and must mention the use of courier and/or postal services and associated costs, if applicable.)</li> --}}
            {{-- <li class="mb-3"><span class="font-weight-bold text-dark">Export restriction (Optional)</span><br>
                The offering on this website is available to South African clients only.</li> --}}
            <li class="mb-3"><span class="font-weight-bold text-dark">Return and Refunds policy</span><br>
                The provision of goods and services by Freelance Cape Town is subject to availability. In cases of unavailability, Freelance Cape Town will refund the
                client in full within 30 days. Cancellation of orders by the client will attract a 0 % administration fee.
                (If appropriate – provide details of your policy regarding damaged goods. Also mention guarantees, warranties, etc.)</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Customer Privacy policy</span><br>
                Freelance Cape Town shall take all reasonable steps to protect the personal information of users. For the purpose of this clause, "personal
                information" shall be defined as detailed in the Promotion of Access to Information Act 2 of 2000 (PAIA). The PAIA may be downloaded from:
                http://www.polity.org.za/attachment.php?aa_id=3569.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Payment options accepted</span><br>
                Payment may be made via Visa and MasterCard.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Card acquiring and security</span><br>
                Card transactions will be acquired for Freelance Cape Town via PayGate (Pty) Ltd who are the approved payment gateway for all South African
                Acquiring Banks. PayGate uses the strictest form of encryption, namely Secure Socket Layer 3 (SSL3) and no Card details are stored on the
                website. Users may go to www.paygate.co.za to view their security certificate and security policy.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Customer details separate from card details</span><br>
                Customer details will be stored by Freelance Cape Town separately from card details which are entered by the client on PayGate’s secure site. For
                more detail on PayGate refer to www.paygate.co.za.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Merchant Outlet country and transaction currency</span><br>
                The merchant outlet country at the time of presenting payment options to the cardholder is South Africa. Transaction currency is
                South African Rand (ZAR).</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Responsibility</span><br>
                Freelance Cape Town takes responsibility for all aspects relating to the transaction including sale of goods and services sold on this website,
                customer service and support, dispute resolution and delivery of goods.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Country of domicile</span><br>
                This website is governed by the laws of South Africa and Freelance Cape Town chooses as its domicilium citandi et executandi for all purposes under
                this agreement, whether in respect of court process, notice, or other documents or communication of whatsoever nature.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Variation</span><br>
                Freelance Cape Town may, in its sole discretion, change this agreement or any part thereof at any time without notice.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Company information</span><br>
            This website is run by Freelance Culture based in South Africa trading as a Pty Ltd and with registration number 2014/122536/07 and Directors.</li>
            <li class="mb-3"><span class="font-weight-bold text-dark">Freelance Cape Town contact details</span><br>
                Company Physical Address: Unit 100 Wolroy House 37 Buitenkant street, Zonnebloem, Cape Town 8001.
                Email: info@freelancecapetown.com Telephone: 0721290279.</li>
          </ol>
        













      </div>

    </div>

  </div>

@endsection