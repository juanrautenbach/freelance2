@extends('backend')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 tw-mt-5">
          <div class="card border-0 tw-shadow tw-px-5 tw-py-4">
            <h3 class="tw-mb-5">Edit User</h3>
            <form class="admin-user-form" action="/settings/user/update" method="post">
                @csrf
                <div class="form-group">
                    <label for="user_name" class="font-weight-bold">Full Name</label>
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="text" class="form-control" name="user_name" value="{{$user->name}}">
                </div>
                <div class="form-group">
                    <label for="user_email" class="font-weight-bold">Email</label>
                    <input type="email" class="form-control" name="user_email" value="{{$user->email}}">
                </div>
                <div class="form-group">
                    <label for="user_role" class="font-weight-bold">Role</label>
                    <select class="form-control" name="user_role">
                      @if ($user->role == "Admin")
                        <option value="{{$user->role}}">{{$user->role}}</option>
                        <option value="Freelancer">Freelancer</option>
                        <option value="Finder">Finder</option>
                        <option value="Co-working">Co-working</option>
                      @elseif ($user->role == "Freelancer")
                        <option value="{{$user->role}}">{{$user->role}}</option>
                        <option value="Admin">Admin</option>
                        <option value="Finder">Finder</option>
                        <option value="Co-working">Co-working</option>
                      @elseif ($user->role == "Finder")
                        <option value="{{$user->role}}">{{$user->role}}</option>
                        <option value="Admin">Admin</option>
                        <option value="Freelancer">Freelancer</option>
                        <option value="Co-working">Co-working</option>
                      @elseif ($user->role == "Co-working")
                        <option value="{{$user->role}}">{{$user->role}}</option>
                        <option value="Admin">Admin</option>
                        <option value="Finder">Finder</option>
                        <option value="Freelancer">Freelancer</option>
                      @endif

                    </select>

                </div>
                <div class="form-group">
                    <label for="user_lastlogin" class="font-weight-bold">Last Login</label>
                    <input type="text" class="form-control" name="user_lastlogin" value="{{$user->last_login}}" disabled>
                </div>
                <button type="submit" name="button" class="btn btn-lg btn-red text-uppercase px-5 letterspace-2 font-weight-bold">Save</button>
            </form>
          </div>

        </div>

    </div>
</div>

@endsection
