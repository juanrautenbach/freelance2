<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-light navbar-brand-center">
    <div class="navbar-wrapper">
        @include('menu.dashboard.navbar_header')
        @include('menu.dashboard.navbar_content')
    </div>
</nav>
<!-- END: Header-->
@if (Request::path() !== 'users/profiles/register' || Request::path() !== 'users/profiles/register/industries')
{{-- {{ 'True' }} --}}
@else
{{-- {{ 'False' }} --}}
@endif

<!-- BEGIN: Main Menu-->
@if (Request::path() == 'users/profiles/register' || Request::path() == 'users/profiles/register/industries')

@else
<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-container main-menu-content tw-w-full" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
            
            <li class="dropdown nav-item" data-menu="">
                <a class="nav-link dropdown-item" href="/dashboard" data-toggle="">
                    <i class="la la-home tw--mt-2 md:tw-mt-0 md:tw-leading-6 md:tw-hidden"></i>
                    <i class="material-icons tw-hidden md:tw-inline-block">home</i>
                    <span class="md:tw-leading-6">Dashboard</span>
                </a>
            </li>

            @if ($user->role == 'Finder')
            <li class="nav-item" data-menu="">
                <a class="nav-link" href="/messages">
                    <i class="la la-envelope tw--mt-2 md:tw-mt-0 md:tw-leading-6 md:tw-hidden"></i>
                    <i class="material-icons tw-hidden md:tw-inline-block">email</i>
                    <span class="md:tw-leading-6">Messages</span>
                </a>
            </li>
            <li class="nav-item" data-menu="">
                <a class="nav-link" href="/posts">
                    <i class="la la-edit tw--mt-2 md:tw-mt-0 md:tw-leading-6 md:tw-hidden"></i>
                    <i class="material-icons tw-hidden md:tw-inline-block">note_alt</i>
                    <span class="md:tw-leading-6">Posts</span>
                </a>
            </li>
            <li class="nav-item" data-menu="">
                <a class="nav-link" href="/dashboard/freelancers">
                    <i class="la la-suitcase tw--mt-2 md:tw-mt-0 md:tw-leading-6 md:tw-hidden"></i><i class="material-icons tw-hidden md:tw-inline-block">people_alt</i>
                    <span class="md:tw-leading-6">Freelancers</span>
                </a>
            </li>
            @endif

            @if ($user->role == 'Freelancer')
            <li class="nav-item position-relative" data-menu="dropdown">
                <a class="nav-link" href="/subscriptions">
                    <i class="material-icons">loyalty</i><span>Subscriptions</span>
                    {{-- <span class="badge badge-pill badge-danger badge-up badge-glow mr-n4" id="unread">4</span> --}}
                </a>
            </li>
            
            @endif


            @if ($user->role == 'Admin')
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="material-icons">settings</i><span class="md:tw-leading-6">Settings</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="d-none" data-menu="">
                        <a class="dropdown-item" href="/settings/industries" data-toggle="">
                            
                            <span data-i18n="eCommerce">Industries</span>
                        </a>
                    </li>
                    <li data-menu="">
                        <a class="dropdown-item" href="/settings" data-toggle="">
                            {{-- <i class="material-icons">add_shopping_cart</i>
                                    --}}
                            <span data-i18n="eCommerce">Users</span>
                        </a>
                    </li>
                    <li data-menu="">
                        <a class="dropdown-item" href="/subscriptionplans" data-toggle="">
                            {{-- <i class="material-icons">filter_1</i>
                                    --}}
                            <span data-i18n="Crypto">Subscription Plans</span>
                        </a>
                    </li>
                    <li class="" data-menu="">
                        <a class="dropdown-item" href="/vouchers" data-toggle="">
                            {{-- <i class="material-icons">local_atm</i>
                                    --}}
                            <span data-i18n="Sales">Vouchers</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="material-icons">forward_to_inbox</i><span class="md:tw-leading-6">Send messages</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="/email/freelancers" data-toggle="">
                            {{-- <i class="material-icons">add_shopping_cart</i>
                                    --}}
                            <span data-i18n="eCommerce">Contact Freelancers</span>
                        </a>
                    </li>
                    <li data-menu="">
                        <a class="dropdown-item" href="/email/finders" data-toggle="">
                            {{-- <i class="material-icons">add_shopping_cart</i>
                                    --}}
                            <span data-i18n="eCommerce">Contact Finders</span>
                        </a>
                    </li>
                    <li data-menu="">
                        <a class="dropdown-item" href="/email/co-working" data-toggle="">
                            {{-- <i class="material-icons">add_shopping_cart</i>
                                    --}}
                            <span data-i18n="eCommerce">Contact Co-working</span>
                        </a>
                    </li>

                </ul>
            </li>
            
            @endif

            <li class="tw-ml-auto dropdown nav-item" data-menu="">
                <a class="nav-link dropdown-item" href="/" data-toggle="">
                    <i class="material-icons">arrow_back_ios_new</i>
                    <span class="md:tw-leading-6">Go to frontpage</span>
                </a>
            </li>

        </ul>
    </div>
</div>
@endif
@if (\Session::has('success'))
<div class="alert alert-success alert-dismissible fade show">
    {!! \Session::get('success') !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if (\Session::has('danger'))
<div class="alert alert-danger alert-dismissible fade show">
    {!! \Session::get('danger') !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (\Session::has('message'))
<script>
    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "0",
                        "hideDuration": "0",
                        "timeOut": "50000",
                        "extendedTimeOut": "30000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
    toastr.success('Payment successfull');
</script>
{{-- @if (\Session::get('message_status') == "success")
<script>
toastr.success('{!! \Session::get('message') !!}');
</script>

@elseif (\Session::get('message_status') == "error")
<script>
    toastr.error('{!! \Session::get('message') !!}');
    </script>
@endif --}}

@endif

<script>
    $(document).ready(function() {
        $.get("/api/user/unread/{{ $user->id }}", function(data, status){
            $("#unread").text(data);
        });
    });
</script>
<!-- END: Main Menu-->