

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light navbar-shadow row">
  <div class="col">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <a class="text-bold-800 grey text-danger darken-2" href="#" target="_blank">Julura Technologies</a></span>
      <span class="float-md-right d-none d-lg-block">
        {{-- Hand-crafted & Made with<i class="ft-heart pink"></i> --}}
        <span id="scroll-top"></span>
      </span>
    </p>
  </div>

    
</footer>
<!-- END: Footer-->
@if (Request::path() == "users/profiles/register/about" || Request::path() == "posts/create" || Request::path() == "users/profiles")
  <script src="https://cdn.jsdelivr.net/npm/places.js@1.18.1"></script>

@endif

@if (Request::path() == "users/profiles/register/about")
<script src="/js/register.js" charset="utf-8"></script>
@endif

@if (Request::path() == "posts/create" || Request::path() == "users/profiles" || Request::path() == "users/profiles/register/about")
  <script src="/js/places.js" charset="utf-8"></script>
@endif

{{-- @if (Request::path() == "users/profiles/register")
  <script type="text/javascript">
    alert("Dashboard");
  </script>
@endif --}}




<!-- BEGIN: Vendor JS-->
<script src="/js/dashboard/material-vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="/js/dashboard/ui/jquery.sticky.js"></script>
<script src="/js/dashboard/charts/jquery.sparkline.min.js"></script>
<script src="/js/dashboard/charts/chart.min.js"></script>
<script src="/js/dashboard/charts/raphael-min.js"></script>
<script src="/js/dashboard/charts/morris.min.js"></script>
<script src="/js/dashboard/charts/jvector/jquery-jvectormap-2.0.3.min.js"></script>
<script src="/js/dashboard/charts/jvector/jquery-jvectormap-world-mill.js"></script>
<script src="/js/dashboard/jvector/visitor-data.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="/js/dashboard/core/app-menu.js"></script>
<script src="/js/dashboard/core/app.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="/js/dashboard/scripts/pages/material-app.js"></script>
{{-- <script src="/js/dashboard/scripts/pages/dashboard-sales.js"></script> --}}
<!-- END: Page JS-->

@if (Request::path() == "vouchers")
  <script src="https://cdn.rawgit.com/hashids/hashids.github.io/master/public/js/lib/hashids.min.js" charset="utf-8"></script>
  <script src="/js/vouchers.js" charset="utf-8"></script>
  <script src="/js/dashboard/scripts/modal/components-modal.min.js" charset="utf-8"></script>
  <script src="/js/dashboard/scripts/pages/app-chat.js"></script>
@endif

{{-- @php
  echo Request::path();
@endphp --}}
@if (Request::path() !== "users/profiles/register/photos")
  {{-- <script src="/js/app.js" charset="utf-8"></script> --}}
@endif

@if (Request::path() !== "/messages")
  <script src="/js/messages.js" charset="utf-8"></script>
@endif

<script src="/js/msc-script.js"></script>

