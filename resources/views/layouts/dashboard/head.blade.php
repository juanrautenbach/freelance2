<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="/css/dashboard.css">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js" charset="utf-8"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


<title>Freelance Cape Town</title>

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    {{-- <link rel="stylesheet" type="text/css" href="/fonts/material-icons/material-icons.css">
     --}}
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/components.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-colors.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/css/dashboard/menu/material-horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="/fonts/simple-line-icons/style.css">

    <link rel="stylesheet" type="text/css" href="/css/dashboard/pages/app-chat.css">

    {{--    COLOURS--}}
    <link rel="stylesheet" type="text/css" href="/css/dashboard/material-palette-gradient.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
   
    <link rel="stylesheet" type="text/css" href="/css/dashboard/style.css">
    
    <!-- END: Custom CSS-->

    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <script src="/js/filter.js" charset="utf-8"></script>

      <script src="https://kit.fontawesome.com/3b6b6a71d3.js" crossorigin="anonymous"></script>

      <script src="/js/toastr.min.js"></script>
<link rel="stylesheet" href="/css/toastr.min.css">
<link rel="stylesheet" href="/css/msc-style.css">


