@extends('dashboard')
@section('content')

<div class="row my-3">
    <div class="col mx-auto d-flex justify-content-center">
        <div class="card border-0 shadow-sm p-4 w-50">
            <h5>Create Co-Workspace</h5>
            <form class="" action="/co-working" method="post">
                @csrf
                <div class="form-group mt-4">
                    <label for="" class="font-weight-bold">Co-Workspace name:</label><br />
                    <input type="text" name="name" class="w-50">
                </div>
                <div class="form-group">
                    <label for="" class="font-weight-bold">Description:</label>
                    <textarea name="description" class="bg-light" rows="8" cols="80">Enter your descrition here</textarea>
                </div>
                <div id="upload"></div>
                <div class="form-group">
                    <label for="" class="font-weight-bold">City:</label><br>
                    <input type="text" name="location-city" class="w-50">
                </div>
                <div class="form-group">
                    <label for="" class="font-weight-bold">Contact number:</label><br>
                    <input type="text" name="contact-number" class="w-50">
                </div>
                <div class="form-group">
                    <label for="" class="font-weight-bold">Contact email:</label><br>
                    <input type="text" name="main-photo" class="w-50">
                </div>
                <div class="form-group">
                    <label for="" class="font-weight-bold">Main photo:</label><br>
                    <input type="file" name="main-photo" class="w-50">
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-red btn-lg text-uppercase px-4 ls-25" name="button">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
