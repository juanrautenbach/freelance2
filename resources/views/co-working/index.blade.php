@extends('minimal')
@section('content')
  <div class="container-fluid my-5">
    <div class="row">
      <div class="col mx-5">
        <div class="card border-0 shadow-sm px-3 py-4">
          <div class="row">
            <div class="col">
              <h3 class="d-inline mb-3">Search co-working space</h3><span class="float-right text-muted">Total results: {{$coworking_count}}</span>
                <div class="row my-3">
                  <div class="col-10 ">
                    <input type="text" name="search" class="form-control mt-1" placeholder="Search">
                  </div>
                  <div class="col text-right">
                    <button class="btn btn-red w-100 py-2" type="button" name="button">SEARCH</button>
                  </div>

                </div>
                <div class="row">
                  <div class="col">

                    @foreach ($coworkingspaces as $coworkingspace)
                      <hr>
                      <div class="d-inline-block">
                        @if (!empty($coworkingspace->profile->profile_pic))
                          <img src="/storage/{{$coworkingspace->profile->profile_pic}}" alt="" style="height: 150px">
                        @else
                          <img src="http://via.placeholder.com/150" alt="">
                        @endif
                      </div>
                      <div class="d-inline-block ml-3">
                        <h4 class="font-weight-bold text-capitalize">{{$coworkingspace->name}}</h4>
                        <p class="text-muted">{{$coworkingspace->role}}</p>
                      </div>
                      <div class="d-inline-block float-right mt-5 mr-4">
                        <a href="/freelancer/show/{{$coworkingspace->id}}" class="btn btn-red">View Profile</a>
                      </div>
                    @endforeach

                  </div>
                </div>
            </div>
          </div>

        </div>

        <div class="row my-5">
          <div class="col text-center mx-auto">
            {{-- <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">1</span>
            <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">2</span>
            <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">3</span>
            <span class="text-white bg-blue rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">4</span> --}}
            {{$coworkingspaces->links()}}
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection
