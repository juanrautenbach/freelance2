
<div class="container-fluid">
    <div class="row profile_portfolio">
        <div class="card shadow-sm col py-3 px-4 border-0">
            <span>
                <h4 class="d-inline">Portfolio</h4>
            </span>
            @livewire('showcase', ['user' => \Auth::user()])
        </div>
    </div>
    <div class="row tw-mb-8">
        @if (Request::path() !== "users/profiles")

                <div class="col text-center tw-px-20">
                    <p class="font-weight-bold">Click here once you are happy with your profile..</p>
                    <button type="button" class="btn btn-red btn-lg btn-block tw-py-6" name="form1" id="submit_1">Next step..</button>
                </div>
  
            @endif
    </div>
</div>
@if (Request::path() !== "users/profiles")
<div class="scroller">
    <div class="chevron text-center"></div>
    <div class="chevron text-center"></div>
    <div class="chevron text-center"></div>
    <span class="text tw-mb-1">Scroll down</span>
    <span class="text tw-mt-1">There is more</span>
  </div>
  @endif
@php
$profile_count = count($user->showcase);
@endphp
