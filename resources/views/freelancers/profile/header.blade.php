<div class="container-fluid">
        @if (Request::path() !== "users/profiles")
        <form id="form1" class="" action="/users/profiles/register/photos" method="post" enctype="multipart/form-data">
        @endif
            <div class="row">
                <div class="p-0">
                    @if (empty($user->profile->profile_cover_pic))
                    <div class="d-block" style="height: 120px; width:100vw; background: #D8D8D8">

                    </div>
                    @else
                    <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image" alt="">
                    @endif
                </div>
            </div>
            @csrf
            <input type="hidden" name="user_id" value="{{ \Auth::user()->id}}">
            <div class="row profile_details">
                <div class="profile_pic shadow" id="profile_photo">
                    @if (empty($user->profile->profile_pic))
                    <div class="form-group m-0">
                        <div class="dropzone mx-auto" id="myDropzone" style="width: 291px; height: 291px">
                            <img src="/img/blank_profile.png" alt="empty profile photo" style="height: 291px" class="hover" onclick="addProfilePhoto({{$user->profile->id}})">
                        </div>
                        <div class="dropzone-previews mx-auto" id="photo-preview" style="display: none; width: 275px; height: 275px; border: 3px dashed #000; ">
                        </div>
                    </div>
                    @else
                    <img src="{{$user->profile->profile_pic}}" alt="" class="profile_photo_image hover" onclick="addProfilePhoto({{$user->profile->id}})">
                    @endif
                </div>
                <div class="user_details pt-3 mt-md-0 w-100 mb-3 mb-md-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="name d-block">
                                <h1>{{$user->name}}</h1>
                            </div>
                        </div>
                        <div class="col">
                            @include('freelancers.profile.rating')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-1 mt-1">
                            @include('freelancers.profile.industries')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col p-1 nav">
                            @include('freelancers.profile.subcategories')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            @include('freelancers.profile.tags')
                        </div>
                    </div>
                </div>

                <div class="col contact_user text-center d-flex align-items-center justify-content-center">
                    @if ($user->role !== "Freelancer")
                    <div class="contact-button d-none">
                        <a href="#" class="btn btn-red btn-lg px-5">Contact me</a>
                    </div>
                    @else
                    <div class="contact-button d-none">
                        <a href="#" class="btn btn-blue btn-lg px-5">Edit Profile</a>
                    </div>
                    @endif
                </div>
            </div>

        </form>
    
    </div>