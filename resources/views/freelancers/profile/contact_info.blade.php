<div class="container-fluid">
            <div class="row profile_contact_info">
                
                <div class="card shadow-sm col py-3 px-md-4 px-1 border-0" id="contactSection">
                    <div class="row">
                        <div class="col-8">
                            <h4>Contact information </h4>
                        </div>
                        <div class="col tw-text-right">
                            <button type="button" name="button" class="tw-bg-transparent tw-text-xs tw-text-gray-600 tw-rounded hover:tw-text-blue-910 tw-outline-none tw-border-0" title="Edit" id="contactEdit"><i class="fas fa-pencil-alt"></i></button> 
                        </div>
                    </div>
                   
                    <table class="table table-borderless table-responsive">
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Company:</span></td>
                            <td id="companyText">{{$user->profile->company_name ?? "Freelancer"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">City:</span></td>
                            <td id="cityText">{{$user->profile->city ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Email:</span></td>
                            <td id="emailText">{{$user->email ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Mobile:</span></td>
                            <td id="mobileText">{{$user->profile->mobile ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Tel:</span></td>
                            <td id="telText">{{$user->profile->tel ?? "Not set"}}</td>
                            <td></td>
                        </tr>
                        @if (!empty($user->profile->social_media) > 0)
                            @php
                            $social = $user->profile->social_media;
                            $social_links = json_decode($social);
                            @endphp
                            @foreach ($social_links as $key => $value)
                            
                                @if (!empty($value))
                                <tr>
                                    <td><span class="font-weight-bold mr-md-5 mr-1 tw-capitalize">{{$key}}:</span></td>
                                    <td class="social_{{ $key }}">{{$value}}</td>
                                    <td></td>
                                </tr>
                                @else
                                <tr>
                                    <td><span class="font-weight-bold mr-md-5 mr-1 tw-capitalize">{{$key}}:</span></td>
                                    <td class="social_{{ $key }}">Not set</td>
                                    <td></td>
                                </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td>Social media</td>
                                <td>{{ "Not set" }}</td>
                                <td></td>
                            </tr>

                        @endif
                    </table>


                </div>

                <div class="card shadow-sm col py-3 px-1 border-0 d-none" id="contactEditSection">
                    <div class="row">
                        <div class="col-8">
                            <h4>Contact information </h4>
                        </div>
                        <div class="col tw-text-right">
                            <button type="button" name="button" class="tw-bg-transparent tw-text-red-910 tw-text-xs tw-text-gray-600 tw-rounded hover:tw-text-blue-910 tw-outline-none tw-border-0" title="Save" id="contactSave"><i class="fas fa-save"></i></button> 
                            <button type="button" name="button" class="tw-bg-transparent tw-text-red-910 tw-text-xs tw-text-gray-600 tw-rounded hover:tw-text-blue-910 tw-outline-none tw-border-0" title="Cancel" id="contactClose"><i class="fas fa-times"></i></button> 
                        </div>
                    </div>
                    <input type="hidden" name="contact_user_id" value="{{ $user->id }}">
                    <table class="table table-borderless table-responsive">
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Company:</span></td>
                            <td class="tw-w-1/2 "><input class="form-control" type="text" name="company" value="{{$user->profile->company_name ?? "Freelancer"}}"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">City:</span></td>
                            <td class="tw-w-1/2 ">
                                <input type="text" name="city" class="" value="{{$user->profile->city ?? "Not set"}}" id="user_city" required>
                                
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Email:</span></td>
                            <td class="tw-w-1/2  tw-border-solid"> <input type="text" class="form-control" value="{{$user->email ?? "Not set"}}" readonly>
                                
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Mobile:</span></td>
                            <td class="tw-w-1/2 ">
                                <input type="text" class="form-control" name="mobile" value="{{$user->profile->mobile ?? "Not set"}}">
                                </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="font-weight-bold mr-md-5">Tel:</span></td>
                            <td class="tw-w-1/2 ">
                                <input type="text" class="form-control" name="tel" value="{{$user->profile->tel ?? "Not set"}}">
                            </td>
                            <td></td>
                        </tr>
                        @if (!empty($user->profile->social_media) > 0)
                        @php
                        $social = $user->profile->social_media;
                        $social_links = json_decode($social);
                        @endphp
                        @foreach ($social_links as $key => $value)
                        
                        @if (!empty($value))
                        <tr>
                            <td><span class="font-weight-bold mr-md-5 mr-1 tw-capitalize">{{ $key }}:</span></td>
                            <td class="tw-w-1/2 ">
                                <input type="text" name="social[]" class="form-control" value="{{$value}}">
                                
                            </td>
                            <td></td>
                        </tr>
                        @else
                        <tr>
                            <td><span class="font-weight-bold mr-md-5 mr-1 tw-capitalize">{{ $key }}:</span></td>
                            <td class="tw-w-1/2 ">
                                <input type="text" name="social[]" class="form-control" value="Not set">
                            </td>
                            <td></td>
                        </tr>
                        @endif

                        @endforeach
                        @else
                        <tr>
                            <td>Social media</td>
                            <td class="tw-w-1/2 ">
                                <input type="text" name="social[]" class="form-control" value="Not set">
                            </td>
                            <td></td>
                        </tr>

                        @endif
                    </table>


                </div>

            </div>
            </div>
            <script>
                 $("#contactEdit").click(function () {
                    $("#contactSection").addClass('d-none');
                    $("#contactEditSection").removeClass('d-none');
                })

                $("#contactClose").click(function () {
                    $("#contactSection").removeClass('d-none');
                    $("#contactEditSection").addClass('d-none');
                })

                $("#contactSave").click(function () {
                    var user_id = $("input[name='contact_user_id']").val();
                    var company = $("input[name='company']").val();
                    var city = $("input[name='city']").val();
                    var mobile = $("input[name='mobile']").val();
                    var tel = $("input[name='tel']").val();
                    var social = $("input[name='social[]']").map(function(){return $(this).val();}).get();
                    // console.log(user_id);
                    axios.post('/api/freelancer/contact', {
                        user_id: user_id,
                        company: company,
                        city: city,
                        mobile: mobile,
                        tel: tel,
                        social: social,
                    })
                    .then(function (response) {
                        // console.log(response.data);
                        $("#companyText").html(response.data.company);
                        $("#cityText").html(response.data.city);
                        $("#mobileText").html(response.data.mobile);
                        $("#telText").html(response.data.tel);
                        $(".social_facebook").html(response.data.facebook);
                        $(".social_whatsapp").html(response.data.whatsapp);
                        $(".social_instagram").html(response.data.instagram);
                        $(".social_linkedin").html(response.data.linkedin);
                        $("#contactSection").removeClass('d-none');
                        $("#contactEditSection").addClass('d-none');   
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                })
            </script>
            
        