@if (!empty($user->tag) > 0)
@foreach ($user->tag as $tag)
<span class="mt-1 skill-tags badge badge-light badge-pill px-1 text-uppercase" style="padding-bottom: 6px !important; padding-top: 6px !important;">{{$tag->name}}</span>
@endforeach
@endif
