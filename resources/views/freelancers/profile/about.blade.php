<div class="container-fluid">
    <div class="row profile_about">
        <div class="card shadow-sm col py-3 px-4 border-0" id="aboutSection">
            <div class="row">
                <div class="col">
                    <h4>About me</h4>
                </div>
                <div class="col tw-text-right">
                    <button type="button" name="button" class="tw-bg-transparent tw-text-xs tw-text-gray-600 tw-rounded hover:tw-text-blue-910 tw-outline-none tw-border-0" title="Edit" id="aboutEdit"><i class="fas fa-pencil-alt"></i></button> 
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p id="aboutText">{!! nl2br(e($user->profile->about)) ?? "Not set"!!}</p>
                </div>
            </div>
            
            
            
        </div>

        <div class="card shadow-sm col py-3 px-4 border-0 d-none" id="editSection">
            <div class="row tw-mb-4">
                <div class="col">
                    <h4 class="tw-inline-block">About me</h4>
                </div>
                <div class="col tw-text-right">
                    <button type="button" name="button" class="tw-bg-transparent tw-text-red-910 tw-text-xs tw-text-gray-600 tw-rounded hover:tw-text-blue-910 tw-outline-none tw-border-0" title="Save" id="aboutSave"><i class="fas fa-save"></i></button> 
                    <button type="button" name="button" class="tw-bg-transparent tw-text-red-910 tw-text-xs tw-text-gray-600 tw-rounded hover:tw-text-blue-910 tw-outline-none tw-border-0" title="Cancel" id="aboutClose"><i class="fas fa-times"></i></button> 
                </div>
            </div>
            <div class="row"> 
                <div class="col">
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <textarea name="about" class="form-control tw-bg-gray-200 tw-p-3">{{ $user->profile->about }}</textarea>                    
                </div>
                
            </div>
            
        </div>
    </div>
</div>

<script>
    $("#aboutEdit").click(function () {
        $("#aboutSection").addClass('d-none');
        $("#editSection").removeClass('d-none');
    })

    $("#aboutClose").click(function () {
        $("#aboutSection").removeClass('d-none');
        $("#editSection").addClass('d-none');
    })

    $("#aboutSave").click(function () {
        var user_id = $("input[name='user_id']").val();
        var about = $("textarea[name='about']").val();
        // alert(about);
        axios.post('/api/freelancer/about', {
            user_id: user_id,
            about: about,
        })
        .then(function (response) {
            // console.log(response.data);
            $("#aboutText").text(response.data);
            $("#aboutSection").removeClass('d-none');
            $("#editSection").addClass('d-none');   
        })
        .catch(function (error) {
            console.log(error);
        });
    })
</script>