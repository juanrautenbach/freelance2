@extends('minimal')
@section('content')
<style media="screen">
    .filter {
        display: none;
    }
</style>

<div class="container-fluid md:tw-my-5 tw-my-3">
    <div class="row">
        <div class="col d-block d-md-none text-right ">
            <button id="filter-btn" class="btn-red btn-sm tw-mb-3 focus:tw-outline-none active:tw-outline-none active:tw-border-none active:tw-shadow-none focus:tw-shadow-none shadow-none focus:tw-border-none"><i class="fas fa-filter"></i></button>
        </div>
        <div id="filter-section" class="col-lg-3 col-md-3 d-none d-md-block tw-z-10 md:tw-z-0">
            <div class="accordion" id="accordionExample">
                <div class="card border-0">
                    <div class="card-header border-0 bg-white text-dark tw-px-2 lg:tw-px-5">
                        <h4 class="font-weight-bold d-inline md:tw-text-base xl:tw-text-xl sm:tw-text-xl">Filter by</h4>
                        <span class="btn btn-red btn-sm float-right" id="reset-btn">Reset</span>
                    </div>
                </div>
                <div class="card border-0" id="industryCard">
                    <div class="card-header bg-white" id="headingOne">
                        <p class="mb-0">
                            <a class="text-dark font-weight-bold d-block" type="button">
                                Industry
                                <span class="float-right d-none" id="industryMinus"><i class="fas fa-minus"></i></span>
                                <span class="float-right" id="industryPlus"><i class="fas fa-plus"></i></span>
                            </a>
                        </p>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="pointer-cursor" onclick="resetFilter()">
                                <p class="font-weight-bold  d-inline">All Industries</p>
                               
                            </div>

                            <ul class="mt-3 pl-0">
                                @foreach ($industries as $industry)
                                <li class="mb-2" style="list-style-type: none"><a class="pointer-cursor text-dark" onclick="filter('category', 'filter-industry', '{{ $industry }}'); this.onclick=null;">{{ $industry }}</a></li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card border-0" id="subIndustryCard">
                    <div class="card-header bg-white" id="headingTwo">
                        <p class="mb-0">
                            <a class="d-block collapsed text-dark font-weight-bold" type="button">
                                Sub Industry
                                <span class="float-right" id="subIndustryPlus"><i class="fas fa-plus"></i></span>
                                <span class="float-right d-none" id="subIndustryMinus"><i class="fas fa-minus"></i></span>
                            </a>
                        </p>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p class="font-weight-bold pointer-cursor" onclick="resetFilter()">All Sub Industries</p>
                            <div class="mt-3 pl-0">
                                
                            @foreach ($subindustries as $subindustry)
                            <li class="mb-2" style="list-style-type: none"><a class="pointer-cursor text-dark" onclick="filter('category', 'sub-industry-filter', '{{ $subindustry }}'); this.onclick=null;">{{ $subindustry }}</a></li>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-0" id="skillCard">
                <div class="card-header bg-white" id="headingThree">
                    <p class="mb-0">
                        <a class="d-block collapsed text-dark font-weight-bold" type="button">
                            Find by skill
                            <span class="float-right" id="skillPlus"><i class="fas fa-plus"></i></span>
                            <span class="float-right d-none" id="skillMinus"><i class="fas fa-minus"></i></span>
                        </a>
                    </p>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        @foreach ($tags as $tag)
                        <li class="mb-2" style="list-style-type: none"><a class="pointer-cursor text-dark text-capitalize" onclick="filter('category', 'tag-filter', '{{ $tag }}')">{{ $tag }}</a></li>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>



        <div class="col-lg-9 col-md-9 mx-0 pr-md-5">
            <div class="card border-0 shadow-sm tw-p-5">
                <div class="row">
                    <div class="col">
                        <h3 class="d-md-inline mb-1 mb-md-3 font-weight-bold">Search Freelancers {{request()->get('industry')}}</h3>
                        <ul class="list-inline mt-3">
                            {{-- @foreach ($tags as $tag)
                            <li class="list-inline-item"><a href="api/freelancers/by/tagname/{{ $tag->name }}" class="badge badge-info p-2 mb-2">{{ $tag->name }}</a></li>
                        @endforeach --}}
                    </ul>
                    <!-- <span class="float-md-right d-block d-md-inline text-muted mt-2">Total results: <span id="count"></span> </span> -->
                    <div class="row my-3">
                        <div class="col-12 px-0">
                            <div class="input-group mb-3 md:tw-ml-3">
                                <!-- <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"> -->
                                <div class="input-group-prepend search_icon tw-h-8 tw-mt-1 d-flex">
                                    <span class="input-group-text tw-border-t-0 tw-border-l-0 tw-border-r-0 tw-border-b-1 bg-white tw-border-gray-500 rounded-0" id="basic-addon2"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" name="search" class="tw-w-5/6 md:tw-w-11/12 lg:tw-w-4/6 tw-h-8 tw-border-0 tw-border-t-0 tw-border-l-0 tw-border-r-0 tw-border-b md:tw-text-xl  tw-border-gray-500 tw-placeholder-opacity-10 rounded-0 mt-1 mb-2 mb-md-0  d-inline-block main-panel-search" placeholder="Search for anything, skill, industry, name" onkeyup="filter('freetext','freelancer-cards','main-panel-search')">

                            </div>

                            <span></span>

                        </div>
                        {{-- <div class="col-12 col-md-2 text-right">
                                <button class="btn btn-red w-100 py-2" type="button" name="button" onclick="filter('freetext','freelancer-cards','main-panel-search')">SEARCH</button>
                            </div> --}}
                    </div>
                    <div class="row">
                        <div class="col">
                            @php
                            $count = 0;
                            @endphp
                            @foreach ($freelancers as $freelancer)
                            @if (empty($freelancer->profile->profile_pic))

                            @else
                            <hr class="my-0">
                            <a href="/freelancer/click/{{$freelancer->id}}" class="freelancer-cards">
                                <div class="row freelancer-row py-3">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col">
                                                <div class="row">
                                                    <div class="sm:tw-w-60 tw-w-full tw-px-0 md:tw-px-4 md:tw-text-left tw-text-center tw-mx-1 md:tw-mx-1">
                                                        @if (!empty($freelancer->profile->profile_pic))
                                                        <div class=" tw-bg-transparent">
                                                            <img class="freelance-profile-pic tw-rounded tw-w-full tw-h-full sm:tw-w-60" src="{{$freelancer->profile->profile_pic}}" alt="Profile Photo">
                                                        </div>
                                                        <div class="tw-absolute tw-rounded tw-w-full tw-h-full sm:tw-h-60 sm:tw-w-60 sm:tw-bg-gray-400 tw-z-10 tw-top-0 freelance_photo"></div>
                                                        @else
                                                        <div>
                                                            <img src="/img/FCPT_Logo.png" alt="Freelance Cape Town Logo" class="tw-rounded tw-w-full md:tw-h-60">
                                                        </div>
                                                        
                                                        @endif
                                                    </div>
                                                    <div class="col tw-px-4 sm:tw-px-8 tw-py-4 sm:tw-py-0">
                                                        <div class="row">
                                                            <div class="col-10 col-lg tw-pl-4">
                                                                <p class="d-inline font-weight-bold text-capitalize">{{$freelancer->name}}</p>
                                                                <p clas="text-muted mb-1"><i class="fas fa-map-marker-alt text-muted mr-2"></i>{{$freelancer->profile->city}}</p>
        
                                                            </div>
                                                            <div class="col-2 tw--ml-4">
                                                                <div class="d-block">
                                                                    <div class="tw-relative">
                                                                            <img src="/img/ratings/filled_star.png" alt="User Rating Star" class="tw-h-12 tw-w-12">
                                                                        <div class="tw-absolute tw-top-0 tw-z-10 tw-h-12 tw-w-12">
                                                                            <div class="tw-flex tw-justify-center tw-content-center flex-wrap tw-h-12">
                                                                                <span class="tw-text-gray-100 tw-text-xs tw-font-bold">56</span> 
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
        
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <small class="text-dark font-weight-bold">About me:</small>
                                                                    <p class="tw-mb-1 tw-text-xs">{{ \Illuminate\Support\Str::limit($freelancer->profile->about, 150, $end='...') }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="filter filter-industry">
                                                                    @foreach ($freelancer->industry as $industry)
                                                                    {{ $industry->industry_name }} |
                                                                    @endforeach
                                                                </div>
                                                                <div class="filter sub-industry-filter">
                                                                    @foreach ($freelancer->subindustry as $subindustry)
                                                                    {{ $subindustry->sub_industry_name }} |
                                                                    @endforeach
                                                                </div>
        
                                                                <div class="filter tag-filter">
                                                                    @foreach ($freelancer->tag as $tag)
                                                                    {{ $tag->name }} |
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <small class="text-dark font-weight-bold">Industry:</small>
                                                                <p class="tw-mb-1">
                                                                    @foreach ($freelancer->industry as $industry)
                                                                    <span class="text-dark d-inline-block tw-text-xs">
                                                                        {{ $industry->industry_name }}
                                                                    </span>
                                                                    @endforeach
                                                                    
                                                                </p>
                                                                <small class="text-dark font-weight-bold">Sub Industries:</small>
                                                                <p class="tw-mb-1">
                                                                    @foreach ($freelancer->subindustry as $subindustry)
                                                                    <span class="text-dark d-inline-block tw-text-xs">{{ $subindustry->sub_industry_name }}</span>
                                                                    @endforeach
                                                                </p>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <small class="font-weight-bold text-dark">Skills:</small>
                                                                <ul class="list-inline sm:tw-mb-4 tw-mb-2">
                                                                    @foreach ($freelancer->tag as $tag)
                                                                    <li class="list-inline-item text-dark text-uppercase ">
                                                                        <small class="tw-bg-red-510 tw-text-gray-100 tw-rounded-sm tw-font-bold tw-p-1 tw-px-2 tw-text-xs">
                                                                            {{ $tag->name }} </small></li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                

                                            </div>
                                        </div>
                                        
                                        

                                    </div>
                                </div>
                            </a>
                            @php
                            $count++;
                            @endphp
                            @endif
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col text-center mx-auto">
                {{-- <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">1</span>
            <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">2</span>
            <span class="text-muted bg-white rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">3</span>
            <span class="text-white bg-blue rounded-circle py-2 px-3 mx-2 f-18 shadow-sm">4</span> --}}
                {{-- {{$freelancers->links()}} --}}
            </div>
        </div>
    </div>

</div>

</div>
<script type="text/javascript">
    // $("#count").text({
    //     {
    //         $count
    //     }
    // });

    $('#industryCard').click(function() {
        
        if ($('#collapseOne').hasClass('show')) {
            $('#collapseOne').removeClass('show');
            $('#collapseTwo').removeClass('show');
            $('#collapseThree').removeClass('show');

            $('#industryMinus').addClass('d-none');
            $('#industryPlus').removeClass('d-none');

            $('#subIndustryPlus').removeClass('d-none');
            $('#subIndustryMinus').addClass('d-none');

            $('#skillMinus').addClass('d-none');
            $('#skillPlus').removeClass('d-none');
        } else {
            $('#collapseOne').addClass('show');
            $('#collapseTwo').removeClass('show');
            $('#collapseThree').removeClass('show');

            $('#industryMinus').removeClass('d-none');
            $('#industryPlus').addClass('d-none');

            $('#subIndustryPlus').removeClass('d-none');
            $('#subIndustryMinus').addClass('d-none');

            $('#skillMinus').addClass('d-none');
            $('#skillPlus').removeClass('d-none');
        }
        
    });

    $('#subIndustryCard').click(function() {

        if ($('#collapseTwo').hasClass('show')) {
            $('#collapseOne').removeClass('show');
            $('#collapseTwo').removeClass('show');
            $('#collapseThree').removeClass('show');

            $('#industryMinus').addClass('d-none');
            $('#industryPlus').removeClass('d-none');

            $('#subIndustryPlus').removeClass('d-none');
            $('#subIndustryMinus').addClass('d-none');

            $('#skillMinus').addClass('d-none');
            $('#skillPlus').removeClass('d-none');
        } else {
            $('#collapseOne').removeClass('show');
            $('#collapseTwo').addClass('show');
            $('#collapseThree').removeClass('show');

            $('#industryMinus').addClass('d-none');
            $('#industryPlus').removeClass('d-none');

            $('#subIndustryPlus').addClass('d-none');
            $('#subIndustryMinus').removeClass('d-none');

            $('#skillMinus').addClass('d-none');
            $('#skillPlus').removeClass('d-none');
        }
        
    });

    $('#skillCard').click(function() {
        if ($('#collapseThree').hasClass('show')) {
            $('#collapseOne').removeClass('show');
            $('#collapseTwo').removeClass('show');
            $('#collapseThree').removeClass('show');

            $('#industryMinus').addClass('d-none');
            $('#industryPlus').removeClass('d-none');

            $('#subIndustryPlus').removeClass('d-none');
            $('#subIndustryMinus').addClass('d-none');

            $('#skillMinus').addClass('d-none');
            $('#skillPlus').removeClass('d-none');
        } else {
            $('#collapseOne').removeClass('show');
            $('#collapseTwo').removeClass('show');
            $('#collapseThree').addClass('show');

            $('#industryMinus').addClass('d-none');
            $('#industryPlus').removeClass('d-none');

            $('#subIndustryPlus').removeClass('d-none');
            $('#subIndustryMinus').addClass('d-none');

            $('#skillMinus').removeClass('d-none');
            $('#skillPlus').addClass('d-none');
        }
        
    });

    $('#filter-btn').click(function () {
        $('#filter-section').toggleClass('d-none');
    })
</script>


@endsection