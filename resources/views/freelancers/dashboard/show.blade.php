@extends('backend')
@section('content')
<div class="container py-4">
  <div class="row">
    <div class="col border-0 shadow-sm card p-2">

        <div style="position: absolute; top:0; right: 0;">
          @if (Carbon\Carbon::parse($freelancer->last_login) < Carbon\Carbon::today()->sub(2, 'week'))
                <p class="btn btn-danger btn-sm" disabled>Not very active</p>
                @else
                <p class="btn btn-success btn-sm">Very active</p>
                @endif
        </div>

      <div class="row">
        <div class="col-md-2 col-3 col-lg-1 text-left">
          @if (!empty($freelancer->profile->profile_pic))
          <img class="freelance-profile-pic" src="{{$freelancer->profile->profile_pic}}" alt="Profile Photo" style="height: 90px">
          @else
          <img src="/img/FCPT_Logo.png" alt="Freelance Cape Town Logo" height="90px" width="90px">
          @endif
        </div>

        <div class="col">
          <div class="row">
            <div class="col-sm-6 col pl-lg-2">

              <h4>{{ $freelancer->name }}</h4>
          <p><i class="text-muted fas fa-map-marker-alt"></i> <span class="text-capitalize">{{ $freelancer->profile->city }}</span></p>
            </div>
            <div class="col-md-2">
              <div class="row">
                <div class="col">
                  <small class="font-weight-bold">Rating</small>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="20px">
                  <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="20px">
                  <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="20px">
                  <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="20px">
                  <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="20px">
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
        
      </div>
      <div class="row my-1">
        <div class="col">
          <small class="font-weight-bold text-dark">Industry:</small>
          <p>
            @foreach ($freelancer->industry as $industry)
              <span class="mr-2">{{ $industry->industry_name }}</span>
          @endforeach
          </p>
          <small class="font-weight-bold text-dark">Sub Industries:</small>
          <p>
            @foreach ($freelancer->subindustry as $subindustry)
              <span class="mr-2">{{ $subindustry->sub_industry_name }}</span>
          @endforeach
          </p>
          
          <small class="font-weight-bold text-dark">About me:</small>
          <p>{{ $freelancer->profile->about }}</p>
        </div>
        <div class="col-5 col-md-3 border-left">

          @php
          $string = substr($freelancer->profile->social_media, 1);
          $string = substr($string, 0, -1);
          $social = explode(',', $string);
          $whatsapp = explode(':', $social[0]);
          $facebook = explode(':', $social[1]);
          $linkedin = explode(':', $social[2]);
          $instagram = explode(':', $social[3]);
          @endphp
          <h5>
            <i class="far fa-envelope"></i>
            <span class="font-weight-normal">{{$freelancer->email}}</span>
          </h5>
          <h5>
            @if (!empty($freelancer->profile->mobile))
            <i class="fas fa-mobile-alt"></i>
            <span class="font-weight-normal">{{$freelancer->profile->mobile}}</span>
            @else
                
            @endif
            
          </h5>
          <h5>

            @if (!empty(substr(substr($whatsapp[1], 1), 0, -1)))
            <i class="fab fa-whatsapp text-success"></i>
            <span class="font-weight-normal">{{substr(substr($whatsapp[1], 1), 0, -1)}}</span>
            @endif
          </h5>
          <h5>
            @if (!empty(substr(substr($facebook[1], 1), 0, -1)))
            <i class="fab fa-facebook text-primary"></i>
            <span class="font-weight-normal">{{substr(substr($facebook[1], 1), 0, -1)}}</span>
            @endif
          </h5>
          <h5>
            @if (!empty(substr(substr($linkedin[1], 1), 0, -1)))
            <i class="fab fa-linkedin"></i>
            <span class="font-weight-normal">{{substr(substr($linkedin[1], 1), 0, -1)}}</span>
            @endif
          </h5>
          <h5>
            @if (!empty(substr(substr($instagram[1], 1), 0, -1)))
            <i class="fab fa-instagram"></i>
            <span class="font-weight-normal">{{substr(substr($instagram[1], 1), 0, -1)}}</span>
            @endif
          </h5>
        </div>
      </div>
 
      
      <div class="row">
        <div class="col">
          <h4 class="font-weight-bold">Showcase</h4>
        </div>
      </div>
 
        <div class="row profile_showcase" id="showcase">

        </div>

  
    <script type="text/javascript">
        $.ajax({
            url: "{{url('')}}/api/user/portfolio/{{$freelancer->id}}/complete",
            type: 'GET',
            dataType: 'json',
            success: function(res) {
                console.log(res);
                if (res.length > 0) {
                    var showcase_data = '';
                    $.each(res, function(index) {
                        console.log(res[index].photo);
                        if (res[index].photo) {
                            var showcase_photo = `<img src="` + res[index].photo + `" class="mb-1" alt="" width="275px">`;
                        } else {
                            var showcase_photo = `<img src="/img/FCPT_Logo.png" class="mb-1" alt="" width="275px">`;
                        }
                        showcase_data += `<div class="col-4 mb-3 mb-sm-1">` +
                            `<div class="card border-0 text-left shadow-sm">` +
                            showcase_photo +
                            `<h4>` + res[index].title + `</h4>` +
                            `<small class="text-dark font-weight-bold">Description:</small>` +
                            `<p>` + res[index].description + `</p>` +
                            `</div>` +
                            `</div>`;
                    })
                    $('#portfolio_header').text("Portfolio");
                    $("#showcase").html(showcase_data);
                    // console.log("Data " + showcase_data);
                } else {}
            }
        });

        
    </script>
      <div class="row">
        <div class="col">

        </div>
      </div>
      <div class="row">
        <div class="col">

        </div>
      </div>
    </div>
  </div>

</div>
@endsection