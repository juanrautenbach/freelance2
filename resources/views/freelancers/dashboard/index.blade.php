@extends('backend')
@section('content')
<style media="screen">
    .filter {
      display: none;
    }
  </style>
  <div class="container-fluid my-1">
    <div class="row">
      <div class="col-lg-3 col-md-3">
        <div class="accordion" id="accordionExample">
          <div class="card border-0">
            <div class="card-header border-0 bg-white text-dark">
              <h4 class="font-weight-bold d-inline">Filter by</h4>
              <span class="btn btn-red btn-sm float-right" id="reset-btn">Reset</span>
            </div>
          </div>
          <div class="card border-0" id="industryCard">
            <div class="card-header bg-white" id="headingOne">
              <p class="mb-0">
                <a class="text-dark font-weight-bold d-block" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Industry
                  <span class="float-right" id="industryMinus"><i class="fas fa-minus"></i></span>
                  <span class="float-right d-none" id="industryPlus"><i class="fas fa-plus"></i></span>
                </a>
              </p>
            </div>
  
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <p class="font-weight-bold mb-1">All Industries</p>
                <ul class="pl-0">
                  @if (!empty($dashboard_industries))
                  
                  {{-- @foreach ($dashboard_industries as $industry)
                    @php
                        echo $industry->industry_name
                    @endphp
                      
                  @endforeach --}}
                  @foreach ($dashboard_industries as $industry)
                    
                    <li style="list-style-type: none"><a class="pointer-cursor text-dark" onclick="filter('category', 'filter-industry', '{{ $industry }}'); this.onclick=null;">- {{ $industry }}</a></li> 
               
                  
                  @endforeach
                  @endif
  
  
                </ul>
              </div>
            </div>
          </div>
          <div class="card border-0" id="subIndustryCard">
            <div class="card-header bg-white" id="headingTwo">
              <p class="mb-0">
                <a class="d-block collapsed text-dark font-weight-bold" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Sub Industry
                  <span class="float-right" id="subIndustryPlus"><i class="fas fa-plus"></i></span>
                  <span class="float-right d-none" id="subIndustryMinus"><i class="fas fa-minus"></i></span>
                </a>
              </p>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                <p class="font-weight-bold mb-1">All Sub Industries</p>
                <div class="pl-0">
                  @if (!empty($dashboard_subindustries))
                  @foreach ($dashboard_subindustries as $subindustry)
                  <li class="mb-1" style="list-style-type: none"><a class="pointer-cursor text-dark" onclick="filter('category', 'sub-industry-filter', '{{ $subindustry }}'); this.onclick=null;">- {{ $subindustry }}</a></li>
                  @endforeach
                  @endif
  
                </div>
              </div>
            </div>
          </div>
          <div class="card border-0" id="skillCard">
            <div class="card-header bg-white" id="headingThree">
              <p class="mb-0">
                <a class="d-block collapsed text-dark font-weight-bold" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Find by skill
                  <span class="float-right" id="skillPlus"><i class="fas fa-plus"></i></span>
                              <span class="float-right d-none" id="skillMinus"><i class="fas fa-minus"></i></span>
                </a>
              </p>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                @if (!empty($dashboard_tags))
                @foreach ($dashboard_tags as $tag)
                <li class="mb-1" style="list-style-type: none"><a class="pointer-cursor text-dark text-uppercase" onclick="filter('category', 'tag-filter', '{{ $tag }}')">- {{ $tag }}</a></li>
                @endforeach
                @endif
  
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9 mx-0 pr-md-5">
        <div class="card border-0 shadow-sm px-md-4 px-2 py-4">
          <div class="row">
            <div class="col">
              <h3 class="d-md-inline mb-1 mb-md-3 font-weight-bold">Search Freelancers</h3>
  
              <span class="float-md-right d-block d-md-inline text-muted mt-2">Total results: <span id="count"></span> </span>
              <div class="row my-1">
                <div class="col-12 col-md-10 px-0">
                  <input type="text" name="search" class="form-control mt-1 mb-2 mb-md-0 main-panel-search" placeholder="Search for any thing, skill, industry, name" onkeyup="filter('freetext','freelancer-cards','main-panel-search')">
                </div>
              </div>
  
              <div class="row">
                <div class="col">
                  @if (!empty($freelancers))
                  @php
                     $count = 0;
                  @endphp
                  @foreach ($freelancers as $freelancer)
                  @if (empty($freelancer->profile->profile_pic))
  
                  @else
                  <hr class="my-0">
                  <a href="/freelancer/show/{{$freelancer->id}}" class="freelancer-cards">
                    <div class="row freelancer-row py-1">
                      <div class="col">
                        <div class="row">
                          <div class="col">
                            <div class="row">
                              <div class="col-1 text-right">
                                @if (!empty($freelancer->profile->profile_pic))
                                <img class="freelance-profile-pic" src="{{$freelancer->profile->profile_pic}}" alt="Profile Photo" style="height: 80px">
                                @else
                                <img src="/img/FCPT_Logo.png" alt="Freelance Cape Town Logo" height="80px" width="80px">
                                @endif
                              </div>
                              <div class="col px-4">
                                <div class="row">
                                  <div class="col">
                                    <p class="d-inline font-weight-bold text-capitalize text-dark">{{$freelancer->name}}</p>
                                    <p class="text-muted">{{$freelancer->role}}</p>
                                  </div>
                                  <div class="col">
                                    <small class="font-weight-bold text-dark d-block">Rating</small>
                                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="20px">
                                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="20px">
                                    <img src="/img/ratings/filled_star.png" alt="User Rating Star" height="20px">
                                    <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="20px">
                                    <img src="/img/ratings/empty_star.png" alt="User Rating Star" height="20px">
                                  </div>
                                </div>
                              </div>
                              
                            </div>
  
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="filter filter-industry">
                              @foreach ($freelancer->industry as $industry)
                              {{ $industry->industry_name }} |
                              @endforeach
                            </div>
                            <div class="filter sub-industry-filter">
                              @foreach ($freelancer->subindustry as $subindustry)
                              {{ $subindustry->sub_industry_name }} |
                              @endforeach
                            </div>
  
                            <div class="filter tag-filter">
                              @foreach ($freelancer->tag as $tag)
                              {{ $tag->name }} |
                              @endforeach
                            </div>
                          </div>
                        </div>
                        <div class="row mt-2">
                          <div class="col">
                            <small class="text-dark font-weight-bold">Industry:</small>
                            <p class="mb-1">
                              @foreach ($freelancer->industry as $industry)
                              <span class="mr-1 text-dark d-inline-block">
                                {{ $industry->industry_name }}
                              </span>
                              @endforeach
                              
                            </p>
                            <small class="text-dark font-weight-bold">Sub Industry:</small>
                            <p>
                              @foreach ($freelancer->subindustry as $subindustry)
                              <span class="mr-1 text-dark mb-1 d-inline-block">{{ $subindustry->sub_industry_name }}</span>
                              @endforeach
                            </p>
                            <small class="mb-1 font-weight-bold text-dark">About me</small>
                            <p class="text-dark">{{ \Illuminate\Support\Str::limit($freelancer->profile->about, 150, $end='...') }}</p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <small class="font-weight-bold mb-1 text-dark">Skills</small>
                            <ul class="list-inline">
                              @foreach ($freelancer->tag as $tag)
                              <li class="list-inline-item text-dark text-uppercase ">
                                <small class="">
                                  <i class="fas fa-angle-right" style="font-size: 14px"></i>
                                  {{ $tag->name }} </small></li>
  
                              @endforeach
                            </ul>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
  
                          </div>
                        </div>
  
                      </div>
                    </div>
                  </a>
                  @php
                     $count++;
                  @endphp
                  @endif
                  @endforeach
                  @else
  
                  @endif
  
  
  
                </div>
              </div>
            </div>
          </div>
  
        </div>
  
      </div>
  
    </div>
  
  </div>
  
  
  <script type="text/javascript">
    // $("#count").text();
  
    $('#industryCard').click(function() {
          // alert('industry');
          $('#collapseOne').addClass('show');
          $('#collapseTwo').removeClass('show');
          $('#collapseThree').removeClass('show');
  
          $('#industryMinus').removeClass('d-none');
          $('#industryPlus').addClass('d-none');
  
          $('#subIndustryPlus').removeClass('d-none');
          $('#subIndustryMinus').addClass('d-none');
  
          $('#skillMinus').addClass('d-none');
          $('#skillPlus').removeClass('d-none');
      });
  
      $('#subIndustryCard').click(function() {
          $('#collapseOne').removeClass('show');
          $('#collapseTwo').addClass('show');
          $('#collapseThree').removeClass('show');
  
          $('#industryMinus').addClass('d-none');
          $('#industryPlus').removeClass('d-none');
  
          $('#subIndustryPlus').addClass('d-none');
          $('#subIndustryMinus').removeClass('d-none');
  
          $('#skillMinus').addClass('d-none');
          $('#skillPlus').removeClass('d-none');
      });
  
      $('#skillCard').click(function() {
          $('#collapseOne').removeClass('show');
          $('#collapseTwo').removeClass('show');
          $('#collapseThree').addClass('show');
  
          $('#industryMinus').addClass('d-none');
          $('#industryPlus').removeClass('d-none');
  
          $('#subIndustryPlus').removeClass('d-none');
          $('#subIndustryMinus').addClass('d-none');
  
          $('#skillMinus').removeClass('d-none');
          $('#skillPlus').addClass('d-none');
      });
  </script>
@endsection