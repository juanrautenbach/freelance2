@extends('backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            {{-- <div class="col-lg-3 col-md-3 h-100 navbar-dark tw-my-3 tw-rounded-r tw-p-4">
                <h1 class="tw-text-red-400">Hello there!</h1>
            </div> --}}
            <div class="col">
                <div class="row tw-my-5">
                    <div class="col">
                        <h3>Subscriptions</h3>
                    </div>
                    <div class="col tw-text-right">
                        <a href="/subscription/add" class="btn btn-red">New</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <table class="table">
                            <tr>
                                <th class="tw-text-sm tw-font-bold">Subscription Plan</th>
                                <th class="tw-text-sm tw-font-bold">Started</th>
                                <th class="tw-text-sm tw-font-bold">Expires</th>
                                <th class="tw-text-sm tw-font-bold">Status</th>
                                {{-- <th>Actions</th> --}}
                            </tr> 
                            @foreach ($subscriptions as $subscription)
                            <tr>
                                <td>
                                    @foreach ($subs_plans as $sub_plan)

                                        @if ($sub_plan->id == $subscription->subs_id)
                                            {{ $sub_plan->name }}
                                        @else
                                            
                                        @endif
                                    @endforeach
                                </td>
                                <td>{{ $subscription->start_date }}</td>
                                <td>{{ \Carbon\Carbon::parse($subscription->expire_date)->DiffForHumans() }}</td>
                                <td>
                                @if (date('Y-m-d') < $subscription->expire_date)
                                    {!! "<span class='tw-text-green-500'>Valid</span> "!!}
                                @else
                                {!! "<span class='tw-text-red-500'>Expired</span> "!!}
                                @endif
                                </td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                            
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection