@extends('front')
@section('content')
<section class="profile-dashboard tw-max-w-max">

    <div class="container-fluid">
        <div class="row profile_cover">
            <div class="col p-0">
                @if (empty($user->profile->profile_cover_pic))
                <div class="d-block" style="height: 120px; width:100vw; background: #D8D8D8">
                </div>
                @else
                <img src="/storage/cover_photo/crop/{{$user->profile->profile_cover_pic}}" class="cover_photo_image " alt="">
                @endif
            </div>
        </div>

        <div class="row tw--mt-8 md:tw--mt-10 tw-mb-4 md:tw-px-10 d-flex">
            <div class="md:tw-w-60 tw-w-full tw-relative tw-px-4 tw-block md:tw-inline ">
                <div class="col">
                    <div class="md:tw-w-60 tw-w-full tw-h-full" id="profile_photo">
                        @if (empty($user->profile->profile_pic))
                        <img src="http://via.placeholder.com/275" alt="" class="tw-shadow tw-max-w-xxs">
                        @else
                        <img src="{{$user->profile->profile_pic}}" alt="" class="tw-shadow tw-border-solid tw-border-4 tw-border-white profile_photo_image tw-w-full xs:tw-w-60">
                        @endif
    
                        <div class="profile_photo_hover" id="profile_photo_hover" style="display:none">
                            <div class="photo_update">
                                <i class="fas fa-camera"></i>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="tw-relative tw-inline tw-px-8 col">
                <div class="row tw-mt-6 md:tw-mt-12">
                    <div class="col tw-px-8">
                        <div class="name">
                            <h3>{{$user->name}}</h3>
                            
                        </div>
                    </div>
                    <div class="col">
                        <div class="tw-relative">
                            <img src="/img/ratings/filled_star.png" alt="User Rating Star" class="tw-h-12 tw-w-12">
                        <div class="tw-absolute tw-top-0 tw-z-10 tw-h-12 tw-w-12">
                            <div class="tw-flex tw-justify-center tw-content-center flex-wrap tw-h-12">
                                <span class="tw-text-gray-100 tw-text-xs tw-font-bold">56</span> 
                            </div>
                            
                            
                        </div>
                    </div>
                    </div>
                    
                </div>
                <div class="row tw-mt-1">
                    <div class="col tw-px-8">
                            @foreach ($user->industry as $value)
                                <h2 class="font-weight-normal tw-inline tw-text-sm">{{$value->industry_name}}</h2>
                            @endforeach
                    </div>
                </div>
                <div class="row tw-mt-1">
                    <div class="col tw-px-8">
                            @foreach ($user->subindustry as $value)
                                <h2 class="font-weight-normal tw-inline tw-text-sm tw-mr-3">{{$value->sub_industry_name}}</h2>
                            @endforeach
                    </div>
                </div>
                <div class="row tw-mt-3">
                    <div class="col tw-px-8">
                        @if (!empty($tags) > 0)
                            @foreach ($tags as $tag)
                                <span class="tw-text-xs tw-shadow tw-rounded-lg tw-bg-red-510 tw-text-gray-100 tw-px-2 tw-py-1 text-uppercase">{{$tag->name}}</span>
                            @endforeach
                        @endif
                    </div>
                </div> 
            </div>
        </div>
            <div class="tw-relative lg:tw--mt-12 d-none d-sm-block">
                <div class="contact-button tw-flex tw-items-end tw-h-12">
                    <div class="tw-ml-auto tw-mr-0">
                        <button href="/dashboard" class="tw-ml-auto btn-red tw-px-5 tw-py-3 tw-mr-10">Contact me</button>
                    </div>
                    
                </div>
            </div>
        
        <div class="row md:tw-px-14 tw-px-8">
            <div class="tw-relative tw-block tw-mt-4 tw-mx-5">
                <small class="mb-4 font-weight-bold tw-text-xs">About me</small>
                <p>{!! nl2br(e($user->profile->about)) ?? "Not set"!!}</p> 
            </div>
        </div>
        
    </div>
    
    
    <div class="container-fluid join-global-co tw-h-40 tw-mt-10">
            <div class="row tw-h-full">
                <div class="tw-w-full md:tw-w-2/3 tw-text-center md:tw-relative md:tw-flex md:tw-justify-center tw-block tw-text-center">
                    
                    <h2 class="tw-text-base md:tw-text-xl md:tw-self-center tw-mt-8 md:tw-my-0">Join the fastest growing global economy</h2>
                </div>
                <div class="tw-w-full md:tw-w-1/3 tw-text-center md:tw-flex md:tw-relative tw-block">
                    <div class="md:tw-self-center tw-text-center tw-mb-3">
                        <a href="/dashboard" class="btn-red tw-inline-block tw-p-3 md:tw-px-6">Brief me</a>
                    </div>
                    
                </div>
            </div>
    </div>

    

    <div class="container-fluid d-none">
        <div class="row profile_portfolio">
            <div class="col p-0">
                <span>
                    <h4 class="d-inline ml-4 pt-5" id="portfolio_header"></h4>
                </span>
            </div>
        </div>

    </div>
    
    @if (count($user->showcase) > 0)
    <div class="container-fluid tw-my-12">
        <div class="row tw-mb-5">
            <div class="col tw-text-center">
                <h2>Showcase</h2>
            </div>
        </div>
        <div class="row profile_showcase" id="showcase">

        </div>
    </div>
    @else
        
    @endif

    <script type="text/javascript">
        $.ajax({
            url: "{{url('')}}/api/user/portfolio/{{$user->id}}/complete",
            type: 'GET',
            dataType: 'json',
            success: function(res) {
                // console.log(res);
                if (res.length > 0) {
                    var showcase_data = '';
                    $.each(res, function(index) {
                        var description = res[index].description;
                        if (description == null) {
                            description = "";
                        } 
                        if (res[index].photo) {
                            var showcase_photo = `<img src="` + res[index].photo + `" class="tw-mb-3 tw-p-2 tw-w-full" alt=""`;
                        } else {
                            var showcase_photo = `<img src="/img/FCPT_Logo.png" class="tw-mb-3 mx-auto tw-w-full" alt="">`;
                        }
                        showcase_data += `<div class="col-md-4 mb-3 mb-sm-1">` +
                            `<div class="card border-0 tw-shadow tw-h-full">` +
                            showcase_photo +
                            `<div class="row"><div class="col"><p>` + description + `</p></div></div>` +
                            `</div>` +
                            `</div>`;
                    })
                    $('#portfolio_header').text("Portfolio");
                    $("#showcase").html(showcase_data);
                    // console.log("Data " + showcase_data);
                } else {}
            }
        });

        $("#aboutEdit").click(function() {
            $("#edit_about").show();
        })
        $("#cancel_about").click(function() {
            $("#edit_about").hide();
        })
        $(".showcaseAdd").click(function() {
            $("#add_showcase").show();
        })

        $("#cancelshowcase").click(function() {
            $("#add_showcase").hide();
        })

        $("#editContact").click(function() {
            $("#edit_contact").show();
        })
        $("#cancel_contact").click(function() {
            $("#edit_contact").hide();
        })

        $("#editskills").click(function() {
            $("#edit_skills").show();
        })
        $("#cancel_skills").click(function() {
            $("#edit_skills").hide();
        })

        $("#up_photo_empty").click(function() {
            $("#up_photo_empty").hide();
            $("#up_photo_selected").show();
            $("#up_video_selected").hide();
            $("#up_video_empty").show();
            $("#video_url").hide();
        })
        $("#up_photo_selected").click(function() {
            $("#up_photo_empty").show();
            $("#up_video_empty").show();
            $("#up_video_selected").hide();
            $("#up_photo_selected").hide();
            $("#video_url").hide();
        })

        $("#up_video_empty").click(function() {
            $("#up_video_empty").hide();
            $("#up_photo_empty").show();
            $("#up_photo_selected").hide();
            $("#up_video_selected").show();
            $("#video_url").show();
        })
        $("#up_video_selected").click(function() {
            $("#up_video_empty").show();
            $("#up_video_selected").hide();
            $("#video_url").hide();
        })
    </script>
</section>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" charset="utf-8"></script>
<script src="/js/dynamicdropdown.js" charset="utf-8"></script>
<script src="/js/tags.js" charset="utf-8"></script>
@endsection