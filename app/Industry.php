<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
  protected $fillable = ['name'];
  // Table Name
  public $table = "industries";
  // Primary Key
  public $primaryKey = "id";
  // Timestamps
  public $timestamps = true;

  protected $with = ['subindustry'];

  public function subindustry()
  {
    return $this->hasMany('App\SubIndustry', 'industry_id');
  }
  public function user()
  {
    return $this->belongsToMany('App\User', 'user_industry', 'industry_id');
  }

  public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

}
