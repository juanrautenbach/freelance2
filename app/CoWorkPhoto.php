<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoWorkPhoto extends Model
{
  public function co_work_space()
  {
    return $this->belongsTo('App\CoWorkSpace');
  }
}
