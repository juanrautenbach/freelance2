<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{

  public $table = "user_profiles";
  // Primary Key
  public $primaryKey = "id";
  // Timestamps
  public $timestamps = true;
  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
