<?php

namespace App\Http\Controllers;
use App\Mail\GeneralMail;
use App\User;

use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function email_freelancers()
    {
        return view('emails.freelancers');
    }

    public function send_mail_freelancers(Request $request)
    {
        $freelancers = $request->freelancers;

        foreach($freelancers as $freelancer)
        {
            
        }
        $users = User::where('email', 'juan@julura.co.za')->get();
        $text = "<h4>Hi there</h4>";

        foreach($users as $user)
        {
            $data = ['message' => $text];
            \Mail::to($user->email)->send(new GeneralMail($user, $data));
        }

        return redirect('/email/freelancers');
    }

    public function email_finders()
    {
        return view('emails.finders');
    }

    public function email_co_working()
    {
        return view('emails.co_working');
    }
}
