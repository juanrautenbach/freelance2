<?php

namespace App\Http\Controllers;

use App\Messages;
use App\User;
use App\Mail\NewMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MessagesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    date_default_timezone_set("Africa/Johannesburg");
    $user = \Auth::user();
    $users = User::where('id', '<>', auth()->id())->orderBy('name', 'asc')->get();
    $user_id = $user->id;
    $all_messages = DB::table('messages')->get();

    // All messages
    $messages = Messages::where('user_id', $user->id)->get();

    foreach ($messages as $message) {
      $message->status = "Read";
      $message->save();
    }

    // $message_users = Messages::where('user_id', '=', $user_id)->distinct()->get('sender_id');


    $last_login = $user->last_login;

    $unread_msg = DB::table('messages')->where([
      ['created_at', '>', $last_login],
      ['user_id', '=', $user_id],
      ['status', '<>', 'Trash']
    ])->count();
    // return "Hello";
    // return $sent_messages;
    return view('messages.index')
      ->with('user', $user)
      ->with('users', $users);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    // $message = new Messages;

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // return $request;
    date_default_timezone_set("Africa/Johannesburg");
    $message = new Messages;
    $message->sender_id = $request->input('sender_id');
    $message->sender_name = $request->input('sender_name');
    $message->user_id = $request->input('user_id');
    $message->user_name = $request->input('user_name');
    $message->subject = "";
    $message->messages = $request->input('message_text');
    $message->post_id = $request->input('post_id');
    $message->status = "New";
    $message->save();

    $notify_email = User::where('id', '=', $message->user_id)->first();

    // $details = ['email' => $notify_email];
    // $emailJob = (new NewMessage($message))->delay(Carbon::now()->addMinutes(1));
    // dispatch($emailJob);

    // \Mail::to($notify_email)->send(new NewMessage($message));

    // return redirect('/messages')->with('message', 'Message sent!')->with('message_type', 'success');
    return redirect()->back()->with('success', 'Message sent!');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Messages  $messages
   * @return \Illuminate\Http\Response
   */
  public function show(Messages $messages)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Messages  $messages
   * @return \Illuminate\Http\Response
   */
  public function edit(Messages $messages)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Messages  $messages
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Messages $messages)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Messages  $messages
   * @return \Illuminate\Http\Response
   */
  public function destroy(Messages $messages)
  {
    //
  }

  public function showSubject($subject)
  {
    $messages = DB::table('messages')->where('subject', $subject)->orderBy('created_at', 'desc')->get();

    return view('messages.view')->with('messages', $messages);
  }

  public function trash($id)
  {
    $message = Messages::find($id);
    $message->status = "Trash";
    $message->save();
    return redirect('/messages')->with('message', 'Message deleted!');
  }
  public function restore($id)
  {
    $message = Messages::find($id);
    $message->status = "new";
    $message->save();
    return redirect('/messages')->with('message', 'Message restored!');
  }

  public function get_message($id)
  {
    $message = Messages::find($id);
    // return $message->sender_id;
    $sender = User::findOrFail($message->sender_id);
    $message->sender_name = $sender->name;
    // return $sender->name;
    // return $message->user_id;
    $message_user = User::findOrFail($message->user_id);
    $message->user_name = $message_user->name;

    return $message;
  }

  public function make_read($id)
  {
    $messages = Messages::where('user_id', $id)->get();

    foreach ($messages as $message) {
      $message->status = "Read";
      $message->save();
    }

    return redirect('/messages');
  }
}
