<?php

namespace App\Http\Controllers;

use App\LinkClicked;
use App\User;
use Illuminate\Http\Request;

class LinkClickedController extends Controller
{
    public function freelance_click($id)
    {
        $freelancer = User::findOrFail($id);

        $freelancer->click_count = $freelancer->click_count + 1;
        $freelancer->save();

        return redirect('/freelancer/' . $id);

    }
}
