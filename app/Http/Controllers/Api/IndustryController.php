<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Industry;

class IndustryController extends Controller
{
    public function industries()
    {
      return Industry::orderBy('name', 'asc')->get(['id','name']);
    }

    public function subindustry(Industry $industry)
    {
        return $industry->subindustry()->orderBy('name', 'asc')->get(['id', 'name']);
    }
}
