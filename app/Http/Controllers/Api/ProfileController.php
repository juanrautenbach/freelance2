<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserProfile;
use App\User;

class ProfileController extends Controller
{
    public function freelance_about_update(Request $request)
    {
        $user_id = $request['user_id'];
        
        $user = User::where('id',$user_id)->first();

        $user->profile->about = $request['about'];
        $user->profile->save();

        return $user->profile->about;
    }

    public function freelance_contact_update(Request $request)
    {
        $user_id = $request['user_id'];
        
        $user = User::where('id',$user_id)->first();

        $data = new \stdClass();

        $social_media = new \stdClass();

        if (!empty($request['social'][0])) {
            $social_media->whatsapp = $request['social'][0];
            $data->whatsapp = $request['social'][0];
        }else {
            $social_media->whatsapp = "";
            $data->whatsapp = $request['social'][0];
        }
        if (!empty($request['social'][1])) {
            $social_media->facebook = $request['social'][1];
            $data->facebook = $request['social'][1];
        }else {
            $social_media->facebook = "";
            $data->facebook = $request['social'][1];
        }
        if (!empty($request['social'][2])) {
            $social_media->linkedin = $request['social'][2];
            $data->linkedin = $request['social'][2];
        }else {
              $social_media->linkedin = "";
              $data->linkedin = $request['social'][2];
        }
        if (!empty($request['social'][3])) {
          $social_media->instagram = $request['social'][3];
          $data->instagram = $request['social'][3];
        }else {
          $social_media->instagram = "";
          $data->instagram = $request['social'][3];
        }

        $data->company = $request['company'];
        $data->city = $request['city'];
        $data->mobile = $request['mobile'];
        $data->tel = $request['tel'];

        $social_media = json_encode($social_media);

        $user->profile->company_name = $request['company'];
        $user->profile->city = $request['city'];
        $user->profile->mobile = $request['mobile'];
        $user->profile->tel = $request['tel'];
        $user->profile->social_media = $social_media;
        $user->profile->save();

        return json_encode($data);
    }
}
