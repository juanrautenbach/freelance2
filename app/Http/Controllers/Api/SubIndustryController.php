<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubIndustry;

class SubIndustryController extends Controller
{
    public function delete_subindustry(Request $request)
    {
        $subindustry = SubIndustry::findOrFail($request->subindustry_id);
        $subindustry->delete();

        return "Deleted";
    }

    public function add_subindustry(Request $request)
    {
        $subindustry = new SubIndustry;
        
    }
}
