<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class FunctionController extends Controller
{
    public function check_email($email)
    {
      $user = User::where('email', $email)->first();

      if (!empty($user)) {
        return "True";
      } else {
        return "False";
      }

    }

    public function get_freelancers()
    {
      $freelancers = User::where('role', 'Freelancer')->get();

      return $freelancers;
    }

    public function get_finders()
    {
      $finders = User::where('role', 'Finder')->get();

      return $finders;
    }

    public function get_freelancer($id)
    {
      $user = User::find($id);

      return $user;
    }
}
