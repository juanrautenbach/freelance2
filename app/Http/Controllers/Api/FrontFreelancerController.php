<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;

class FrontFreelancerController extends Controller
{
   public function freelancer_by_tag_name($name)
   {
     // return $name;

     $tag_users = DB::table('tags')->where('name', $name)->get();

     if (empty($tag_users)) {
       return "No data";
     } else {
       $user_data = array();
       foreach ($tag_users as $tag_user) {
         array_push($user_data, $tag_user->user_id);
       }
       $data = array();
       $user_data = array_unique($user_data);
       foreach ($user_data as $value) {
         $freelancers = DB::table('users')->where('id', $value)->get();
         array_push($data, $freelancers);
       }
       return $data;
     }


   }

   public function freelancer_by_sub_industry_name($name)
   {
     return $name;

     $tag_users = DB::table('tags')->where('name', $name)->get();

     if (empty($tag_users)) {
       return "No data";
     } else {
       $user_data = array();
       foreach ($tag_users as $tag_user) {
         array_push($user_data, $tag_user->user_id);
       }
       $data = array();
       foreach ($user_data as $value) {
         $freelancers = DB::table('users')->where('id', $value)->get();
         array_push($data, $freelancers);
       }
       return $data;
     }


   }

   public function freelancer_active_all()
   {
     $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->get();

     return $freelancers;
   }
}
