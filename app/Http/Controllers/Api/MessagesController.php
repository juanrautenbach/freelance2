<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Messages;
use \stdClass;

class MessagesController extends Controller
{
    public function all_user_messages($id)
    {
        // return $id;
         $user = User::where('id', $id)->first()->messages;

         return $user;
    }

    public function message_users($id){

        $message_users = Messages::where('user_id', '=', $id)->distinct()->get('sender_id');

        $data = new stdClass();
        

        // return $message_users;
        $users = array();
        foreach ($message_users as $message_user){
            
            $user = User::where('id', $message_user->sender_id)->first();
            
            array_push($users, $user);
            
        }

        $data->senders = $users;
        $data->user_id = $id;
        return json_encode($data);
    } 

    public function getMessages($sender_id, $user_id)
    {

        $messages_data = Messages::where([['sender_id', $sender_id], ['user_id', $user_id]])->orWhere([['sender_id', $user_id], ['user_id', $sender_id]])->get();

        $messages = new stdClass();
        
        $messages->data = $messages_data;

        // $sendMessages = Messages::where([
        //     ['sender_id', '=', $sender_id],
        //     ['user_id', '=', $user_id]
        // ])->orderBy('created_at', 'desc')->get();

        // $receivedMessages = Messages::where([
        //     ['sender_id', '=', $user_id],
        //     ['user_id', '=', $sender_id]
        // ])->orderBy('created_at', 'desc')->get();

        $sender = User::find($sender_id);
        $user = User::find($user_id);

        // $messages->received = $receivedMessages;
        // $messages->send = $sendMessages;
        $messages->sender = $sender;
        $messages->user = $user;

        return json_encode($messages);
    }
}
