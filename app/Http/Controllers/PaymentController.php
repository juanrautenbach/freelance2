<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use IoDigital\Payfast\Contracts\PaymentProcessor;
// use Billow\Contracts\PaymentProcessor;
use Carbon\Carbon;
use App\User;
use \stdClass;
use App\Transaction;
use App\Payment;
use App\Voucher;
use App\SubscriptionPlan;

use App\Gateways\Paygate\PaygateGlobal;
use App\Gateways\PayGate\PayWeb3;


class PaymentController extends Controller
{
    public function cancelled(){
      return redirect('/dashboard')->with('message', 'Payment not successful!')->with('message_type', 'error');
    }
    public function success(PaymentProcessor $payfast){

      // return $payfast;
      $user_id = \Auth::user()->id;

      $user_subs = \DB::table('subscriptions')->where('user_id', $user_id)->latest()->first();

      if (empty($user_subs)) {
        $update_login_table = \DB::table('subscriptions')->insert([
          'user_id' => $user_id,
          'subs_id' => '1',
          'start_date' => date('Y-m-d H:i:s'),
          'expire_date' => Carbon::now()->addMonths(6),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      } else {
        $update_login_table = \DB::table('subscriptions')->insert([
          'user_id' => $user_id,
          'subs_id' => '1',
          'start_date' => $user_subs->expire_date->addDay(),
          'expire_date' => $user_subs->expire_date->addDay()->addMonths(6),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      }
      

      $update_login_table = \DB::table('subscriptions')->insert([
                'user_id' => $user_id,
                'subs_id' => '1',
                'start_date' => date('Y-m-d H:i:s'),
                'expire_date' => Carbon::now()->addMonths(6),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
      ]);

      $user = User::findOrFail($user_id);

      $user->status = "Activated";
      $user->save();
      // dd($payfast);
      return $payfast->responseVars();
      // return view('payments.success');
      // return redirect('/users/profiles/')->with('success', 'Payment Successful!')->with('message_type', 'success')->with('paygate', $payfast);
    }



// Class PaymentController extends Controller
// {

    public function confirmPayment(Request $request)
    {
      
      $user_id = \Auth::user()->id;
      if (!empty($request->voucher)) {
        $voucher = Voucher::where('code', $request->voucher)->first();
        $voucher->status = "Redeemed";
        $voucher->save();

        $voucher_amount = $voucher->amount;
        $voucher_code = $voucher->code;
      } else {
        $voucher = '';
        $voucher_amount = 0;
        $voucher_code = '';
      }

      $subscription = SubscriptionPlan::where('id', $request->subsplan_id)->first();
      // return $subscription;
      $expiry_date_current = \DB::table('subscriptions')->where('user_id', $user_id)->orderBy('expire_date', 'desc')->first();

      // $expiry_date_add = Carbon::parse($subscription->duration)->format('Y-m-d');

      if (empty($expiry_date_current)) {
        $expiry_date_add = Carbon::parse($subscription->duration)->format('Y-m-d');
        $start_date = date('Y-m-d H:i:s');
      } else {
        $months = explode(" ", $subscription->duration);

        $expiry_date_add = Carbon::parse($expiry_date_current->expire_date)->addMonths($months[0])->format('Y-m-d');
        $start_date = $expiry_date_current->expire_date;
      }

      if ($request->cart_total == 0) {
        

      $update_login_table = \DB::table('subscriptions')->insert([
                'user_id' => $user_id,
                'subs_id' => $subscription->id,
                'start_date' => $start_date,
                'expire_date' => $expiry_date_add,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
      ]);

      $user = User::findOrFail($user_id);

      $user->status = "Activated";
      $user->save();
      return redirect('/users/profiles/')->with('message', 'Payment Successful!')->with('message_type', 'success');
      
    
    }else{

      $voucher_amount = str_replace('R ', '', $voucher_amount);
      // return $voucher_amount;

      $sub_amount = (float)$voucher_amount + (float)$request->subs_price;
      // return $sub_amount;
      // return $voucher;

      $return_url = config('custom.return-url');
      $notify_url = config('custom.notify-url');
      $paygate_id = config('custom.paygate_id');
      $paygate_secret = config('custom.paygate_secret');

      $payment_details = new stdClass();

      $transaction = new Transaction;
      $transaction->user_id	= \Auth::user()->id;
      $transaction->voucher_code = $voucher_code;
      $transaction->voucher_amount = $voucher_amount;
      $transaction->subscription_amount = $sub_amount;
      $transaction->subscription_duration = $request->item_title;
      $transaction->total_amount = $request->cart_total;
      $transaction->first_name = $request->user_firstname;
      $transaction->last_name = $request->user_lastname;
      $transaction->email = $request->user_email;
      $transaction->description = $request->item_description;
      $transaction->reference = $request->user_id;
      $transaction->save();

      // return $transaction;
      $payment_details->transaction = $transaction;


          $encryption_key = $paygate_secret;
          $trackingid = md5($transaction->user_id . $transaction->total_amount . $transaction->reference . $transaction->first_name . $transaction->last_name . $transaction->email);

          $transaction->payment_reference = $trackingid;
          $transaction->save();

          $DateTime = new \DateTime();

          $mandatoryFields = array(
        		'PAYGATE_ID'        => filter_var($paygate_id, FILTER_SANITIZE_STRING),
        		'REFERENCE'         => $request->user_id,
        		'AMOUNT'            => filter_var($transaction->total_amount * 100, FILTER_SANITIZE_NUMBER_INT),
        		'CURRENCY'          => "ZAR",
        		'RETURN_URL'        => filter_var($return_url . '/' . $trackingid, FILTER_SANITIZE_URL),
        		'TRANSACTION_DATE'  => filter_var($DateTime->format('Y-m-d H:i:s'), FILTER_SANITIZE_STRING),
        		'LOCALE'            => filter_var('en-za', FILTER_SANITIZE_STRING),
        		'COUNTRY'           => filter_var('ZAF', FILTER_SANITIZE_STRING),
        		'EMAIL'             => filter_var($transaction->email, FILTER_SANITIZE_EMAIL)
        	);

        	$optionalFields = array(
        		'PAY_METHOD'        => (isset($payment_methods) ? filter_var($_POST['PAY_METHOD'], FILTER_SANITIZE_STRING) : ''),
        		'PAY_METHOD_DETAIL' => (isset($_POST['PAY_METHOD_DETAIL']) ? filter_var($_POST['PAY_METHOD_DETAIL'], FILTER_SANITIZE_STRING) : ''),
        		'NOTIFY_URL'        => $notify_url . '/' . $trackingid,
        		'USER1'             => (isset($transaction->description) ? $transaction->description : ''),
        		'USER2'             => (isset($_POST['USER2']) ? filter_var($_POST['USER2'], FILTER_SANITIZE_URL) : ''),
        		'USER3'             => (isset($_POST['USER3']) ? filter_var($_POST['USER3'], FILTER_SANITIZE_URL) : ''),
        		'VAULT'             => (isset($_POST['VAULT']) ? filter_var($_POST['VAULT'], FILTER_SANITIZE_NUMBER_INT) : ''),
        		'VAULT_ID'          => (isset($_POST['VAULT_ID']) ? filter_var($_POST['VAULT_ID'], FILTER_SANITIZE_STRING) : '')
        	);

        	$data = array_merge($mandatoryFields, $optionalFields);

          // return $data;

        		$checksum = '';

        		foreach($mandatoryFields as $key => $value){
        			if($value != ''){
        				$checksum .= $value;
        			}
        		}
        		$checksum .= $encryption_key;

        		$check_sum =  md5($checksum);

            $payment = Payment::where('reference', $trackingid)->first();

            if (empty($payment)) {
              $payment = new Payment;
            }

            $payment->transaction_id = $transaction->id;
            $payment->reference = $trackingid;
            $payment->amount = $transaction->total_amount;
            $payment->status = 'Initiated';
            $payment->trans_reference = $transaction->payment_reference;
            $payment->save();

            // return $data;

          return redirect('/payment/paygate')
                ->with('data', $data)
                ->with('pgid', $data['PAYGATE_ID'])
                ->with('reference', $data['REFERENCE'])
                ->with('key', $encryption_key);

      
      }
    }

// }


// {

    public function itn(Request $request, PaymentProcessor $payfast, $transid)
    {

        return $transid;
        return $payfast->responseVars();
        // Retrieve the Order from persistance. Eloquent Example.
        $order = Order::where('m_payment_id', $request->get('m_payment_id'))->firstOrFail(); // Eloquent Example

        try {
            $verification = $payfast->verify($request, $order->amount);
            $status = $payfast->status();
        } catch (\Exception $e) {
            Log::error('PAYFAST ERROR: ' . $e->getMessage());
            $status = false;
        }

        // Verify the payment status.
        $status = (int) $payfast->verify($request, $order->amount)->status();

        // Handle the result of the transaction.
        switch( $status )
        {
            case 'COMPLETE': // Things went as planned, update your order status and notify the customer/admins.

                break;
            case 'FAILED': // We've got problems, notify admin and contact Payfast Support.
                break;
            case 'PENDING': // We've got problems, notify admin and contact Payfast Support.
                break;
            default: // We've got problems, notify admin to check logs.
                break;
        }
    }

    public function paygate_payment()
    {

      return view('payments.paygate.index');
    }

    public function return(Request $request, $trackingid)
    {
      // return $request;
      $status = $request->input("TRANSACTION_STATUS");
      $request_id = $request->input("PAY_REQUEST_ID");
      $checksum = $request->input("CHECKSUM");

      // $url = 'https://secure.paygate.co.za/payweb3/query.trans';
      // $encryptionKey = 'secret';
      // $data = array(
      //     'PAYGATE_ID'        => 10011072130,
      //     'PAY_REQUEST_ID'    => $request_id,
      //     'REFERENCE'         => $trackingid
      // );
      // $checksum = md5(implode('', $data) . $encryptionKey);
      //
      // $data['CHECKSUM'] = $checksum;
      // $options = array(
      //         'http' => array(
      //         'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
      //         'method'  => 'POST',
      //         'content' => http_build_query($data),
      //     )
      // );
      //
      // $context  = stream_context_create($options);
      // $result = file_get_contents($url, false, $context);
      // return $result;

      $payment = Payment::where('reference', $trackingid)->first();

      $transaction = Transaction::where('id', $payment->transaction_id)->first();

      // Credit Card Errors - These RESULT_CODEs are returned if the transaction cannot be authorised due to a problem with the card. The TRANSACTION_STATUS will be 2
      // ==================
      // Result Code |||| Description
      // 900001	Call for Approval
      // 900002	Card Expired
      // 900003	Insufficient Funds
      // 900004	Invalid Card Number
      // 900005	Bank Interface Timeout	Indicates a communications failure between the banks systems.
      // 900006	Invalid Card
      // 900007	Declined
      // 900009	Lost Card
      // 900010	Invalid Card Length
      // 900011	Suspected Fraud
      // 900012	Card Reported as Stolen
      // 900013	Restricted Card
      // 900014	Excessive Card Usage
      // 900015	Card Blacklisted
      // 900207	Declined; authentication failed	Indicates the cardholder did not enter their MasterCard SecureCode / Verified by Visa password correctly.
      // 990020	Auth Declined
      // 900210	3D Secure Lookup Timeout
      // 991001	Invalid expiry date
      // 991002	Invalid Amount

      // Transaction Success
      // 990017	Auth Done

      // Communication Errors - These RESULT_CODEs are returned if the transaction cannot be completed due to an unexpected error. TRANSACTION_STATUS will be 0.
      // 900205	Unexpected authentication result (phase 1)
      // 900206	Unexpected authentication result (phase 2)
      // 990001	Could not insert into Database
      // 990022	Bank not available
      // 990053	Error processing transaction

      // Miscellaneous - Unless otherwise noted, the TRANSACTION_STATUS will be 0.
      // 900209	Transaction verification failed (phase 2)	Indicates the verification data returned from MasterCard SecureCode / Verified-by-Visa has been altered.
      // 900210	Authentication complete; transaction must be restarted	Indicates that the MasterCard SecureCode / Verified-by-Visa transaction has already been completed. Most likely caused by a customer clicking the refresh button.
      // 900019	Invalid PayVault Scope
      // 990013	Error processing a batch transaction
      // 990024	Duplicate Transaction Detected. Please check before submitting
      // 990028	Transaction cancelled	Customer clicks the ‘Cancel’ button on the payment page.

      // Transaction Status
      // ==================
      // 0	Not Done
      // 1	Approved
      // 2	Declined
      // 3	Cancelled
      // 4	User Cancelled
      // 5	Received by PayGate
      // 7	Settlement Voided

      if ($status == 1) {
        $payment->status = "Approved";
        $payment->save();

        $transaction->status = "Approved - Completed";
        $transaction->reference = $trackingid;
        $transaction->save();

        $message = "Approved, thank you!";
        $message_status = "success";

        $user_id = \Auth::user()->id;

        $user_subs = \DB::table('subscriptions')->where('user_id', $user_id)->latest()->first();

      if (empty($user_subs)) {
        $update_login_table = \DB::table('subscriptions')->insert([
          'user_id' => $user_id,
          'subs_id' => '1',
          'start_date' => date('Y-m-d H:i:s'),
          'expire_date' => Carbon::now()->addMonths(6),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      } else {
        $update_login_table = \DB::table('subscriptions')->insert([
          'user_id' => $user_id,
          'subs_id' => '1',
          'start_date' => Carbon::parse($user_subs->expire_date)->addDay(),
          'expire_date' => Carbon::parse($user_subs->expire_date)->addDay()->addMonths(6),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      }

        // $update_login_table = \DB::table('subscriptions')->insert([
        //           'user_id' => $user_id,
        //           'subs_id' => '1',
        //           'start_date' => date('Y-m-d H:i:s'),
        //           'expire_date' => Carbon::now()->addMonths(6),
        //           'created_at' => date('Y-m-d H:i:s'),
        //           'updated_at' => date('Y-m-d H:i:s')
        // ]);

        $user = User::findOrFail($user_id);

        $user->status = "Activated";
        $user->save();

      } elseif ($status == 0) {
        $payment->status = "Not Done";
        $payment->save();

        $transaction->status = "Not done";
        $transaction->reference = $trackingid;
        $transaction->save();

        $message = "Error, payment not done!";
        $message_status = "error";

      } elseif ($status == 2) {
        $payment->status = "Declined";
        $payment->save();

        $transaction->status = "Declined - Incomplete";
        $transaction->reference = $trackingid;
        $transaction->save();

        $message = "Error, payment declined!";
        $message_status = "error";
      } elseif ($status == 3) {
        $payment->status = "Cancelled";
        $payment->save();

        $transaction->status = "Cancelled - Incomplete";
        $transaction->reference = $trackingid;
        $transaction->save();

        $message = "Error, payment was cancelled!";
        $message_status = "error";
      } elseif ($status == 4) {
        $payment->status = "User Cancelled";
        $payment->save();

        $transaction->status = "User Declined - Incomplete";
        $transaction->reference = $trackingid;
        $transaction->save();

        $message = "Error, payment was cancelled by you!";
        $message_status = "error";
      }

      $transaction_details = new stdClass();

      $transaction_details->first_name = $transaction->first_name;
      $transaction_details->last_name = $transaction->last_name;
      $transaction_details->email = $transaction->email;
      $transaction_details->protel_acc = $transaction->protel_acc;
      $transaction_details->currency = $transaction->currency;
      $transaction_details->total_amount = $transaction->total_amount;
      $transaction_details->payment_amount = $payment->amount;
      $transaction_details->status = $payment->status;
      $transaction_details->payment_method = $payment->payment_method;
      $transaction_details->payment_currency = $payment->currency;

      // return json_encode($transaction_details);
      // $transaction_details->payment = $payment;




      return redirect('/users/profiles/')->with('message', $message)->with('message_type', $message_status);


    }

    public function notify(Request $request, $trackingid)
    {
      return $request;
    }

// }
}
