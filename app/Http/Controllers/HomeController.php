<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Messages;
use App\User;
use App\Post;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth' => 'verified']);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        date_default_timezone_set("Africa/Johannesburg");
        $user = \Auth::user();
        $user_id = $user->id;

        $posts = Post::all();
        $update_login = User::find($user->id);
        $login_count = $update_login->login_count;
        $update_login->login_count = $login_count + 1;
        $update_login->last_login =  (date('Y-m-d H:i:s'));

        $last_login = $user->last_login;
        $update_login_table = DB::table('login_history')->insert([
                  'user_id' => $user_id,
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s')
        ]);
        $unread_msg = 0;
        // return $user;
        foreach ($user->messages as $message) {
          if ($message->status == "New") {
            $unread_msg++;
          }
        }
        // $unread_msg = DB::table('messages')->where('created_at', '>', $last_login)->where('user_id', '=', $user_id)->count();
        $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->get();

        $freelancer_ids = array();
        $industries  = array();
        $subindustries = array();

        foreach ($freelancers as $freelancer) {
          $industry =  DB::table('industry_users')->where('user_id', $freelancer->id)->get('industry_name');
          $subindustry = DB::table('sub_industry_users')->where('user_id', $freelancer->id)->get('sub_industry_name');
          foreach ($industry as $value) {
            array_push($industries, $value->industry_name);
          }
          foreach ($subindustry as $sub_industry) {
            array_push($subindustries, $sub_industry->sub_industry_name);
          }
           array_push($freelancer_ids, $freelancer->id); 
           
        }

        $profile = DB::table('user_profiles')->where('user_id', $user->id)->first();
        $subs_plans = DB::table('subscription_plans')->get();
        // Counts
        $total_freelancers = DB::table('users')->where('role', 'Freelancer')->count();
        $freelancers_older_than_2_weeks = User::where('role', 'Freelancer')->where('last_login', '<=', Carbon::now()->subDays(14)->toDateTimeString())->count();
        $finders_older_than_2_weeks = User::where('role', 'Finder')->where('last_login', '<=', Carbon::now()->subDays(14)->toDateTimeString())->count();
        $unverified_freelancers = User::where('role', 'Freelancer')->whereNull('email_verified_at')->count();
        $unverified_finders = User::where('role', 'Finder')->whereNull('email_verified_at')->count();


            // Freelancers filter section Finder Dashboard

            // $freelancers_subindustries = array();

            // foreach ($freelancers as $user) {
            //   foreach ($user->subindustry as $subindustry) {
            //     array_push($freelancers_subindustries, $subindustry->sub_industry_name);
            //   }
            // }

            // $freelancers_subindustries_result = array();
            // foreach ($freelancers_subindustries as $key => $value){
            //   if(!in_array($value, $freelancers_subindustries_result))
            //     // $subs_result=$value;
            //     array_push($freelancers_subindustries_result, $value);
            // }

            // sort($freelancers_subindustries_result);
            // array_unique($subindustries);
            // sort($subindustries);

        //   $freelancers_industries = array();

        //   foreach ($freelancers as $user) {
        //     foreach ($user->industry as $industry) {
        //       array_push($freelancers_industries, $industry->industry_name);
        //     }
        //   }

        //   $freelancers_industries_result = array();
        // foreach ($industries as $key => $value){
        //   if(!in_array($value, $freelancers_industries_result))
        //     $freelancers_industries_result[$key]=$value;
        // }

        // $freelancers_subindustries_result = array();
        // foreach ($subindustries as $key => $value){
        //   if(!in_array($value, $freelancers_subindustries_result))
        //     $freelancers_subindustries_result[$key]=$value;
        // }


        // return $freelancers_industries_result;

        //   sort($freelancers_industries_result);

        //   $freelancers_tags = array();

        //   foreach ($freelancers as $user) {
        //     foreach ($user->tag as $tag) {
        //       array_push($freelancers_tags, $tag->name);
        //     }
        //   }


        //   $freelancers_tag_result = array();
        // foreach ($freelancers_tags as $key => $value){
        //   if(!in_array($value, $freelancers_tag_result))
        //     $freelancers_tag_result[$key]=$value;
        // }

        // sort($freelancers_tag_result);

        // END FILTER


        // JOB POSTS FILTER
        //

        $posts_industries = array();
        $posts_subindustries = array();
        $posts_cities = array();

        foreach ($posts as $post) {
          foreach ($post->industries as $industry) {
            array_push($posts_industries, $industry->name);
          }
          foreach ($post->sub_industries as $subindustry) {
            array_push($posts_subindustries, $subindustry->name);
          }
            array_push($posts_cities, $post->location);
        }
        sort($posts_industries);

        $posts_industries_results = array();
        foreach ($posts_industries as $key => $value){
          if(!in_array($value, $posts_industries_results))
            $posts_industries_results[$key]=$value;
        }

        sort($posts_subindustries);

        $posts_subindustries_results = array();
        foreach ($posts_subindustries as $key => $value){
          if(!in_array($value, $posts_subindustries_results))
            $posts_subindustries_results[$key]=$value;
        }

        sort($posts_cities);

        $posts_cities_results = array();
        foreach ($posts_cities as $key => $value){
          if(!in_array($value, $posts_cities_results))
            $posts_cities_results[$key]=$value;
        }

        $freelancers_unpaid = 0;
        foreach ($freelancers as $freelancer) {
          $freelancer_paid = DB::table('subscriptions')->where('user_id', $freelancer->id)->first();
          if (empty($freelancer_paid)) {
            $freelancers_unpaid ++;
          }
        }

        $freelancers_per_week = User::where('role', 'Freelancer')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
        $freelancers_per_month = User::where('role', 'Freelancer')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 month')) )->count();

        $finders_per_day = User::where('role', 'Finder')->whereDate('created_at', Carbon::today())->count();
        $finders_per_week = User::where('role', 'Finder')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
        $finders_per_month = User::where('role', 'Finder')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 month')) )->count();

        $messages_per_day = Messages::whereDate('created_at', Carbon::today())->count();
        $messages_per_week = Messages::whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
        $messages_per_month = Messages::whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 month')) )->count();

        $total_finders = DB::table('users')->where('role', 'Finder')->count();
        $total_msg = $all_messages = DB::table('messages')->count();
        $unread_count = Messages::where('status', 'New')->count();

        // return $unread_count;

        $subs_users = DB::table('subscriptions')->where('user_id', $user->id)->count();

        $posts = Post::all();

        if ($profile === null) {
          $profile_value = 0;
        }else {
          $profile_value = 1;
        }
        // return $freelancers_industries_result;

        return view('dashboard.index')
            ->with('profile', $profile_value)
            // ->with('cities', $cities)
            ->with('total_msg', $total_msg)
            ->with('unread_count', $unread_count)
            ->with('messages_per_day', $messages_per_day)
            ->with('messages_per_week', $messages_per_week)
            ->with('messages_per_month', $messages_per_month)
            ->with('unverified_freelancers', $unverified_freelancers)
            ->with('total_freelancers', $total_freelancers)
            ->with('total_finders', $total_finders)
            ->with('login_count', $login_count)
            ->with('subs_plans', $subs_plans)
            ->with('industries', $industries)
            ->with('subindustries', $subindustries)
            ->with('subs_users', $subs_users)
            // ->with('freelancers', $freelancers)
            ->with('posts', $posts)
            // ->with('dashboard_industries', $freelancers_industries_result)
            // ->with('dashboard_subindustries', $freelancers_subindustries_result)
            // ->with('dashboard_tags', $freelancers_tag_result)
            ->with('post_industries', $posts_industries_results)
            ->with('post_subindustries', $posts_subindustries_results)
            ->with('post_cities', $posts_cities_results)
            ->with('posts_count', $posts->count())
            ->with('freelancers_older_than_2_weeks', $freelancers_older_than_2_weeks)
            ->with('freelancers_unpaid', $freelancers_unpaid)
            ->with('freelancers_per_week', $freelancers_per_week)
            ->with('freelancers_per_month', $freelancers_per_month)
            ->with('finders_older_than_2_weeks', $finders_older_than_2_weeks)
            ->with('unverified_finders', $unverified_finders)
            ->with('finders_per_day', $finders_per_day)
            ->with('finders_per_week', $finders_per_week)
            ->with('finders_per_month', $finders_per_month);
    }
    public function action(Request $request) //Live search
    {
      $freelancers = User::where('role', 'Freelancer');

      if($request->ajax())
      {
        $output = '';
        $query = $request->get('query');
        if ($query != '')
        {
          $data = $freelancers->where('name', 'like', '%'. $query . '%')
                                    ->orWhere('email','like', '%'. $query . '%')
                                    ->orWhere('role','like', '%'. $query . '%')
                                    ->orderBy('id', 'desc')->get()->load('profile')->load('industry');
        }
        else
        {
          // $freelancers = User::where('role', 'Freelancer');
          $data = $freelancers->orderBy('id', 'desc')->get()->load('profile');
        }
        $total_row = $data->count();

        if ($total_row > 0) {
          foreach ($data as $row) {
            if (empty($row->profile->profile_pic)) {
              $user_pic = "";
            } else {
              $user_pic = '<img src="/storage/'.$row->profile->profile_pic .'" alt="User Profile Pic" height="80px">';
            }
            $user_industry = DB::table('user_industry')->where('user_id', '=', $row->id)->get();
            $industry_name = '';
            foreach ($user_industry as $industry) {
              $names = DB::table('sub_industries')->where('id', '=', $industry->subindustry_id)->get();
              foreach ($names as $name) {
                $industry_name .= $name->name . "<br>";
              }
            }
            // $industry_name = json_decode($industry_name);
            $output .= '
                <tr>
                <td> ' . $user_pic . ' </td>
                <td></td>
                 <td>
                    <a href="/profile/'.$row->id.'">'.$row->name.'</a> <br>
                    '.$row->role.'
                 </td>
                 <td>'.$row->email.'</td>
                 <td></td>
                </tr>
        ';
          }
        }
        else
        {

          $output = '
       <tr>
        <td align="center" colspan="3">No Data Found</td>
       </tr>
       ';
        }
        $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );
      return json_encode($data);
      }
    }
    public function importcities()
    {
      // include 'westerncapecities.csv';
      if ($fh = fopen('westerncapecities.csv', 'r')) {
        while (!feof($fh)) {
          $line = fgets($fh);
          $data_line = explode(';', $line);
        print_r($data_line);
        echo $data_line[0];
        DB::table('sa_cities')->insert(
            [
              'city' => $data_line[0],
              'accentCity' => $data_line[1],
              'provinceName' => $data_line[2],
              'latitude' => $data_line[3],
              'longitude' => $data_line[4],
              'provinceID' => $data_line[5]
            ]
            );
        echo "<br>";
        echo "<hr>";
        }
        fclose($fh);
        }
    }
    public function upload(Request $request)
    {
      dd($request);
    }
}
