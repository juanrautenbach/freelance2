<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Industry;
use App\Messages;
use App\Tag;
use Illuminate\Support\Facades\DB;
use App\Post;
use Carbon\Carbon;

class PagesController extends Controller
{
    public function backend(){

      date_default_timezone_set("Africa/Johannesburg");
      $user = \Auth::user();
      $user_id = $user->id;
      $posts = Post::all();
      $update_login = User::find($user->id);
      $login_count = $update_login->login_count;
      $update_login->login_count = $login_count + 1;
      $update_login->last_login =  (date('Y-m-d H:i:s'));
      // $update_login->save();

      // return $user;

      $last_login = $user->last_login;
      // $unread_msg = Messages::where('created_at', '>', $last_login)->count();
      $update_login_table = DB::table('login_history')->insert([
                'user_id' => $user_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
      ]);
      $unread_msg = DB::table('messages')->where([
          ['created_at', '>', $last_login],
          ['user_id', '=', $user_id],
          ])->count();
      $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->get();

      $industries = DB::table('industries')->orderBy('name','asc')->get();
      $subindustries = DB::table('sub_industries')->orderBy('name','asc')->get();
      $profile = DB::table('user_profiles')->where('user_id', $user->id)->first();
      $subs_plans = DB::table('subscription_plans')->get();
      // Counts
      $total_freelancers = DB::table('users')->where('role', 'Freelancer')->count();
      // return $total_freelancers;
      $freelancers_older_than_2_weeks = User::where('role', 'Freelancer')->where('last_login', '<=', Carbon::now()->subDays(14)->toDateTimeString())->count();
      $finders_older_than_2_weeks = User::where('role', 'Finder')->where('last_login', '<=', Carbon::now()->subDays(14)->toDateTimeString())->count();
      $unverified_freelancers = User::where('role', 'Freelancer')->whereNull('email_verified_at')->count();
      $unverified_finders = User::where('role', 'Finder')->whereNull('email_verified_at')->count();

          // Freelancers filter section Finder Dashboard

          $freelancers_subindustries = array();

          foreach ($freelancers as $user) {
            foreach ($user->subindustry as $subindustry) {
              array_push($freelancers_subindustries, $subindustry->sub_industry_name);
            }
          }

          $freelancers_subindustries_result = array();
          foreach ($freelancers_subindustries as $key => $value){
            if(!in_array($value, $freelancers_subindustries_result))
              // $subs_result=$value;
              array_push($freelancers_subindustries_result, $value);
          }

          sort($freelancers_subindustries_result);
          // array_unique($subindustries);
          // sort($subindustries);

        $freelancers_industries = array();

        foreach ($freelancers as $user) {
          foreach ($user->industry as $industry) {
            array_push($freelancers_industries, $industry->industry_name);
          }
        }

        $freelancers_industries_result = array();
      foreach ($industries as $key => $value){
        if(!in_array($value, $freelancers_industries_result))
          $freelancers_industries_result[$key]=$value;
      }

        sort($freelancers_industries_result);

        $freelancers_tags = array();

        foreach ($freelancers as $user) {
          foreach ($user->tag as $tag) {
            array_push($freelancers_tags, $tag->name);
          }
        }


        $freelancers_tag_result = array();
      foreach ($freelancers_tags as $key => $value){
        if(!in_array($value, $freelancers_tag_result))
          $freelancers_tag_result[$key]=$value;
      }

      sort($freelancers_tag_result);

      // END FILTER


      // JOB POSTS FILTER
      //

      $posts_industries = array();
      $posts_subindustries = array();
      $posts_cities = array();

      foreach ($posts as $post) {
        foreach ($post->industries as $industry) {
          array_push($posts_industries, $industry->name);
        }
        foreach ($post->sub_industries as $subindustry) {
          array_push($posts_subindustries, $subindustry->name);
        }
          array_push($posts_cities, $post->location);
      }
      sort($posts_industries);

      $posts_industries_results = array();
      foreach ($posts_industries as $key => $value){
        if(!in_array($value, $posts_industries_results))
          $posts_industries_results[$key]=$value;
      }

      sort($posts_subindustries);

      $posts_subindustries_results = array();
      foreach ($posts_subindustries as $key => $value){
        if(!in_array($value, $posts_subindustries_results))
          $posts_subindustries_results[$key]=$value;
      }

      sort($posts_cities);

      $posts_cities_results = array();
      foreach ($posts_cities as $key => $value){
        if(!in_array($value, $posts_cities_results))
          $posts_cities_results[$key]=$value;
      }

      $freelancers_unpaid = 0;
      foreach ($freelancers as $freelancer) {
        $freelancer_paid = DB::table('subscriptions')->where('user_id', $freelancer->id)->first();
        if (empty($freelancer_paid)) {
          $freelancers_unpaid ++;
        }
      }

      $freelancers_per_week = User::where('role', 'Freelancer')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
      $freelancers_per_month = User::where('role', 'Freelancer')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 month')) )->count();

      $finders_per_day = User::where('role', 'Finder')->whereDate('created_at', Carbon::today())->count();
      $finders_per_week = User::where('role', 'Finder')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
      $finders_per_month = User::where('role', 'Finder')->whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 month')) )->count();

      $messages_per_day = Messages::whereDate('created_at', Carbon::today())->count();
      $messages_per_week = Messages::whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
      $messages_per_month = Messages::whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 month')) )->count();

      $total_finders = DB::table('users')->where('role', 'Finder')->count();
      $total_msg = $all_messages = DB::table('messages')->count();
      $unread_count = Messages::where('status', 'New')->count();

     

      $subs_users = DB::table('subscriptions')->where('user_id', $user->id)->count();

      $posts = Post::all();

      if ($profile === null) {
        $profile_value = 0;
      }else {
        $profile_value = 1;
      }
      // return $freelancers_industries_result;
      
      return view('pages.backend')
          ->with('profile', $profile_value)
          // ->with('cities', $cities)
          ->with('total_msg', $total_msg)
          ->with('unread_count', $unread_count)
          ->with('messages_per_day', $messages_per_day)
          ->with('messages_per_week', $messages_per_week)
          ->with('messages_per_month', $messages_per_month)
          ->with('unverified_freelancers', $unverified_freelancers)
          ->with('total_freelancers', $total_freelancers)
          ->with('total_finders', $total_finders)
          ->with('login_count', $login_count)
          ->with('subs_plans', $subs_plans)
          ->with('industries', $industries)
          ->with('subindustries', $subindustries)
          ->with('subs_users', $subs_users)
          ->with('freelancers', $freelancers)
          ->with('posts', $posts)
          ->with('dashboard_industries', $freelancers_industries_result)
          ->with('dashboard_subindustries', $freelancers_subindustries_result)
          ->with('dashboard_tags', $freelancers_tag_result)
          ->with('post_industries', $posts_industries_results)
          ->with('post_subindustries', $posts_subindustries_results)
          ->with('post_cities', $posts_cities_results)
          ->with('posts_count', $posts->count())
          ->with('freelancers_older_than_2_weeks', $freelancers_older_than_2_weeks)
          ->with('freelancers_unpaid', $freelancers_unpaid)
          ->with('freelancers_per_week', $freelancers_per_week)
          ->with('freelancers_per_month', $freelancers_per_month)
          ->with('finders_older_than_2_weeks', $finders_older_than_2_weeks)
          ->with('unverified_finders', $unverified_finders)
          ->with('finders_per_day', $finders_per_day)
          ->with('finders_per_week', $finders_per_week)
          ->with('finders_per_month', $finders_per_month);
    }
    public function home()
    {

      $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->get();
      $message_count = DB::table('messages')->count();
      $finder_count = User::where('role', 'Finder')->count();
      $freelancers_count = $freelancers->count();
      $industries = Industry::orderBy('name', 'asc')->get();


      return view('pages.home')
              ->with('freelancers', $freelancers)
              ->with('freelancer_count', $freelancers_count)
              ->with('industries', $industries)
              ->with('finder_count', $finder_count)
              ->with('message_count', $message_count);

    }
    public function make($type)
    {
      return view('users.make')->with('user_type', $type);
    }

    public function settings()
    {
      $all_users = User::orderBy('last_login', 'desc')->get();

      $user = \Auth::user();
      $user_id = $user->id;
      $last_login = $user->last_login;
      $unread_msg = DB::table('messages')->where([
          ['created_at', '>', $last_login],
          ['user_id', '=', $user_id],
          ])->count();

      return view('pages.settings')
            ->with('all_users', $all_users)
            ->with('unread_msg', $unread_msg);
    }

    public function editUser($id)
    {
      $user = DB::table('users')->where('id', $id)->first();

      return view('pages.users')->with('user', $user);
    }

    public function freelancers()
    {
      $freelancers_count = User::where('role', 'Freelancer')->where('status', 'Activated')->count();
      $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->simplePaginate(15);

    $users = $freelancers;

    $subindustries = array();

    foreach ($users as $user) {
      foreach ($user->subindustry as $subindustry) {
        array_push($subindustries, $subindustry->sub_industry_name);
      }
    }

    $subs_result = array();
    foreach ($subindustries as $key => $value){
      if(!in_array($value, $subs_result))
        // $subs_result=$value;
        array_push($subs_result, $value);
    }

      sort($subs_result);
      // array_unique($subindustries);
      // sort($subindustries);

      $industries = array();

      foreach ($users as $user) {
        foreach ($user->industry as $industry) {
          array_push($industries, $industry->industry_name);
        }
      }

      $indus_result = array();
    foreach ($industries as $key => $value){
      if(!in_array($value, $indus_result))
        $indus_result[$key]=$value;
    }

      sort($indus_result);

      $tags = array();

      foreach ($users as $user) {
        foreach ($user->tag as $tag) {
          array_push($tags, $tag->name);
        }
      }

      $tag_result = array();
    foreach ($tags as $key => $value){
      if(!in_array($value, $tag_result))
        $tag_result[$key]=$value;
    }

    sort($tag_result);

      return view('freelancers.index')
                              ->with('freelancer_count', $freelancers_count)
                              ->with('freelancers', $freelancers)
                              ->with('tags', $tag_result)
                              ->with('industries', $indus_result)
                              ->with('subindustries', $subs_result);
    }

    public function coworking()
    {
      $coworking_count = User::where('role', 'Co-Working')->where('status', 'Activated')->count();
      $coworkingspaces = User::where('role', 'Co-Working')->where('status', 'Activated')->simplePaginate(15);
      return view('co-working.index')
                  ->with('coworking_count', $coworking_count)
                  ->with('coworkingspaces', $coworkingspaces);
    }

    public function suggestion(Request $request)
    {
      return $request;
    }

    public function thankyou()
    {
      return view('pages.thankyou');
    }

    public function payment_terms()
    {
      return view('pages.payment_terms');
    }
}
