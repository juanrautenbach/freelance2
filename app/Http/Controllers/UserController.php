<?php

namespace App\Http\Controllers;
use App\Package;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        //
//    }

    public function checkemail($email)
    {
      $check_email = User::where('email', $email)->count();

        return $check_email;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify($hash)
    {
      $message = 'success';
      return view('users.verify')->with('message', $message);
    }
    public function payment()
    {
      return view('users.payment');
    }

    public function register(Request $request)
    {
          $email = $request->input('email');
          $password = $request->input('password');
          $user_role = $request->input('user_role');

          $to = $email;
          $subject = "Freelance Cape Town - Verify Email";

          $message = "
          <html>
          <head>
          <title>Freelance Cape Town - Verify Email</title>
          </head>
          <body>
          <p>This email contains HTML Tags!</p>
          <h1 style='font-size:48px; font-weight: bold;'>Welcome to Freelance Cape Town.</h1>
          <p>Please click on the button below to verify your email address and complete the registration process.</p><br>
          <a href='http://127.0.0.1:8000/verify/'>Click me</a>
          </body>
          </html>
          ";


          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

          // More headers
          $headers .= 'From: <info@freelancecapetown.com>' . "\r\n";
          // $headers .= 'Cc: myboss@example.com' . "\r\n";

          mail($to,$subject,$message,$headers);

    }
    public function package()
    {
      $package = DB::table('packages')->get();
      return view('payments.package')->with('packages', $package);
    }
    public function paymentselection(Request $request){

      $package_id = $request->input('package');
      $package = Package::find($package_id);

      return view('payments.checkout')->with('package', $package);
    }

    public function userrole(Request $request){

      $user_id = $request->input('user_id');
      $update_role = User::find($user_id);
      $update_role->role = $request->input('user_role');
      $update_role->save();

      return redirect('/settings');
    }

    public function activate_user($id)
    {
      $user = User::findOrFail($id);
      $user->status = "Activated";
      $user->save();
      return redirect('/settings')->with('message', 'User Activated');
    }

    public function deactivate_user($id)
    {
      $user = User::findOrFail($id);
      $user->status = "Deactivated";
      $user->save();
      return redirect('/settings')->with('message', 'User Deactivated');
    }

    public function get_user_name($id)
    {
      $user = User::findOrFail($id);

      return $user->name;
    }

    public function freelancers_search($search)
    {
      $sub_industries_id = DB::table('sub_industries')->where('name', 'like', $search)->get('id');

      foreach ($sub_industries_id as $value) {
        $freelancers_id = DB::table('sub_industries_users')->where('sub_industry_id', '=', $value->id)->get('user_id');



        if (isset($freelancers_id) && count($freelancers_id) > 0) {
          $ids = [];
          foreach ($freelancers_id as $id) {
            $id = array_push($ids, $id->user_id);
          }
            $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->findMany($ids);
            return $freelancers;
        } else {
          return response()->json([
                    'Error' => 'Nothing found'
                ]);
        }



      }

    }
}
