<?php

namespace App\Http\Controllers;

use App\UserShowcase;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use App\Classes\Slim;
use Illuminate\Routing\UrlGenerator;
use Intervention\Image\ImageManager;

class UserShowcaseController extends Controller
{
    public $imageManager;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $message = "";
        if($request->edit_photo_id){
           $showcase = UserShowcase::find($request->edit_photo_id);
           $showcase->delete();
           $message = "Showcase photo has been updated!";
        }
        $slim = json_decode($request->slim[0]);
        $id = \Auth::user()->id;
        \Storage::disk('local')->makeDirectory('public/showcase_photos/' . $id);
        $img = $this->imageManager->make($slim->output->image);
        $photo_name = "photo". time() . ".jpg";
        $img->save(storage_path('app/public/showcase_photos/' . $id . '/'. $photo_name));
        $path = "/storage/showcase_photos/" . $id . '/'. $photo_name;

        $userprofile = new UserShowcase;
        $userprofile->title = "";
        $userprofile->description = $request->input('showcase_desc');
        $userprofile->user_id = $id;
        $userprofile->photo = $path;

        if (strlen($message) == 0) {
            $message = "Showcase created!";
        }

        $userprofile->save();
        return redirect()->back()->with('success', $message);

    }

    public function addProfilePhoto(Request $request)
    {
        // return $request;
        if($request->edit_photo_id){
            $profile = UserProfile::find($request->edit_photo_id);
            
         }
         else{
            $profile = new UserProfile;
         }
         $slim = json_decode($request->slim[0]);
         $id = \Auth::user()->id;
         \Storage::disk('local')->makeDirectory('public/profile_photos/' . $id);
         $img = $this->imageManager->make($slim->output->image);
         $photo_name = "photo". time() . ".jpg";
         $img->save(storage_path('app/public/profile_photos/' . $id . '/'. $photo_name));
         $path = "/storage/profile_photos/" . $id . '/'. $photo_name;
 
         
         $profile->profile_pic = $path;
        //  $userprofile->description = $request->input('showcase_desc');
        //  $userprofile->user_id = $id;
        //  $userprofile->photo = $path;
 
         $profile->save();
         return redirect()->back()->with('success', "User's profile picture has been updated!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserShowcase  $userShowcase
     * @return \Illuminate\Http\Response
     */
    public function show(UserShowcase $userShowcase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserShowcase  $userShowcase
     * @return \Illuminate\Http\Response
     */
    public function edit(UserShowcase $userShowcase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserShowcase  $userShowcase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserShowcase $userShowcase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserShowcase  $userShowcase
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserShowcase $userShowcase)
    {
        //
    }

    public function profilephoto(Request $request)
    {
      $id = $request->input('user_id');
      // return $id;
      $profile_val =  DB::table('user_profiles')->where('user_id', '=', $id)->first();

      if (empty($profile_val->id)) {
          $userprofile = new UserProfile;
      } else {
          $userprofile = DB::table('user_profiles')->where('user_id', '=', $id)->first();
          $userprofile = UserProfile::where('user_id', $id);
      }

      if ($request->has('profile_photo')) {
        $file = $request->file('profile_photo');
       $userprofile->update(['profile_pic' => $request->file('profile_photo')->store('profilepics', 'public')]);
      }

      return redirect('/users/profiles')->with('success', 'Profile saved');
    }
    public function profilecover(Request $request)
    {
        $id = $request->input('user_id');
        // return $id;
        $profile_val =  DB::table('user_profiles')->where('user_id', '=', $id)->first();

        if (empty($profile_val->id)) {
            $userprofile = new UserProfile;
        } else {
            $userprofile = DB::table('user_profiles')->where('user_id', '=', $id)->first();
            $userprofile = UserProfile::where('user_id', $id);
        }

        // if ($request->has('cover_photo')) {
        //   $file = $request->file('cover_photo');
        //  $userprofile->update(['profile_cover_pic' => $request->file('cover_photo')->store('coverpics', 'public')]);
        // }

        if($request->hasFile('cover_photo')) {
        //get filename with extension
        $filenamewithextension = $request->file('cover_photo')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('cover_photo')->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;

        //Upload File
        $request->file('cover_photo')->storeAs('public/cover_photo', $filenametostore);

        if(!file_exists(public_path('storage/cover_photo/crop'))) {
            mkdir(public_path('storage/cover_photo/crop'), 0755);
        }

        // crop image
        $img = Image::make(public_path('storage/cover_photo/'.$filenametostore));
        $croppath = public_path('storage/cover_photo/crop/'.$filenametostore);

        $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
        $img->save($croppath);

        // you can save crop image path below in database
        $path = asset('storage/cover_photo/crop/'.$filenametostore);
        $userprofile->update(['profile_cover_pic' => $filenametostore]);
        return redirect('/users/profiles')->with('success', 'Profile saved');
    }





    }
}
