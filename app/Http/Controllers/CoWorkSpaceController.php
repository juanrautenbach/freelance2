<?php

namespace App\Http\Controllers;

use App\CoWorkSpace;
use Illuminate\Http\Request;

class CoWorkSpaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminindex()
    {
        return view('co-working.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminstore(Request $request)
    {
        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CoWorkSpace  $coWorkSpace
     * @return \Illuminate\Http\Response
     */
    public function show(CoWorkSpace $coWorkSpace)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CoWorkSpace  $coWorkSpace
     * @return \Illuminate\Http\Response
     */
    public function edit(CoWorkSpace $coWorkSpace)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CoWorkSpace  $coWorkSpace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CoWorkSpace $coWorkSpace)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CoWorkSpace  $coWorkSpace
     * @return \Illuminate\Http\Response
     */
    public function destroy(CoWorkSpace $coWorkSpace)
    {
        //
    }
}
