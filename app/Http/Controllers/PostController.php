<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\User;
use App\Messages;
use App\Mail\NewMessage;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        date_default_timezone_set("Africa/Johannesburg");
        $today_date = date('Y-m-d H:i:s');

        $all_posts = Post::orderBy('deadline', 'desc')->get();

        return view('posts.index')->with('posts', $all_posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $user_id = \Auth::user()->id;
        $mail_user_ids = [];
        $deadline = Carbon::parse($request->input("brief_deadline"))->format('y-m-d');
        // $deadline = new DateTime($request->input("brief_deadline"));

        $post = new Post;
        $post->user_id = $user_id;
        $post->title = $request->input("post_title");
        $post->deadline = $deadline;
        $post->budget = $request->input("brief_budget");
        $post->contact_info = $request->input("brief_contact");
        $post->location = $request->input("city");
        $post->description = $request->input("description");
        $post->message_count = 0;
        $post->post_clicks = 0;
        $post->attachments = "";
        $post->status = "New";
        $post->save();

        // return $post->id;
        $industry_id = $request->input('industry');

        $industry_name = DB::table("industries")->where("id", $industry_id)->first();

        $post_industry = DB::table("industry_posts")->insert([
          'post_id' => $post->id,
          'industry_id' => $industry_id,
          'industry_name' => $industry_name->name,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);

        $sub_industry_ids = $request->input('subindustries');
        $sub_industry_ids = array_values(array_filter($sub_industry_ids));
        $subcategory_names = array();

        foreach ($sub_industry_ids as $value) {
          $sub_industry = DB::table("sub_industries")->where("id", $value)->first();

          array_push($subcategory_names, $sub_industry->name);
        }

        for ($i=0; $i < count($sub_industry_ids); $i++) {
          $update_posts_sub_industries_table = DB::table('post_sub_industries')->insert([
                        'post_id' => $post->id,
                        'sub_industry_id' => $sub_industry_ids[$i],
                        'sub_industry_name' => $subcategory_names[$i],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
              ]);
        }

        // return $industry_id;
        $industry_users = DB::table("industry_users")->where("industry_id", $industry_id)->get();
        // return $industry_users;
        foreach ($industry_users as $industry_user) {
          array_push($mail_user_ids, $industry_user->user_id);
        }

        // return $sub_industry_ids;

        foreach ($sub_industry_ids as $subindustry) {
          $subindustry_users = DB::table("sub_industry_users")->where("sub_industry_id", $subindustry)->get();
          // return $industry_users;
          foreach ($subindustry_users as $subindustry_user) {
            array_push($mail_user_ids, $subindustry_user->user_id);
          }
        }


        $mail_users = array_unique($mail_user_ids);

        return $mail_users;
        foreach ($mail_users as $mail_user) {
          $message = new Messages;
            $message->user_id = $mail_user;
            $message->sender_id = $user_id;
            $message->subject = $request->input('post_title');
            $message->messages = $request->input('description');
            $message->status = "New";
            $message->post_id = $post->id;
            $message->save();
          $notify_email = User::where('id', '=', $mail_user)->first();

          \Mail::to($notify_email)->send(new NewMessage($message));
        }

        return redirect('/dashboard')->with('message', 'Post created!')->with('message_type', 'success');

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $post = Post::find($id);
        $finder = User::find($post->user_id);
        return view('posts.view')
                    ->with('post', $post)
                    ->with('finder', $finder);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    public function post_counter($id)
    {
      $post = Post::where('id', $id)->first();
      
      $views = $post->post_clicks;

      $post->post_clicks = $views + 1;
      $post->save();
      $url = "/post/". $post->id;
      return redirect($url);
    }
}
