<?php

namespace App\Http\Controllers;

use App\SubscriptionPlan;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        $user_id = $user->id;
        $last_login = $user->last_login;
        $unread_msg = DB::table('messages')->where([
            ['created_at', '>', $last_login],
            ['user_id', '=', $user_id],
        ])->count();


        $subsplans = DB::table('subscription_plans')->get();
        return view('subscriptionplans.index')
            ->with('subsplans', $subsplans)
            ->with('unread_msg', $unread_msg);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \Auth::user();
        $user_id = $user->id;
        $last_login = $user->last_login;
        $unread_msg = DB::table('messages')->where([
            ['created_at', '>', $last_login],
            ['user_id', '=', $user_id],
        ])->count();
        return view('subscriptionplans.create')
            ->with('unread_msg', $unread_msg);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duration = $request->input('subs_duration') . " months";
        // return $duration;
        $subs = new SubscriptionPlan;
        $subs->name = $request->input('subs_name');
        $subs->description = $request->input('subs_desc');
        $subs->duration = $duration;
        $subs->type = $request->input('subs_type');
        $subs->price = $request->input('subs_price');
        $subs->status = "Active";
        $subs->save();

        return redirect('/subscriptionplans')->with('success', 'Created successfully');
        // return redirect('/subscriptionplans')->with('message', 'Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPlan $subscriptionPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionPlan $subscriptionPlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubscriptionPlan $subscriptionPlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subsplan = SubscriptionPlan::where('id', $id)->first();
        $subsplan->delete();

        return redirect()->back()->with('danger', 'Deleted successfully');
    }

    public function deactivate($id)
    {
        $subsplan = SubscriptionPlan::where('id', $id)->first();
        $subsplan->status = "Deactivated";
        $subsplan->save();

        return redirect()->back()->with('success', 'Deactivated successfully');
    }

    public function activate($id)
    {
        $subsplan = SubscriptionPlan::where('id', $id)->first();
        $subsplan->status = "Active";
        $subsplan->save();

        return redirect()->back()->with('success', 'Activated successfully');
    }
}
