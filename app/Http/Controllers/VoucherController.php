<?php

namespace App\Http\Controllers;

use App\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vouchers = Voucher::orderBy('created_at', 'DESC')->where('status', 'Active')->get();
        return view('vouchers.index')
                  ->with('vouchers', $vouchers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request;
      // return \Auth::user()->name;
      $count = $request->input("number");
      

      if (empty($request->input("voucher_manual"))) {
        $voucher_code = $request->input("code");
        $amount = "R " . $request->input("amount");

        for ($i=0; $i < $count; $i++) {
          $new_voucher = new Voucher;
          $new_voucher->code = $voucher_code;
          $new_voucher->amount = $amount;
          $new_voucher->valid_until = $request->input("valid_until");
          $new_voucher->status = "Active";
          $new_voucher->created_by = \Auth::user()->name;
          $new_voucher->save();
        }


      } else {
        $voucher_code = $request->input("voucher_manual");
        $amount = "R " . $request->input("amount");

        for ($i=0; $i < $count; $i++) {
          $new_voucher = new Voucher;
          $new_voucher->code = $voucher_code;
          $new_voucher->amount = $amount;
          $new_voucher->valid_until = $request->input("valid_until");
          $new_voucher->status = "Active";
          $new_voucher->created_by = \Auth::user()->name;
          $new_voucher->save();
        }
      }




      return redirect("/vouchers");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        //
    }

    public function validateVoucher($voucher)
    {
      return Voucher::where('code', $voucher)->first();
    }
}
