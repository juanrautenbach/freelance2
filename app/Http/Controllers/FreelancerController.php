<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Messages;
use App\User;
use App\Post;
use Carbon\Carbon;

class FreelancerController extends Controller
{
    public function freelancer_dashboard()
    {
        $freelancers = User::where('role', 'Freelancer')->where('status', 'Activated')->get();

        $freelancer_ids = array();
        $industries  = array();
        $subindustries = array();

        // return $freelancers;

        foreach ($freelancers as $freelancer) {
          $industry =  DB::table('industry_users')->where('user_id', $freelancer->id)->get('industry_name');
          $subindustry = DB::table('sub_industry_users')->where('user_id', $freelancer->id)->get('sub_industry_name');
          foreach ($industry as $value) {
            array_push($industries, $value->industry_name);
          }
          foreach ($subindustry as $sub_industry) {
            array_push($subindustries, $sub_industry->sub_industry_name);
          }
           array_push($freelancer_ids, $freelancer->id); 
           
        }
        $freelancers_industries = array();

          foreach ($freelancers as $user) {
            foreach ($user->industry as $industry) {
              array_push($freelancers_industries, $industry->industry_name);
            }
          }

          $freelancers_industries_result = array();
        foreach ($industries as $key => $value){
          if(!in_array($value, $freelancers_industries_result))
            $freelancers_industries_result[$key]=$value;
        }

        $freelancers_subindustries_result = array();
        foreach ($subindustries as $key => $value){
          if(!in_array($value, $freelancers_subindustries_result))
            $freelancers_subindustries_result[$key]=$value;
        }

        sort($freelancers_industries_result);

          $freelancers_tags = array();

          foreach ($freelancers as $user) {
            foreach ($user->tag as $tag) {
              array_push($freelancers_tags, $tag->name);
            }
          }


          $freelancers_tag_result = array();
        foreach ($freelancers_tags as $key => $value){
          if(!in_array($value, $freelancers_tag_result))
            $freelancers_tag_result[$key]=$value;
        }

        sort($freelancers_tag_result);

        return view('freelancers.dashboard.index')
            ->with('freelancers', $freelancers)
            ->with('dashboard_industries', $freelancers_industries_result)
            ->with('dashboard_subindustries', $freelancers_subindustries_result)
            ->with('dashboard_tags', $freelancers_tag_result);
    }

    public function freelancer_counter($id)
    {
      $freelancer = User::where('id', $id)->first();
      
      $views = $freelancer->click_count;

      $freelancer->click_count = $views + 1;
      $freelancer->save();
      $url = "/freelancer/". $freelancer->id;
      return redirect($url);
    }
}
