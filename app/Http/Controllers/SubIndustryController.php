<?php

namespace App\Http\Controllers;

use App\SubIndustry;
use Illuminate\Http\Request;

class SubIndustryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'subindustry' => 'required'
      ]);
      $subindustry = new SubIndustry;
      $subindustry->industry_id = $request->input('industry');
      $subindustry->name = $request->input('subindustry');
      $subindustry->save();
      return redirect('/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubIndustry  $subIndustry
     * @return \Illuminate\Http\Response
     */
    public function show(SubIndustry $subIndustry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubIndustry  $subIndustry
     * @return \Illuminate\Http\Response
     */
    public function edit(SubIndustry $subIndustry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubIndustry  $subIndustry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubIndustry $subIndustry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubIndustry  $subIndustry
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubIndustry $subIndustry)
    {
        //
    }
}
