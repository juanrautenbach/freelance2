<?php

namespace App\Http\Controllers;

use App\UserProfile;
use App\User;
use App\Subscription;
use App\SubscriptionPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Classes\Slim;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        // return $user->id;
        $user_showcase = DB::table('user_showcases')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        $showcase_count = $user_showcase->count();
        $user_subindustries = DB::table('sub_industry_users')->where('user_id', '=', $user->id)->get();
        $tags = DB::table('tags')->where('user_id', '=', $user->id)->get();
        $subindustries = [];
        foreach ($user_subindustries as $user_subindustry) {
          $sub_id = $user_subindustry->sub_industry_id;
          // $ind_id = $user_subindustry->industry_id;
          // $industry_name = DB::table('industries')->where('id', '=', $ind_id)->first();
          $subindustry_name = DB::table('sub_industries')->where('id', '=', $sub_id)->first();
          array_push($subindustries, $subindustry_name->name);

        }
        // if ($user_subindustries->count() == 0) {
        //   $industry_name = '';
        // }else {
        //   // $industry_name = $industry_name->name;
        // }

        return view('users.profiles.index')
                ->with('user', $user)
                ->with('user_showcases', $user_showcase)
                ->with('subindustries', $subindustries)
                // ->with('industry_name', $industry_name)
                ->with('showcase_count', $showcase_count)
                ->with('tags', $tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.profiles.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set("Africa/Johannesburg");
        $user_id = $request->input('user_id');

        $subindustries = $request->input('subindustries');
        foreach ($subindustries as $subindustry) {
          $user_industry = $request->input('industry');
          $update_user_industry_table = DB::table('user_industry')->insert([
                    'user_id' => $user_id,
                    'industry_id' => $user_industry,
                    'subindustry_id' => $subindustry,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
          ]);
        }
        $tags = explode(";" ,$request->input('user_tags'));

        foreach ($tags as $tag) {
          $update_tags_table = DB::table('tags')->insert([
            'name' => $tag,
            'user_id' => $user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ]);
        }

        $userprofile = new UserProfile;
        $this->validate($request,[
          'mobile' => 'required'
        ]);


        $about = $request->input('about');
        $tel = $request->input('tel');
        $mobile = $request->input('mobile');
        $company = $request->input('company_name');
        $location = $request->input('city');
        $facebook = $request->social_media[0];
        $twitter = $request->social_media[1];
        $skills = $request->input('user_tags');


        // $userprofile->profile_pic = $profilephoto;
        // $userprofile->profile_cover_pic = $coverphoto;
        $userprofile->company_name = $company;
        $userprofile->about = $about;
        $userprofile->tel = $tel;
        $userprofile->mobile = $mobile;
        $userprofile->city = $location;
        $userprofile->social_media = '{ "facebook": "'.$facebook.'", "twitter": "'.$twitter.'" }';
        $userprofile->skills = $skills;
        $userprofile->user_id = $user_id;
        $userprofile->save();



        return view('dashboard.index')->with('success', 'Profile saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function show(UserProfile $userProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(UserProfile $userProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserProfile $userProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserProfile $userProfile)
    {
        //
    }

    public function profilephotos()
    {
      return view('dashboard.settings.profilephotos');
    }

    public function basics()
    {
      $user = \Auth::user();
      return view('users.profiles.createbasics')
                ->with('user', $user);
    }

    public function socialmedia(Request $request)
    {
      $social_links = explode(",", $request->input('social_media'));
      $social = array('facebook' => $social_links[0], 'twitter' => $social_links[1] , 'linkedin' => $social_links[2]);

      $user_profile = UserProfile::where('user_id', $request->input('user_id'))->count();
      if ($user_profile == 0) {
        $user_profile = new UserProfile;
        $user_profile->user_id = $request->input('user_id');
      }else {
        $user_profile = UserProfile::where('user_id', $request->input('user_id'))->first();
      }

      $json_social = json_encode($social);

      $user_profile->social_media = $json_social;
      $user_profile->save();

      return redirect('/profile/basics');
    }
    public function editabout(Request $request, $id)
    {

      $user_profile = UserProfile::where('user_id', $id)->count();
      if ($user_profile == 0) {
        $user_profile = new UserProfile;
        $user_profile->user_id = $id;
      }else {
        $user_profile = UserProfile::where('user_id', $id)->first();
      }

      $user_profile->about = $request->input('about');
      $user_profile->save();

      return redirect('/users/profiles')->with('message', 'Update success!');
    }

    public function editcontact(Request $request, $id)
    {

      $user_profile = UserProfile::where('user_id', $id)->count();
      if ($user_profile == 0) {
        $user_profile = new UserProfile;
        $user_profile->user_id = $id;
      }else {
        $user_profile = UserProfile::where('user_id', $id)->first();
      }

      $user_id = $id;
      $social_media = new \stdClass();

      if (!empty($request->input('whatsapp'))) {
          $social_media->whatsapp = $request->input('whatsapp');
      }else {
          $social_media->whatsapp = "";
      }
      if (!empty($request->input('facebook'))) {
          $social_media->facebook = $request->input('facebook');
      }else {
          $social_media->facebook = "";
      }
      if (!empty($request->input('linkedin'))) {
          $social_media->linkedin = $request->input('linkedin');
      }else {
            $social_media->linkedin = "";
      }
      if (!empty($request->input('instagram'))) {
        $social_media->instagram = $request->input('instagram');
      }else {
        $social_media->instagram = "";
      }
      $social_media = json_encode($social_media);

      $user_profile = UserProfile::where('user_id', $id)->first();
      $user_profile->company_name = $request->input('company_name');
      $user_profile->city = $request->input('city');
      $user_profile->mobile = $request->input('mobile');
      $user_profile->tel = $request->input('tel');
      $user_profile->social_media = $social_media;
      $user_profile->save();

      return redirect('/users/profiles')->with('message', 'Update success!');
    }

    public function editskills(Request $request, $id)
    {
      // return $request;
      $user_id = $id;
      if (!empty($request->input('user_tags'))) {
        $tag_count = DB::table('tags')->where('user_id', $user_id)->count();

        DB::table('tags')->where('user_id', $user_id)->delete();

        $tags = explode(";" ,$request->input('user_tags'));

        foreach ($tags as $tag) {
          $update_tags_table = DB::table('tags')->insert([
            'name' => $tag,
            'user_id' => $user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ]);
        }
      }

      if (!empty($request->input('subindustries'))) {

        DB::table('sub_industry_users')->where('user_id', $user_id)->delete();
        $subindustries = $request->input('subindustries');
        foreach ($subindustries as $subindustry) {
          if (!empty($subindustry)) {
            $sub_name = DB::table('sub_industries')->where('id', $subindustry)->first();
            $update_user_industry_table = DB::table('sub_industry_users')->insert([
                      'user_id' => $user_id,
                      'sub_industry_id' => $subindustry,
                      'sub_industry_name' => $sub_name->name,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s')
            ]);
          }

        }
      }

      return redirect('/users/profiles')->with('message', 'Update success!');

    }

    // public function updateprofile(Request $request)
    // {
    //   // return $request;
    //   $user_profile = UserProfile::where('user_id', $request->input('user_id'))->count();
    //   if ($user_profile == 0) {
    //     $user_profile = new UserProfile;
    //     $user_profile->user_id = $request->input('user_id');
    //   }else {
    //     $user_profile = UserProfile::where('user_id', $request->input('user_id'))->first();
    //   }
    //   $user_profile->city = $request->input('city');
    //   $user_profile->company_name = $request->input('company');
    //   $user_profile->mobile = $request->input('mobile');
    //   $user_profile->tel = $request->input('telephone');
    //   $user_profile->save();
    //
    //   return redirect('/profile/basics');
    // }
    // public function registerstep2b()
    // {
    //
    //   return view('users.profiles.register.step2b');
    // }
    //
    // public function registerstep3()
    // {
    //   $user = \Auth::user();
    //   // return $user->id;
    //   $user_showcase = DB::table('user_showcases')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
    //   $showcase_count = $user_showcase->count();
    //   $user_subindustries = DB::table('user_industry')->where('user_id', '=', $user->id)->get();
    //   $tags = DB::table('tags')->where('user_id', '=', $user->id)->get();
    //   $subindustries = [];
    //   foreach ($user_subindustries as $user_subindustry) {
    //     $sub_id = $user_subindustry->subindustry_id;
    //     $ind_id = $user_subindustry->industry_id;
    //     $industry_name = DB::table('industries')->where('id', '=', $ind_id)->first();
    //     $subindustry_name = DB::table('sub_industries')->where('id', '=', $sub_id)->first();
    //     array_push($subindustries, $subindustry_name->name);
    //
    //   }
    //   if ($user_subindustries->count() == 0) {
    //     $industry_name = '';
    //   }else {
    //     $industry_name = $industry_name->name;
    //   }
    //   return view('users.profiles.register.step3')
    //                       ->with('user', $user)
    //                       ->with('user_showcases', $user_showcase)
    //                       ->with('subindustries', $subindustries)
    //                       ->with('industry_name', $industry_name)
    //                       ->with('showcase_count', $showcase_count)
    //                       ->with('tags', $tags);
    // // }
    //
    // public function registerstep4()
    // {
    //   $subsplans = DB::table('subscription_plans')->get();
    //   return view('users.profiles.register.step4')->with('subsplans', $subsplans);
    // }


    public function register_industries()
    {
      return view('users.profiles.register.industries');
    }
    public function store_industries(Request $request)
    {
      // return $request;
      $user_id = \Auth::user()->id;
      // $user_id = $request->input('user_id');
      if (!empty($request->input('user_tags'))) {
        $tags = explode(";" ,$request->input('user_tags'));

        foreach ($tags as $tag) {
          $update_tags_table = DB::table('tags')->insert([
            'name' => $tag,
            'user_id' => $user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ]);
        }
      }

      if (!empty($request->input('subindustries'))) {
        $subindustries = $request->input('subindustries');
        foreach ($subindustries as $subindustry) {
          if (!empty($subindustry)) {
            $sub_name = DB::table('sub_industries')->where('id', $subindustry)->first();
            $update_user_industry_table = DB::table('sub_industry_users')->insert([
                      'user_id' => $user_id,
                      'sub_industry_id' => $subindustry,
                      'sub_industry_name' => $sub_name->name,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s')
            ]);
          }

        }
      }

      if (!empty($request->input('industry'))) {
        $industry_name = DB::table('industries')->where('id', $request->input('industry'))->first();
        DB::table('industry_users')->insert([
          'user_id' => $user_id,
          'industry_id' => $request->input('industry'),
          'industry_name' => $industry_name->name,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      }

      return redirect('/users/profiles/register/about');

    }

    public function register_about()
    {

      return view('users.profiles.register.about');
    }

    public function store_about(Request $request)
    {
      // return $request;
      $user_profile = UserProfile::where('user_id', $request->input('user_id'))->count();
      if ($user_profile == 0) {
        $user_profile = new UserProfile;
        $user_profile->user_id = $request->input('user_id');
      }else {
        $user_profile = UserProfile::where('user_id', $request->input('user_id'))->first();
      }
      $user_profile->about = $request->input('about');
      $user_profile->tel = $request->input('office');
      $user_profile->mobile = $request->input('mobile');
      $user_profile->city = $request->input('city');
      $user_profile->save();

      return redirect('/users/profiles/register/social');
    }

    public function register_social()
    {
      $mobile = \Auth::user()->profile->mobile;
      return view('users.profiles.register.social')->with('mobile', $mobile);
    }

    public function store_social(Request $request)
    {
      date_default_timezone_set("Africa/Johannesburg");
      // return $request;
      $user_id = $request->input('user_id');
      $social_media = new \stdClass();

      $whatsapp_check = $request->input('whatsapp-check');
      $facebook_check = $request->input('facebook-check');
      $linkedin_check = $request->input('linkedin-check');
      $twitter_check = $request->input('twitter-check');

      // return $request;


        if (!empty($request->input('whatsapp'))) {
            $social_media->whatsapp = $request->input('whatsapp');
        }else {
            $social_media->whatsapp = "";
        }


        if (!empty($request->input('facebook'))) {
            $social_media->facebook = $request->input('facebook');
        }else {
            $social_media->facebook = "";
        }


        if (!empty($request->input('linkedin'))) {
            $social_media->linkedin = $request->input('linkedin');
        }else {
              $social_media->linkedin = "";
        }


        if (!empty($request->input('instagram'))) {
          $social_media->instagram = $request->input('instagram');
        }else {
          $social_media->instagram = "";
        }

      $social_media = json_encode($social_media);

      $user_profile = UserProfile::where('user_id', $request->input('user_id'))->first();
      $user_profile->company_name = $request->input('company_name');
      $user_profile->social_media = $social_media;
      $user_profile->skills = $request->input('user_tags');
      $user_profile->save();

      return redirect('/users/profiles/register/photos');
    }

    public function register_photos()
    {
      // return $request;
      $user = \Auth::user();
      $user_showcase = DB::table('user_showcases')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
      $showcase_count = $user_showcase->count();
      $user_subindustries = DB::table('sub_industry_users')->where('user_id', '=', $user->id)->get();
      $tags = DB::table('tags')->where('user_id', '=', $user->id)->get();
      $subindustries = [];
      foreach ($user_subindustries as $user_subindustry) {
        $sub_id = $user_subindustry->sub_industry_id;
        $subindustry_name = DB::table('sub_industries')->where('id', '=', $sub_id)->first();
        array_push($subindustries, $subindustry_name->name);

      }
      return view('users.profiles.register.photos')
              ->with('user', $user)
              ->with('user_showcases', $user_showcase)
              ->with('subindustries', $subindustries)
              ->with('showcase_count', $showcase_count)
              ->with('tags', $tags);
    }

    public function store_photos(Request $request)
    {
    //   // return $request;
    //   $file_data = $request->input('profile_photo');

    //   $file_data = str_replace('data:image/png;base64,', '', $file_data);
    //   $file_data = str_replace(' ', '+', $file_data);
    //   $user_id = $request->input('user_id');
    //  //generating unique file name;
    //  $file_name = $user_id . '_'.time().'.png';
    //  //@list($type, $file_data) = explode(';', $file_data);
    //  //@list(, $file_data)      = explode(',', $file_data);
    //  $user_profile = UserProfile::where('user_id', $request->input('user_id'))->first();
    //  $user_profile->profile_pic = 'profile_pics/'. $file_name;
    //  $user_profile->save();
    //  if($file_data!=""){
    //    // storing image in storage/app/public Folder
    //    \Storage::disk('public')->put('profile_pics/'.$file_name,base64_decode($file_data));
    //  }

     $subsplans = DB::table('subscription_plans')->where('status', 'Active')->get();

     return view('users.profiles.register.step4')
                ->with('subsplans', $subsplans);

    }

    public function subscription_add()
    {
      $subsplans = DB::table('subscription_plans')->where('status', 'Active')->get();

     return view('users.profiles.register.step4')
                ->with('subsplans', $subsplans);
    }

    public function register_cover_photos()
    {
      $user = \Auth::user();
      $user_showcase = DB::table('user_showcases')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
      $showcase_count = $user_showcase->count();
      $user_subindustries = DB::table('sub_industries_users')->where('user_id', '=', $user->id)->get();
      $tags = DB::table('tags')->where('user_id', '=', $user->id)->get();
      $subindustries = [];
      foreach ($user_subindustries as $user_subindustry) {
        $sub_id = $user_subindustry->sub_industry_id;
        $subindustry_name = DB::table('sub_industries')->where('id', '=', $sub_id)->first();
        array_push($subindustries, $subindustry_name->name);

      }
      return view('users.profiles.register.coverphotos')
              ->with('user', $user)
              ->with('user_showcases', $user_showcase)
              ->with('subindustries', $subindustries)
              ->with('showcase_count', $showcase_count)
              ->with('tags', $tags);
    }

    public function api_user_profile_status($id)
    {
      $profile_status = UserProfile::where('user_id', $id)->count();

      if ($profile_status == 0) {
        return "Not complete";
      } else {
        return "Complete";
      }

    }

    public function api_user_subs_status($id)
    {
      $today = Carbon::now();
      $subs_status_count = DB::table('subscriptions')->where('user_id', $id)->orderBy('created_at', 'desc')->count();
      $subs_status = DB::table('subscriptions')->where('user_id', $id)->orderBy('created_at', 'desc')->get();

      if ($subs_status_count > 0) {
        $expire_date = Carbon::createFromFormat('Y-m-d', $subs_status[0]->expire_date)->toDateTimeString();
        return response()->json([
          'Status' => $subs_status,

        ]);
      } else {
        return response()->json([
          'Status' => 'None',

        ]);
      }
      // $expire_date = Carbon::createFromFormat('Y-m-d', $subs_status[0]->expire_date)->toDateTimeString();


      // return Carbon::parse($expire_date)->diffForHumans();
      // return Carbon::parse($today)->diffInDays($expire_date, false);

    }

    public function show_freelancer($id)
    {
      {
          $user = User::where('id', $id)->first();
          // $profile_check = DB::table('user_profiles')->where('user_id', $id)->count();
          // // return $profile_check;
          // $user_showcase = DB::table('user_showcases')->where('user_id', '=', $id)->orderBy('created_at', 'desc')->get();
          // $showcase_count = $user_showcase->count();
          // $user_subindustries = DB::table('sub_industry_users')->where('user_id', '=', $id)->get();
          // $tags = DB::table('tags')->where('user_id', '=', $id)->get();
          // $subindustries = [];
          // foreach ($user_subindustries as $user_subindustry) {
          //   $sub_id = $user_subindustry->sub_industry_id;
          //   // $ind_id = $user_subindustry->industry_id;
          //   // $industry_name = DB::table('industries')->where('id', '=', $ind_id)->first();
          //   $subindustry_name = DB::table('sub_industries')->where('id', '=', $sub_id)->first();
          //   array_push($subindustries, $subindustry_name->name);

          // }
          // if ($user_subindustries->count() == 0) {
          //   $industry_name = '';
          // }else {
          //   // $industry_name = $industry_name->name;
          // }


          return view('freelancers.show')->with('user', $user);
                  // ->with('user_showcases', $user_showcase)
                  // ->with('subindustries', $subindustries)
                  // // ->with('industry_name', $industry_name)
                  // ->with('showcase_count', $showcase_count)
                  // ->with('tags', $tags);
      }
    }

    public function api_user_portfolio_complete($id)
    {
      $showcase = DB::table('user_showcases')->where('user_id', $id)->get();

      return $showcase;
    }

    public function api_user_tags_complete($id)
    {
      $tags = DB::table('tags')->where('user_id', $id)->get();

      return $tags;
    }

    public function users_where_tag($tags)
    {
      $users = DB::table('tags')->where('name', 'LIKE', $tags)->get();

      return $users;
    }

    public function freelancer_show($id)
    {
      $freelancer = User::find($id);
      $subindustries = DB::table('sub_industries')->get();
      return view('freelancers.dashboard.show')
                  ->with('freelancer', $freelancer)
                  ->with('sub_industries', $subindustries);
    }

    public function user_subscriptions()
    {
      $user = \Auth::user();

      $subs_plans = SubscriptionPlan::all();

      $user_subs = Subscription::where('user_id', $user->id)->latest()->get();

      return view('freelancers.dashboard.subscriptions')
              ->with('subscriptions', $user_subs)
              ->with('subs_plans', $subs_plans);
    }

}
