<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShowcaseItem extends Component
{
    public function render()
    {
        return view('livewire.showcase-item');
    }
}
