<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\User;

class Showcase extends Component
{

    public $user;

    public function mount($user)
    {
      $this->user = $user;
    }

    public function render()
    {
        return view('livewire.showcase');
    }
}
