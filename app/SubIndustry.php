<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubIndustry extends Model
{
  protected $fillable = ['name'];
  // Table Name
  public $table = "sub_industries";
  // Primary Key
  public $primaryKey = "id";
  // Timestamps
  public $timestamps = true;

  public function industry()
  {
    return $this->belongsTo('App\Industry');
  }

  public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

  // public function user()
  // {
  //   return $this->belongsToMany('App\User');
  // }

}
