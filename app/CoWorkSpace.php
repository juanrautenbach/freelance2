<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoWorkSpace extends Model
{
  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function co_work_photos()
  {
    return $this->hasMany('App\CoWorkPhoto', 'co_workspace_id');
  }
}
