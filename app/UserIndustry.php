<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIndustry extends Model
{

  protected $fillable = ['name'];
  // Table Name
  public $table = "user_industry";
  // Primary Key
  public $primaryKey = "id";
  // Timestamps
  public $timestamps = true;
  public function user()
  {
    return $this->belongsToMany('App\User', 'user_industry', 'industry_id');
  }
}
