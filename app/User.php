<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_login', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = ['profile', 'showcase', 'industry', 'subindustry', 'subscription', 'tag', 'messages', 'transactions', 'jobpost'];

    public function profile()
    {
      return $this->hasOne('App\UserProfile');
    }
    public function messages()
    {
      return $this->hasMany('App\Messages', 'user_id');
    }

    public function showcase()
    {
      return $this->hasMany('App\UserShowcase', 'user_id');
    }
    public function subscription()
    {
      return $this->hasMany('App\Subscription', 'user_id');
    }

    public function industry()
    {
      return $this->hasMany('App\IndustryUser', 'user_id');
    }

    public function subindustry()
    {
      return $this->hasMany('App\SubIndustryUser', 'user_id');
    }

    public function tag()
    {
      return $this->hasMany('App\Tag', 'user_id');
    }

    public function transactions()
    {
      return $this->hasMany('App\Transaction', 'user_id');
    }

    public function coworkspace()
    {
      return $this->hasMany('App\CoWorkSpace', 'user_id');
    }

    public function jobpost()
    {
      return $this->hasMany('App\Post', 'user_id');
    }

}
