<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShowcase extends Model
{
  protected $fillable = ['title', 'description', 'showcase_photo', 'user_id'];
  // Table Name
  public $table = "user_showcases";
  // Primary Key
  public $primaryKey = "id";
  // Timestamps
  public $timestamps = true;

  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
