<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{

  // protected $with = ['post'];
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

  public function sender()
  {
    return $this->belongsTo('App\User', 'sender_id');
  }

}
