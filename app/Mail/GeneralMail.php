<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneralMail extends Mailable
{
    use Queueable, SerializesModels;

    public $receiver;
    public $text;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($receiver, $text)
    {
        $this->receiver = $receiver;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->subject('Email from Freelance Cape Town')->view('emails.generalMail')->with('message', $this->text);
    }
}
