<?php

namespace App\Mail;

use App\Messages;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessage extends Mailable
{
    use Queueable, SerializesModels;

    // public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        //

        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $sender_name = User::where('id', '=', $this->message->sender_id)->first();
        $to_name = User::where('id', '=', $this->message->user_id)->first();

        return $this->view('mail.newmessage')
                    ->with([
                      'from' => $sender_name->name,
                      'to' => $to_name->name
                    ])
                    ->subject('New message from Freelance Cape Town');
    }
}
