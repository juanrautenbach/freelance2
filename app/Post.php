<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

  protected $with = ['industries', 'sub_industries'];

  public function industries()
  {
      return $this->belongsToMany('App\Industry', 'industry_posts');
  }

  public function sub_industries()
  {
      return $this->belongsToMany('App\SubIndustry', 'post_sub_industries');
  }

  public function user()
  {
      return $this->belongsToMany('App\User');
  }
  // public function messages()
  // {
  //   return $this->hasMany('App\Messages', 'post_id');
  // }
}
