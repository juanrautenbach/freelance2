<?php


return [
  'return-url' => env('RETURN_URL'),
  'notify-url' => env('NOTIFY_URL'),
  'paygate_id' => env('PAYGATE_ID'),
  'paygate_secret' => env('PAYGATE_SECRET'), 
];


 ?>
