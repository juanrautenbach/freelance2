$(document).ready(function(){
  $('.testimonial-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    autoplaySpeed: 4000,
    // speed: 1000,
    autoplay: false,
    fade: true,
    // cssEase: 'ease-in-out',
    arrows: false,
    asNavFor: '.slider-nav',

  });
  $('.slider-nav').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: true,
   centerPadding: '60px',
  asNavFor: '.testimonial-slider',
  focusOnSelect: true,
  autoplay: true,
  speed: 4000,
  dots: false,
  arrows: false,
  focusOnSelect: true
});
});
