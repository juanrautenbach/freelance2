$("select[name='intended_degree']").change(function(){
  var val = $("select[name='intended_degree']").val();

  if (val == "Other") {
    $("#other_degree").show();
    $("#consider_change_degree").show();
    $("input[name='other_degree_val']").prop('required',true);
    $("input[name='consider_change_degree']").prop('required',true);
  }else {
    $("#other_degree").hide();
    $("#consider_change_degree").hide();
    $("input[name='other_degree_val']").prop('required',false);
    $("input[name='consider_change_degree']").prop('required',false);

  }
})
