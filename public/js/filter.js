// alert("hello");
$(document).ready(function() {
    $("#reset-btn").click(function() {
        console.log("clicked");
        $(".freelancer-cards").css("display", "block");
    });

    $(".freelancer-cards").css("display", "block");
});

function resetFilter() {
    $(".freelancer-cards").css("display", "block");
    // initialisePage();
}

function filter(filter_type, criteria_container_class, filter_criteria_value) {
    $(".freelancer-cards").css("display", "block");
    if (filter_type !== "") {
        if (filter_type === "freetext") {
            var filterValue = $("." + filter_criteria_value)
                .val()
                .toLowerCase();

            $("." + criteria_container_class).each(function() {
                var contents = $(this)
                    .html()
                    .toLowerCase();
                if (contents.indexOf(filterValue) > -1) {
                    if ($(this).hasClass("freelancer-cards")) {
                        $(this).slideDown(50);
                        $(this).css("display", "block");
                        $(this)
                            .prev("hr")
                            .slideDown(50);
                    } else {
                        $(this)
                            .closest(".freelancer-cards")
                            .slideDown(50);
                        $(this).css("display", "block");
                        $(this)
                            .closest(".freelancer-cards")
                            .prev("hr")
                            .slideDown(50);
                    }
                } else {
                    if ($(this).hasClass("freelancer-cards")) {
                        $(this).slideUp(50);
                        $(this)
                            .prev("hr")
                            .css("display", "none");
                    } else {
                        $(this)
                            .closest(".freelancer-cards")
                            .slideUp(50);
                        $(this)
                            .closest(".freelancer-cards")
                            .prev("hr")
                            .slideUp(50);
                    }
                }
            });
        } else {
            $(
                "." +
                    criteria_container_class +
                    ":not(:contains(" +
                    filter_criteria_value +
                    "))"
            )
                .closest(".freelancer-cards")
                .slideUp(50);
            $(
                "." +
                    criteria_container_class +
                    ":not(:contains(" +
                    filter_criteria_value +
                    "))"
            )
                .closest(".freelancer-cards")
                .prev("hr")
                .slideUp(50);
            $(
                "." +
                    criteria_container_class +
                    ":contains(" +
                    filter_criteria_value +
                    ")"
            )
                .closest(".freelancer-cards")
                .slideDown(50);
            $(
                "." +
                    criteria_container_class +
                    ":contains(" +
                    filter_criteria_value +
                    ")"
            )
                .closest(".freelancer-cards")
                .css("display", "block");
            $(
                "." +
                    criteria_container_class +
                    ":contains(" +
                    filter_criteria_value +
                    ")"
            )
                .closest(".freelancer-cards")
                .prev("hr")
                .slideDown(50);
        }
    }

    setTimeout(function() {
        if ($('.freelancer-cards:not([style*="display: none"])').length === 0) {
            $("#filter-noresults").fadeIn(50);
        } else {
            $("#filter-noresults").fadeOut(50);
        }
        $("#count").text(
            $('.freelancer-cards:not([style*="display: none"])').length
        );
    }, 500);
}
