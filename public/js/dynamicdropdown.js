$(function() {
    dynamicDropdown('/api/industries', '#industry_id');

    $('#industry_id').change(function() {
      $("#subcategory_id").empty();
        let url = `/api/subindustries/${this.value}`;
        let target = '#subcategory_id';

        dynamicDropdown1(url, target);

    });
});

function dynamicDropdown(url, selector) {
    $.get(url, function(data) {
        let $select = $(selector);

        $select.find('option').not(':first').remove();

        let options = [];
        $.each(data, function(index, item) {
            options.push(`<option value="${item.id}">${item.name}</option>`);
        })

        $select.append(options);
    });
}

function dynamicDropdown1(url, selector) {
    $.get(url, function(data) {
        let $select = $(selector);

        $select.find('option').not(':first').remove();

        let options = [];
        let scriptText = "";
        let scriptTag = document.createElement("script");
        $.each(data, function(index, item) {
            let name = item.name;
            name = name.replace(/\s/g,'');
            name = name.replace(/[^a-zA-Z ]/g, "");
            console.log(name);
            options.push(`
              <div class="d-block">
              <i class="far fa-square text-muted mr-2 ml-1" id="${name}" onclick="${name}()"></i>
              <i class="far fa-check-square mr-2 text-blue ml-1" style="display:none" id="${name}_clicked" onclick="${name}_clicked()"></i>
              <label class="mb-0">${item.name}</label>
              <input type="hidden" class="" name="subindustries[]" id="${name}-input">
              </div>
              `);
              scriptText += `
              function ${name}() {
                  $("#${name}").hide();
                  $('#${name}_clicked').show();
                  $('#${name}-input').val("${item.id}");
              };
              function ${name}_clicked() {
                  $("#${name}").show();
                  $('#${name}_clicked').hide();
                  $('#${name}-input').val("");
              };`;

        })
        $('#subcategory_id_input').hide();
        console.log(scriptText);
        scriptTag.innerHTML = scriptText;
        document.body.appendChild(scriptTag);
        $select.append(options);
    });
}
