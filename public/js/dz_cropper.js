var viewWidth = x + 16;
var viewHeight = y + 16;
Dropzone.options.myDropzone = {
  maxFiles: 1,
  thumbnailWidth: x,
  thumbnailHeight: y,

  addRemoveLinks: true,
  acceptedFiles: 'image/*',
  dictDefaultMessage: dzMessage,
  dictRemoveFile: "Nope, try again!",
  createImageThumbnails: true,
  previewTemplate: `<div class="dz-preview dz-file-preview">
  <div class="dz-details">
    <img data-dz-thumbnail />
  </div>
</div>`,
  previewsContainer: document.getElementById("photo-preview"),
  url: path_url,

  transformFile: function(file, done) {
  // Create Dropzone reference for use in confirm button click handler
  var myDropZone = this;

  // Create the image editor overlay
  var cropContainer = document.createElement('div');
  cropContainer.setAttribute('style','width: 100vw; height: 100vh; top:0; left:0; background-color: rgba(0,0,0,0.5);');
  cropContainer.style.position = 'fixed';
  cropContainer.style.zIndex = 8800;
  var editor = document.createElement('div');
  editor.setAttribute('id','editor');
  editor.setAttribute('style',popupContainer);

  editor.style.position = 'fixed';
  editor.style.zIndex = 9000;
  editor.style.backgroundColor = '#000';
  document.body.appendChild(cropContainer);
  cropContainer.appendChild(editor);
  // Create confirm button at the top left of the viewport
  var buttonConfirm = document.createElement('button');
  buttonConfirm.setAttribute('class','btn btn-blue mx-auto');
  buttonConfirm.setAttribute('style','transform: translateX(-50%); left: 50%; bottom: -40px; z-index:9999; position: absolute;');
  buttonConfirm.textContent = 'Accept';
  editor.appendChild(buttonConfirm);
  buttonConfirm.addEventListener('click', function() {
    // Get the canvas with image data from Cropper.js
    $("#photo-preview").show();
    $("#photo-preview").css('border','');
    $("#myDropzone").hide();
    $(".dz-remove").addClass("btn btn-blue btn-sm");
    $(".dz-details").addClass("mb-2");
    // $(".dz-details").addClass("shadow-sm");
    // $(".dz-details").css('border','8px solid #fff');
    $(".dz-details").css('width',viewWidth);
    $(".dz-details").css('height',viewHeight);
    $(".dz-remove").click(function(){
      $("#myDropzone").show();
      $("#photo-preview").hide();
    })
    var canvas = cropper.getCroppedCanvas({
      width: x,
      height: y,
      modal: false
    });
    // Turn the canvas into a Blob (file object without a name)
    canvas.toBlob(function(blob) {
      // Create a new Dropzone file thumbnail
      myDropZone.createThumbnail(
        blob,
        myDropZone.options.thumbnailWidth,
        myDropZone.options.thumbnailHeight,
        myDropZone.options.thumbnailMethod,
        false,
        function(dataURL) {

          // Update the Dropzone file thumbnail
          myDropZone.emit('thumbnail', file, dataURL);
          // Return the file to Dropzone
          $("#pphoto").val(dataURL);
          done(blob);
      });
    });
    // Remove the editor from the view
    document.body.removeChild(cropContainer);
    cropContainer.removeChild(editor);
  });
  // Create an image node for Cropper.js
  var image = new Image();
  image.src = URL.createObjectURL(file);
  editor.appendChild(image);

  // Create Cropper.js
  var cropper = new Cropper(image, { aspectRatio: pic_ratio });
}
};
