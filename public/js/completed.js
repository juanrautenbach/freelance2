var user_id = $("#user_id").val();

$("#completeBtn").click(function() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })
    console.log(user_id);
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "I have filled in everything and I'm sure all is in order.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, I have filled in everything!',
        cancelButtonText: 'Cancel',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            swalWithBootstrapButtons.fire(
                'Complete!',
                'Thank you for applying to the Jakes Gerwel Fellowship.  Look out for an email from us that will outline the timeline for the rest of the application process.',
                'success'
            ).then(function() {
              window.location.href = `/api/user/completed/${user_id}`;

            });

        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Please make sure you complete all the relevant sections.',
                'error'
            )
        }
    })
})
