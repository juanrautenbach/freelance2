
function messageUsers(user_name) {
    axios.get("/api/user/"+ user_name +"/message/users")
  .then(function (response) {
    var senders = response.data.senders;
    var user_id = response.data.user_id;
    // console.log(response.data.senders[0].name);
    // console.log(user_id);
    var html = "";
    for (let index = 0; index < senders.length; index++) {
        // console.log(senders[index]);
        var profile_photo = "";
        var name = senders[index].name;
        var sender_id = senders[index].id;
        if (senders[index].profile == null) {
            profile_photo = "/img/FCPT_Logo.png";
        }else{
            profile_photo = senders[index].profile.profile_pic;
        }
        // console.log(profile_photo);
        // console.log(name);

        html += `<li onclick="getMessages(${sender_id}, ${user_id})">
        <div class="d-flex align-items-center">
            <div class="avatar m-0 mr-50">
            <a class="avatar m-0">
                <img src="${profile_photo}" alt="avatar" height="36" width="36" />
            </a>
                <span class="avatar-status-busy"></span>
            </div>
            <div class="chat-sidebar-name">
                <h6 class="mb-0">${name}</h6>
            </div>
        </div>
    </li>`;

    }
    $("#chat-sidebar-list").append(html);
    // console.log(html);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });
}


function getMessages(sender_name, user_name) {
    // console.log("Sender: " + sender_name);
    // console.log("User: " + user_name);
    $("#chat-content").empty();
    $("#chat-start").addClass('d-none');
    $("#chat-header").removeClass('d-none');
    $("#chat-area").removeClass('d-none');
    $("#chat-footer").removeClass('d-none');
    $("input[name='user_id']").val(sender_name);
    $("input[name='sender_id']").val(user_name);

    axios.get("/api/messages/between/"+ sender_name +"/" + user_name)
  .then(function (response) {
    var sender_id = response.data.sender.id;
    var sender_name = response.data.sender.name;
    var sender_photo = "";
    var user_name = response.data.user.name;
    var user_photo = "";
    $("input[name='user_name']").val(sender_name);
    $("input[name='sender_name']").val(user_name);

    if(response.data.sender.profile == null){
        sender_photo = "/img/FCPT_Logo.png";
    }else{
        sender_photo = response.data.sender.profile.profile_pic;
    }

    if(response.data.user.profile == null){
        user_photo = "/img/FCPT_Logo.png";
    }else{
        user_photo = response.data.user.profile.profile_pic;
    }
    // var sender_photo = response.data.sender.profile.profile_pic;
    // console.log(sender_name);
    // console.log(sender_photo);
    $('#message_user').text(sender_name);
    $('#avatar-photo').attr('src', sender_photo);
    // console.log(response);
    var messages = response.data.data;
    // console.log(messages);
    var messageHTML = ""; 
    var count = 0;
    for (let index = 0; index < messages.length; index++) {
        
        
    
        if (messages[index].user_id == sender_id) {
           messageHTML += `<div class="chat" id="message_${messages[index].id}">
              <div class="chat-avatar">
                <a class="avatar m-0">
                  <img src="${ user_photo }" alt="avatar" height="36" width="36">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-message">
                  <p>${messages[index].messages}</p>
                  <span class="chat-time">${messages[index].created_at}</span>
                </div>
              </div>
            </div>`;
        } else {
            messageHTML += `<div class="chat chat-left" id="message_${messages[index].id}">
              <div class="chat-avatar">
                <a class="avatar m-0">
                  <img src="${ sender_photo }" alt="avatar" height="36" width="36">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-message">
                  <p>${messages[index].messages}</p>
                  <span class="chat-time">${messages[index].created_at}</span>
                </div>
              </div>
            </div>`;
        }
        
        
        count = messages[index].id;
    }
    console.log(count);
    $("#chat-content").append(messageHTML);
    $(location).attr('href','#message_'+count);
    // $.scrollTo($('#message_86'),1000);

    

  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });
}