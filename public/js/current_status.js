$("input[name='education_status']").change(function(){
  var val = $("input[name='education_status']:checked").val();

  if (val == "current_gr12") {
    $("#completed_gr12").hide();
    $("input[name='grade12_completed_year']").prop('required',false);
    $("input[name='average_gr12_results']").prop('required',false);
    $('textarea').prop("required", false);
    $("#grade12_text").hide();
  }else if (val == "gap_year") {
    $("#completed_gr12").show();
    $("#grade12_text").show();
  }else if (val == "university_1st_year") {
    $("#completed_gr12").show();
    $("#grade12_text").show();
    $("input[name='grade12_completed_year']").prop('required',false);
    $("textarea[name='time_spend_since_g12']").prop('required',true);

  }

})
