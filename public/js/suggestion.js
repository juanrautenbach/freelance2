$("#suggestionBox").click(function(){
  $("#suggestion-box-container").show();

})

$("#closeBtn").click(function () {
  $("#suggestion-box-container").hide();
  $("#star1").removeClass('text-red');
  $("#star2").removeClass('text-red');
  $("#star3").removeClass('text-red');
  $("#star4").removeClass('text-red');
  $("#star5").removeClass('text-red');
});

$("#star1").mouseover(function(){
  $("#star1").addClass('text-blue');
})
$("#star1").mouseout(function(){
  $("#star1").removeClass('text-blue');
})

$("#star2").mouseover(function(){
  $("#star1").addClass('text-blue');
  $("#star2").addClass('text-blue');
})
$("#star2").mouseout(function(){
  $("#star2").removeClass('text-blue');
  $("#star1").removeClass('text-blue');
})

$("#star3").mouseover(function(){
  $("#star1").addClass('text-blue');
  $("#star2").addClass('text-blue');
  $("#star3").addClass('text-blue');
})
$("#star3").mouseout(function(){
  $("#star1").removeClass('text-blue');
  $("#star2").removeClass('text-blue');
  $("#star3").removeClass('text-blue');
})

$("#star4").mouseover(function(){
  $("#star1").addClass('text-blue');
  $("#star2").addClass('text-blue');
  $("#star3").addClass('text-blue');
  $("#star4").addClass('text-blue');
})
$("#star4").mouseout(function(){
  $("#star1").removeClass('text-blue');
  $("#star2").removeClass('text-blue');
  $("#star3").removeClass('text-blue');
  $("#star4").removeClass('text-blue');
})

$("#star5").mouseover(function(){
  $("#star1").addClass('text-blue');
  $("#star2").addClass('text-blue');
  $("#star3").addClass('text-blue');
  $("#star4").addClass('text-blue');
  $("#star5").addClass('text-blue');
})
$("#star5").mouseout(function(){
  $("#star1").removeClass('text-blue');
  $("#star2").removeClass('text-blue');
  $("#star3").removeClass('text-blue');
  $("#star4").removeClass('text-blue');
  $("#star5").removeClass('text-blue');
})

$("#star1").click(function(){
  $("#star1").addClass('text-red');
  $("#star2").removeClass('text-red');
  $("#star3").removeClass('text-red');
  $("#star4").removeClass('text-red');
  $("#star5").removeClass('text-red');
  $("#star_rating").val("1");
})

$("#star2").click(function(){
  $("#star1").addClass('text-red');
  $("#star2").addClass('text-red');
  $("#star3").removeClass('text-red');
  $("#star4").removeClass('text-red');
  $("#star5").removeClass('text-red');
  $("#star_rating").val("2");
})

$("#star3").click(function(){
  $("#star1").addClass('text-red');
  $("#star2").addClass('text-red');
  $("#star3").addClass('text-red');
  $("#star4").removeClass('text-red');
  $("#star5").removeClass('text-red');
  $("#star_rating").val("3");
})

$("#star4").click(function(){
  $("#star1").addClass('text-red');
  $("#star2").addClass('text-red');
  $("#star3").addClass('text-red');
  $("#star4").addClass('text-red');
  $("#star5").removeClass('text-red');
  $("#star_rating").val("4");
})

$("#star5").click(function(){
  $("#star1").addClass('text-red');
  $("#star2").addClass('text-red');
  $("#star3").addClass('text-red');
  $("#star4").addClass('text-red');
  $("#star5").addClass('text-red');
  $("#star_rating").val("5");
})
