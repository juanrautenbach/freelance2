
    $(document).ready(function() {
        var value = $("#name").val().length;
        if (value === 0) {
            $("#registerForm").show();
        } else {
            $("#nextRegister").show();
        }

        var name = $('#name').val();

        if (name) {
          $("#register-front-top").hide();
          $("#register-front-main").hide();
        } else {
          $("#register-front-top").show();
          $("#register-front-main").show();
        }

        $("#email").change(function(){
          $email = $("#email").val();

          $.get("/api/user/email/used/"+ $email, function(data, status){
              console.log("Data: " + data + "\nStatus: " + status);
              if (data == "True") {
                $("#email_error").show();
                $("button").attr("disabled", true);
              } else {
                $("#email_error").hide();
                $("button").attr("disabled", false);
              }
            });

        });
    });

    function nextRegister($role) {
        $("#register-front-top").hide();
        $("#register-front-main").hide();
        $("#nextRegister").show();
        $("input[name=role]").val($role);
        // $("#send_to").val($id);
    }



    // (function() {
    // var placesAutocomplete = places({
    //   appId: 'plMX367D0G0I',
    //   apiKey: 'a3f0d23c4dbec20306f7796636e6b0aa',
    //   container: document.querySelector('#user_city'),
    //   templates: {
    //     value: function(suggestion) {
    //       return suggestion.name;
    //     }
    //   }
    // }).configure({
    //   // type: 'city',
    //   countries: ['za'],
    //   aroundLatLngViaIP: false,
    // });
    // })();
    var width = 0;

      $('#mobile').click(function() {
          width = $(window).width();
          $('#mobile-input').removeClass('d-none');
          if (width >= "768") {
            $('#mobile-input').addClass('d-inline');
          } else {
            $('#mobile-input').addClass('d-block');
          }
          $('#mobile').hide();
          $('#mobile_clicked').show();
      });
      $('#mobile_clicked').click(function() {
          $('#mobile-input').addClass('d-none');
          if (width >= "768") {
            $('#mobile-input').removeClass('d-inline');
          } else {
            $('#mobile-input').removeClass('d-block');
          }
          $('#mobile').show();
          $('#mobile_clicked').hide();
      });
      $('#office').click(function() {
        width = $(window).width();
          $('#office-input').removeClass('d-none');
          if (width >= "768") {
            $('#office-input').addClass('d-inline');
          } else {
            $('#office-input').addClass('d-block');
          }
          $('#office').hide();
          $('#office_clicked').show();
      });
      $('#office_clicked').click(function() {
          $('#office-input').addClass('d-none');
          if (width >= "768") {
            $('#office-input').removeClass('d-inline');
          } else {
            $('#office-input').removeClass('d-block');
          }
          $('#office').show();
          $('#office_clicked').hide();
      });

      $('#mobile-selected').click(function() {
          var n = $("#mobile-selected:checked").length;
          if (n === 1) {
            $('#mobile-input').show();
          } else {
            $('#mobile-input').hide();
          }
      });
      $('#office-selected').click(function() {
          var n = $("#office-selected:checked").length;
          if (n === 1) {
            $('#office-input').show();
          } else {
            $('#office-input').hide();
          }
      });

      // alert("hi");
      // $("input[name='email']").blur(function(){
      //   alert(this.val());
      // })
