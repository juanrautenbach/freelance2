
function addPhoto(id) {
    $("#add_showcase").show();
    $("input[name='edit_photo_id']").val(id);

    $("textarea[name='showcase_desc']").css({
        // 'overflow-y': 'scroll',
        // 'height': '100%'
    });
    $("#add_showcase").css({
        overflow: 'auto',
        height: '100%',
    });
    $('html, body').css({
        overflow: 'hidden',
        height: '100%'
    });
    $("#add_showcase .card").css({
        'max-height': '100vh',
        overflow: 'auto'
    })
}

$("#cancelshowcase").click(function() {
    $("#add_showcase").hide();

    $('html, body').css({
        'overflow-y': 'auto',
        height: 'auto'
    });

})

function editPhoto(id){
    addPhoto(id);

}

function addProfilePhoto(id){
    $("input[name='edit_photo_id']").val(id);
    $("#add_profile").show();
}

$("#cancelprofilephoto").click(function() {
    $("#add_profile").hide();

    $('html, body').css({
        'overflow-y': 'auto',
        height: 'auto'
    });

})