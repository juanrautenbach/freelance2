<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndustriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('industries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('industries')->insert(array('name' => 'Graphic and Design'));
        DB::table('industries')->insert(array('name' => 'Digital Marketing'));
        DB::table('industries')->insert(array('name' => 'Writing & Translation'));
        DB::table('industries')->insert(array('name' => 'Video & Animation'));
        DB::table('industries')->insert(array('name' => 'Music & Audio'));
        DB::table('industries')->insert(array('name' => 'Web Development'));
        DB::table('industries')->insert(array('name' => 'Business'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('industries');
    }
}
