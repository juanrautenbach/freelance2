<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubIndustriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_industries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('industry_id');
            $table->timestamps();
        });


          DB::table('sub_industries')->insert(array('name' => 'Logo Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Brand Style Guides', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Game Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Business Cards & Stationery', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Illustration', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Brochure Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Poster Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Flyer Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Book & Album Covers', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Packaging Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Storyboards', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Web & Mobile Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Menu Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Postcard Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Catalog Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Social Media Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Portraits & Caricatures', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Cartoons & Comics', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Car Wraps', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Banner Ads', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Photoshop Editing', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Architecture & Interior Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => '3D Models & Product Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'T-Shirts', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Presentation Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Infographic Design', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Invitations', 'industry_id' => '1'));
          DB::table('sub_industries')->insert(array('name' => 'Social Media Marketing', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'SEO', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Content Marketing', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Video Marketing', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Email Marketing', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Marketing Strategy', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Surveys', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Web Analytics', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Influencer Marketing', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Local Listings', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Domain Research', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'E-Commerce Marketing', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Mobile Marketing & Advertising', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Web Traffic', 'industry_id' => '2'));
          DB::table('sub_industries')->insert(array('name' => 'Articles & Blog Posts', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Resumes & Cover Letters', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Technical Writing', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Translation', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Creative Writing', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Research & Summaries', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Sales Copy', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Press Releases', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Transcripts', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Legal Writing', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Email Copy', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Business Names & Slogans', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Website Content', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Scriptwriting', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Book & eBook Writing', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Product Descriptions', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Speechwriting', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Beta Reading', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Proofreading & Editing', 'industry_id' => '3'));
          DB::table('sub_industries')->insert(array('name' => 'Whiteboard & Animated Explainers', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Video Editing', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Short Video Ads', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Animated GIFs', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Logo Animation', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Intros & Outros', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Live Action Explainers', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Character Animation', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => '3D Product Animation', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Lyric & Music Videos', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Spokespersons Videos', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Subtitles & Captions', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Visual Effects', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Animation for Kids', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Slideshows & Promo Videos', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Game Trailers', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Animation for Streamers', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Product Photography', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Local Photography', 'industry_id' => '4'));
          DB::table('sub_industries')->insert(array('name' => 'Voice Over', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Mixing & Mastering', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Producers & Composers', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Singer-Songwriters', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Session Musicians & Singers', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Podcast & Spoken-Word Editing', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Music Transcription', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Vocal Tuning', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Jingles & Drops', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'Sound Effects', 'industry_id' => '5'));
          DB::table('sub_industries')->insert(array('name' => 'WordPress', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Website Builders & CMS', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Game Development', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Web Programming', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'E-Commerce Development', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Mobile Apps & Web', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Desktop Applications', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Support & IT', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Chatbots', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Data Analysis & Reports', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Convert Files', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Databases', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'User Testing', 'industry_id' => '6'));
          DB::table('sub_industries')->insert(array('name' => 'Virtual Assistant', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Data Entry', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Market Research', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Product Research', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Business Plans', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Branding Services', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Legal Consulting', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Financial Consulting', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Business Tips', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Presentations', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Career Advice', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Flyer Distribution', 'industry_id' => '7'));
          DB::table('sub_industries')->insert(array('name' => 'Lead Generation', 'industry_id' => '7'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_industries');
    }
}
