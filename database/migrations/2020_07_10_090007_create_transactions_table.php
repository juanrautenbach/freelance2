<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('voucher_code')->nullable();
            $table->string('voucher_amount')->nullable();
            $table->string('subscription_amount')->nullable();
            $table->string('subscription_duration')->nullable();
            $table->string('total_amount');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('description')->nullable();
            $table->string('reference')->nullable();
            $table->text('payment_reference')->nullable();
            $table->string('status')->default('In progress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
