<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('profile_pic')->nullable();
            $table->string('profile_cover_pic')->nullable();
            $table->string('company_name')->nullable();
            $table->text('about')->nullable();
            $table->string('tel')->nullable();
            $table->string('mobile')->nullable();
            $table->string('city')->nullable();
            $table->text('social_media')->nullable();
            $table->text('skills')->nullable();
            $table->integer('click_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
