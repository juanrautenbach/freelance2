<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Juan Rautenbach',
                'email' => 'juan@julura.co.za',
                'role' => 'Admin',
                'email_verified_at' => (date('Y-m-d H:i:s')),
                'password' => Hash::make('1zeoa11!'),
                'status' => 'active'
            ]
          );

          User::create(
            [
                'name' => 'Lana Rautenbach',
                'email' => 'lana@abc.co.za',
                'role' => 'Freelancer',
                'email_verified_at' => (date('Y-m-d H:i:s')),
                'password' => Hash::make('1zeoa11!'),
                'status' => null
            ]
          );

          User::create(
            [
                'name' => 'Ludri Rautenbach',
                'email' => 'ludri@abc.co.za',
                'role' => 'Finder',
                'email_verified_at' => (date('Y-m-d H:i:s')),
                'password' => Hash::make('1zeoa11!'),
                'status' => null
            ]
          );
          User::create(
            [
              'name' => 'Marius Vosloo',
              'email' => 'marius@freelancecapetown.com',
              'email_verified_at' => Carbon::now(),
              'password' => Hash::make('FCTP2020!'),
              'role' => 'Admin',
              'status' => null
            ]
          );
          User::create(
            [
              'name' => 'Dawid Möller',
              'email' => 'dawid@freelancecapetown.com',
              'email_verified_at' => Carbon::now(),
              'password' => Hash::make('FCTP2020!'),
              'role' => 'Admin',
              'status' => null
            ]
          );

          
    }
}
