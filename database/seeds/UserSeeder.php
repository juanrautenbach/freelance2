<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    User::create(
      [
        'name' => 'Koos Kombuis',
        'email' => 'koos@abc.co.za',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('1zeoa11!'),
        'role' => 'Freelancer',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]
    );
    User::create(
      [
        'name' => 'Juan Rautenbach',
        'email' => 'juan@julura.co.za',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('1zeoa11!'),
        'role' => 'Admin',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]
    );
    User::create(
      [
        'name' => 'Marius Vosloo',
        'email' => 'marius@freelancecapetown.com',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('FCTP2020!'),
        'role' => 'Admin',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]
    );
    User::create(
      [
        'name' => 'Dawid Möller',
        'email' => 'dawid@freelancecapetown.com',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('FCTP2020!'),
        'role' => 'Admin',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]
    );
    User::create(
      [
        'name' => 'Lana Rautenbach',
        'email' => 'lana@abc.co.za',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('1zeoa11!'),
        'role' => 'Finder',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]
    );
    User::create(
      [
        'name' => 'Ludri Rautenbach',
        'email' => 'ludri@abc.co.za',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('1zeoa11!'),
        'role' => 'Co-working',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]
    );
  }
}
