<?php

use App\Messages;
use Illuminate\Database\Seeder;

class TestMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0; $i<=40; $i++){
            Messages::create(
                [
                    'user_id' => '2',
                    'user_name' => 'Lana Rautenbach',
                    'sender_id' => '1',
                    'sender_name' => 'Juan Rautenbach',	
                    'subject' => $faker->sentence,	
                    'messages' => $faker->paragraph,	
                    'status' => 'New',	
                    'post_id' => null,
                ]
              );
              Messages::create(
                [
                    'user_id' => '1',
                    'user_name' => 'Juan Rautenbach',
                    'sender_id' => '2',
                    'sender_name' => 'Lana Rautenbach',	
                    'subject' => $faker->sentence,	
                    'messages' => $faker->paragraph,	
                    'status' => 'New',	
                    'post_id' => null,
                ]
              );
        }
        
    }
}
