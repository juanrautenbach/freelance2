<?php

use Illuminate\Database\Seeder;
use App\SubscriptionPlan;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlan::create(
            [
                'name' => '6 month plan',
                'description' => '6 month plan',
                'duration' => '6 months',
                'type' => 'Once off',
                'price' => '699',
                'status' => 'Active'
            ]
          );

          SubscriptionPlan::create(
            [
                'name' => '12 month plan',
              'description' => '12 month plan',
              'duration' => '12 months',
              'type' => 'Once off',
              'price' => '899',
                'status' => 'Active'
            ]
          );
    }
}
