
@extends('front')
@section('content')
  @include('components.carousel')
  @include('components.joincalltoaction')
  @include('components.frontpagefreelancers')
  @include('components.joincalltoaction')
  @include('components.freelanceplatform')
  @include('components.joincalltoaction')
  @include('components.registerPopupForm')
  @include('components.testimonials')
  {{-- @include('components.looking')
  @include('components.pricetable') --}}
  {{-- @include('components.looking') --}}

@endsection
