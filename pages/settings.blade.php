@extends('dashboard')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col">
        <table class="table">
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Last Login</th>
            <th>Actions</th>
          </tr>
          @foreach ($all_users as $user)
            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->role}}</td>
              <td>{{$user->last_login}}</td>
              <td> <a href="/edit/user/{{$user->id}}" class="btn btn-red"><i class="fas fa-pencil-alt"></i></a>
                <form class="" action="/deactivate" method="post">
                  @csrf
                  <input type="hidden" name="user_id" value="{{$user->id}}">
                  <button type="button" class="btn btn-danger" name="button">Deactivate</button>
                </form>

              </td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>

@endsection
