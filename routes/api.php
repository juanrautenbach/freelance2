<?php

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Messages;

Route::get('/messages/between/{sender_id}/{user_id}', 'Api\MessagesController@getMessages');

Route::get('/industries', 'Api\IndustryController@industries');
Route::get('/subindustries/{industry}', 'Api\IndustryController@subindustry');

Route::get('/vouchers/{voucher}', 'VoucherController@validateVoucher');

Route::get('/user/profile/{id}/status', 'UserProfileController@api_user_profile_status');

Route::get('/user/subscription/{id}/status', 'UserProfileController@api_user_subs_status');

Route::get('/user/portfolio/{id}/complete', 'UserProfileController@api_user_portfolio_complete');

Route::get('/user/tags/{id}/complete', 'UserProfileController@api_user_tags_complete');

Route::get('/user/tags/{tag}/search', 'UserProfileController@users_where_tag');

Route::get('/message/{id}', 'MessagesController@get_message');

Route::get('/user/{id}/name', 'UserController@get_user_name');

Route::get('/users/freelancers/{search}', 'UserController@freelancers_search');

Route::get('/co-working/show/user/{user}', 'Api\CoWorkController@coworkshowall');

Route::get('/freelancers/by/tagname/{tagname}', 'Api\FrontFreelancerController@freelancer_by_tag_name');

Route::get('/freelancers/by/subindustry/{name}', 'Api\FrontFreelancerController@freelancer_by_sub_industry_name');

Route::get('/freelancers/active/all', 'Api\FrontFreelancerController@freelancer_active_all');

Route::get('/freelancers/test/all', function (){
  $users = User::all()->where('role', 'Freelancer');
  // dd($users);
  return $users;
});

Route::get('/freelancers/test/{id}', function ($id){
  $users = User::where('id', $id)->where('role', 'Freelancer')->first();
  // dd($users);
  return $users;
});

Route::get('/user/unread/{id}', function ($id){
  // $users = User::where('id', $id)->first();
  $unread = Messages::where('user_id', $id)->where('status', 'New')->count();
  return $unread;
});

Route::get('/messages/test/all', function (){
  $messages = Messages::all();

  return $messages;
});

Route::get('/posts/test/all', function (){
  $posts = Post::all();

  return $posts;
});

Route::get('/user/messages/{id}', 'Api\MessagesController@all_user_messages');

Route::get('/user/{id}', 'Api\FunctionController@get_freelancer');

Route::get('/user/{id}/message/users', 'Api\MessagesController@message_users');

Route::get('/users/all/freelancers', 'Api\FunctionController@get_freelancers');

Route::get('/users/all/finders', 'Api\FunctionController@get_finders');

// Route::get('/posts/test/all', function (){
//   $posts = Post::all();
//
//   // $industries = array();
//   // foreach ($posts as $post) {
//   //   foreach ($post->industries as $industry) {
//   //     array_push($industries, $industry->name);
//   //   }
//   //
//   // }
//   //
//   // return $industries;
//   //
//   $subindustries = array();
//   foreach ($posts as $post) {
//     foreach ($post->sub_industries as $subindustry) {
//       array_push($subindustries, $subindustry->name);
//     }
//
//   }
//   sort($subindustries);
//
//   $subs_result = array();
// foreach ($subindustries as $key => $value){
//   if(!in_array($value, $subs_result))
//     // $subs_result=$value;
//     array_push($subs_result, $value);
// }
//
//   return $subs_result;
// });


Route::get("/user/email/used/{email}", "Api\FunctionController@check_email");

Route::post("/user/freelancer/profile/photo", "Api\ProfileController@store_profile_photo");

Route::post("/user/{id}/showcase/create", "Api\ShowcaseController@create1");

Route::post("/freelancer/about", 'Api\ProfileController@freelance_about_update');
Route::post("/freelancer/contact", 'Api\ProfileController@freelance_contact_update');

Route::post("/subindustry/delete", 'Api\SubIndustryController@delete_subindustry');
