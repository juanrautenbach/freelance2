<?php

use Illuminate\Http\Request;
use App\Mail\SuggestionMail;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.home');
// });
Route::get('/', 'PagesController@home');
Route::get('/about', function () {
    return view('pages.about');
});
Route::get('/contact', function () {
    return view('pages.contact');
});
Route::get('/terms', function () {
    return view('pages.terms');
});
Route::get('/policy', function () {
    return view('pages.policy');
});

Route::get('/freelancers', 'PagesController@freelancers');
Route::get('/co-working', 'PagesController@coworking');

Route::get('/live_search/action', 'HomeController@action')->name('live_search.action');

Route::get('/settings', 'PagesController@settings')->middleware('verified');
Route::get('/settings/industries', 'IndustryController@view_industries');
// Auth::routes();
Route::post('/message/delete/{id}', 'MessagesController@trash');
Route::post('/message/restore/{id}', 'MessagesController@restore');
// Packages
Route::resource('packages', 'PackageController')->middleware('verified');
Route::resource('posts', 'PostController')->middleware('verified');
Route::resource('messages', 'MessagesController')->middleware('verified');
Route::resource('subscriptionplans', 'SubscriptionPlanController')->middleware('verified');
Route::resource('industries', 'IndustryController')->middleware('verified');
Route::resource('subindustries', 'SubIndustryController')->middleware('verified');
Route::resource('usershowcase', 'UserShowcaseController')->middleware('verified');
Route::resource('vouchers', 'VoucherController')->middleware('verified');
// Route::resource('co-working', 'CoWorkSpaceController');
Route::resource('links', 'LinkClickedController');

Route::get('/co-working/admin', 'CoWorkSpaceController@adminindex');
Route::post('/co-working', 'CoWorkSpaceController@adminstore');

// Users
Route::post('/register', 'UserController@register');
Route::get('/package', 'UserController@package');
Route::post('/package', 'UserController@paymentselection');
Route::get('/dashboard', 'HomeController@index')->middleware('verified');
Route::get('/make/{type}', 'PagesController@make');
Route::get('/verify/{hash}', 'UserController@verify');
Route::get('/payment', 'UserController@payment');
Route::get('/edit/user/{id}', 'PagesController@editUser');

Route::get('/profile/basics', 'UserProfileController@basics');
Route::get('/profile/coverphoto', 'UserProfileController@coverphoto');
Route::get('/profile/profilephoto', 'UserProfileController@profilephoto');

Route::post('/profile/socialmedia', 'UserProfileController@socialmedia');
Route::post('/profile/update', 'UserProfileController@updateprofile');
Route::put('/user/profile/about/edit/{id}', 'UserProfileController@editabout');
Route::put('/user/profile/contact/edit/{id}', 'UserProfileController@editcontact');
Route::put('/user/profile/skills/edit/{id}', 'UserProfileController@editskills');

Route::post('/user/showcase/add', 'UserShowcaseController@add');

Route::post('/user/profile_photo/add', 'UserShowcaseController@addProfilePhoto');
// User profile
Route::get('/profile/{id}', 'UserProfileController@show');
Route::get('/users/profiles/', 'UserProfileController@index');
Route::get('/users/profiles/register', 'UserProfileController@create');
Route::post('/users/profiles/', 'UserProfileController@store');
Route::get('/users/profiles/photos', 'UserProfileController@profilephotos');
Route::post('/settings/user/update', 'UserController@userrole');

Route::put('/user/action/activate/{id}', 'UserController@activate_user');
Route::put('/user/action/deactivate/{id}', 'UserController@deactivate_user');

// Userprofile Photos
Route::post('/usershowcase/profile_photo', 'UserShowcaseController@profilephoto'); 
Route::post('/usershowcase/profile_cover', 'UserShowcaseController@profilecover');
Route::get('/subscriptions', 'UserProfileController@user_subscriptions');
Route::post('/subscriptionplans/deactivate/{id}', 'SubscriptionPlanController@deactivate');
Route::post('/subscriptionplans/activate/{id}', 'SubscriptionPlanController@activate');

// Payment pages Routes
Route::post('/confirmpayment', 'PaymentController@confirmPayment');
Route::post('/itn/{trans_id}', 'PaymentController@itn');
Route::post('/success', 'PaymentController@success');
Route::get('/cancelled', 'PaymentController@cancelled');

Auth::routes(['verify' => true]);

//User profile
Route::get('/users/profiles/register/social', 'UserProfileController@register_social')->middleware('auth');
Route::post('/users/profiles/register/social', 'UserProfileController@store_social')->middleware('auth');
Route::get('/users/profiles/register/about', 'UserProfileController@register_about')->middleware('auth');
Route::post('/users/profiles/register/about', 'UserProfileController@store_about')->middleware('auth');
Route::get('/users/profiles/register/industries', 'UserProfileController@register_industries')->middleware('auth');
Route::post('/users/profiles/register/industries', 'UserProfileController@store_industries')->middleware('auth');
Route::get('/users/profiles/register/photos', 'UserProfileController@register_photos')->middleware('auth');
Route::post('/users/profiles/register/photos', 'UserProfileController@store_photos')->middleware('auth');

Route::get('/freelancer/{id}', 'UserProfileController@show_freelancer');

Route::get('/freelancer/show/{id}', 'UserProfileController@freelancer_show');
// Messages
Route::get('/messages/subject/{subject}', 'MessagesController@showSubject');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/upload', 'HomeController@upload');

Route::get('/importcities', 'HomeController@importcities');

Route::post('/suggestion', function (Request $request) {
    Mail::send(new SuggestionMail($request));
    return redirect('/')->with('message', 'Suggestion sent successfully!');
});

Route::post('/contact', function (Request $request) {
    Mail::send(new ContactMail($request));
    return redirect('/')->with('message', 'Email sent successfully!');
});

Route::get('/posts', 'PostController@index');
Route::get('/post/{id}', 'PostController@show');
Route::get('/post/click/{id}', 'PostController@post_counter');

Route::get('/messages/read/{id}', 'MessagesController@make_read')->middleware('auth');

Route::get('/backend', 'PagesController@backend1');

Route::get('/payment/paygate', 'PaymentController@paygate_payment');

Route::get('/thankyou', function () {
    return redirect('/login')->with('message', 'Payment Success!');
});

Route::post('/return/{id}', 'PaymentController@return');

Route::post('/notify/{id}', 'PaymentController@notify');

Route::get('/thankyou', 'PagesController@thankyou');

Route::get('/email/freelancers', 'EmailController@email_freelancers')->middleware('auth');
Route::post('/send/email/freelancers', 'EmailController@send_mail_freelancers')->middleware('auth');

Route::get('/email/finders', 'EmailController@email_finders')->middleware('auth');

Route::get('/email/co-working', 'EmailController@email_co_working')->middleware('auth');

Route::get('/terms/payment', 'PagesController@payment_terms');

Route::get('/dashboard/freelancers', 'FreelancerController@freelancer_dashboard')->middleware('auth');

Route::get('/subscription/add', 'UserProfileController@subscription_add');

Route::get('/freelancer/click/{id}', 'LinkClickedController@freelance_click');

Route::post('/industry/add', 'IndustryController@add_industry');